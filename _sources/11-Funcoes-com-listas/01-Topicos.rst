
..  Copyright (C) Carlos Hitoshi Morimoto e José Coelho de Pina Jr.
    É permitida a cópia, distribuição e/ou modificação desde documento
    sob os termos da Licença de Documentação Livre GNU (GNU Free 
    Documentation License) versão 1.3 ou versão subsequente
    publicada pela Fundação de Software Livre (Free Software 
    Foundation); com as seções invariantes sendo Prefácio e 
    Lista de Contribuidores, sem texto de capa ou contra-capa.  
    Uma cópia da licença está disponível em:
    https://www.gnu.org/licenses/licenses.pt-br.html#FDL


.. qnum::
	:prefix: cap11-01
	:start: 1

..  shortname:: Funções com listas
..  description:: Funções com listas


Funções com listas
==================

.. index:: pertinência, clone, referência, apelido


Tópicos
-------

    - `Pertinência em uma lista <https://python.ime.usp.br/pensepy/static/pensepy/09-Listas/listas.html#pertinencia-em-uma-lista>`__;
    - `Listas como parâmetros <https://python.ime.usp.br/pensepy/static/pensepy/09-Listas/listas.html#listas-como-parametros>`__;
    - Reforço: muito cuidado com atribuição de listas (apelidos x clones). 

Vídeos
......

