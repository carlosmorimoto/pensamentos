
..  Copyright (C) Carlos Hitoshi Morimoto e José Coelho de Pina Jr.
    É permitida a cópia, distribuição e/ou modificação desde documento
    sob os termos da Licença de Documentação Livre GNU (GNU Free 
    Documentation License) versão 1.3 ou versão subsequente
    publicada pela Fundação de Software Livre (Free Software 
    Foundation); com as seções invariantes sendo Prefácio e 
    Lista de Contribuidores, sem texto de capa ou contra-capa.  
    Uma cópia da licença está disponível em:
    https://www.gnu.org/licenses/licenses.pt-br.html#FDL


.. qnum::
	:prefix: cap11-02
	:start: 1


      
      
Exercícios
----------

Exercício 1
...........

Escreva a função abaixo:

.. activecode:: aula_funlist_ex01_tentativa

    #-------------------------------------------------
    def pertence(item,lista):
        '''(objeto, list) -> bool

        Recebe uma lista de itens e um item e 
        retorna True se o item eh um elemento da lista e
        False em caso contrario.
        ''' 
        print("Vixe! Ainda nao fiz a funcao!")


    #---------------------------------------------------
    # testes
    lista  = [1, "oi", 3.14, 7, True]

    # teste 1
    if pertence("oi",lista):
        print("Passou no primeiro teste! :-)")
    else:
        print("Nao passou no primeiro teste! :-(")

    # teste 2
    if pertence(True,lista):
        print("Passou no segundo teste! :-)")
    else:
        print("Nao passou no segundo teste! :-(")

    # teste 3
    if not pertence(False,lista):
        print("Passou no terceiro teste! :-)")
    else:
        print("Nao passou no terceiro teste! :-(")

    # outros testes

..
       def insere_se_novo(x, lista):
           """ (float, list) --> list
           que devolve a posição em que o real x ocorre em lista ou, caso x
           não estiver na lista, insere x no final da lista e devolve o
           índice dessa posição.
           """
           # escreva abaixo o corpo da função

       # teste 1
       lista  = [1,2,3]
       lista2 = insere_se_novo(4,lista)
       if lista2 == [1,2,3,4]:
          print("Passou no primeiro teste! :-)")
       else:
          print("Nao passou no primeiro teste! :-(")

       # teste 2
       lista  = [1,2,3]
       lista2 = insere_se_novo(2,lista)
       if lista2 == [1,2,3]:
          print("Passou no segundo teste! :-)")
       else:
          print("Nao passou no segundo teste! :-(")
      
.. Ainda sem solução
.. Clique `aqui <exercicios/ex101.html>`__ para ver uma solução.


Exercício 2
...........

Escreva um programa que dados
um número inteiro ``n`` e uma sequência com ``n`` números inteiros,
imprimi-los eliminando as repetições.

Escreva uma solução que usa a função ``pertence()`` 
do exercício anterior.

Escreva uma solução que usa as construções

.. sourcecode:: python 

    if item in lista:
        | 
        | bloco de comandos a serem executados 
        | se item pertence a lista
        | 

    if item not in lista:
        | 
        | bloco de comandos a serem executados 
        | se item nao pertence a lista
        | 

    if not item in lista:
        | 
        | bloco de comandos a serem executados 
        | se item nao pertence a lista
        | 

..
   Dada uma sequência de n números reais, escreva um programa (função
   ``main`` usando a
   função do item anterior) que conte quantas vezes cada número ocorre na
   sequência.

.. activecode:: aula_funlist_ex02_tentativa

    #-----------------------------------------------
    def main():
        '''
        Dada uma sequencia de n > 0 numeros inteiros,
        imprimi-la eliminando as repeticoes.
        '''
        # escreva o seu programa abaixo e remova o print()
        print("Vixe! Ainda nao fiz o exercicio!")

    #-----------------------------------------------
    def pertence(item,lista):
        '''(objeto, list) -> bool

        Recebe uma lista de itens e um item e 
        retorna True se o item eh um elemento da lista e
        False em caso contrario.
        ''' 
        print("Vixe! Ainda nao fiz a funcao!")

    main()



Exercício 3
...........

Escreva a função abaixo:

.. activecode:: aula_funlist_ex03_tentativa

    #-----------------------------------------------
    def indice(item, lista):
        '''(objeto,list) -> int ou None

        Recebe um objeto 'item' e uma lista 'lista' e retorna o
        indice da posicao em que item ocorre na lista.
        Caso item nao ocorra na lista a funcao retorna None
        '''
        print("Vixe! Ainda nao fiz a funcao!")


    #-----------------------------------------------
    # testes
    lista  = [1, "oi", 3.14, 7, True]

    # teste 1
    if indice("oi",lista) == 1:
        print("Passou no primeiro teste! :-)")
    else:
        print("Nao passou no primeiro teste! :-(")

    # teste 2
    if indice(True,lista) == 4:
        print("Passou no segundo teste! :-)")
    else:
        print("Nao passou no segundo teste! :-(")

    # teste 3
    if indice(False,lista) == None:
        print("Passou no terceiro teste! :-)")
    else:
        print("Nao passou no terceiro teste! :-(")

    # outros testes


Exercício 4
...........

Escreva um programa que dados um número inteiro ``n`` e uma sequência
com ``n`` números inteiros, conta e imprime o número de vezes que cada
número ocorre na sequência.

Escreva uma solução que usa a função ``indice()`` 
do exercício anterior.

.. activecode:: aula_funlist_ex04_tentativa

    #-----------------------------------------------
    def main():
        '''
        Dados n e uma sequencia com n numeros inteiros, 
        conta e imprime o numero de vezes que cada 
        numero ocorre na sequencia.
        '''
        # escreva o seu programa abaixo e remova o print()
        print("Vixe! Ainda nao fiz o exercicio!")

    #-----------------------------------------------
    def indice(item, lista):
        '''(objeto,list) -> int ou None

        Recebe um objeto 'item' e uma lista 'lista' e retorna o
        indice da posicao em que item ocorre na lista.
        Caso item nao ocorra na lista a funcao retorna None
        '''
        print("Vixe! Ainda nao fiz a funcao!")


    #-----------------------------------------------
    main()


Exercício 5
...........

Escreva a função abaixo:

.. activecode:: aula_funlist_ex05_tentativa

    def soma_elementos(ini, fim, lista):
        """ (int, int, lista) --> int

        Recebe uma lista `lista` de numero e dois indices ini e fim, 
        retorna a soma da sublista (= fatia) formada pelos elementos de
        indices ini, ini+1,...,fim-1.

        Pre-condicao: a funcao supoe que: 

               0 <= ini <= fim <= len(lista)
        """
        # escreva abaixo o corpo da função
        print("Vixe! Ainda nao fiz a funcao!")            


    #-----------------------------------------------
    # testes
    lista  = [1, 2, 3, 4, 5]

    # teste 1
    if soma_elementos(0,len(lista),lista) == 15:
        print("Passou no primeiro teste! :-)")
    else:
        print("Nao passou no primeiro teste! :-(")

    # teste 2
    if soma_elementos(1,4,lista) == 9:
        print("Passou no segundo teste! :-)")
    else:
        print("Nao passou no segundo teste! :-(")

    # teste 3
    if soma_elementos(2,2,lista) == 0:
        print("Passou no terceiro teste! :-)")
    else:
        print("Nao passou no terceiro teste! :-(")

    # outros testes



Exercício 6
...........

Usando a função do exercício anterior, escreva um programa que leia
n>0 e uma sequência com n números reais, e determina um segmento de
soma máxima.

Na sequência

.. sourcecode:: python

    5   -2   -2   -7   3   14  10  -3   9   -6   4   1 

a soma máxima de um segmento é ``3+14+10-3+9 = 33``.
 
.. activecode:: aula_funlist_ex06_tentativa

    #-----------------------------------------------
    def main():
        """
        programa que calcula segmento de soma maxima
        """    
        # escreva abaixo o corpo da funcao
        print("Vixe! Ainda nao fiz o exercicio!")

    #-----------------------------------------------
    main()
              
