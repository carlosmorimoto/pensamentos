
..  Copyright (C) Carlos Hitoshi Morimoto e José Coelho de Pina Jr.
    É permitida a cópia, distribuição e/ou modificação desde documento
    sob os termos da Licença de Documentação Livre GNU (GNU Free 
    Documentation License) versão 1.3 ou versão subsequente
    publicada pela Fundação de Software Livre (Free Software 
    Foundation); com as seções invariantes sendo Prefácio e 
    Lista de Contribuidores, sem texto de capa ou contra-capa.  
    Uma cópia da licença está disponível em:
    https://www.gnu.org/licenses/licenses.pt-br.html#FDL


.. qnum::
	:prefix: cap08-01
	:start: 1

..  shortname:: Tópico: números reais
..  description:: Tipo float


Números reais
=============

.. index:: float, float()
.. index:: /, //


Tópicos
-------

    - Leia sobre o tipo ``float`` na seção `Variáveis de tipos de dados <https://python.ime.usp.br/pensepy/static/pensepy/02-Conceitos/conceitos.html#variaveis-e-tipos-de-dados>`__.
    - Leia sobre a função de conversão ``float()``, que converte de ``str`` para ``float``, nas seções 
        - `Funções para conversão de valores <https://python.ime.usp.br/pensepy/static/pensepy/02-Conceitos/conceitos.html#funcoes-para-conversao-de-valores>`__ e 
        - `Operadores e operandos <https://python.ime.usp.br/pensepy/static/pensepy/02-Conceitos/conceitos.html#operadores-e-operandos>`__. 
        - Veja a diferença entre ``/`` e ``//`` e como ``%`` se comporta com ``float``.

    - Exercícios com o tipo `float`.

