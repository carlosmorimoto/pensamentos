
..  Copyright (C) Carlos Hitoshi Morimoto e José Coelho de Pina Jr.
    É permitida a cópia, distribuição e/ou modificação desde documento
    sob os termos da Licença de Documentação Livre GNU (GNU Free 
    Documentation License) versão 1.3 ou versão subsequente
    publicada pela Fundação de Software Livre (Free Software 
    Foundation); com as seções invariantes sendo Prefácio e 
    Lista de Contribuidores, sem texto de capa ou contra-capa.  
    Uma cópia da licença está disponível em:
    https://www.gnu.org/licenses/licenses.pt-br.html#FDL


.. qnum::
	:prefix: cap08-03
	:start: 1


      
Introdução
----------

Um **número real** em um computador é representado na notação de `ponto
flutuante <https://pt.wikipedia.org/wiki/Ponto_flutuante>`__ (do inglês
`floating point`). Dada a limitação de dígitos imposta por essa notação, os
resultados dos cálculos realizados pelos computadores são aproximações dos
valores reais. Por exemplo, o valor real de `Pi` tem infinitas casas decimais
mas, no computador, é necessário limitar o número de casas decimais,
aproximando o valor de `Pi`.

Para que possamos confiar nos resultados, uma *boa prática* é tentar minimizar
a propagação de erros devido ao uso dessas aproximações. Assim, em geral:

    - Operações de divisão e multiplicação são "seguras", podem ser executadas em qualquer ordem.
    - Adições e subtrações são "perigosas", pois quando números de
      magnitudes muito diferentes são envolvidos, os dígitos menos
      significativos são perdidos.
    - Essa perda de dígitos pode ser inevitável e sem consequência
      (quando os dígitos perdidos são insignificantes para o resultado
      final) ou catastróficos (quando a perda é magnificada e causa um
      grande erro no resultado).
    - Quanto mais cálculos são realizados (em particular quando os
      cálculos são feitos de forma iterativa), mais importante é
      considerar esse exercício.
    - O método de cálculo pode ser estável (tende a reduzir os erros
      de arredondamento) ou instável (tende a magnificar os
      erros). Frequentemente, há soluções estáveis e instáveis para um
      exercício.
    - Para saber mais dê uma olhada em http://floating-point-gui.de/errors/propagation.


Se você já aprendeu a utilizar funções,
você pode escrever as soluções dos exercícios a seguir usando funções.

