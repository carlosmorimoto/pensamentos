
..  Copyright (C) Carlos Hitoshi Morimoto e José Coelho de Pina Jr.
    É permitida a cópia, distribuição e/ou modificação desde documento
    sob os termos da Licença de Documentação Livre GNU (GNU Free 
    Documentation License) versão 1.3 ou versão subsequente
    publicada pela Fundação de Software Livre (Free Software 
    Foundation); com as seções invariantes sendo Prefácio e 
    Lista de Contribuidores, sem texto de capa ou contra-capa.  
    Uma cópia da licença está disponível em:
    https://www.gnu.org/licenses/licenses.pt-br.html#FDL


.. qnum::
	:prefix: cap08-02
	:start: 1



Vídeos
------

    - `Valores e tipos em Python <https://www.youtube.com/watch?v=UZ7_oudJ150&index=6&list=PLcoJJSvnDgcKpOi_UeneTNTIVOigRQwcnVídeos------>`__;
    - `Exercício Resolvido: Bhaskara <https://www.youtube.com/watch?v=v_QoUdfahng&index=11&list=PLcoJJSvnDgcKpOi_UeneTNTIVOigRQwcn>`__.

