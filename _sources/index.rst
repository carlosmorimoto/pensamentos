
.. Pensamentos com Python
    Você pode editar esse arquivo, mas ele deve conter ao menos a `toctree`
    para a raiz. 

.. meta:: 
    :description: Uma versão interativa do livro "Pensamentos com Python"
    :keywords: pensamento computacional, ciência da computação, python

.. toc_version: 2

.. _t_o_c:

.. raw:: html

   <div style="text-align:center" class="center-block">
   <h1>Pensamentos com Python</h1>
   <h3>-- um curso interativo de introdução à computação --</h3>

   <p>por <a href="https://www.ime.usp.br/~hitoshi">Carlos Hitoshi Morimoto</a>  e <a href="https://www.ime.usp.br/~coelho">José Coelho de Pina Jr.</a>
   </p>
   <style>
   button.reveal_button {
       margin-left: auto;
       margin-right: auto;
   }
   </style>
   </div>


Índice
::::::

.. toctree::
    :numbered:
    :maxdepth: 2

    ./01-Pensamentos/toctree.rst
    ./02-Pensamentos-logico-aritmetico/toctree.rst
    ./03-Pensamentos-comunicacao/toctree.rst
    ./04-Pensamentos-alternativos/toctree.rst
    ./05-Pensamentos-deja-vu/toctree.rst
    ./06-Indicadores-passagem/toctree.rst
    ./07-Pensamentos-dentro-pensamentos/toctree.rst
    ./08-Numeros-reais/toctree.rst
    ./09-Funcoes/toctree.rst
    ./10-Listas/toctree.rst
    ./11-Funcoes-com-listas/toctree.rst
    ./12-Strings-Arquivos/toctree.rst
    ./13-Matrizes/toctree.rst
    ./14-Algoritmos-Busca/toctree.rst
    ./15-Algoritmos-Elementares-Ordenacao/toctree.rst

Índice remissivo
::::::::::::::::

* :ref:`genindex`
* :ref:`search`


.. raw:: html

   <div style="width: 500px; margin-left: auto; margin-right: auto;">
   <a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/">
   <img alt="Creative Commons License" style="border-width:0; display:block; margin-left: auto; margin-right:auto;" src="https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png" />
   </a><br />
   <span xmlns:dct="http://purl.org/dc/terms/" href="http://purl.org/dc/dcmitype/InteractiveResource" property="dct:title" rel="dct:type">Pensamentos com Python</span><br /> 
   por <span xmlns:cc="http://creativecommons.org/ns#" property="cc:attributionName">Carlos Hitoshi Morimoto e José Coelho de Pina Jr.</span> <br />
   está sob a licença <a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/">Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License</a>.</div>

