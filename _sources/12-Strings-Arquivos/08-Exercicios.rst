
..  Copyright (C) Carlos Hitoshi Morimoto e José Coelho de Pina Jr.
    É permitida a cópia, distribuição e/ou modificação desde documento
    sob os termos da Licença de Documentação Livre GNU (GNU Free 
    Documentation License) versão 1.3 ou versão subsequente
    publicada pela Fundação de Software Livre (Free Software 
    Foundation); com as seções invariantes sendo Prefácio e 
    Lista de Contribuidores, sem texto de capa ou contra-capa.  
    Uma cópia da licença está disponível em:
    https://www.gnu.org/licenses/licenses.pt-br.html#FDL


.. qnum::
	:prefix: cap12-08
	:start: 1



Exercícios
----------

Exercício 1
..............

**PARTE A**

Escreva uma função separa, que recebe um string texto e um caractere separador. A função "corta" o texto nos separadores, retornando uma lista com as palavras do texto.

Exemplo:

para o texto: ",1,,2,3," a saída deve ser a lista:
['', '1', '', '2', '3', '']
onde '' indica uma palavra vazia (entre 2 separadores consecutivos).


.. activecode:: aula13_ex131a_tentativa

    def separa(texto, sep):
        '''
        (str, str) -> list
	recebe um text e separador, e devolve o texto separado
        '''
        # escreva o seu programa abaixo e remova o print()
        print("Vixe! Ainda nao fiz a funcao!")

**PARTE B**

Usando a função do item anterior, escreva um programa que leia uma linha com palavras separadas por vírgula, e determina o comprimento da maior palavra. A linha pode conter palavras vazias.

.. activecode:: aula13_ex131b_tentativa

    #-----------------------------------------------
    def main():
        '''
	lê uma linha com palavras separadas por vírgula e
	determina o comprimento da maior palavra.
        '''
        # escreva o seu programa abaixo e remova o print()
        print("Vixe! Ainda nao fiz o exercicio!")

    #-----------------------------------------------
    main()

**PARTE C**

Escreva o programa da parte B usando a função ``split()``.


Exercício 2
..............

Escreva um programa que leia vários números reais em uma mesma linha, e imprima a soma.

.. activecode:: aula13_ex132_tentativa

    #-----------------------------------------------
    def main():
        '''
	lê uma linha com numeros reais e calcula a soma dos numeros
        '''
        # escreva o seu programa abaixo e remova o print()
        print("Vixe! Ainda nao fiz o exercicio!")

    #-----------------------------------------------
    main()


Exercício 3
..............

Escreva um programa que leia um arquivo contendo ao menos 1 número real por linha, e para cada linha imprima a sua soma. Ao final, o programa deve imprimir também a soma total. Exemplo: para o arquivo numeros.txt com:

.. sourcecode:: python
		
    5 4 3 2 1
    4 4 4
    2.7
    3.14 2.1
  
a saída deve ser:

.. sourcecode:: python
		
    Digite o nome do arquivo: numeros.txt
    Soma = 15.000000
    Soma = 12.000000
    Soma = 2.700000
    Soma = 5.240000
    Total = 34.940000
  
Dessa vez, use um editor como o Idle no seu computador para escrever o programa.


Exercício 4
...........


**Parte A**

Escreva uma função com protótipo

.. sourcecode:: python

    def insereSeNovo (nome, lista):
        ''' (str, list) -> int
            modifica a lista, inserindo nome na lista.
	    Retorna a posição de nome na lista ou None caso ele já exista.
	'''

que devolve a posição em que o string x ocorre em lista ou, caso x não estiver na lista, insere x no final da lista e devolve o índice dessa posição.

.. activecode:: aula16_ex01a_tentativa

    # escreva a sua função abaixo e remova o print()
    def insereSeNovo (nome, lista):
	print("Vixe! Ainda nao fiz essa funcao!")

    # Codigo para testar a sua funcao
    lista = ["Ronaldo", "Romario", "Rivelino"]
    pos = insereSeNovo("Romario", lista)
    pos = insereSeNovo("Pele", lista)
    pos = insereSeNovo("Rivelino", lista)
    print(lista, " que deve ser Ronaldo, Romario, Rivelino, Pele")

**Parte B**

Escreva um programa que leia uma sequência de nomes e, usando a função do
item anterior, conte quantas vezes cada nome ocorre na sequência.

Exemplo:

    - Para `n = 7` e a sequência `Neimar Messi Kaka Neimar Neimar Messi Zico`
    - a saída deve ser::

        Neimar ocorre 3 vezes
        Messi ocorre 2 vezes
        Kaka ocorre 1 vezes
        Zico ocorre 1 vezes

.. activecode:: aula16_ex01b_tentativa

    # escreva o seu programa abaixo e remova o print()
    print("Vixe! Ainda nao fiz esse exercicio!")

