
..  Copyright (C) Carlos Hitoshi Morimoto e José Coelho de Pina Jr.
    É permitida a cópia, distribuição e/ou modificação desde documento
    sob os termos da Licença de Documentação Livre GNU (GNU Free 
    Documentation License) versão 1.3 ou versão subsequente
    publicada pela Fundação de Software Livre (Free Software 
    Foundation); com as seções invariantes sendo Prefácio e 
    Lista de Contribuidores, sem texto de capa ou contra-capa.  
    Uma cópia da licença está disponível em:
    https://www.gnu.org/licenses/licenses.pt-br.html#FDL


.. qnum::
	:prefix: cap12-04
	:start: 1



Strings: criação, indexação, e comparação
-----------------------------------------

O exemplo a seguir mostrar como criar um string
a partir de um string vazio e a 
concatenação de outros strings.

.. activecode:: strings_criacao

    def main():
        frase = ""   # string vazio
        frase = frase + "Esse string usa "
        meio  = '"apóstrofes" '
        fim   = "como demarcador. "
	frase = frase + meio + fim
        print("A frase: ", frase)
        print("Tem comprimento: ", len(frase))

    main()

Modifique o código para que o programa escreva `aspas` ao invés de `apóstrofes`.
Experimente trocar as " (aspas)  por ' (apóstrofes) no programa todo (de forma
coerente) para se certificar que nada muda.

Assim como os elementos de uma lista, os caracteres de um string
podem ser indexados por um índice (um número inteiro) entre colchetes.
Execute o programa abaixo para 
    
.. activecode:: strings_indexacao

   def main():
        frase = "Eu posso ajudar respondendo perguntas no fórum!"
        sorteio = [5, -4, 3, -8, 11]  # não foi bem um sorteio :)
	acertos = 0

	for i in sorteio: 
	    resposta = input("Qual o caractere de índice %d? "%(i))
		if frase[i] == resposta:
		    print("Parabéns, você acertou!")
		else:
		    print("Você errou. O caractere de índice %d é: %s"%(i, frase[i]))
	print("Você acertou %d de % perguntas."%(acertos, len(sorteio)))

    main()
    
Assim como listas
um string também pode ser fatiado. Lembre-se que para definir uma fatia na sequência de caracteres, é necessário definir o início e o fim da fatia e, se desejar, um passo. Assim como nas fatias de listas, o intervalo definido por início e fim é fechado à esquerda e aberto à direita. Veja o exemplo:

.. activecode:: strings_fatia_intervalos

    def main():
        ini, fim = 3, 7
        texto = "Eu posso ajudar respondendo perguntas no fórum!"
	fatia = texto[ ini : fim ]
	print("Seja o texto:\n", texto)
        resposta = input("Digite o string que corresponde à fatia texto[%d:%d] = "%(ini,fim))
        if resposta == fatia:
	    print("Muito bem, você acertou!!")
	else:
	    print("Você escreveu        : ", resposta)
	    print("mas o valor correto é: ", fatia)

    main()


Modifique os valores das variáveis `ini` e `fim` desse programa, para 5 e 15 respetivamente e veja se você consegue adivinhar qual a fatia resultante. Teste outros valores, inclusive valores negativos, como [-15 : -10], e procure entender o que e Python faz em cada caso.

.. admonition:: Índices negativos não são inválidos!
		    
    Observe que o Python usa índices negativos para percorrer a sequência da direita para a esquerda (fim para o começo) e, portanto, índices negativos não são inválidos. Um índice é **inválido** apenas quando ele cai fora da sequência, que pode ser um número positivo ou negativo maior que o tamnanho da sequência.

Lembre-se também que quando desejamos uma fatia começando do primeiro caractere (índice zero), não é necessário escrever o zero explicitamente, simplificando assim a notação para `fatia = texto[:5]` para o intervalo `[0:5]`. De forma semelhante, não é necessário escrever o valor do último índice caso desejamos uma fatia até o final, como por exemplo `fatia = texto[-6:]` para definir uma fatia com os 6 últimos caracteres do string `texto`.

Nesses exemplos nem consideramos o `passo`, pois assumimos que ele era 1. Veja como usar passos diferentes de 1 usando o exemplo a seguir.

.. activecode:: strings_fatia_passos

    def main():
        ini, fim, passo = 3, 13, 2 
        texto = "Eu posso ajudar respondendo perguntas no fórum!"    
	fatia = texto[ ini : fim : passo ]
        resposta = input("Digite o string que corresponde à fatia texto[%d:%d:%d] = "%(ini,fim,passo))
        if resposta == fatia:
	    print("Muito bem, você acertou!!")
	else:
	    print("Você escreveu        : ", resposta)
	    print("mas o valor correto é: ", fatia)

    main()

Modifique os valores das variáveis `ini`, `fim` e `passo` desse programa, inclusive para valores negativos, e procure entender o que e Python faz em cada caso.


