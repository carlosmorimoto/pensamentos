Strings e Arquivos
::::::::::::::::::

.. toctree::
	:maxdepth: 1

	01-Topicos.rst
	02-Videos.rst
	03-Introducao.rst
	04-Strings.rst
	05-Strings-nao-sao-listas!.rst
	06-Representacao-caracteres.rst
	07-Arquivos.rst
	08-Exercicios.rst

