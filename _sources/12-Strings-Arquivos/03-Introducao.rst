
..  Copyright (C) Carlos Hitoshi Morimoto e José Coelho de Pina Jr.
    É permitida a cópia, distribuição e/ou modificação desde documento
    sob os termos da Licença de Documentação Livre GNU (GNU Free 
    Documentation License) versão 1.3 ou versão subsequente
    publicada pela Fundação de Software Livre (Free Software 
    Foundation); com as seções invariantes sendo Prefácio e 
    Lista de Contribuidores, sem texto de capa ou contra-capa.  
    Uma cópia da licença está disponível em:
    https://www.gnu.org/licenses/licenses.pt-br.html#FDL


.. qnum::
	:prefix: cap12-03
	:start: 1



Introdução
----------

Um string (=tipo ``str``) é uma sequência de caracteres. Em Python,
podemos representar um string colando-o entre aspas ou apóstrofes.
O uso desses 2 marcadores é útil quando queremos utilizar um deles
dentro do string, como:

.. sourcecode:: python

    >>> texto = "Esse string usa 'aspas' como demarcador"
    >>> print(texto)
    Esse string usa 'aspas' como demarcador
    >>> novo_texto = 'Esse string usa "apóstrofes" como demarcador'
    >>> print(novo_texto)
    Esse string usa "apóstrofes" como demarcador
    >>> 

Por ser uma sequência de caracteres, podemos fazer uma analogia de strings com listas em Python. Assim como listas, um caractere de um string pode ser acessado por meio de índices, o comprimento pode ser dado pela função ``len()``, e os caracteres podem ser fatiados e concatenados.

