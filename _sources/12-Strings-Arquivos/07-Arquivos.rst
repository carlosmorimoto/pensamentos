
..  Copyright (C) Carlos Hitoshi Morimoto e José Coelho de Pina Jr.
    É permitida a cópia, distribuição e/ou modificação desde documento
    sob os termos da Licença de Documentação Livre GNU (GNU Free 
    Documentation License) versão 1.3 ou versão subsequente
    publicada pela Fundação de Software Livre (Free Software 
    Foundation); com as seções invariantes sendo Prefácio e 
    Lista de Contribuidores, sem texto de capa ou contra-capa.  
    Uma cópia da licença está disponível em:
    https://www.gnu.org/licenses/licenses.pt-br.html#FDL


.. qnum::
	:prefix: cap12-07
	:start: 1



Arquivos
--------

Utilizamos arquivos para armazenar informação, programas, documentos, etc. 

Para os fins desse curso, vamos trabalhar apenas com arquivos tipo texto, chamados assim porque seu conteúdo pode ser lido facilmente por uma pessoa (com extensões como .txt, .py, .html, etc). Existem também arquivos binários, feitos para serem lidos ou processados por computadores, como arquivos executáveis, arquivos comprimidos (tipo .zip, .gz, etc), arquivos com imagens (tipo .jpg, .gif, .tiff, etc), e muitos outros.


Criação de arquivos
...................

Para criar um arquivo e poder gravar informações nele, vamos utilizar
a seguinte forma:

.. sourcecode:: python

    nome = "poesia.txt"    # poderia ser um input("Digite o nome do arquivo: ")
    
    with open(nome, 'w', encoding='utf-8') as arq:
        # CORPO DO WITH
	arq.write("    O poeta é um fingidor.      \n")
	arq.write("    Finge tão completamente     \n")
	arq.write("    Que chega a fingir que é dor\n")
	arq.write("    A dor que deveras sente.    \n")
	arq.write("                Fernando Pessoa.\n")
	
Utilizamos o comando ``with`` para nos proteger contra alguns problemas que podem acontecer com arquivos (que não iremos detalhar no momento). A função ``open()`` abre o arquivo de nome `poesia.txt` (certifique-se de incluir todo o caminho (``path``) no nome, ou o arquivo será gravado no mesmo diretório do programa sendo executado) para gravação (devido a opção `w` do inglês `write`) e, para garantir que o arquivo será gravado em ``utf-8``, definimos também a forma de codificação pela opção ``encoding``.

Dentro comando ``with`` (corpo do ``with``), devemos fazer todas as operações com o arquivo ``arq``, nesse exemplo, escrever algumas linhas de texto usando o método ``write()``. Observe que o ``\n`` faz a mudança de linha explicitamente.
Uma grande vantagem de utilizar o comando ``with`` é que o arquivo é fechado automaticamente ao final do corpo, não sendo necessário fechar o arquivo explicitamente.


Leitura de arquivos
...................

Agora vamos abrir o arquivo "poesia.txt" para processar o seu conteúdo, utilizando o comando ``with`` novamente:

.. sourcecode:: python

    nome = "poesia.txt"    # poderia ser um input("Digite o nome do arquivo: ")
    
    with open(nome, 'r', encoding='utf-8') as arq_entrada:
        # CORPO DO WITH
        conteudo = arq_entrada.read()
	
    # continue o programa usando conteudo
    print(conteudo)


A saída seria:

.. sourcecode:: python

	O poeta é um fingidor.
        Finge tão completamente     
        Que chega a fingir que é dor
        A dor que deveras sente.    
                    Fernando Pessoa.

	
A função ``open()`` abre o arquivo com nome indicado no string ``nome`` (certifique-se de passar todo o caminho (``path``) no nome, ou o arquivo deve estar no mesmo diretório do programa sendo executado) para leitura (devido a opção `r` do inglês `read`) e usamos a mesma forma de codificação, no caso, `utf-8`.

No corpo do comando ``with`` devemos fazer toda a computação com o arquivo, acessando-o pela variável ``arq_entrada`` (poderia ser qualquer outro nome). O método ``read()`` lê todo o conteúdo de ``arq_entrada`` e o atribui para ``conteudo``. Nesse caso, a variável ``conteudo`` faz referência a um string cujo conteúdo foi carregado do arquivo.
Ao final do corpo do ``with``, esse comando fecha automaticamente o arquivo. 

Ao invés de carregar o arquivo inteiro de uma vez, muitos vezes é mais conveniente processar uma linha de cada vez para converte-lo ou extrair as informações necessárias, e assim evitar de manter o arquivo todo (que pode ser muito grande) na memória do computador.

Para isso, o Python oferece a seguinte forma de processar as linhas de um arquivo:

.. sourcecode:: python

    nome = "poesia.txt"
    
    with open(nome, 'r', encoding='utf-8') as arquivo:
        for linha in arquivo:
	    print(linha)

A saída seria:

.. sourcecode:: python
    
        O poeta é um fingidor.
	
        Finge tão completamente
	
        Que chega a fingir que é dor
	
        A dor que deveras sente.
	
                    Fernando Pessoa.
	
Nesse caso, é como se o arquivo fosse uma lista de linhas.
Ao rodar esse programa no entanto, você deve notar que o espaçamento entre linhas aumentou. Isso porque a linha contém o ``\n`` e a função ``print()`` muda automaticamente de linha.

Como já mencionamos, 
o Python oferece vários métodos para operar com strings (consulte a `documentação do Python sobre strings  <https://docs.python.org/3/library/stdtypes.html#text-sequence-type-str>`__).
O método ``strip()`` é um método para eliminar o ``\n`` e demais caracteres considerados "brancos" (como o espaço e o tab) no início e final de um string.
Experimente agora rodar o programa abaixo:

.. sourcecode:: python

    nome = "poesia.txt"
    
    with open(nome, 'r', encoding='utf-8') as arquivo:
        for linha in arquivo:
            linha_sem_brancos = linha.strip()
	    print(linha_sem_brancos)

e nesse caso a saída seria:

.. sourcecode:: python

    O poeta é um fingidor.
    Finge tão completamente
    Que chega a fingir que é dor
    A dor que deveras sente.
    Fernando Pessoa.

O ``strip()`` é muito útil quando estamos interessados apenas no texto e podemos desconsiderar a formatação. Observe que não apenas o ``\n`` foi desconsiderado, mas também o tab no início de cada linha do poema.

Finalmente, se além do texto queremos separar a frase em palavras, podemos utilizar o método ``split()``, que em seu comportamento padrão separa a frase em palavras usando os caracteres considerados "brancos" como separadores.
Rode o seguinte programa para ver o resultado de ``split()`` nas linhas do poema.

.. activecode:: aula13_exemplo_split

    linhas = [
		"    O poeta é um fingidor.      \n",
		"    Finge tão completamente     \n",
		"    Que chega a fingir que é dor\n",
		"    A dor que deveras sente.    \n",
		"                Fernando Pessoa.\n"
		]

    for linha in linhas:
        linha_sem_brancos = linha.strip()
        print(linha_sem_brancos)
	palavras = linha_sem_brancos.split()
	print(palavras)
   
Experimente rodar esse mesmo programa sem o ``strip()``.

