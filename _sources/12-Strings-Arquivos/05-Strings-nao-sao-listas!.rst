
..  Copyright (C) Carlos Hitoshi Morimoto e José Coelho de Pina Jr.
    É permitida a cópia, distribuição e/ou modificação desde documento
    sob os termos da Licença de Documentação Livre GNU (GNU Free 
    Documentation License) versão 1.3 ou versão subsequente
    publicada pela Fundação de Software Livre (Free Software 
    Foundation); com as seções invariantes sendo Prefácio e 
    Lista de Contribuidores, sem texto de capa ou contra-capa.  
    Uma cópia da licença está disponível em:
    https://www.gnu.org/licenses/licenses.pt-br.html#FDL


.. qnum::
	:prefix: cap12-05
	:start: 1



Strings não são listas!
-----------------------

Apesar de serem criados usando um par de " (aspas) ou ' (apóstrofes) como demarcadores, vimos que a função `len()`, a concatenação, a indexação e o fatiamento de strings são realizados de forma semelhante às listas.

A grande diferença é que strings são **imutáveis**, ou seja, não é possível alterar os seus elementos. Execute o programa abaixo e veja o que o Pytho faz quando tentamos modificar `texto[2]` para `S`. 

.. activecode:: strings_nao_sao_listas

    def main():
        texto = "Esse é um 'string'"
	print(texto)
        texto[2] = 'S'
	print(texto)

    main()

Como strings são imutáveis, caso você precise de um string diferente, é necessário criar um novo utilizando, caso desejar, fatias (partes) de outros strings.

.. activecode:: strings_novo_string

    def main():
        texto = "Esse é um 'string'"
	print(texto)
        novo_texto = 'Mais' + texto[6:]
	print(novo_texto)

    main()


Modifique esse programa para criar e imprimir o texto mudando a palavra `string` 
para `mundo pequeno`.		

