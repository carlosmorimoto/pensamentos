
..  Copyright (C) Carlos Hitoshi Morimoto e José Coelho de Pina Jr.
    É permitida a cópia, distribuição e/ou modificação desde documento
    sob os termos da Licença de Documentação Livre GNU (GNU Free 
    Documentation License) versão 1.3 ou versão subsequente
    publicada pela Fundação de Software Livre (Free Software 
    Foundation); com as seções invariantes sendo Prefácio e 
    Lista de Contribuidores, sem texto de capa ou contra-capa.  
    Uma cópia da licença está disponível em:
    https://www.gnu.org/licenses/licenses.pt-br.html#FDL


.. qnum::
	:prefix: cap12-01
	:start: 1

..  shortname:: Strings e Arquivos
..  description:: Strings e Arquivos


Strings e Arquivos
==================

.. index:: string, fatia de string, arquivo texto, encoding, representação de caracteres

Tópicos
-------

    - `String (tipo str) <https://panda.ime.usp.br/pensepy/static/pensepy/08-Strings/strings.html>`__
    - `Fatia de string <https://panda.ime.usp.br/pensepy/static/pensepy/08-Strings/strings.html#fatiamento>`__
    - `Arquivo de texto <https://panda.ime.usp.br/pensepy/static/pensepy/10-Arquivos/files.html>`__

