
..  Copyright (C) Carlos Hitoshi Morimoto e José Coelho de Pina Jr.
    É permitida a cópia, distribuição e/ou modificação desde documento
    sob os termos da Licença de Documentação Livre GNU (GNU Free 
    Documentation License) versão 1.3 ou versão subsequente
    publicada pela Fundação de Software Livre (Free Software 
    Foundation); com as seções invariantes sendo Prefácio e 
    Lista de Contribuidores, sem texto de capa ou contra-capa.  
    Uma cópia da licença está disponível em:
    https://www.gnu.org/licenses/licenses.pt-br.html#FDL


.. qnum::
	:prefix: cap12-06
	:start: 1



Representação de caracteres
---------------------------

Um conhecimento importante para trabalhar com strings é a conhecer a
sua forma de representação (ou `encoding`). Uma grande diferença entre
as versões 2.X e 3.X do Python é que, na versão 2.X os strings são
codificados em ASCII e na versão 3.X em UTF-8.

No `ASCII (American Standard Code for Information Interchange)
<http://en.wikipedia.org/wiki/ASCII>`__, cada
caractere é representado usando apenas 7 bits, sendo que os caracteres
de 0 a 31 são considerados de controle, e os de 32 a 127 correspondem
aos caracteres comuns de um teclado de computador em inglês.
Execute o seguinte programa para ver os caracteres de 32 a 127:

.. activecode:: aula13_tabela_ascii

    print("Tabela ASCII de 32 a 127: ")
    for i in range(32, 128, 3):
        print("ASCII[ %3d ] = '%s'  |  ASCII[ %3d ] = '%s'  |  ASCII[ %3d ] = '%s'"%
	(i, chr(i), i+1, chr(i+1), i+2, chr(i+2)))

Esse programa usa a função ``chr`` que converte um índice da tabela ASCII em seu caractere correspondente. Observe que a ordem dos caracteres na tabela não é arbitrária. Por exemplo, os caracteres de `a` a `z` (tanto minúsculos quanto maiúsculos) estão agrupados sequencialmente, respeitando a ordem alfabética.
Observe no entanto que não há caracteres acentuados, pois estes não fazem parte da língua inglesa.

O *Unicode* é um conjunto de caracteres que inclui os símbolos de todas as línguas humanas, desde os milhares de caracteres de línguas asiáticas como o chinês, até os caracteres de línguas mortas como os hieróglifos usados pelos egípicios. Para representar os caracteres do Unicode, foram desenvolvidos diversas formas de codificação. Uma delas é a `UTF-32 <http://en.wikipedia.org/wiki/UTF-32>`__ (*U* de Universal Character Set e o *TF* de Transformation Format - usando 32 bits). Essa forma utiliza sempre 32 bits (ou 4 bytes) para codificar um caractere (ou, mais exatamente, para codificar o índice do caractere na tabela Unicode).

O UTF-32 não é muito utilizado hoje pois, apesar de ser simples de decodificar, seus arquivos tornam-se muito grandes. O `UTF-16 <http://ergoemacs.org/emacs/unicode_basics.html>`__ exige ao menos 2 bytes (16 bits) para codificar cada caractere, criando arquivos menores em geral. Esse padrão é utilizado na linguagem Java e em sistemas operacionais como o Microsoft Windows e o Mac OS X. Já o padrão mais utilizado para codificar páginas na Internet atualmente é o `UTF-8 <http://en.wikipedia.org/wiki/UTF-32>`__, que usa pelo menos 8 bits para representar cada caractere. 

O Python oferece vários métodos para operar com strings (consulte a `documentação do Python sobre strings  <https://docs.python.org/3/library/stdtypes.html#text-sequence-type-str>`__). A seguir veremos apenas alguns mais úteis para processar arquivos.

