
..  Copyright (C) Carlos Hitoshi Morimoto e José Coelho de Pina Jr.
    É permitida a cópia, distribuição e/ou modificação desde documento
    sob os termos da Licença de Documentação Livre GNU (GNU Free 
    Documentation License) versão 1.3 ou versão subsequente
    publicada pela Fundação de Software Livre (Free Software 
    Foundation); com as seções invariantes sendo Prefácio e 
    Lista de Contribuidores, sem texto de capa ou contra-capa.  
    Uma cópia da licença está disponível em:
    https://www.gnu.org/licenses/licenses.pt-br.html#FDL


.. qnum::
	:prefix: cap12-02
	:start: 1



Vídeos
------      

    - `Strings <https://www.youtube.com/watch?v=DdhNltkI_hE&list=PLcoJJSvnDgcKpOi_UeneTNTIVOigRQwcn&index=33>`__
    - `Comparação de strings <https://www.youtube.com/watch?v=EWQTdbtCtKw&index=34&list=PLcoJJSvnDgcKpOi_UeneTNTIVOigRQwcn>`__

