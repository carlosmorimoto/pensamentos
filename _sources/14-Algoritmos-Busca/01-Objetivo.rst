
..  Copyright (C) Carlos Hitoshi Morimoto e José Coelho de Pina Jr.
    É permitida a cópia, distribuição e/ou modificação desde documento
    sob os termos da Licença de Documentação Livre GNU (GNU Free 
    Documentation License) versão 1.3 ou versão subsequente
    publicada pela Fundação de Software Livre (Free Software 
    Foundation); com as seções invariantes sendo Prefácio e 
    Lista de Contribuidores, sem texto de capa ou contra-capa.  
    Uma cópia da licença está disponível em:
    https://www.gnu.org/licenses/licenses.pt-br.html#FDL


.. qnum::
	:prefix: cap14-01
	:start: 1

.. -*- coding: utf-8 -*-

..  shortname:: Algoritmos de busca
..  description:: Algoritmos de Busca


Algoritmos de Busca
===================

Objetivo
--------

Ao final dessa aula você vai saber escrever algoritmos eficientes para realizar
buscas em listas.

