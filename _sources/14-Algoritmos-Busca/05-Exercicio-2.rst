
..  Copyright (C) Carlos Hitoshi Morimoto e José Coelho de Pina Jr.
    É permitida a cópia, distribuição e/ou modificação desde documento
    sob os termos da Licença de Documentação Livre GNU (GNU Free 
    Documentation License) versão 1.3 ou versão subsequente
    publicada pela Fundação de Software Livre (Free Software 
    Foundation); com as seções invariantes sendo Prefácio e 
    Lista de Contribuidores, sem texto de capa ou contra-capa.  
    Uma cópia da licença está disponível em:
    https://www.gnu.org/licenses/licenses.pt-br.html#FDL


.. qnum::
	:prefix: cap14-05
	:start: 1



Exercício 2
-----------

Quando utilizamos o algoritmo de busca sequencial para procurar um elemento de valor `x` em uma sequência `seq`, toda a sequência precisa ser varrida quando `x` não está presente em `seq`. 

Para criarmos um algoritmo mais eficiente, vamos assumir que a sequência esteja em ordem alfabética, como em um dicionário. Nesse caso, ao invés de testar um elemento de cada vez sequencialmente, podemos aplicar o seguinte algoritmo:

    - considere o elemento `M`, no meio da lista.
    - caso `x` for igual a `M`, então a busca termina pois encontramos o valor procurado.
    - caso `M` for maior que `x`, então `x` deve estar na primeira metade da sequência. A busca deve continuar **apenas** nessa metade. Mas se o comprimento dessa metade for nulo, a busca deve termina e o valor não foi encontrado.
    - caso `M` for menor que `x`, então `x` deve estar na segunda metade da sequência. A busca deve continuar **apenas** nessa metade. mas se o comprimento dessa metade for nulo, então a busca termina e o valor não foi encontrado.
      
Esse algoritmo é conhecido como **Busca Binária** pois a cada iteração metade da sequência é eliminada da busca.
Dessa forma, usando o algoritmo de busca sequencial em uma sequência com 1024 elementos, todos os 1024 elementos devem ser testados antes do algoritmo indicar que o elemento não está na lista. No caso da busca binária, o primeiro teste elimina 512 elementos, o segundo 256, o terceiro 128, e depois 64, 32, 16, 8, 4, 2, até que a lista contenha apenas 1 elemento. Dessa forma, ao invés de 1024, apenas 10 elementos (ou `log(len(seq))`) precisam ser testados.

.. activecode:: ex_busca_binaria

    def busca_binaria(x, seq):
       ''' (float, list) -> bool
	    retorna a posição em que x ocorre na lista ordenada,
	    ou None caso contrário, usando o algoritmo de busca binária.
	    '''
	# escreva a sua função
	return None


    # escreva alguns testes da função busca_binaria
    seq = [4, 10, 80, 90, 91, 99, 100, 101]
    testes = [80, 50]

    for t in testes:
	pos = busca_binaria(t, seq)
	if pos is None:
	    print("Nao achei ", t)
	else:
            print("Achei ", t)
	    
