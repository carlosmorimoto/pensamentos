Algoritmos de Busca
:::::::::::::::::::

.. toctree::
	:maxdepth: 1

	01-Objetivo.rst
	02-Topicos.rst
	03-Algoritmos-Busca.rst
	04-Exercicio-1.rst
	05-Exercicio-2.rst

