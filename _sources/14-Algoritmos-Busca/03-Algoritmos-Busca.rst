
..  Copyright (C) Carlos Hitoshi Morimoto e José Coelho de Pina Jr.
    É permitida a cópia, distribuição e/ou modificação desde documento
    sob os termos da Licença de Documentação Livre GNU (GNU Free 
    Documentation License) versão 1.3 ou versão subsequente
    publicada pela Fundação de Software Livre (Free Software 
    Foundation); com as seções invariantes sendo Prefácio e 
    Lista de Contribuidores, sem texto de capa ou contra-capa.  
    Uma cópia da licença está disponível em:
    https://www.gnu.org/licenses/licenses.pt-br.html#FDL


.. qnum::
	:prefix: cap14-03
	:start: 1



Algoritmos de Busca
-------------------

Algoritmos de busca verificam se uma dada informação ocorre em uma sequência ou não.
Por exemplo, dada uma sequência de números guardados em uma lista `seq` e um número `x`, escreva uma função que responda à pergunta: *x ocorre na sequência?*

.. admonition:: Por que não usar *x in seq*? 

     Sim, em Python a gente pode usar *x in seq*, que resulta em `True` ou `False` caso  `x` pertença à lista ou não. Mas nesse capítulo nosso objetivo é esclarecer como o `in` pode ser implementado e discutir formas eficientes de implementar busca em sequências. Um outro método útil é o `index`. Caso `x` pertença a `seq`, você pode utilizar `seq.index(x)` para descobrir o índice de `x` em `seq`.

Uma possível solução é percorrer a lista toda variando o índice `i` de 0 a `len(seq)-1` e comparando cada elemento `seq[i]` com `x`. Caso o valor seja encontrado a função retorna `True` e, caso contrário, retorna `False`.
Essa solução é conhecida como **Busca Sequencial**.

.. activecode:: algoritmo_de_busca_sequencial

    def busca_sequencial( x, seq):
        '''(float, list) -> bool'''
	for i in range(len(seq)):
	    if seq[i] == x:
	        return True
	return False

