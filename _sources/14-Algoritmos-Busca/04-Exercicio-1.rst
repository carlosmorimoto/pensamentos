
..  Copyright (C) Carlos Hitoshi Morimoto e José Coelho de Pina Jr.
    É permitida a cópia, distribuição e/ou modificação desde documento
    sob os termos da Licença de Documentação Livre GNU (GNU Free 
    Documentation License) versão 1.3 ou versão subsequente
    publicada pela Fundação de Software Livre (Free Software 
    Foundation); com as seções invariantes sendo Prefácio e 
    Lista de Contribuidores, sem texto de capa ou contra-capa.  
    Uma cópia da licença está disponível em:
    https://www.gnu.org/licenses/licenses.pt-br.html#FDL


.. qnum::
	:prefix: cap14-04
	:start: 1



Exercício 1
-----------

Escreva um programa que leia uma sequência com `N` números reais e imprime a sequência eliminando os elementos repetidos. Esse exercício pode ser dividido em 2 partes:

**Parte A**

Escreva a função:

.. activecode:: ex_busca_1_parte_A

    def acha(x, seq):
        ''' (float, list) -> int
	    retorna a posição em que x ocorre na lista, ou None caso contrário
	    '''
	# escreva a função

	
** Parte B**

.. activecode:: ex_busca_1_parte_B

    def main():
        ''' programa que lê uma sequência com N elementos e a imprime
	    sem repetições.
	    '''

	# escreva o programa

    main()


