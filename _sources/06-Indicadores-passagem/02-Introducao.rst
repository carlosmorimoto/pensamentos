
..  Copyright (C) Carlos Hitoshi Morimoto e José Coelho de Pina Jr.
    É permitida a cópia, distribuição e/ou modificação desde documento
    sob os termos da Licença de Documentação Livre GNU (GNU Free 
    Documentation License) versão 1.3 ou versão subsequente
    publicada pela Fundação de Software Livre (Free Software 
    Foundation); com as seções invariantes sendo Prefácio e 
    Lista de Contribuidores, sem texto de capa ou contra-capa.  
    Uma cópia da licença está disponível em:
    https://www.gnu.org/licenses/licenses.pt-br.html#FDL


.. qnum::
	:prefix: cap06-02
	:start: 1



Introdução
----------

Um indicador de passagem é um padrão comum em computação que auxilia
no controle de um programa.

.. Essencialmente, um **indicador de passagem** (= *flags*) é 
    uma variável utilizada para indicar a ocorrência de um evento específico. Esse evento  é definido através da *passagem* do fluxo de execução em algum determinado ponto do código.

Uma **variável** utilizada como **indicador de passagem** indica se algo *ocorreu* ou *não ocorreu* durante a execução de um programa e, portanto, seu valor 
*é* ou *não é*, *verdadeiro* ou *falso*. Por essa razão, em Python, essas variáveis são do
tipo booleano (`bool`).

A utilização de um indicador de passagem é bastante simples.
Inicialmente a variável deve ser inicializada com um valor ``True`` ou ``False`` adequado. 
Em seguida, durante a execução do programa, o valor da variável é alterado apenas se o evento for detectado. Observe que, uma vez detectado o evento, o valor do indicador não deve ser mais alterado. 


Exemplo
.......

Dado um número inteiro ``n > 0`` e uma sequência com ``n`` números inteiros,
verifique se a sequência está em ordem crescente.

**Primeira tentativa de solução**

.. activecode:: crescente_tentativa_1

    n = int(input("Digite o valor de n: "))
    anterior = int(input("Digite um número da sequência: ")) 
    i = 1   # i conta quanto números foram lidos. Por isso já começa com 1.
    while i < n:
        x = int(input("Digite um número da sequência: ")) 
        if x <= anterior:
            print("A sequência não está ordenada") 
        else:
            print("A sequência está ordenada") 
        anterior = x
        i = i + 1
	    
	    
Antes de prosseguirmos, leia a tentativa de solução com atenção e entenda o
que o programa faz. Em seguida, simule e  teste o programa com algumas sequências diferentes, crescentes e não crescentes, para identificar possíveis problemas.

.. admonition:: **O que esse programa faz?**

    - O tamanho da sequência é armazenado na variável ``n``.
    - A variável ``i`` é utilizada para controlar o final do ``while``, para que o ``while`` pare após ler os ``n`` elementos da sequência. Observe que ``i`` é incrementada ao final do bloco dentro do ``while``.
    - A variável ``anterior`` armazena o valor usado previamente e ``x`` a variável atual. A ideia dessa tentativa é comparar ``x`` e ``anterior`` verificando se o par está ordenado e imprimindo as mensagens supostamente apropriadas em cada caso.
    - Observe que a variável ``anterior`` recebe o valor de ``x`` no final de cada iteração, já que o valor representado por ``x`` assumirá o papel de ``anterior`` na próxima iteração.

Essa primeira tentativa tem alguns problemas:

    - imprime uma mensagem em cada iteração, quando apenas uma mensagem deveria ser impressa pelo programa;
    - as mensagens podem ser inconsistentes: pode imprimir que a sequência está *ordenada* e *não ordenada*;
    - imprime uma mensagem após verificar cada par, mas a resposta depende de toda a sequência.

**Segunda tentativa**      

Para evitar a impressão de várias mensagens, a impressão deve ocorrer
fora do ``while``. Para isso vamos criar uma variável para indicar se a sequência está ou não em ordem crescente, como abaixo.

.. activecode:: crescente_tentativa_2

    n = int(input("Digite o valor de n: "))
    anterior = int(input("Digite um número da sequência: ")) 
    i = 1   # já lemos o 1o número da sequência

    crescente = True

    while i < n:
        x = int(input("Digite um número da sequência: ")) 
        if x <= anterior:
            crescente = False
        else:
            crescente = True
        anterior = x
        i = i + 1

    if crescente:
        print("A sequência está ordenada") 
    else:
        print("A sequência não está ordenada")


O uso da variável *crescente* melhora o controle do programa.
Inicialmente, o programa inicializa crescente com True para indicar
que a sequência com um elemento é considerada crescente. A seguir,
todos os pares consecutivos são testados.

Com isso, apenas uma mensagem é fornecida ao final, ao invés das várias mensagens
inconsistentes da primeira tentativa.

Antes de prosseguir, simule e teste essa versão para identificar os
problemas dessa solução.

Essa segunda tentativa apresenta o seguinte problema:

    - a mensagem é definida apenas pelo **último par** da sequência. Assim, se a sequência for não decrescente mas o último par estiver em ordem crescente, a mensagem dada será que a sequência está ordenada.

Tente corrigir esse problema antes de ver a próxima tentativa.


**Terceira tentativa de solução**

.. activecode:: crescente_tentativa_3

    n = int(input("Digite o valor de n: "))
    anterior = int(input("Digite um número da sequência: ")) 
    i = 1   # já lemos o 1o número da sequência

    crescente = True

    while i < n:
        x = int(input("Digite um número da sequência: ")) 
        if x <= anterior:
            crescente = False
        anterior = x
        i = i + 1

    if crescente:
        print("A sequência está ordenada") 
    else:
        print("A sequência não está ordenada")


Observe que nessa terceira tentativa retiramos o ``else``. Dessa
forma, quando o programa encontra um par fora de ordem, esse evento é
marcado na variável crescente. Ao comparar outros pares da sequência,
mesmo que eles estiverem em ordem, a variável mantém o seu estado até
o final da sequência. Dessa forma, a mensagem correta, que considera o
resultado entre todos os pares, é impressa.

Nesse caso, podemos chamar a variável crescente como um **indicador de passagem**, pois ela muda de valor ao encontrar um par fora de ordem e permanece com esse valor até o final, embora outros pares também fora de ordem podem marcar novamente a variável como ``False``.

.. admonition:: **Boa prática de programação**

    Um programa que calcula o mesmo valor várias vezes sem necessidade
    ou que continua calculando quando o resultado já é conhecido é
    ineficiente e, portanto, esses trechos devem ser evitados.

    No caso da terceira tentativa, não há mais razão para continuar
    processando a sequência após o indicador de passagem mudar de
    estado. Veja a próxima tentativa para ver um código um pouco
    melhor.

**Quarta tentativa de solução**

.. activecode:: crescente_tentativa_4

    n = int(input("Digite o valor de n: "))
    anterior = int(input("Digite um número da sequência: ")) 
    i = 1   # já lemos o 1o número da sequência

    crescente = True
    
    while i < n and crescente:
        x = int(input("Digite um número da sequência: ")) 
        if x <= anterior:
            crescente = False
        anterior = x
        i = i + 1

    if crescente:
        print("A sequência está ordenada") 
    else:
        print("A sequência não está ordenada") 


Observe a condição do ``while``. Agora o while é interrompido assim que a
variável crescente se torna ``False``, ou seja, assim que o programa
identifica que a sequência não é crescente e portanto não há mais
razão de continuar verificando.
    		
