
..  Copyright (C) Carlos Hitoshi Morimoto e José Coelho de Pina Jr.
    É permitida a cópia, distribuição e/ou modificação desde documento
    sob os termos da Licença de Documentação Livre GNU (GNU Free 
    Documentation License) versão 1.3 ou versão subsequente
    publicada pela Fundação de Software Livre (Free Software 
    Foundation); com as seções invariantes sendo Prefácio e 
    Lista de Contribuidores, sem texto de capa ou contra-capa.  
    Uma cópia da licença está disponível em:
    https://www.gnu.org/licenses/licenses.pt-br.html#FDL


.. qnum::
	:prefix: cap06-03
	:start: 1



Exercícios
----------

Exercício 1
...........

Dado um número inteiro ``n > 1`` e uma sequência com ``n`` números inteiros, verificar se
todos os elementos da sequência são pares

Exemplos:

    - Para ``n = 5`` e a sequência ``24 10 6 14 74`` a resposta é sim 
    - Para ``n = 4`` e a sequência ``2 4 5 6`` a resposta é não

Utilize o padrão de indicador de passagem na sua solução.      

.. activecode:: indicador_de_passagem_ex1_tentativa

    # Escreva o seu programa
                


Exercício 2
...........

Dado um número inteiro ``n > 1``, verificar se ``n`` é primo.

Utilize o padrão de indicador de passagem na sua solução.      

.. activecode:: indicador_de_passagem_ex2_tentativa
      
    # Escreva o seu programa
                


Exercício 3 (resolvido)
.......................

Dado um número inteiro ``n``, ``n > 0``, verificar se este
número contém dois dígitos adjacentes iguais.    

Por exemplo, para

.. sourcecode:: python

    1234321 a resposta é não e
    1234556 a resposta é sim. 

.. activecode:: aula05_pr3

    n_salvo = n = int(input("Digite um numero: "))

    anterior = n % 10
    n = n // 10;
    adj_iguais = False;
    pos = 0

    while n > 0 and not adj_iguais:
        atual = n % 10
        if atual == anterior: 
            adj_iguais = True   
        else:
            pos += 1
        anterior = atual
        n = n // 10
    
    if adj_iguais:
        print(n_salvo, "tem dois digitos", atual, "adjacentes")
    else:
        print(n_salvo, "nao tem digitos iguais adjacentes")
      		
    

Exercício 4
...........

Dados um número inteiro ``n``, ``n > 0``,
e uma sequência com ``n`` números inteiros,
verificar se a sequência é uma progressão aritmética. 

.. activecode:: indicador_de_passagem_ex4_tentativa
		
    # Escreva o seu programa
                
 

