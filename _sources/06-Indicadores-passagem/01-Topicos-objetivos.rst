
..  Copyright (C) Carlos Hitoshi Morimoto e José Coelho de Pina Jr.
    É permitida a cópia, distribuição e/ou modificação desde documento
    sob os termos da Licença de Documentação Livre GNU (GNU Free 
    Documentation License) versão 1.3 ou versão subsequente
    publicada pela Fundação de Software Livre (Free Software 
    Foundation); com as seções invariantes sendo Prefácio e 
    Lista de Contribuidores, sem texto de capa ou contra-capa.  
    Uma cópia da licença está disponível em:
    https://www.gnu.org/licenses/licenses.pt-br.html#FDL


.. qnum::
	:prefix: cap06-01
	:start: 1

..  shortname:: Indicador de passagem
..  description:: Indicador de passagem


Indicadores de passagem
=======================

.. index:: indicador de passagem

Tópicos e objetivos
-------------------

Ao final dessa aula você deverá saber:

    - descrever o funcionamento de indicadores de passagem;
    - identificar situações onde indicadores de passagem são úteis;
    - utilizar indicadores de passagem em seus programas.

