
..  Copyright (C) Carlos Hitoshi Morimoto e José Coelho de Pina Jr.
    É permitida a cópia, distribuição e/ou modificação desde documento
    sob os termos da Licença de Documentação Livre GNU (GNU Free 
    Documentation License) versão 1.3 ou versão subsequente
    publicada pela Fundação de Software Livre (Free Software 
    Foundation); com as seções invariantes sendo Prefácio e 
    Lista de Contribuidores, sem texto de capa ou contra-capa.  
    Uma cópia da licença está disponível em:
    https://www.gnu.org/licenses/licenses.pt-br.html#FDL


.. qnum::
	:prefix: cap09-06
	:start: 1


	
Exercícios
----------

Exercício 2
...........

Complete a função ``fatorial`` abaixo,
que recebe como parâmetro um número 
inteiro ``k``, ``k >= 0``, e retorna k!.

Escreva apenas o corpo da função.
Observe que o código já inclui
chamadas para a função ``fatorial``, para que
você possa testar a função.


.. activecode:: aula_funcao_ex02_tentativa

    def main():
        ''' testes da função fatorial '''
        print("0! =", fatorial(0))
        print("1! =", fatorial(1))
        print("5! =", fatorial(5))   
        print("17! =", fatorial(17))   
		
    #-----------------------------------------------------
    
    def fatorial(k):
        '''(int) -> int

        Recebe um inteiro k e retorna o valor de k!

        Pre-condição: supõe que k é um número inteiro não negativo. 
        '''

        k_fat = 1

	# COMPLETE ESSA FUNÇÃO
	
        return k_fat 

    #-----------------------------------------------------
    
    if __name__ == '__main__': # chamada da funcao principal 
        main() # chamada da função main

Clique `aqui <exercicios/funcao02.html>`__ para ver uma solução.

Exercício 3
...........

Usando a função do exercício 6.2, escreva uma função que recebe
dois inteiros, ``m`` e ``n``, como parâmetros e retorna a combinação
``m!/((m-n)!n!)``. 

.. activecode:: aula_funcao_ex03_tentativa

    def main():
        ''' Testes da função combincao '''
	print("Combinacao(4,2) =", combinacao(4,2))
	print("Combinacao(5,2) =", combinacao(5,2))
	print("Combinacao(10,4) =", combinacao(10,4))
		
    #-----------------------------------------------------
    
    def combinacao(m, n):
        '''(int, int) -> int
        Recebe dois inteiros m e n, e retorna o valor de m!/((m-n)! n!)
        '''

	# COMPLETE ESSA FUNÇÃO E MUDE O RETURN ABAIXO

        return "o resultado"

    #-----------------------------------------------------
    
    if __name__ == '__main__': # chamada da funcao principal 
        main() 


Clique `aqui <exercicios/funcao03.html>`__ para ver uma solução.


Exercício 4
...........

Usando as funções ``fatorial`` e ``combinacao`` dos exercícios anteriores,
escreva um programa que lê um inteiro ``n``, ``n >= 0`` e imprime 
os coeficientes da expansão de ``(x+y)`` elevado a ``n``.

Lembre-se de utilizar o esqueleto de programa com funções em Python.

.. activecode:: aula_funcao_ex04_tentativa

    # Escreva o seu programa usando o esqueleto sugerido
    
    def main():
        ''' Escreva aqui alguns testes '''

	print("Vixe! Ainda não fiz esse exercício.")

    #-----------------------------------------------------            
    if __name__ == '__main__': # chamada da funcao principal 
        main() 


Clique `aqui <exercicios/funcao04.html>`__ para ver uma solução.



