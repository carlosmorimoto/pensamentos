
..  Copyright (C) Carlos Hitoshi Morimoto e José Coelho de Pina Jr.
    É permitida a cópia, distribuição e/ou modificação desde documento
    sob os termos da Licença de Documentação Livre GNU (GNU Free 
    Documentation License) versão 1.3 ou versão subsequente
    publicada pela Fundação de Software Livre (Free Software 
    Foundation); com as seções invariantes sendo Prefácio e 
    Lista de Contribuidores, sem texto de capa ou contra-capa.  
    Uma cópia da licença está disponível em:
    https://www.gnu.org/licenses/licenses.pt-br.html#FDL


.. qnum::
	:prefix: cap09-02
	:start: 1




Vídeos
------

    - `Funções
      <https://www.youtube.com/watch?v=AMUxtcJ8yfc&index=17&list=PLcoJJSvnDgcKpOi_UeneTNTIVOigRQwcn>`__;
    - `Exercício resolvido -- coeficiente binomial
      <https://www.youtube.com/watch?v=se9JcK05PGw&list=PLcoJJSvnDgcKpOi_UeneTNTIVOigRQwcn&index=18>`__;
    - `Exercício resolvido -- função éPrimo
      <https://www.youtube.com/watch?v=Wcug8mjU8Sg&index=26&list=PLcoJJSvnDgcKpOi_UeneTNTIVOigRQwcn>`__. 
      
