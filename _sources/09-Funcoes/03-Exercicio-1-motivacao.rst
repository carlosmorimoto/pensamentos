
..  Copyright (C) Carlos Hitoshi Morimoto e José Coelho de Pina Jr.
    É permitida a cópia, distribuição e/ou modificação desde documento
    sob os termos da Licença de Documentação Livre GNU (GNU Free 
    Documentation License) versão 1.3 ou versão subsequente
    publicada pela Fundação de Software Livre (Free Software 
    Foundation); com as seções invariantes sendo Prefácio e 
    Lista de Contribuidores, sem texto de capa ou contra-capa.  
    Uma cópia da licença está disponível em:
    https://www.gnu.org/licenses/licenses.pt-br.html#FDL


.. qnum::
	:prefix: cap09-03
	:start: 1




Exercício 1 de motivação
------------------------

O número de combinações possíveis de ``m`` elementos em grupos de ``n``
elementos (n <= m) é dada pela fórmula de combinação
``m!/((m-n)!n!)``. 

Escreva um programa que lê dois inteiros ``m`` e ``n`` e calcula a
combinação de ``m``, ``n`` a ``n``.

.. activecode:: aula_funcao_ex01_tentativa

    # Escreva o seu programa
    
    def main():

    #-----------------------------------------------------            
    if __name__ == '__main__': # chamada da funcao principal 
        main() 

Clique `aqui <exercicios/funcao01.html>`__ para ver uma solução.

Observe que o pedaço de código que calcula o fatorial é repetido
várias vezes. Uma importante ferramenta computacional é a capacidade
de abstrair partes relevantes e/ou que se repetem mais de uma vez
na forma de **funções**.

