Funções
:::::::

.. toctree::
	:maxdepth: 1

	01-Topicos.rst
	02-Videos.rst
	03-Exercicio-1-motivacao.rst
	04-Funcoes-Python.rst
	05-Esqueleto-programa-Python.rst
	06-Exercicios.rst

