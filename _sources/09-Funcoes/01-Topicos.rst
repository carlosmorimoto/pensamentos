
..  Copyright (C) Carlos Hitoshi Morimoto e José Coelho de Pina Jr.
    É permitida a cópia, distribuição e/ou modificação desde documento
    sob os termos da Licença de Documentação Livre GNU (GNU Free 
    Documentation License) versão 1.3 ou versão subsequente
    publicada pela Fundação de Software Livre (Free Software 
    Foundation); com as seções invariantes sendo Prefácio e 
    Lista de Contribuidores, sem texto de capa ou contra-capa.  
    Uma cópia da licença está disponível em:
    https://www.gnu.org/licenses/licenses.pt-br.html#FDL


.. qnum::
	:prefix: cap09-01
	:start: 1

..  shortname:: Tópico: funções
..  description:: Como escrever funções em Python

Funções
=======

.. index:: funções, parâmetros, variáveis locais, def, docstring


Tópicos
-------

    - `Funções <https://panda.ime.usp.br/pensepy/static/pensepy/05-Funcoes/funcoes.html>`__;
    - `Variáveis locais
      <https://panda.ime.usp.br/pensepy/static/pensepy/05-Funcoes/funcoes.html#variaveis-e-parametros-sao-locais>`__;  
    - documentação: *docstring*

