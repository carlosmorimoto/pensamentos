
..  Copyright (C) Carlos Hitoshi Morimoto e José Coelho de Pina Jr.
    É permitida a cópia, distribuição e/ou modificação desde documento
    sob os termos da Licença de Documentação Livre GNU (GNU Free 
    Documentation License) versão 1.3 ou versão subsequente
    publicada pela Fundação de Software Livre (Free Software 
    Foundation); com as seções invariantes sendo Prefácio e 
    Lista de Contribuidores, sem texto de capa ou contra-capa.  
    Uma cópia da licença está disponível em:
    https://www.gnu.org/licenses/licenses.pt-br.html#FDL


.. qnum::
	:prefix: cap09-05
	:start: 1



Esqueleto de um programa em Python
----------------------------------

Para nos prevenir de alguns problemas (que serão explicados mais
tarde), vamos adotar nesse curso o seguinte esqueleto para escrever
programas em Python com funções. Nesse esqueleto, a primeira função
sempre será chamada de ``main`` e corresponderá a função principal do
programa, ou seja, aquela que resolve o problema. As demais funções
auxiliares devem ser definidas, em qualquer ordem, após a definição da
``main`` e, por fim, para executar o programa, a função ``main``
precisa ser chamada.

.. sourcecode:: python

    # função  principal  
    def main():
        ''' 
        Função principal, será a primeira a ser executado e
        será a responsável pela chamada de outras funções que 
        por sua vez podem ou não chamar outras funções que 
        por sua vez ...
        '''
        # corpo da função main
        |
        | bloco de comandos     
        | 

    # Declaração das funções 
    def f( parâmetros_de_f ):
        '''
        docstring da função f
        '''
        # corpo da função f
        |
        | bloco de comandos     
        | 

    def g( parâmetros_de_g ):
        '''
        docstring da função g
        '''
        # corpo da função g
        |
        | bloco de comandos     
        | 

    [...]

    # início da execução do programa
    #-----------------------------------------------------            
    if __name__ == '__main__': # chamada da funcao principal 
        main() # chamada da função main

	
O uso do "if __name__ == '__main__'" para a chamada da função main
permite que esse arquivo contendo uma ou várias funções seja incluído em
outros programas (usando "import" e suas variações) sem a necessidade
de reescrever ou copiar o código.

