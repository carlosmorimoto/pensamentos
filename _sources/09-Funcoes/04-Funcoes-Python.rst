
..  Copyright (C) Carlos Hitoshi Morimoto e José Coelho de Pina Jr.
    É permitida a cópia, distribuição e/ou modificação desde documento
    sob os termos da Licença de Documentação Livre GNU (GNU Free 
    Documentation License) versão 1.3 ou versão subsequente
    publicada pela Fundação de Software Livre (Free Software 
    Foundation); com as seções invariantes sendo Prefácio e 
    Lista de Contribuidores, sem texto de capa ou contra-capa.  
    Uma cópia da licença está disponível em:
    https://www.gnu.org/licenses/licenses.pt-br.html#FDL


.. qnum::
	:prefix: cap09-04
	:start: 1



Funções em Python
-----------------

Para declarar uma função em Python faça o seguinte:


.. sourcecode:: python

    def nome_da_função ( parâmetros ):
        '''
        docstring contendo comentários sobre a função.
	Embora opcionais são fortemente recomendados. Os comentários
	devem descrever o papel dos parâmetros e o que a função faz.
        '''
        # corpo da função
        |
        | bloco de comandos
        |

