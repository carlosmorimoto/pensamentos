
..  Copyright (C) Carlos Hitoshi Morimoto e José Coelho de Pina Jr.
    É permitida a cópia, distribuição e/ou modificação desde documento
    sob os termos da Licença de Documentação Livre GNU (GNU Free 
    Documentation License) versão 1.3 ou versão subsequente
    publicada pela Fundação de Software Livre (Free Software 
    Foundation); com as seções invariantes sendo Prefácio e 
    Lista de Contribuidores, sem texto de capa ou contra-capa.  
    Uma cópia da licença está disponível em:
    https://www.gnu.org/licenses/licenses.pt-br.html#FDL


.. qnum::
	:prefix: cap05-01
	:start: 1

Introdução
----------

.. -*- coding: utf-8 -*-

..  shortname:: while
                
..  description:: Comando de repetição while

.. index:: while, comando de repetição, repetição, expressão relacional


Tópicos
-------

Ao final dessa parte do curso você deverá saber:

    - utilizar comandos de repetição na resolução de problemas computacionais;
    - definir condições, com valores iniciais e de parada, para o comando ``while``;
    - simular o processamento do comando while.



Introdução
----------

O comando de repetição ``while`` (= enquanto) permite repetir instruções enquanto uma condição for verdadeira.
Para utilizar o comando corretamente você precisa:

    - inicializar as variáveis de controle antes do comando;
    - criar uma condição que usa a variável de controle e se mantenha verdadeira pelo número correto de iterações;
    - modificar a variável de controle para garantir a terminação; e
    - realizar as computações sucessivas para se chegar a resposta correta.


