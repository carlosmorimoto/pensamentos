
..  Copyright (C) Carlos Hitoshi Morimoto e José Coelho de Pina Jr.
    É permitida a cópia, distribuição e/ou modificação desde documento
    sob os termos da Licença de Documentação Livre GNU (GNU Free 
    Documentation License) versão 1.3 ou versão subsequente
    publicada pela Fundação de Software Livre (Free Software 
    Foundation); com as seções invariantes sendo Prefácio e 
    Lista de Contribuidores, sem texto de capa ou contra-capa.  
    Uma cópia da licença está disponível em:
    https://www.gnu.org/licenses/licenses.pt-br.html#FDL


.. qnum::
	:prefix: cap05-03
	:start: 1


   
Descrição do fluxo do programa
..............................

O comando ``while`` repete a sequência de comandos definida em seu corpo enquanto a ``condição`` permanecer verdadeira.

Quando o comando while é executado, a ``condição`` é testada e, caso
verdadeira, o seu corpo é executado um comando de cada vez, ou seja
``comando_1`` primeiro, depois o ``comando_2``, até o ``comando_n``.  Após a
execução do ``comando_n`` a ``condição`` volta a ser testada e, caso
verdadeira, o processo se repete. O while termina quando, ao testar a
``condição``, o resultado do teste for ``False``.


Exemplo
.......

Observe o fluxo do programa abaixo, executando-o passo-a-passo:

.. codelens:: Exemplo_de_while
    :showoutput:

    # inicialização
    fim  = 5        # número de iterações
    cont = 0        # variável de controle

    while cont < fim: 
        # faça alguma coisa, nesse caso, apenas imprima cont
        print("Iteracao numero: ", cont)

        cont = cont + 1  # variável de controle precisa ser
                         # atualizada para garantir o fim do while

   
Esse programa imprime os números de 1 até ``fim``. Procure entender bem as partes desse programa:

    - inicialização das variáveis antes do ``while``
    - condição do ``while``, que define o número de iterações
    - atualização da variável de controle, que garante o fim do ``while``.
      

.. admonition:: Nota sobre **Simulação de um Programa**

   Os programas em Python são constituídos por comandos executados um de cada vez, passo-a-passo. Saber simular um programa é uma habilidade muito importante para entender o que um programa faz, encontrar problemas (`bugs <https://en.wikipedia.org/wiki/Software_bug>`__) e testar soluções.

   Para simular um programa você deve primeiro definir um teste, ou seja, você precisa definir uma entrada e a correspondente saída esperada pelo programa. A "saída" não precisa ser algo impresso mas pode ser uma certa configuração de valores de variáveis. Durante uma simulação devemos executar cada comando e verificar se a evolução dos valores das variáveis está correta.

   Ferramentas como o codelens ajudam, mas você deve aprender a simular um programa manualmente, **sem** o auxílio de um computador. 


