
..  Copyright (C) Carlos Hitoshi Morimoto e José Coelho de Pina Jr.
    É permitida a cópia, distribuição e/ou modificação desde documento
    sob os termos da Licença de Documentação Livre GNU (GNU Free 
    Documentation License) versão 1.3 ou versão subsequente
    publicada pela Fundação de Software Livre (Free Software 
    Foundation); com as seções invariantes sendo Prefácio e 
    Lista de Contribuidores, sem texto de capa ou contra-capa.  
    Uma cópia da licença está disponível em:
    https://www.gnu.org/licenses/licenses.pt-br.html#FDL


.. qnum::
	:prefix: cap05-02
	:start: 1


   
Sintaxe do comando while
........................

A sintaxe do comando ``while`` é a seguinte::
  
  while condição:
      # sequência de comandos executados no corpo do while
      comando_1
      comando_2
      ...
      comando_n

A ``condição`` é em geral definida na forma de uma `expressão lógica <02-booleanos.html>`__. 
O resultado de uma expressão lógica é sempre ``True`` ou ``False``. 
    
A sequência de comandos ``comando_1``, ``comando_2``, ..., ``comando_n``
pode conter qualquer comando do Python como atribuição, entrada ou saída, e outros, até mesmo outro ``while``. 

.. admonition:: Nota sobre **Programação Estruturada**

   Uma característica importante do Python é que ele força a estruturação do programa em blocos que podem ser facilmente visualizados pela **tabulação**. Observe que os comandos *dentro* do ``while``, ou seja, os comandos <comando_1> a <comando_n>, precisam ser deslocados, ou seja, devem começar em uma coluna deslocada para a direita do while. Caso existam comandos a serem executados após o while terminar, esses comandos devem ser escritos usando a mesma tabulação (na mesma coluna) que o while.

