
..  Copyright (C) Carlos Hitoshi Morimoto e José Coelho de Pina Jr.
    É permitida a cópia, distribuição e/ou modificação desde documento
    sob os termos da Licença de Documentação Livre GNU (GNU Free 
    Documentation License) versão 1.3 ou versão subsequente
    publicada pela Fundação de Software Livre (Free Software 
    Foundation); com as seções invariantes sendo Prefácio e 
    Lista de Contribuidores, sem texto de capa ou contra-capa.  
    Uma cópia da licença está disponível em:
    https://www.gnu.org/licenses/licenses.pt-br.html#FDL


.. qnum::
	:prefix: cap05-04
	:start: 1



Exercícios
----------
   
Exercício 1
...........

Dada uma sequência de números inteiros diferentes de zero,
terminada por um zero, calcular a sua soma. Por exemplo, 
para a sequência: 

.. sourcecode:: python

    12   17   4   -6   8   0      

o seu programa deve escrever o número ``35``. 

Tente escrever a sua solução primeiro e, depois,
clique `aqui <exercicios/while/while_ex1.html>`__ para ver uma.

.. activecode:: aula_while_ex1_tentativa

    # escreva aqui a sua solução


    
Exercício 2
...........

   Exercício 4 da `lista sobre inteiros <http://www.ime.usp.br/~macmulti/exercicios/inteiros/index.html>`__.

Dados números inteiros ``n`` e ``k``, com ``k >= 0``, 
calcular ``n`` elevado a ``k``.
Por exemplo, dados os números ``3`` e ``4`` o seu
programa deve escrever o número ``81``.


Tente escrever a sua solução abaixo primeiro e, depois,
clique `aqui <exercicios/while/while_ex2.html>`__ para ver uma.

.. activecode:: aula_while_ex2_tentativa

    # escreva aqui a sua solução
    

   
Exercício 3
...........

   Exercício 8 da `lista sobre inteiros <http://www.ime.usp.br/~macmulti/exercicios/inteiros/index.html>`__.

Dado um número inteiro ``n >= 0``, calcular ``n!``. 

Tente escrever a sua solução abaixo primeiro e, depois,
clique `aqui <exercicios/while/while_ex3.html>`__ para ver uma.

.. activecode:: aula_while_ex03_tentativa

    # escreva aqui a sua solução



Exercício 4
...........

    Exercício 9 da `lista sobre inteiros <http://www.ime.usp.br/~macmulti/exercicios/inteiros/index.html">`__. 

Dados números inteiros ``n``, ``i`` e ``j``,
todos maiores do que zero,  
imprimir em ordem crescente os  ``n`` primeiros naturais que
são múltiplos de ``i`` ou de ``j`` e ou de ambos.

Por exemplo, para ``n = 6``, ``i = 2`` e ``j = 3`` a saída deverá ser: 

.. sourcecode:: python
 
    0   2   3   4   6   8


.. activecode:: aula_while_ex04_tentativa

    # Digite a sua solucao
    

Clique `aqui <exercicios/multiplos_i_j.html>`__ para ver uma solução.

    
Exercício 5
...........

    Exercício 12 da `lista sobre inteiros <http://www.ime.usp.br/~macmulti/exercicios/inteiros/index.html">`__. 

Dados dois inteiros positivos calcular o máximo divisor
comum entre eles usando o `algoritmo de Euclides <https://pt.wikipedia.org/wiki/Algoritmo_de_Euclides>`__.

.. activecode:: aula_while_ex05_tentativa
		
    # Escreva o seu programa


Clique `aqui <exercicios/euclides.html>`__ para ver uma solução.


