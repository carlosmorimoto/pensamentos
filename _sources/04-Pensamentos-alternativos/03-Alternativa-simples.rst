
..  Copyright (C) Carlos Hitoshi Morimoto e José Coelho de Pina Jr.
    É permitida a cópia, distribuição e/ou modificação desde documento
    sob os termos da Licença de Documentação Livre GNU (GNU Free 
    Documentation License) versão 1.3 ou versão subsequente
    publicada pela Fundação de Software Livre (Free Software 
    Foundation); com as seções invariantes sendo Prefácio e 
    Lista de Contribuidores, sem texto de capa ou contra-capa.  
    Uma cópia da licença está disponível em:
    https://www.gnu.org/licenses/licenses.pt-br.html#FDL


.. qnum::
	:prefix: cap04-03
	:start: 1



Alternativa simples
-------------------

Antes de pensar na solução é necessário ter um profundo entendimento do problema. 

Ao pensar no problema muitas alternativas tendem a surgir naturalmente.
Problemas muito complexos apresentam um grande número de casos que precisamos considerar.
Para tratar problemas complexos assim, precisamos quebrar o problema em subproblemas mais simples, pedaços ou
"classes" de problemas e depois quebrar as classes em subclasses, e
assim por diante, até que consigamos resolver o subproblema sem
precisar decompo-lo.

Para ajudar você a pensar no problema, um primeiro passo portanto é
identificar as alternativas ou classes como, por exemplo, escolher se
devemos ficar ou correr.  A seguir podemos tentar identificar
propriedades dos dados que nos permita tomar decisões como, se o bicho
for gordo e lento a gente corre, e  o bicho for manso a gente
fica, e o bicho for forte e rápido...
A identificação dessas classes e propriedades fazem parte do modelamento do problema.

Problemas simples tem poucas classes e propriedades. Por exemplo, para
saber se uma pessoa tem idade para tirar a carteira de motorista,
podemos testar a propriedade "idade" e caso a idade for maior ou igual
a 18 anos, então a pessoa pode tirar a carteira.
Caso contrário, a pessoa não pode.
Essa situação tem portanto apenas 2 classes, que
são as das pessoas com 18 ou mais anos e as com menos de 18 anos de idade.

Assim, um problema mais simples de classificação surge quando podemos
dividir o problema original em apenas duas classes como, por exemplo, maior ou
menor, com ou sem fome, dentro ou fora, alto ou baixo, certo ou
errado, verdadeiro ou falso etc.


