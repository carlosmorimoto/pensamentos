
..  Copyright (C) Carlos Hitoshi Morimoto e José Coelho de Pina Jr.
    É permitida a cópia, distribuição e/ou modificação desde documento
    sob os termos da Licença de Documentação Livre GNU (GNU Free 
    Documentation License) versão 1.3 ou versão subsequente
    publicada pela Fundação de Software Livre (Free Software 
    Foundation); com as seções invariantes sendo Prefácio e 
    Lista de Contribuidores, sem texto de capa ou contra-capa.  
    Uma cópia da licença está disponível em:
    https://www.gnu.org/licenses/licenses.pt-br.html#FDL


.. qnum::
	:prefix: cap04-01
	:start: 1

Introdução
----------

.. index:: if, if-else, if-elif-else

.. index:: % 
  
.. raw:: html

    <div align="right">
    <p></p>
    <blockquote>
    <p>
    "
    Se correr o bicho pega.</br>
    Se ficar o bicho come. 
    "
    </p>
    <p>Ditado popular</p>
    </blockquote>
    <p></p>
    </div>

O foco do capítulo anterior foi na entrada e na saída dos dados, que permitem que programas
se comuniquem em particular com as pessoas. 
Realçamos a importância da escolha dos tipos para representar cada dado e
aprendemos a salvar e manipular dados por meio de variáveis. 

O processamento dos dados que realizamos até aqui ficou limitado ao cálculo de expressões, 
feito de forma **linear** ou **sequencial** como no caso da conversão de uma temperatura em 
Celsius para Fahrenheit.
Nesse processamento há uma sequência de instruções em que cada uma é executada, uma após a outra.

Soluções de problemas mais complexos requererem o tratamento de casos
alternativos, criando bifurcações no fluxo de execução fazendo que nem
toda instrução é seja executada.
Em forma não linear alguns trechos de código são pulados ou tratados alternativas.
Por exemplo, um caminho da bifurcação por cuidar da conversãi de graus Celsius para Fahrenheit,
enquanto que um caminho alternatico pode fazer a conversão de Fahrenheit para Celsius.
Para isso, é necessário instruir o computador para executar certos trechos do programa apenas
quando alguma condição é satisfeita: o valor recebido está em graus Celsius e deve ser convertido para Fahrenheit ou
está em Fahrenheit e deve ser convertido para Celsius.

A identificação e decomposição de um problema em alternativas,  
definição das condições e da lógica para aplicação dessas condições 
é o fundamento do pensamento computacional que vamos treinar nesse capítulo.

