
..  Copyright (C) Carlos Hitoshi Morimoto e José Coelho de Pina Jr.
    É permitida a cópia, distribuição e/ou modificação desde documento
    sob os termos da Licença de Documentação Livre GNU (GNU Free 
    Documentation License) versão 1.3 ou versão subsequente
    publicada pela Fundação de Software Livre (Free Software 
    Foundation); com as seções invariantes sendo Prefácio e 
    Lista de Contribuidores, sem texto de capa ou contra-capa.  
    Uma cópia da licença está disponível em:
    https://www.gnu.org/licenses/licenses.pt-br.html#FDL


.. qnum::
	:prefix: cap04-11
	:start: 1



Comando ``if-elif-else``
........................

Para simplificar ainda mais o código de programas com ``if-else`` aninhados, o Python oferece o comando ``if-elif-else``, cuja sintaxe é a seguinte.


.. Sintaxe do comando if-elif-else

.. code-block:: Python

    if condição:
        # bloco contendo comandos a serem executados 
        dentro_do_if_1
        dentro_do_if_2
        ...
        dentro_do_if_p

    elif condição elif1:
        dentro_do_elif1_1
        dentro_do_elif1_2
        ...
        dentro_do_elif1_m1

    elif condição elif2:
        dentro_do_elif2_1
        dentro_do_elif2_2
        ...
        dentro_do_elif2_m2

    # outros possíveis blocos de elif, cada um com a sua condição 

    elif condição elifk:
        dentro_do_elifk_1
        dentro_do_elifk_2
        ...
        dentro_do_elifk_mk
    
    else:                   # o else é opcional
        dentro_do_else_1
        dentro_do_else_2
        ...
        dentro_do_else_q
    
    comando_apos_if


Ou seja, o ``if-elif-else`` pode ter um ou mais blocos com ``elif``, cada um
com a sua condição específica. Cada bloco (condição) é testado um de cada vez,
até que uma condição seja satisfeita e apenas os comandos dentro desse bloco
são executados. Quando nenhuma condição é satisfeita, os comandos dentro do
``else`` são executados. Assim,  **não** é necessário que haja um ``else``
no final.


    
A figura 4.4 ilustra mostra como a contração de ``else-if`` como ``efif`` (em pseudocódigo estamos usando "senão-se") pode tornar o código mais legível e portanto mais fácil de ler e entender. Como o Python desloca cada bloco de um tab, após vários tabs pode se tornar difícil ler o código que fica espremido no canto direito da página. O pseudocódigo também ilustra esse benefício de formatação e simplificação da leitura do código ao comparar o uso de ``else`` (senão) seguido de ``if`` (se) e de ``elif`` (senão-se junto). 

..  O fonte da figura está em:  
    https://app.diagrams.net/#G1Y2cw7OCcMmKkng7AbzPbM1NKWEjS_Z2l

.. figure:: ../Figuras/cap04/if-elif-else.png
    :align: center
    :alt: Figura 4.4: Exemplo de uso de if-elif-else para classificar um aluno

    Figura 4.4: Fluxogramas e pseudocódigo para classificar a situação de um aluno entre ``aprovado``, ``reprovado`` e de ``recuperação`` usando a contração "senão-se" (``elif``).


Usando ``elif`` poderíamos escrever o algoritmo da figura 4.4 da seguinte forma:
    
.. activecode:: cl04_Exemplo_de_uso_do_if-elif-else

    nota = float(input("Digite uma nota: "))

    if nota >= 5.0:
        situação = "aprovado"
    elif nota >= 3.0:
        situação = "recuperação"
    else:
        situação = "reprovado"
    print( situação )	


O ``elif`` portanto é apenas uma contração do ``else if`` que torna mais claro
o tratamento das várias alternativas, encadeando as condições. Blocos de
``elif`` podem ser repetidos várias vezes. Suponha por exemplo que gostaríamos
de conhecer os alunos aprovados ``com louvor``, ou seja, com nota superior
a 9.0. Nesse caso, uma outra solução seria:

    
.. activecode:: cl04_Outro_exemplo_de_uso_do_if-elif-else
    
        nota = float(input("Digite uma nota: "))
        if nota >= 9.0: 
            situação = "com louvor!"
        elif nota >= 5.0:
            situação = "aprovado"
        elif nota >= 3.0:
            situação = "recuperação"
        else:
            situação = "reprovado"
        print( situação )	


Faça os seus próprios experimentos com o trecho de código a seguir.

.. activecode:: condicao_em_cadeia

    n = 25   # altere esse valor e simule esse trecho novamente
    if n < 0:
        print(n, "< 0")
    elif n == 0:
        print(n, "== 0")
    elif n < 10:
        print("0 < ", n,"< 10")
    elif n < 20:
        print("10 <=", n, "< 20")
    elif n < 30:
        print("20 <=", n, "< 30")
    elif n < 50:
        print("30 <=", n, "< 50")
    else:
        print("50 <=", n)

    print("Termino do teste.")
  
