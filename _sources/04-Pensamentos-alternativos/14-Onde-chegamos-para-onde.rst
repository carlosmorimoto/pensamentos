
..  Copyright (C) Carlos Hitoshi Morimoto e José Coelho de Pina Jr.
    É permitida a cópia, distribuição e/ou modificação desde documento
    sob os termos da Licença de Documentação Livre GNU (GNU Free 
    Documentation License) versão 1.3 ou versão subsequente
    publicada pela Fundação de Software Livre (Free Software 
    Foundation); com as seções invariantes sendo Prefácio e 
    Lista de Contribuidores, sem texto de capa ou contra-capa.  
    Uma cópia da licença está disponível em:
    https://www.gnu.org/licenses/licenses.pt-br.html#FDL


.. qnum::
	:prefix: cap04-14
	:start: 1




Onde chegamos e para onde vamos?
--------------------------------

Nesse capítulo introduzimos os comandos ``if`` ``if-else`` que são usados para controlar o fluxo da computação, permitindo que certos trechos de código sejam executados condicionalmente. As condições devem ser o resultado de uma expressão lógica ou relacional.

O entendimento de problemas tipicamente exige um modelamento que organiza os dados em classes, como dentro ou fora, alto ou baixo, aprovado, reprovado ou de recuperação, etc. Ao longo do curso vamos resolver mais exercícios para desenvolver essa habilidade de **reconhecer padrões** que facilitam o desenolvimento de soluções. 

Como primeira estratégia para começar a pensar nos algoritmos, vimos que começar assumindo um estado inicial traz benefícios e facilita nosso pensando a focar nas condições que devem ocorrer para que o programa calcule o estado correto. No laboratório, vimos uma solução (algoritmo) pode ser mais ou menos complexa dependendo desse estado inicial. Portanto, antes de você se decidir por escrever um programa que implementa algum algoritmo, é sempre bom considerar outros algoritmos. Uma forma simples é trocar a condição inicial (como assumir que o ponto está dentro ou fora) e explorando as implicações dessas condições e alternativas.

O grande poder da computação, no entanto, vem de sua capacidade de executar as mesmas instruções milhares e milhares de vezes, rapidamente, repetidamente, sem se cansar e sempre com a mesma qualidade. Esse poder é expresso pelos comandos de repetição que permitem executar um bloco de comandos de novo, e de novo, e de novo... 








O próximo capítulo introduz outro fundamento que revela 
