
..  Copyright (C) Carlos Hitoshi Morimoto e José Coelho de Pina Jr.
    É permitida a cópia, distribuição e/ou modificação desde documento
    sob os termos da Licença de Documentação Livre GNU (GNU Free 
    Documentation License) versão 1.3 ou versão subsequente
    publicada pela Fundação de Software Livre (Free Software 
    Foundation); com as seções invariantes sendo Prefácio e 
    Lista de Contribuidores, sem texto de capa ou contra-capa.  
    Uma cópia da licença está disponível em:
    https://www.gnu.org/licenses/licenses.pt-br.html#FDL


.. qnum::
	:prefix: cap04-02
	:start: 1



Para onde vamos
-------------------

Depois do treinamento neste capítulo você terá aplicado e identificado situações para o emprego:

    - de execução *condicional* com ``if``;
    - de execução *alternativa* com  ``if-else``;
    - de execução *em cadei* com  ``if-elif-else``;
    - de condições na forma de expressões relacionais e lógicas; e
    - do operador de resto de divisão ``%``.

..     - Usar o operador de divisão inteira ``//``; e
  
