
..  Copyright (C) Carlos Hitoshi Morimoto e José Coelho de Pina Jr.
    É permitida a cópia, distribuição e/ou modificação desde documento
    sob os termos da Licença de Documentação Livre GNU (GNU Free 
    Documentation License) versão 1.3 ou versão subsequente
    publicada pela Fundação de Software Livre (Free Software 
    Foundation); com as seções invariantes sendo Prefácio e 
    Lista de Contribuidores, sem texto de capa ou contra-capa.  
    Uma cópia da licença está disponível em:
    https://www.gnu.org/licenses/licenses.pt-br.html#FDL


.. qnum::
	:prefix: cap04-13
	:start: 1



Laboratório: perdendo o medo do Spyder
--------------------------------------

O último laboratório introduziu o Spyder, o ambiente de desenvolvimento integrado (IDE) que adotamos nesse curso por ser aberto (Open Source), grátis e fácil de instalar. Ele na verdade vem junto com o Anaconda, um pacote que reúne, além do Sypder, vários módulos que facilitam o desenvolvimento de programas científicos em Python. Nesse laboratório vamos reforçar o uso do Spyder e aproveitar para ganhar ilustrar como linhas distintas de pensamento podem ser exploradas para criar várias soluções para um mesmo problema.

Se pudermos comparar "aprender a resolver problemas" com "aprender a andar de bicicleta", muitas vezes o foco é na bicleta que vamos usar. Nessa metáfora, vamos considerar o ambiente de programação como a roupa que a gente precisa "vestir" para poder andar melhor de bicicleta, que ajudam nos ajudam a nos sentir confortáveis e seguros.

Assim como um roupa nova pode parecer justa ou apertada, como pode ser difícil de ajustar o cinto, ou como o capacete pode parecer um pouco pesado e desconfortável, toda ferramenta nova também cria um certo desconforto mas, depois que a gente consegue se acostumar e a roupa se ajusta na gente, o desconforto some, a confiança aumenta e permite que a gente se esqueça do traje e consiga focar em pedalar e, quem sabe, pensar e apreciar a paisagem.

Por isso, sugerimos que você use o Spyder para criar, digitar e executar vários programas nesse laboratório.
Todos esses programas usam algoritmos distintos mas que produzem o mesmo resultado. Gostaríamos que vocês consigam ficar confortáveis o suficiente com o uso do Spyder para conseguirem pensar e apreciar todos os cheiros, cores e texturas de cada alternativa.

.. Mas sabemos que, no início, pode ser díficil mudar de marcha, fazer uma curva, ou desviar de um buraco. Alguns  novatos podem também começar a sentir desconforto e dor em músculos que foram pouco exercitados antes, alguns caem ou se sentem frustrados por não ter a força para pedalar mais rápido. 

Mas para atingir esse estágio a gente vai precisar pedalar bastante. Nesse laboratório, procure digitar caractere a caractere os programas abaixo, ou seja, não use CTRL-C para copiar e CTRL-V para colar o programa no editor. 

O Spyder é uma ferramenta bastante sofisticada que pode ajudar você de várias maneiras. 
Mesmo que você saiba digitar bem rápido, ao digitar várias programas você vai começar a entender e descobrir os recursos que o editor do Spyder lhe oferece. Aprenda a usar as teclas de aceleração como `F5` para executar e CTRL-S para salvar o arquivo (embora algumas combinações dependam do seu sistema operacional), observe que o Python muda a cor de algumas palavras, procure entender o que cada cor significa, procure entender o que o Spyder tenta comunicar sobre o seu programa enquanto você está digitando. Aprenda a configurar o Spyder, modificando a cor de fundo, ou o tamanho da fonte, ou usando um tema diferente para todo o ambiente.

É natural ter um pouco de medo de usar algo complexo e sofisticado, mas quanto mais você usar, mais você vai se sentir confortável e leve usando o Spyder e, quem sabe, até se divertir um pouco!). 

Exercício: classifique um ponto como dentro ou fora
...................................................

Esse exercício foi baseado na questão 1 da `Prova 1 de 2014 <http://www.ime.usp.br/~mac2166/provas/P1-2014.html">`__ da disciplina MAC2166, criada pelo [Prof. Arnaldo Mandel](https://www.ime.usp.br/~am).

Use o Spyder para escrever um programa que lê as coordenadas cartesianas ``(x, y)`` de um ponto, ambas do tipo ``float`` e imprime ``dentro`` se esse ponto está na região, e ``fora`` caso contrário.
A região sombreada não inclui as bordas, ou seja, pontos nas bordas de uma região estão ``fora``.

Note que o eixo ``y`` cai bem no meio da figura, e usamos o lado do quadrado para indicar as ordenadas correspondentes.

.. figure:: ../Figuras/cap04/face.png
    :align: center
    :alt: Figura 4.5: exercício pra classificar os pontos da região sombreada.

    Figura 4.5: exercício pra classificar os pontos da região sombreada. Pontos sobre as bordas deem ser considerados ``fora``.


Para a sua conveniência, você pode executar cada solução abaixo usando o CodeLens. Mesmo assim, digite cada solução no seu computador para "perder o medo" do Spyder.

Solução 0 
.........

Essa solução começa supondo que está ``dentro`` e realiza testes para verificar se o ponto está ``fora``. 
A variável  ``dentro`` é uma variável de estado também chamada de **indicador de passagem** do tipo ``bool``.

.. activecode:: cl04_solucao0_p1_2014

    x = float(input("Digite x: "))
    y = float(input("Digite y: "))

    # suponha que (x,y) esta dentro
    dentro = True

    if x <= -5 or 5 <= x or y <= 0 or 8 <= y:
        # aqui sabemos que (x,y) esta fora da face
        dentro = False
    elif -3 <= x <=  3 and 1 <= y <= 2:
        # aqui sabemos que (x,y) esta na boca
        dentro = False
    elif -4 <= x <= -1 and 4 <= y <= 7:
        # aqui sabemos que (x,y) esta no olho esquerdo
        dentro = False
        if -3 < x < -2 and 5 < y < 6:
            # aqui sabemos que na verdade (x,y) esta na iris esquerda
            dentro = True
    elif  1 <= x <=  4 and 4 <= y <= 7:
        # aqui sabemos que (x,y) esta no olho direito
        dentro = False
        if  2 < x <  3 and 5 < y < 6:
            # aqui sabemos que na verdade (x,y) esta na iris direita
            dentro = True

    if dentro:
        print("dentro")
    else:
        print("fora")
            

Solução 1
.........

Essa solução começa supondo que está fora e depois...
A variável  ``dentro`` é uma variável de estado também chamada de **indicador de passagem** do tipo ``bool``.

.. activecode:: cl04_solucao1_p1_2014

    x = float(input("Digite x: "))
    y = float(input("Digite y: "))

    # suponha que (x,y) esta fora
    dentro = False

    if -5 < x < 5 and 0 < y < 8:
        # aqui sabemos que (x,y) esta na face
        dentro = True
        if -3 <= x <= 3 and 1 <= y <= 2:
            # aqui sabemos que (x,y) esta na face, mas esta na boca
            dentro = False
        elif -4 <= x <= -1 and 4 <= y <= 7:
            # aqui sabemos que (x,y) esta no olho esquerdo
            dentro = False
            if -3 < x < -2 and 5 < y < 6:
                # aqui sabemos que na verdade (x,y) esta na iris esquerda
                dentro = True
        elif  1 <= x <=  4 and 4 <= y <= 7:
            # aqui sabemos que (x,y) esta no olho direito
            dentro = False
            if  2 < x < 3 and 5 < y < 6:
                # aqui sabemos que na verdade (x,y) esta na iris direita
                dentro = True

    if dentro:
        print("dentro")
    else:
        print("fora")
        

Solução 2
.........

explora a simetria da figura em relação ao eixo y.
A variável ``x_pos`` pode ser trocada por x.      
A variável  ``dentro`` é uma variável de estado também chamada de **indicador de passagem** do tipo ``bool``.

.. activecode:: cl04_solucao2_p1_2014

    x_pos = x = float(input("Digite x: "))
    y = float(input("Digite y: "))

    if x < 0: # simetria ;-)
        x_pos = -x

    # suponha que (x,y) que esta dentro
    dentro = True

    if x_pos >= 5 or y >= 8 or y <= 0:
        # aqui sabemos que (x,y) esta fora da face
        dentro = False
    elif 0 <= x_pos <= 3 and 1 <= y <= 2:
        # aqui sabemos que (x,y) esta na boca
        dentro = False
    elif 1 <= x_pos <= 4 and 4 <= y <= 7:
        # aqui sabemos que (x,y) esta em um olho
        if not (2 < x < 3 or 5 < y < 6):
            # aqui sabemos que (x,y) esta fora da iris
            dentro = False

    if dentro:
        print("dentro")
    else:
        print("fora")
        

Solução 3
.........

Essa solução é idêntica a anterior, explora a simetria da figura em relação ao eixo ``y`` e utiliza um string para a armazenar resposta. 
A variável ``x_pos`` pode ser trocada por ``x``.      
A variável ``dentro`` é um indicador de passagem do tipo  ``str`` .

.. activecode:: cl04_solucao3_p1_2014

    x_pos = x = float(input("Digite x: "))
    y = float(input("Digite y: "))

    if x < 0: # simetria ;-)
        x_pos = -x

    # suponha que (x,y) que esta dentro
    resposta = "dentro"

    if x_pos >= 5 or y >= 8 or y <= 0:
        # aqui sabemos que (x,y) esta na face
        resposta = "fora"
    elif 0 <= x_pos <= 3 and 1 <= y <= 2:
        # aqui sabemos que (x,y) esta na boca
        resposta = "fora"
    elif 1 <= x_pos <= 4 and 4 <= y <= 7:
        # aqui sabemos que (x,y) esta em um olho
        if not (2 < x < 3 or 5 < y < 6):
            # aqui sabemos que (x,y) esta fora de uma iris
            resposta = "fora"

    print(resposta)
        

Solução 4
.........

Essa solução utiliza uma variável ``bool`` para indicar em qual parte da face está o ponto: face, boca
olho direito, olho esquerdo, íris do olho direito, íris do olho esquerdo. A condição para decidir se o ponto está ou não na região sombreada pode parecer complicada, mas é bem elegante e é um excelente exercício para treinar expressões lógicas.

.. activecode:: cl04_solucao4_p1_2014

    x = float(input("Digite x: "))
    y = float(input("Digite y: "))

    # face == True se (x,y) esta na face, False em caso contrário
    face = -5 < x < 5 and 0 < y < 8 

    # boca == True se (x,y) esta na boca, False em caso contrário
    boca = -3 <= x <= 3 and 1 <= y <= 2

    # olho_esquerdo == True se (x,y) esta no olho esquerdo
    olho_esquerdo = -4 <= x <= -1 and 4 <= y <= 7

    # iris_esquerda == True se (x,y) esta na iris esquerda
    iris_esquerda = -3 <= x <= -2 and 5 <= y <= 6

    # olho_direito  == True se (x,y) esta no olho direito
    olho_direito = 1 <= x <= 4 and 4 <= y <= 7

    # iris_direita  == True se (x,y) esta na iris direita
    iris_direita = 2 <= x <= 3 and 5 <= y <= 6

    # vixe! :-D complicado? certamente elegante...
    if iris_esquerda or iris_direita or face and not (boca or olho_esquerdo or olho_direito):
        print("dentro")
    else:
        print("fora")
        

Solução 5
.........

Essa solução é idêntica a anterior, mas explora a simetria como na solução 3.

.. activecode:: cl04_solucao5_p1_2014

    x_pos = x = float(input("Digite x: "))
    y = float(input("Digite y: "))

    if x < 0:
        x_pos = -x 

    # face == True se (x,y) esta na face
    face = x_pos <  5 and 0 < y < 8 

    # boca == True se (x,y) esta na boca
    boca = x_pos <= 3 and 1 <= y <= 2

    # olho == True se (x,y) esta em um dos olhos
    olho = 1 <= x_pos <= 4 and 4 <= y <= 7

    # iris == True se (x,y) esta em uma das iris
    iris = 2 <= x_pos <= 3 and 5 <= y <= 6

    # vixe! :-D complicado?! certamente muito elegante
    if iris or face and not (boca or olho):
        print("dentro")
    else:
        print("fora")

