
..  Copyright (C) Carlos Hitoshi Morimoto e José Coelho de Pina Jr.
    É permitida a cópia, distribuição e/ou modificação desde documento
    sob os termos da Licença de Documentação Livre GNU (GNU Free 
    Documentation License) versão 1.3 ou versão subsequente
    publicada pela Fundação de Software Livre (Free Software 
    Foundation); com as seções invariantes sendo Prefácio e 
    Lista de Contribuidores, sem texto de capa ou contra-capa.  
    Uma cópia da licença está disponível em:
    https://www.gnu.org/licenses/licenses.pt-br.html#FDL


.. qnum::
	:prefix: cap04-07
	:start: 1



Comando ``if-else``
...................

Para escrever a execução mutuamente exclusiva em Python podemos utilizar o comando ``if-else`` (que significa *se-senão* em português), cuja sintaxe é a seguinte::

.. Sintaxe do comando if-else

.. code-block:: Python

    if condição:
        # bloco contendo comandos a serem executados 
        dentro_do_if_1
        dentro_do_if_2
        ...
        dentro_do_if_m
    else:
        dentro_do_else_1
        dentro_do_else_2
        ...
        dentro_do_else_n
        
    comando_após_if


Apenas caso a ``condição`` for verdadeira, o bloco contendo os comandos ``dentro_do_if_1``  a ``dentro_do_if_m`` é executado. Caso contrário, esse bloco ``dentro_do_if`` é pulado e o programa executa os comandos ``dentro_do_else_1`` a ``dentro_do_else_n``. Ao final do bloco, a execução do programa continua com o comando ``comando_após_if``.

O fluxograma da figura 4.2 ilustra que, 
ao invés de iniciar uma variável de estado com a aluna em situação de ``aprovada`` como mostrado na figura 4.1, podemos usar essa construção com execução mutuamente exclusiva, como ilustrado pelo seguinte trecho de código em Python (novamente, muito parecido com o pseudocódigo, mas em inglês):


.. activecode:: cl04_if_aprovado_else_reprovado

    nota = float(input('Digite a nota: '))
    if nota >= 5.0:
        situacao = 'aprovada'
    else:
        situacao = 'reprovada'
    print( situacao )


.. admonition:: **Como funciona esse trecho?**

    Logo após ler a nota na linha *1*), a nota é testada na linha *2* e, caso seja maior ou igual 5.0, a situação da aluna é marcada como `aprovada`. Caso contrário, o programa executa o bloco da cláusula ``else`` na linha *5*, marcando a situação como `reprovada`. A última linha imprime a situação da aluna.

    Embora essa solução seja bastante clara e elegante para aplicar o ``if-else``, 
    para situações que dependam de uma **variável de estado**, recomendamos o uso da versão **sem** o ``else``. Além de ser mais curto escrever apenas o ``if``, esse tipo de solução força o pensamento a começar em algum estado (inicialização da variável de estado) e isso pode ajudar a estruturar melhor a solução.

    **Mas então, quando devemos usar o ``else``?** 
    
    A resposta (ao menos por enquanto) é usar o ``if-else`` para evitar a execução de computações desnecessárias e caras. Por exemplo, imagine que o Cebolinha dependa da resposta da Mônica para pintar a casa de vermelho ou de verde. O Cebolinha pode arriscar e primeiro pintar a casa de verde e, dependendo da resposta da Mônica, ter de repintar a casa depois de vermelho. Em pseudo código, isso poderia ser escrito como:

    * pinte a casa de verde
    * leia a cor preferida da Mônica
    * se a cor preferida for vermelho
        * pinte a casa de vermelho

    Veja que há um risco grande do Cebolinha ter de gastar muita tinta seguindo esse algoritmo. Nesse caso, seria mais ecológico seguir os seguintes passos:

    * leia a cor preferida da Mônica
    * se a cor preferida for vermelho
        * pinte a casa de vermelho
    * senão:
        * pinte a casa de verde

    Veja que desse jeito a gente consegue ajudar o Cebolinha a economizar muita tinta e tempo!

    Assim como é bom para o Cebolinha economizar tinta nesse exemplo, ao escrever programas, **procure evitar computações desnecessárias**. O ``else`` pode ser muito útil para isso. No caso da variável de estado, o custo computacional é irrelevante e podemos ganhar na qualidade da solução com uma estratégia mais clara de começar em um estado e ir decompondo o problema em subalternativas.

