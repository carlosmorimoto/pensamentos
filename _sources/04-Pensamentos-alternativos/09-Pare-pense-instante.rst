
..  Copyright (C) Carlos Hitoshi Morimoto e José Coelho de Pina Jr.
    É permitida a cópia, distribuição e/ou modificação desde documento
    sob os termos da Licença de Documentação Livre GNU (GNU Free 
    Documentation License) versão 1.3 ou versão subsequente
    publicada pela Fundação de Software Livre (Free Software 
    Foundation); com as seções invariantes sendo Prefácio e 
    Lista de Contribuidores, sem texto de capa ou contra-capa.  
    Uma cópia da licença está disponível em:
    https://www.gnu.org/licenses/licenses.pt-br.html#FDL


.. qnum::
	:prefix: cap04-09
	:start: 1



Pare e pense um instante
........................

Procure pensar em como evitar o ``if-if`` nesse caso, modificando o código acima para (edite no próprio ActiveCode) para:

* **Solução alternativa 1**: usar ``if-else`` ao invés de ``if-if``: altere o ``if`` na linha *5* para usar um ``else``. Observe que essa solução é mais eficiente pois evita a computação de "n não é múltiplo de 2", e também é mais elegante e clara pois o uso do ``else`` indica que o processamento é feito de forma mutualmente exclusiva; 

.. activecode:: ac04_troque_if_if_por_if_else
    :nocanvas:

    n = int(input("Digite um numero: ")

    if n % 2 == 0:  # se n é múltiplo de 2
        estado = "par"
    if n % 2 != 0:  # se n não é múltiplo de 2
        estado = "ímpar"

    print(f"o número {n} é {estado}")	

* **Solução alternativa 2**: usar um só ``if`` ao invés de ``if-if``: simplesmente assumindo que o valor inicial de ``estado = "ímpar"`` antes do primeiro ``if``; e

.. activecode:: ac04_troque_if_if_por_um_if_apenas
    :nocanvas:

    n = int(input("Digite um numero: ")

    if n % 2 == 0:  # se n é múltiplo de 2
        estado = "par"
    if n % 2 != 0:  # se n não é múltiplo de 2
        estado = "ímpar"

    print(f"o número {n} é {estado}")	

* **Outros múltiplos**: modifique a condição do ``if`` para verificar se um número é múltiplo de 3 ou não. Experimente outros números também para entender o operador ``%``.

Embora a solução ``if-if`` produza resultados corretos, o uso do comando ``if-else`` ou apenas o ``if`` torna o programa bem mais elegante e seguro.


.. admonition:: **Variável de estado**

    Pense agora na variável ``estado``. Você já deve ter pensando que essa variável é  inútil e que podemos eliminá-la e colocar ``prints`` nas linhas *4* e *6*, com as mensagens adequadas. 

    Se você pensou assim, com relação ao funcionamento do programa, você tem razão. 

    Por outro lado, gostaríamos que você procure usar esse padrão de pensamento:
    
    #. carregar os dados e definir os estados iniciais;
    #. processar os dados e ir evoluindo os estados até que, só ao final, 
    #. terminar o programa exibindo o resultado. 
   
    Seguiremos esse padrão ao longo desse livro pois facilita a leitura, o entendimento e a manutenção do código. 
    
