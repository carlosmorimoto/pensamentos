
..  Copyright (C) Carlos Hitoshi Morimoto e José Coelho de Pina Jr.
    É permitida a cópia, distribuição e/ou modificação desde documento
    sob os termos da Licença de Documentação Livre GNU (GNU Free 
    Documentation License) versão 1.3 ou versão subsequente
    publicada pela Fundação de Software Livre (Free Software 
    Foundation); com as seções invariantes sendo Prefácio e 
    Lista de Contribuidores, sem texto de capa ou contra-capa.  
    Uma cópia da licença está disponível em:
    https://www.gnu.org/licenses/licenses.pt-br.html#FDL


.. qnum::
	:prefix: cap04-10
	:start: 1

 


Alternativas múltiplas
----------------------

Tipicamente, quando um programa aumenta de complexidade, o número de alternativas cresce.
Por exemplo, além de aprovado ou reprovado, considere que um aluno pode também ficar de recuperação se sua nota estiver entre 3.0 e 5.0. Há várias formas de resolver esse problema. Vamos discutir por hora apenas a forma usando ``if-else`` aninhados para ilustrar outro recurso do Python para tornar a notação desses casos mais compacta e, portanto, mais simples de ler e manter.

..  O fonte da figura está em:  
    https://app.diagrams.net/#G17T3Xf_-qnTIfeZYPSzWp2HqxVtTE5YQR

.. figure:: ../Figuras/cap04/if-else-if-else.png
    :align: center
    :alt: Figura 4.3: Exemplo de uso de if-else aninhados para classificar um aluno

    Figura 4.3: Fluxogramas e pseudocódigo para classificar a situação de um aluno entre ``aprovado``, ``reprovado`` e de ``recuperação``.  


A figura 4.3 exibe um fluxograma e um pseudocódigo equivalente para classificar a situação de uma aluna como aprovada, reprovada ou de recuperação.
Uma solução **aninhando** comandos ``if-else`` seria:


.. activecode:: acl04_Exemplo_de_uso_do_if-else_aninhados_00

    nota = int(input("Digite uma nota: "))

    if nota >= 5.0:
        situação = "aprovado"
    else:
        if nota >= 3.0:
            situação = "recuperação"
        else:
            situação = "reprovado"
    print( situação )	

       
.. admonition:: **Como funciona esse trecho?**

   Observe com atenção a **tabulação** desse programa, que mostra um ``if-else`` na mesma coluna da linha *1* e, dentro do primeiro ``else``, vemos um outro comando ``if-else``. Por esse deslocamento sabemos que o segundo ``if`` só será testado caso a condição do primeiro ``if`` (da linha *3*) seja falsa.

   Vamos simular esse trecho usando 3 testes: nota 2 (que deve resultar em reprovado), nota 4 (que deve resultar em recuperação) e nota 7 que deve resultar em aprovado. Lembre-se de cobrir todas as alternativas ao definir seus testes.

   Quando a nota é 7, a condição do primeiro ``if`` é verdadeira e assim o programa guarda que a situação do aluno é "aprovado". 
   Quando a condição do ``if`` é verdadeira o ``else`` correspondente não é executado, e o programa termina imprimindo "aprovado", que é o valor associado à ``situação``. 

   Quando a nota é 4, a condição do primeiro  ``if`` é falsa e o bloco dentro do ``else`` correspondente (que está na mesma coluna desse ``if``) é executado, ou seja, o segundo ``if-else`` é executado. Para isso a condição do ``if`` na linha *6* é testada, resultando em verdadeiro. O programa guarda que a ``situação`` do aluno é então de "recuperação", o ``else`` correspondente não é executado e o programa termina imprimindo "recuperação", que é o valor associado à ``situação``. 

   Finalmente quando a nota é 2, a condição dos dois ``ifs`` é falsa e, nesse caso, o bloco dentro do segundo ``else`` (linha *8*) é executado, associando o valor "reprovado" à variável ``situação``. O programa passa para o final, imprimindo o resultado em ``situação``. 



