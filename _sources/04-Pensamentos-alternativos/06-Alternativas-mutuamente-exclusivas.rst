
..  Copyright (C) Carlos Hitoshi Morimoto e José Coelho de Pina Jr.
    É permitida a cópia, distribuição e/ou modificação desde documento
    sob os termos da Licença de Documentação Livre GNU (GNU Free 
    Documentation License) versão 1.3 ou versão subsequente
    publicada pela Fundação de Software Livre (Free Software 
    Foundation); com as seções invariantes sendo Prefácio e 
    Lista de Contribuidores, sem texto de capa ou contra-capa.  
    Uma cópia da licença está disponível em:
    https://www.gnu.org/licenses/licenses.pt-br.html#FDL


.. qnum::
	:prefix: cap04-06
	:start: 1

 

Alternativas mutuamente exclusivas
----------------------------------

Em muitas ocasiões temos que decidir entre duas ou mais possibilidades de ação.
Ir ao cinema ou estudar computação?
Dificilmente faríamos as duas coisas simultaneamente: ir ao cinema estudar computação.
Frequentemente há mais do que duas possibilidades, entretanto no momento nos concentraremos em apenas duas possibilidades.

Do ponto de vista de programação, em várias ocasiões é necessário executar blocos de forma mutuamente exclusiva.
A execução de um exclui a execução da outra e vice-versa.
Essa situação  corresponde a uma bifurcação no caminho do fluxo de execução do programa.
Exatamente igual a uma bifurcação em uma estrada.
Para aqueles e aquelas familiares com o mapa do estado de São Paulo esse bifurcação é semelhante a ir de São Paulo a Campinas pela Rodovia Anhanguera ou pela Rodovia Bandeirantes.
A origem e o destino são fixos, entretendo o caminho que nos leva de um ponto a outro é diferente.

O problema de classificação binária pode também ser entendido como um problema de execução "mutuamente exclusiva",
como ilustra o fluxograma e o código fonte na figura 4.2.

..  O fonte da figura está em:
    https://app.diagrams.net/#G1lKtCwQuhcSWHzf0vq__NtrbnakUUMLde


.. figure:: ../Figuras/cap04/alternativa_if_else.png
    :align: center
    :alt: Figura 4.2: Exemplo de alternativa com execução mutuamente exclusiva para classificar um aluno

    Figura 4.2: Fluxogramas e pseudocódigo do exemplo anterior mas com execução mutuamente exclusiva.

Na figura, o losango com a condição ``nota >= 5`` é um  ponto de bifurcação.
Se ``nota >= 5.0`` é ``False`` o fluxo segue pelo caminho da esquerda e ``situação`` passa a ser ``reprovada``, em caso contrário o fluxo segue o caminho da direita
é ``situação`` será ``aprovada``.

