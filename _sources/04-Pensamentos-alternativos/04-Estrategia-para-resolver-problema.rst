
..  Copyright (C) Carlos Hitoshi Morimoto e José Coelho de Pina Jr.
    É permitida a cópia, distribuição e/ou modificação desde documento
    sob os termos da Licença de Documentação Livre GNU (GNU Free 
    Documentation License) versão 1.3 ou versão subsequente
    publicada pela Fundação de Software Livre (Free Software 
    Foundation); com as seções invariantes sendo Prefácio e 
    Lista de Contribuidores, sem texto de capa ou contra-capa.  
    Uma cópia da licença está disponível em:
    https://www.gnu.org/licenses/licenses.pt-br.html#FDL


.. qnum::
	:prefix: cap04-04
	:start: 1



Estratégia para resolver um problema de classificação
.....................................................

Uma estratégia para resolver um problema binário de classificação, ou
seja, um problema que tem apenas duas classes, é criar uma **variável de estado** ou **indicadora**.
Essa variável indicará o subproblema que deverá ser tratado.
Inicialmente, a variável é deixada arbitrariamente em um dos possíveis estados.
Em seguida um teste é realizado para garantir que a variável está no estado correto ou
que o seu estado deverá ser alterado. 

Considerando o problema anterior de decidir se uma pessoa tem idade para
ter uma carteira de motorista, que é de 18 anos, podemos pensar da seguinte forma

#. Suponha que a pessoa tem menos de 18 anos 
#. Se a idade da pessoa for maior ou igual a 18:
    #. então agora sabemos que a pessoa tem menos de 18 

O `Se a idade...` é o ponto que, digamos, verificamos a carteira de identidade da pessoa.
Após seguir os passos acima saberemos se a pessoa em questão pode ou não tirar sua carta de motorista.

Vamos considerar um outro exemplo.
Suponha que uma aluno está aprovado em uma disciplina se sua nota é maior ou igual a 5.
Para decidirmos se o aluno está aprovado podemos seguir passos idênticos aos que fizemos acima. 
Consideraremos duas classes: aprovado e reprovado.
Construirmos uma solução supondo que o aluno está em uma classe, digamos na classe dos aprovados. 
Podemos indicar isso usando uma variável de estado ``aprovado`` e atribuir o valor booleano ``True``
como ``aprovado = True``. É evidente que essa nossa suposição inicial pode estar incorreta e,
para corrigir isso, podemos olhar o histórico escolar e testar
"se a nota é menor que 5" e, se for o caso, alteramos a condição do aluno para aprovado fazendo ``aprovado = False``.

Uma outra solução possível seria inicialmente supor que o aluno está reprovado, indicando
isso na nossa variável de estado fazendo ``aprovado = False``.
Essas duas alternativas de solução estão
ilustrados na figura 4.1, na forma de *fluxogramas* e também de *pseudocódigos*.
Apesar dessas duas  estratégias, políticas ou algoritmos serem idênticos, há situações em que a seleção arbitrária do estado inicial pode tornar a solução mais simples ou mais rebuscadas.

..  O fonte da figura está em:
    https://app.diagrams.net/#G1EDPqosSsuMViX4KcJVdpB-7F2ax-Sb__


.. figure:: ../Figuras/cap04/alternativa_simples.png
    :align: center
    :alt: Figura 4.1: alternativas para classificar um aluno

    Figura 4.1: Fluxogramas e pseudocódigo facilitam o pensamento.
    
Um **fluxograma** é uma forma gráfica para visualizarmos fluxo de execução em que os dados são processados.

Passemos a examinar mais atentamente fluxograma da esquerda.
O  "Início" indica o primeiro bloco de instruções a serem executados.
Nesse bloco a nota é lida e a variável de estado ``situação`` recebe um valor é inicial supondo
que o aluno está "aprovado". 

Depois deste bloco o processamento segue para o losango que contém
teste que será feito.  O resultado deste teste ou condição pode ser
verdadeiro (``True``) ou falso (``False``).  A partir do losango, o
fluxo de execução seguirá o caminho indicado pela seta com rótulo
correspondente ao resultado da condição.  Observe que há um *desvio*
do fluxo quando a condição ``nota < 5.0`` no losango é satisfeita e o
seu resultado é ``True``.  Nesse caso a classe do aluno indicada por
``situação`` será modificada de "aprovado" para "reprovado".  Após a
execução condicional desse bloco o processamento volta ao seu curso
normal, imprimindo o valor "aprovado" ou "reprovado" de ``situação``.
No caso da condição da condição ``nota < 5.0`` ser ``False`` o
processamento segue direto para o bloco que imprime o valor de
``situação``.

No fluxograma da direita mostra que alternativamente podemos
inicializar ``situação`` no estado "reprovado".  Nesse casso é
necessário também modificar a condição no losango para ``nota >= 5.0``
e alterar o estado de ``situação`` de "reprovado" para "aprovado" se
essa nova condição for ``True``.

Uma outra forma de pensar é usar **pseudocódigo**, como ilustrado na
parte inferior da figura 4.1.  Note a  tabulação da quarta linha no
pseudocódigo. Essa tabulação indica que a instrução da linha está subordinada ao
resultado da condição; ela só será executada se a condição for ``True``.

A diferença entre os dois pseudocódigos apresentados está em uma
*decisão de projeto*. No pseudocódigo da esquerda optamos por escrever
``situação = "aprovado"`` declarando assim que o estado inicial, ou
*default*, do aluno é "aprovado". No restante do código procuramos
certificar essa decisão inicial. Após verificar a nota, se necessário,
modificando o estado fazendo ``situação = "reprovado"``.
O pseudocódigo da direita corresponde ao estado default de "reprovado".

Fluxogramas e pseudocódigos são usados para escrevermos soluções ou
esboços de solução de uma forma mais solta sem nos preocuparmos
prematuramente com detalhes da linguagem de programação.
Isso permite que tenhamos mais liberdade para concentrar o nosso
pensamento no projeto da solução.  

A seguir veremos como transformar soluções na forma de fluxograma ou pseudocódigo para um
script na linguagem Python.

.. vale a pena sempre ficar trocando "classe" e "estado"?

