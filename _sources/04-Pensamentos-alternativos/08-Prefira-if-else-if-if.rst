
..  Copyright (C) Carlos Hitoshi Morimoto e José Coelho de Pina Jr.
    É permitida a cópia, distribuição e/ou modificação desde documento
    sob os termos da Licença de Documentação Livre GNU (GNU Free 
    Documentation License) versão 1.3 ou versão subsequente
    publicada pela Fundação de Software Livre (Free Software 
    Foundation); com as seções invariantes sendo Prefácio e 
    Lista de Contribuidores, sem texto de capa ou contra-capa.  
    Uma cópia da licença está disponível em:
    https://www.gnu.org/licenses/licenses.pt-br.html#FDL


.. qnum::
	:prefix: cap04-08
	:start: 1



Prefira ``if-else`` a ``if-if``
.................................

Muitos programadores novatos tendem a limitar o pensamento a casos específicos, ou seja, tentam tratar um caso de cada vez por meio de vários ``if``s. Esse padrão ``if-if`` é desaconselhável pois torna o programa mais difícil de ler, o que também dificulta corrigir e fazer a manutenção do código. Como exemplo, considere o seguinte trecho de código para determinar se um número é par ou ímpar:

.. activecode:: ac04_nao_use_if_if_se_puder_evitar
    :nocanvas:

    n = int(input("Digite um numero: ")

    if n % 2 == 0:  # se n é múltiplo de 2
        estado = "par"
    if n % 2 != 0:  # se n não é múltiplo de 2
        estado = "ímpar"

    print(f"o número {n} é {estado}")	

.. admonition:: **Como funciona esse trecho?**

    Após ``n`` receber um número, o programa testa, na linha *3*, se ``n`` é múltiplo de 2
    usando o operador aritmético ``%``. Esse operador determina o resto da divisão
    de ``n`` por ``2``. Caso a condição seja verdadeira (= ``True``),
    o programa armazena na variável ``estado`` que o número é par.

    Na linha *5* o programa recalcula o resto da divisão de ``n`` por
    2 e verifica se o resto é diferente de zero, ou seja, se o número não é
    múltiplo de 2 e, nesse caso, armazena na variável ``estado`` que o número é ímpar.

    Observe que a condição dos ``ifs`` nas linhas *3* e *5* foram executadas,
    mesmo quando na linha *3* o número já foi identificado como par, o teste da
    linha *5* é realizado.

