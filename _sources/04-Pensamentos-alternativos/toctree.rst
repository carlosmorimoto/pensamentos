Pensamentos alternativos
::::::::::::::::::::::::

.. toctree::
	:maxdepth: 1

	01-Introducao.rst
	02-Para-onde-vamos.rst
	03-Alternativa-simples.rst
	04-Estrategia-para-resolver-problema.rst
	05-Comando-if.rst
	06-Alternativas-mutuamente-exclusivas.rst
	07-Comando-if-else.rst
	08-Prefira-if-else-if-if.rst
	09-Pare-pense-instante.rst
	10-Alternativas-multiplas.rst
	11-Comando-if-elif-else.rst
	12-Exercicios.rst
	13-Laboratorio.rst
	14-Onde-chegamos-para-onde.rst

