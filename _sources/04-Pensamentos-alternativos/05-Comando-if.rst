
..  Copyright (C) Carlos Hitoshi Morimoto e José Coelho de Pina Jr.
    É permitida a cópia, distribuição e/ou modificação desde documento
    sob os termos da Licença de Documentação Livre GNU (GNU Free 
    Documentation License) versão 1.3 ou versão subsequente
    publicada pela Fundação de Software Livre (Free Software 
    Foundation); com as seções invariantes sendo Prefácio e 
    Lista de Contribuidores, sem texto de capa ou contra-capa.  
    Uma cópia da licença está disponível em:
    https://www.gnu.org/licenses/licenses.pt-br.html#FDL


.. qnum::
	:prefix: cap04-05
	:start: 1



Comando ``if``        
..............

O pseudocódigo ilustrado na figura 4.1 está em português, mas adota uma estrutura muito semelhante com Python.
Se trocarmos "leia a nota" pela função ``input()``, e traduzirmos a "se" para ``if`` estaremos muito próximos de um
trecho de código válido em Python, como o ``CodeLens`` abaixo pode atestar.
Execute esse programa passo-a-passo com valores diferentes de nota, como ``4.5``, ``5.0`` e ``6.2``,
para monitorar quando a variável ``situacao`` muda de valor.
Como nomes de variáveis nos códigos em Python não usaremos acentuação como em ``éãüàê`` ou símbolos gráfico como ``ç``.

.. activecode:: cl04_pseudo_se_para_if_python
                                            
    nota = float(input('Digite a nota: ')) # linha 1
    situacao = 'aprovado'                  # linha 2 
    if nota < 5.0:                         # linha 3  
        situacao = 'reprovado'             # linha 4
    print( situacao )                      # linha 5

.. admonition:: **Como funciona esse trecho?**

    Inicialmente, na ``linha 1``, o programa lê um valor e associa a variável ``nota`` a este valor.
    Assim, ``nota`` passa a ser uma referência ou apelido para o valor lido.
    Em seguida, na ``linha 2`` é criada a variável de estado ``situacao`` associada a um valor arbitrário com um valor apropriado, o programa testa se a nota é
    menor que 5.0 na ``linha 3``. Se o resultado da condição for
    ``True``, o programa altera ``situacao`` de ``'aprovado'`` para
    ``'reprovado'``.  Se o resultado da condição for ``False``, a variável
    ``situacao`` fica inalterada pois nossa hipotése inicial se confirmou.
   
    Veja que nesse exemplo a situação do aluno é definida por uma string ``'aprovado'`` ou ``'reprovado'``.
    Isso permite que o resultado seja impresso diretamente na ``linha 5``.
    Entretanto, a variável de estado poderia armazenar qualquer coisa para indicar a situação, como um valor booleano ``True`` ou ``False``. 

    Em português ``if` significa *se*. 
    O comando ``if`` permite que uma parte do programa seja executada apenas quando uma certa condição é ``True``.
    A sintaxe do comando ``ìf`` é a seguinte::

..   Sintaxe do comando if

.. code-block:: Python

    if condição_do_if:
        # bloco executado se a condição for verdadeira
        comando_1
        comando_2
        ...
        comando_m       # último comando do bloco
        
    comando_após_if

Apenas se ``condição_do_if`` for ``True``, o bloco contendo os comandos ``comando_1``  a ``comando_m`` é executado.
Em caso contrário, se ``condição_do_if`` for ``False``, esse bloco não é executado, ele é "pulado",  e a execução do programa continua com o
comando ``comando_após_if``. Observe com **muita atenção** os dois pontos ``:`` e a tabulação das linhas seguintes.
Essa tabulação é absolutamente fundamental para indicar indica o bloco ou lista de comandos que serão executados se ``condição_do_if`` é ``True``.
Veremos isto várias outras vezes, mas ajuda a nos habituarmos e notarmos que em  Python os dois pontos ``:``
indicam que estamos prestes a definir uma lista de comando que serão executados como uma unidade, um bloco.

 
.. admonition:: **Como testar o seu programa**

    Quanto mais complexo um algoritmo, mais fácil é esquecer     de tratar alguma classe.
    Uma forma de melhorar a qualidade e correção dos seus programas é **testá-los**.
    Testes não asseguram que um programa está correto, mas são úteis para encontrarmos erro.

    Para testar o seu programa, para cada categoria escolha valores representativos. 
    No caso do exemplo do exercício para verificar se um aluno está aprovado ou reprovado, não deixe de testar o programa com valores
    limites de cada categoria tais como 0.0, 4.9, 5.0, e 10.0. 

    Testar se a entrada de um programa é válida não é uma tarefa fácil.
    Consistência de dados é um exercício a parte.
    Para nos concentrarmos em um exercício por vez, **supermos que a entrada é sempre válida** a não ser que seja explicitamente pedido para que se teste consitência.
    Desta forma, no exercício das notas, você pode supor que os valores fornecidos ao programa são valores ``float`` entre ``0`` e ``10``.
    Portanto, não se preocupe se o valor fornecido ao programa é um ``float`` entre ``0`` e ``10`` ou se é uma string como `'a'` ou se é qualquer outra valor.
    

