
..  Copyright (C) Carlos Hitoshi Morimoto e José Coelho de Pina Jr.
    É permitida a cópia, distribuição e/ou modificação desde documento
    sob os termos da Licença de Documentação Livre GNU (GNU Free 
    Documentation License) versão 1.3 ou versão subsequente
    publicada pela Fundação de Software Livre (Free Software 
    Foundation); com as seções invariantes sendo Prefácio e 
    Lista de Contribuidores, sem texto de capa ou contra-capa.  
    Uma cópia da licença está disponível em:
    https://www.gnu.org/licenses/licenses.pt-br.html#FDL


.. qnum::
	:prefix: cap04-12
	:start: 1



Exercícios
----------

Nessa seção de exercícios, procure **desenhar** sua solução usando fluxogramas antes de escrever o código em Python. Desenhar a solução e conseguir ver um mapa do fluxo da informação é uma ferramenta poderosa tanto para aprendizes, para desenvolver a habilidade de identificar e organizar essas estruturas, quanto para programadores avançados trabalhando em problemas complexos.


Exercício 1
...........

Escreva um programa que leia dois números inteiros e determina a soma dos pares.
Por exemplo, para os números 6 e 7, 
como apenas o 6 é par, a soma deve ser 6. 
Para os número 1 e 3, o resultado deve ser zero e para 4 e 12, o resultado deve ser 16.

Dica: para resolver esse exercício utilize o operador "%", que retorna o resto da divisão. Assim::

.. Exemplos de operações com %

:: 

    1 % 3 é 1
    2 % 3 é 2 
    3 % 3 é 0
    4 % 3 é 1

.. activecode:: cap04_ex1_soma_pares_tentativa

    # Escreva o seu programa aqui


Exercício 2
...........

Escreva um programa que leia 3 números inteiros e determinar quantos números da sequência são pares e
quantos são ímpares.  Por exemplo, para os números
1, 2 e 3, o resultado deve ser 1 par e 2 ímpares.

.. activecode:: cap04__ex2_conta_pares_e_impares

    # Escreva o seu programa aqui



Exercício 3
...........

Escreva um programa que leia três números naturais e verifica se eles formam os lados de um triângulo retângulo. **DICA** Os números não precisam estar ordenados. Por exemplo, para os número 3, 5 e 4, a resposta deve ser sim, e para 8, 1, 6 a resposta deve ser não. 

.. activecode:: cap04_ex3_lados_triangulo_retangulo

    # Escreva o seu programa aqui


Exercício 4
...........

Escreva um programa que leia três números inteiros
e os imprima em ordem crescente.
Por exemplo, para os número 13, 7 e 4, a resposta deve ser 4, 7 e 13 e para 8, 9, 10 a resposta deve ser 8, 9 e 10.

.. activecode:: cap04_ex4_ordena3

    # Escreva o seu programa aqui

