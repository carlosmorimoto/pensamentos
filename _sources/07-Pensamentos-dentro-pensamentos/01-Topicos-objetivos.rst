
..  Copyright (C) Carlos Hitoshi Morimoto e José Coelho de Pina Jr.
    É permitida a cópia, distribuição e/ou modificação desde documento
    sob os termos da Licença de Documentação Livre GNU (GNU Free 
    Documentation License) versão 1.3 ou versão subsequente
    publicada pela Fundação de Software Livre (Free Software 
    Foundation); com as seções invariantes sendo Prefácio e 
    Lista de Contribuidores, sem texto de capa ou contra-capa.  
    Uma cópia da licença está disponível em:
    https://www.gnu.org/licenses/licenses.pt-br.html#FDL


.. qnum::
	:prefix: cap07-01
	:start: 1

. -*- coding: utf-8 -*-

..  shortname:: Repetições encaixadas
..  description:: Repetições encaixadas

Repetições encaixadas
=====================


Tópicos e objetivos
-------------------

    - identificar situações onde é necessário utilizar repetições encaixadas;
    - utilizar repetições encaixadas em seus programas;
    - utilizar indicadores de passagem em repetições encaixadas;
    - simular programas com repetições encaixadas.


