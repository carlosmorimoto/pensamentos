
..  Copyright (C) Carlos Hitoshi Morimoto e José Coelho de Pina Jr.
    É permitida a cópia, distribuição e/ou modificação desde documento
    sob os termos da Licença de Documentação Livre GNU (GNU Free 
    Documentation License) versão 1.3 ou versão subsequente
    publicada pela Fundação de Software Livre (Free Software 
    Foundation); com as seções invariantes sendo Prefácio e 
    Lista de Contribuidores, sem texto de capa ou contra-capa.  
    Uma cópia da licença está disponível em:
    https://www.gnu.org/licenses/licenses.pt-br.html#FDL


.. qnum::
	:prefix: cap07-05
	:start: 1



Para saber mais
---------------

* Vídeos

    - `Depuração
      <https://www.youtube.com/watch?v=Aa8-39qwbUU&list=PLcoJJSvnDgcKpOi_UeneTNTIVOigRQwcn&index=14>`__;
    - `Repetições encaixadas <https://www.youtube.com/watch?v=dRCDOH1lugs&list=PLcoJJSvnDgcKpOi_UeneTNTIVOigRQwcn&index=23>`__;
    - `Repetições encaixadas - exercício fatoriais
      <https://www.youtube.com/watch?v=dRCDOH1lugs&index=21&list=PLcoJJSvnDgcKpOi_UeneTNTIVOigRQwcn>`__;
    - `Repetições encaixadas - exercício fatoração <https://www.youtube.com/watch?v=9AUu2xjOACI&index=25&list=PLcoJJSvnDgcKpOi_UeneTNTIVOigRQwcn>`__.  
