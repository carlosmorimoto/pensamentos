
..  Copyright (C) Carlos Hitoshi Morimoto e José Coelho de Pina Jr.
    É permitida a cópia, distribuição e/ou modificação desde documento
    sob os termos da Licença de Documentação Livre GNU (GNU Free 
    Documentation License) versão 1.3 ou versão subsequente
    publicada pela Fundação de Software Livre (Free Software 
    Foundation); com as seções invariantes sendo Prefácio e 
    Lista de Contribuidores, sem texto de capa ou contra-capa.  
    Uma cópia da licença está disponível em:
    https://www.gnu.org/licenses/licenses.pt-br.html#FDL


.. qnum::
	:prefix: cap07-03
	:start: 1




Problema de motivação
---------------------

Considere a função :math:`y + x y - x^2`.

Uma forma computacional para estimar o máximo dessa função dentro de um intervalo é testar vários pares (x,y), "procurando" pela solução. 
A ideia é escrever um programa que gera todos os pares (x,y) para encontrar o par que corresponde ao máximo da função.

Exemplo
........

Vamos considerar apenas os números inteiros no intervalo 0 <= x <= 2
e 0 <= y <= 3.
Para gerar todos os pares (x,y) nesse intervalo devemos varrer todos os valores de x e, para cada valor de x, varrer todos os valores de y, algo como:
       # varrer todos os valores de x
    x = 0
    while x <= 2:
        print('para x igual a', x)

        # varrer os valores de y, para cada valor de x
        y = 0
        while y <= 3:
            print('    (', x, ',', y, ')'  )
            y = y + 1

        x = x + 1

Execute esse código no *activecode*. A saída é mostrada abaixo:

.. code-block:: Python

    para x igual a 0
        ( 0 , 0 )
        ( 0 , 1 )
        ( 0 , 2 )
        ( 0 , 3 )
    para x igual a 1
        ( 1 , 0 )
        ( 1 , 1 )
        ( 1 , 2 )
        ( 1 , 3 )
    para x igual a 2
        ( 2 , 0 )
        ( 2 , 1 )
        ( 2 , 2 )
        ( 2 , 3 )

Estude o código acima e verifique que o bloco de comandos que controla a 
varredura dos valores de x é muito parecido com o de y, como no trecho a seguir.

.. code-block:: Python

    # comandos que controlam a varredura dos valores de x
    x_inicial = 0
    x_final   = 2

    x = x_inicial
    while x <= x_final:

        x = x + 1

Calculando o ponto de máximo
~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Agora que sabemos varrer todos os pares (x,y), para determinar o ponto de máximo podemos usar a seguinte ideia:

    * No início, não sabemos qual é o ponto de máximo. 
        - Assim, vamos assumir que o máximo é um ponto qualquer
        - como o ponto (0,0)
    * O valor máximo é iniciado com o valor da função nesse ponto inicial
    * Varremos agora todos os pontos, um de cada vez 
        - calculamos o valor da função nesse ponto (x,y)
        - se o valor no ponto for maior que o valor visto até agora
            - atualizamos o valor com esse novo valor de máximo 

Em pseudo código, mais perto de Python, podemos escrever:

.. code-block:: Python

    # Vamos usar o ponto (0,0) como ponto máximo inicial
    x_max = 0
    y_max = 0
    v_max = valor da função no ponto inicial (x_max, y_max)

    para cada novo par (x,y) no intervalo:
        valor = valor da função em (x,y)
        se valor > máximo:
            v_max = valor 
            x_max = x
            y_max = y

    imprima o valor v_max e as coordenadas x_max e y_max

Lembre-se agora que, para varrer os pontos, usamos a repetição encaixada como vimos anteriormente e obtemos o seguinte código em Python:

.. activecode:: exemplo_estima_ponto_de_maximo_final

    # Inicialização: intervalo da varredura
    x_ini = 0
    x_fim = 2

    y_ini = 0
    y_fim = 3

    # Inicialização: cálculo do ponto de máximo
    x_max = 0
    y_max = 0
    v_max = y_ini + x_ini * y_ini - x_ini * x_ini

    # Varredura dos pontos no intervalo 
    x = x_ini
    while x <= x_fim:
        # varrer os valores de y, para cada valor de x
        y = y_ini
        while y <= y_fim:
            valor = y + x*y - x*x
            if valor > v_max:
                v_max = valor
                x_max = x
                y_max = y

            y = y + 1
        x = x + 1

    print('Valor máximo: ', v_max)
    print('Obtido no ponto: (', x_max,  ',', y_max, ')')

      
