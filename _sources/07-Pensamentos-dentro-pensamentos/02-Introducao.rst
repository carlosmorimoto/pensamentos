
..  Copyright (C) Carlos Hitoshi Morimoto e José Coelho de Pina Jr.
    É permitida a cópia, distribuição e/ou modificação desde documento
    sob os termos da Licença de Documentação Livre GNU (GNU Free 
    Documentation License) versão 1.3 ou versão subsequente
    publicada pela Fundação de Software Livre (Free Software 
    Foundation); com as seções invariantes sendo Prefácio e 
    Lista de Contribuidores, sem texto de capa ou contra-capa.  
    Uma cópia da licença está disponível em:
    https://www.gnu.org/licenses/licenses.pt-br.html#FDL


.. qnum::
	:prefix: cap07-02
	:start: 1



Introdução
----------

Uma característica importante dos computadores é executar ações repetitivas inúmeras vezes sem se cansar, sempre com a mesma precisão e desempenho.
Ações complexas precisam ser decompostas para que possam ser mais facilmente descritas na forma de um algoritmo. A habilidade de resolver problemas computacionais depende muito da habilidade de decompor problemas em problemas mais simples e, uma vez que as soluções dos problemas mais simples forem desenvolvidas, precisamos compor essas soluções para resolver os problemas mais complexos. 

Tanto os comandos de execução condicional quanto os de repetição podem ser "encaixados", ou seja, um comando de repetição pode ser colocado dentro de outra repetição, como por exemplo:

.. code-block:: Python

    while condição_1:
        # bloco do while 1
        
        while condição_2:
            # bloco do while 2
             
            while condição_3:
                # bloco do while 3                
                
            # instruções após fim do while condição_3
        # instruções após fim do while condição_2
    # instruções após fim do while condição_1


