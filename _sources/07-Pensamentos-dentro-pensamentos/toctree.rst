Pensamentos dentro de pensamentos
:::::::::::::::::::::::::::::::::

.. toctree::
	:maxdepth: 1

	01-Topicos-objetivos.rst
	02-Introducao.rst
	03-Problema-motivacao.rst
	04-Exercicios.rst
	05-Para-saber-mais.rst

