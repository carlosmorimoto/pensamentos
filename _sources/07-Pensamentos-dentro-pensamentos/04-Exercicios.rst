
..  Copyright (C) Carlos Hitoshi Morimoto e José Coelho de Pina Jr.
    É permitida a cópia, distribuição e/ou modificação desde documento
    sob os termos da Licença de Documentação Livre GNU (GNU Free 
    Documentation License) versão 1.3 ou versão subsequente
    publicada pela Fundação de Software Livre (Free Software 
    Foundation); com as seções invariantes sendo Prefácio e 
    Lista de Contribuidores, sem texto de capa ou contra-capa.  
    Uma cópia da licença está disponível em:
    https://www.gnu.org/licenses/licenses.pt-br.html#FDL


.. qnum::
	:prefix: cap07-04
	:start: 1



Exercícios
----------

Exercício 1
...........

    Exercício 6 da `lista sobre repetições encaixadas <http://www.ime.usp.br/~macmulti/exercicios/repetenc/index.html>`__.

Dado um número inteiro ``n``, ``n > 1``, imprimir sua decomposição 
em fatores primos, indicando também a mutiplicidade de cada fator.
   

    fator 2 multiplicidade 3
    fator 3 multiplicidade 1
    fator 5 multiplicidade 2


.. activecode:: aula_repeticoes_ex01_tentativa

    # escreva o seu programa abaixo e remova o print()
    print("Vixe! Ainda nao fiz este exercício!")
                

Clique `aqui <exercicios/repeticao01.html>`__ para ver uma solução.


Exercício 2
...........

    Exercício 7 da `lista sobre repetições encaixadas <http://www.ime.usp.br/~macmulti/exercicios/repetenc/index.html>`__. 

Dados um número inteiro ``n > 0`` e uma sequência com
``n`` números inteiros maiores do que zero,
determinar o máximo divisor comum entre eles.

Por exemplo, para a sequência 

.. sourcecode:: python

    3    42    105      

o seu programa deve escrever o número ``3``.


.. activecode:: aula_repeticoes_ex02_tentativa

    # escreva o seu programa abaixo e remova o print()
    print("Vixe! Ainda nao fiz este exercício!")
                
        
Clique `aqui <exercicios/repeticao02.html>`__ para ver uma solução.


Exercício 3
...........

Dados um número inteiro ``n``, ``n > 0``, e uma sequência com
``n`` números inteiros maiores do que zero,
determinar o fatorial de cada número da sequência.
   

.. activecode:: aula_repeticoes_ex03_tentativa

    # escreva o seu programa abaixo e remova o print()
    print("Vixe! Ainda nao fiz este exercício!")
                
    
Clique `aqui <exercicios/repeticao03.html>`__ para ver uma solução.


Exercício 4
...........

Dada uma sequência de números inteiros maiores que um, terminada por
um zero, determinar quantos números primos há na sequência.

.. activecode:: aula_repeticao__ex04_tentativa

    # escreva o seu programa abaixo e remova o print()
    print("Vixe! Ainda nao fiz este exercício!")
                
                
Clique `aqui <exercicios/repeticao04.html>`__ para ver uma solução.


Exercício 5
...........

    Exercício 5 da `lista sobre repetições encaixadas <http://www.ime.usp.br/~macmulti/exercicios/repetenc/index.html">`__. 
    
Sabe-se que cada número da forma ``n**3`` é igual a soma de ``n``
ímpares consecutivos.
Por exemplo,

.. sourcecode:: Python

    1**3 = 2
    2**3 = 3 + 5
    3**3 = 7 + 9 + 11
    4**3 = 13 + 15 + 17 + 19

Dado um número inteiro ``m``, ``m > 0``, determinar os ímpares
consecutivos cuja soma é igual a ``n**3``, para n
assumindo valores de 1 a ``m``.
 
.. activecode:: aula_repeticoes_ex05_tentativa
		
    # escreva o seu programa abaixo e remova o print()
    print("Vixe! Ainda nao fiz este exercício!")
                
    
