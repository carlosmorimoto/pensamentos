
..  Copyright (C) Carlos Hitoshi Morimoto e José Coelho de Pina Jr.
    É permitida a cópia, distribuição e/ou modificação desde documento
    sob os termos da Licença de Documentação Livre GNU (GNU Free 
    Documentation License) versão 1.3 ou versão subsequente
    publicada pela Fundação de Software Livre (Free Software 
    Foundation); com as seções invariantes sendo Prefácio e 
    Lista de Contribuidores, sem texto de capa ou contra-capa.  
    Uma cópia da licença está disponível em:
    https://www.gnu.org/licenses/licenses.pt-br.html#FDL


.. qnum::
	:prefix: cap03-05
	:start: 1



Tipos de dados
--------------

.. index:: int, float, bool

Antes de pensar na solução é necessário ter um profundo entendimento do problema. 

Um passo importante para o entendimento do problema é a **especificação** dos tipos de 
dados usados na comunicação, ou seja, na entrada e na saída dos dados de seus programas.

Já conhecemos 3 tipos usados no Python para trabalhar com expressões aritméticas e lógicas:

* ``int``: para números inteiros, 
* ``float``: para números reais e 
* ``bool``: para os valores booleanos ``True`` e ``False``).

Em nosso dia-a-dia, muitas vezes, não nos preocupamos com o tipo de um resultado e costumamos informar apenas um valor. Por exemplo, considere as seguintes perguntas:

* quantos anos você tem?
* qual a sua altura?
* você já almoçou?

Imagine agora que você é o computador, que só sabe se comunicar em código binário, e recebe algo como "00010000". O que você, computador, faz com isso? 

Para saber o que fazer, precisamos saber que tipo de dado esses bits representam para conseguir decodificá-los. 
Por exemplo, esses bits podem ser usados para representar um inteiro ``16``, ou um real ``1.6`` ou ainda um booleano ``True``. 


.. admonition:: Importância da especificação

    Você pode encontrar certa dificultade para definir o tipo de algum dado, como "idade", pois algumas pessoas preferem responder 16.5 ao invés de 16 anos, por exemplo. Para simplificar nosso raciocínio e reduzir o trabalho de codificação, vamos assumir nesse curso que todos os dados de entrada e de saída possuem um tipo específico e que não deve ser alterado.

    Assim, se a especificação define que o programa deve devolver um inteiro (com o valor da idade), mas você resolver modificar o tipo para um real, outros programas que usam esse resultado podem não mais funcionar pois não vão "entender" a resposta do seu programa. 
    

