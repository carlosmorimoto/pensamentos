
..  Copyright (C) Carlos Hitoshi Morimoto e José Coelho de Pina Jr.
    É permitida a cópia, distribuição e/ou modificação desde documento
    sob os termos da Licença de Documentação Livre GNU (GNU Free 
    Documentation License) versão 1.3 ou versão subsequente
    publicada pela Fundação de Software Livre (Free Software 
    Foundation); com as seções invariantes sendo Prefácio e 
    Lista de Contribuidores, sem texto de capa ou contra-capa.  
    Uma cópia da licença está disponível em:
    https://www.gnu.org/licenses/licenses.pt-br.html#FDL


.. qnum::
	:prefix: cap03-11
	:start: 1



Laboratório: Spyder
--------------------

.. index:: IDE (Integrated Development Environment), Spyder

O Spyder `<https://www.spyder-ide.org/>`__ 
é um ambiente integrado de desenvolvimento de programas em Python que vem junto com o Anaconda. 
Apesar da interface do Spyder parecer um tanto complexa por reunir muitas ferramentas, não se assuste. Uma vez que você entenda o funcionamento e o propósito de cada uma, elas lhe serão muito úteis no desenvolvimento dos seus programas pois permitem que você teste cada ideia e elimine suas dúvidas rapidamente. 

Caso você não tenha ainda instalado Python em seu computador siga as instruções fornecidas no laboratório do capítulo 1. Quando terminar de instalar, inicie o Spyder. 

Para abrir o Spyder, digite "spyder" na área de busca do Windows (como você fez anteriormente para abrir o Anaconda Prompt). 
A figura 3.2 mostra a tela inicial do Spyder. 
Ela deve aparecer com essas 3 partes (que vamos chamar de janelas). 
Caso você não esteja vendo alguma dessas partes, clique na opção
"Tools" da barra de menu (topo da janela) e depois em 
"Reset Spyder to factory defaults" para que o Spyder volte a ter essa
configuração.

.. figure:: ../Figuras/spyder/spyder01.png
    :alt: Figura 3.2: Tela inicial do Spyder.
    :align: center

    Figura 3.2: Tela inicial do Spyder.

A janela na parte superior direita dessa configuração inicial tem 3 "tabs":
"Variable explorer", "File explorer" e "Help". Selecione (clique) o tab "Variable explorer" para que o Spyder exiba os valores das variáveis que você utilizar (como na figura). O tab "Help" possui informações sobre o Spyder e o tab "File explorer" ajuda você a achar arquivos na sua máquina.

A janela na parte inferior direita tem 2 tabs. Selecione o tab "IPython console". O IPython console (que vamos chamar simplesmente de IPython) é a mesma ferramenta que usamos no laboratório do capítulo anterior, mas integrada ao Spyder. 


Usando o editor do Spyder para escrever programas em Python
...........................................................

Vamos agora escrever um programa simples utilizando o Spyder. 
O programa deve ler dois inteiros ``a`` e ``b`` e imprimir sua soma.

Uma primeira tentativa pode ser a seguinte:

.. code-block:: Python

    a = input("Digite o valor de a: ")
    b = input("Digite o valor de b: ")
        soma = a + b
        print("A soma de a + b é: ", soma)

Copie esse trecho de código no editor do Spyder e **salve** o programa.

Para salvar o programa em um arquivo no computador que você estiver usando, clique na opção "File -> Save as" na barra de menu do Spyder. O nome precisa terminar com a extensão ".py" para indicar que é um arquivo em Python, como "primeiro_prog.py". 

Depois de salvo, execute o programa utilizando a opção "Run -> Run" do menu (ou use a tecla de atalho 'F5'). O Spyder oferece vários "atalhos" (combinações de teclas) que você pode aprender para agilizar o seu trabalho. Essas combinações de teclas existentes são mostradas no canto direito de cada opção do menu. 

O programa é executado na janela do IPython.
Se você digitou o programa exatamente como ilustrado, o Python deve indicar um erro de tabulação (*IndentationError*) no console ou shell. Isso porque a coluna onde cada linha inicia indica ao Python a que bloco a linha pertence. Por isso, a primeira linha de código **precisa** começar na primeira coluna do editor (ou seja, sem nenhum espaço no início da linha). 

Para corrigir esse problema, remova todos os espaços iniciais de todas as linhas como:

.. code-block:: Python

    a = input("Digite o valor de a: ")
    b = input("Digite o valor de b: ")
    soma = a + b
    print("A soma de a + b é: ", soma)

salve e execute novamente. 

O Python não deve indicar nenhum erro dessa vez. Se para ``a`` digitarmos o 3 e para ``b`` digitarmos 4, a saída do programa nesse caso é '34'. 

Isso porque "esquecemos" que a função ``input()`` devolve uma string, que precisa ser convertida para um inteiro. Isso pode ser corrigido convertendo-se cada string para inteiro da seguinte maneira:

.. code-block:: Python
    :linenos:

    a_str = input("Digite o valor de a: ")
    a_int = int(a_str)
    b = int(input("Digite o valor de b: "))
    soma = a_int + b
    print("A soma de a + b é: ", soma)

Nesse programa, ``a_str`` recebe uma string que é convertida para um valor inteiro pela função ``int()`` e esse valor inteiro é atribuído para a variável ``a_int``. 
A linha 3 mostra que essas funções podem ser compostas para produzir um código mais compacto mas ainda legível. 

Salve e execute esse programa e verifique se ele se comporta como esperado.


Usando o iPython dentro do Spyder
.................................

Observe a sequência de comandos digitados na janela do iPython na figura 3.3 e
que mostra as janelas "Variable explorer" e "IPython console" do Spyder (as duas janelas do lado direito) após a execução dos comandos. 
Procure digitar e executar a mesma sequência no iPython do Spyder em seu computador.

.. figure::  ../Figuras/spyder/console01.png
    :alt: Figura 3.3: Janelas "Variable explorer" e "IPython console" do Spyder.
    :align: center

    
Quando o Python é iniciado nenhuma variável está definida. Assim ao digitarmos a palavra (nome da variável) ``pi``, o Python devolve o erro ``NameError`` indicando que o nome ``pi`` ainda não foi definido. 

O comando de atribuição digitado na linha [2] faz com que a variável com nome ``pi`` seja criada e associada ao valor da expressão do lado direito do comando, nesse caso, ``3.14``. 
Note que na janela "Variable explorer" a variável ``pi`` é criada com esse valor. 

Observe que o comando de atribuição na linha [2] não gera uma saída, ou linha com ``Out[2]``. Lembre-se que o shell interpreta e executa comandos. No caso de uma atribuição, uma variável é criada ou atualizada (modificada). Sem o comando de atribuição ``=``, o Python tenta entender o nome como alguma variável conhecida e resolver uma expressão usando o seu valor associado. 
Observe que, ao digitarmos novamente o nome ``pi`` como na linha [3], o IPython reconhece o nome e resolve a expressão substituindo o nome pelo valor associado (armazenado na memória como mostrado na janela Variable explorer). 

A linha [4] é semelhante à linha [2] e cria uma nova variável de nome ``novo_pi``. Como a expressão do lado direito do símbolo ``=`` contém apenas a variável ``pi``, o **valor** referenciado por ``pi`` é atribuído à ``novo_pi`` (veja o que acontece na janela "Variable explorer" após a execução de cada comando de atribuição). 

A linha [5] mostra como uma variável pode ser atualizada. Primeiro a expressão à direita é calculado usando o valor atual de ``pi`` e o novo valor (6.28) é atribuído a própria variável. Observe que a mesma variável ``pi`` tem um valor associado **antes** da atribuição, quando a expressão é calculada, e um valor diferente **após** a execução do comando.

A linha [6] mostra o novo valor de ``pi`` (ou calcula o valor da expressão, que nesse caso é o valor associado a ``pi``) e a linha [7] mostra  que a variável ``novo_pi`` **não é alterada**, mesmo tendo recebido o valor associado a ``pi`` na linha [4].
Ou seja, cada variável faz referência ao último valor que lhe é atribuído e, portanto, os valores antigos são perdidos a cada nova atribuição. 
            
