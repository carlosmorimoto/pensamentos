Pensamentos e comunicação
:::::::::::::::::::::::::

.. toctree::
	:maxdepth: 1

	01-Introducao.rst
	02-Para-onde-vamos.rst
	03-Comunicacao-interacao.rst
	04-Variaveis-comando-atribuicao.rst
	05-Tipos-dados.rst
	06-Tipos-valores.rst
	07-Tipo-string.rst
	08-Conversao-entre-tipos.rst
	09-Funcoes-para-se-comunicar.rst
	10-Exercicio-comentado.rst
	11-Laboratorio.rst
	12-Teste-seu-conhecimento.rst
	13-Onde-chegamos-para-onde.rst

