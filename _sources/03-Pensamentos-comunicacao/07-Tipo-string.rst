
..  Copyright (C) Carlos Hitoshi Morimoto e José Coelho de Pina Jr.
    É permitida a cópia, distribuição e/ou modificação desde documento
    sob os termos da Licença de Documentação Livre GNU (GNU Free 
    Documentation License) versão 1.3 ou versão subsequente
    publicada pela Fundação de Software Livre (Free Software 
    Foundation); com as seções invariantes sendo Prefácio e 
    Lista de Contribuidores, sem texto de capa ou contra-capa.  
    Uma cópia da licença está disponível em:
    https://www.gnu.org/licenses/licenses.pt-br.html#FDL


.. qnum::
	:prefix: cap03-07
	:start: 1



Tipo string (str)
-----------------

Um outro tipo que vamos utilizar bastante é chamado de "string" e é abreviado por ``str``. Na verdade já usamos esse tipo quando introduzimos a função ``print()``, vamos agora entender melhor o seu funcionamento.

Uma string é uma sequência de 0 ou mais caracteres que representam um texto. Para indicar uma string no Python, devemos escrevê-lo entre apóstrofes (') ou aspas ("). Essa dualidade facilita a inclusão do caractere apóstrofe ou aspas dentro de uma string, como ilustra o trecho de código abaixo. Execute esse programa passo-a-passo (clicando em ``Next``) e verifique o resultado de cada comando de atribuição. 

.. codelens:: cl03_exemplo_strings

    carmem = "Carmem Miranda"
    print( carmem )
    lata = "d'água"
    print( lata )
    "d'água"
    seq = ' 1, "dois", 3'
    print( seq )
    print( type(seq) )


Concatenação de strings
.......................

É possível **concatenar** duas strings usando o operador ``+``. Por exemplo, execute passo-a-passo o trecho de programa abaixo e veja o que ocorre com as variáveis e na saída do programa. Como sempre, procure pensar no resultado antes de clicar ``Next``. 

.. codelens:: cl03_exemplo_concatenacao

    p = "Pedro"
    a = "Álvares"
    c = "Cabral"
    pa = p + a
    print( pa, p, a )
    nome = p + ' ' + a + c
    print( nome )

Observe que os caracteres em branco são parte da string.
Assim, a concatenação de ``p + a`` resulta em 'PedroÁlvares' pois 
nem ``p`` nem ``a`` foram definidas com espaço. 
Observe que na expressão (com strings!) que cria a variável ``nome`` apenas um espaço
foi definido entre os dois primeiros nomes, mas não há espaço entre os
dois últimos nomes.

.. admonition:: Concatenação ou Soma?

    O operador ``+`` é utilizado tanto para concatenar strings quando os dois operandos são do tipo ``str`` como para somar quando os dois operadores são números. No entanto, quando um operador é string e o outro for um número, o Python indica um erro como mostrado em uma sessão do iPython abaixo. 

    .. code-block:: Python

        In [4]: 'Pedro' + 3
        ---------------------------------------------------------------------------
        TypeError                                 Traceback (most recent call last)
        <ipython-input-1-cb21c6a527de> in <module>
        ----> 1 'Pedro' + 3
        
        TypeError: can only concatenate str (not "int") to str
        
        In [5]: 'Pedro ' * 3
        Out[5]: 'Pedro Pedro Pedro'

    No entanto, observe na linha de comando "In [5]" que é possível realizar múltiplas concateções usando o operador ``*``. 

    Execute o código abaixo e note que a mensagem de erro pode ser diferente do iPython. Corrija o erro substituindo o operador ``+`` pelo operador ``*`` e execute novamente para ver o resultado.

    .. activecode:: ac03_teste_concatenacao_multipla_de_strings

        soma_3 = 'Pedro' + 3
        mult_3 = 'Pedro ' * 3 


