
..  Copyright (C) Carlos Hitoshi Morimoto e José Coelho de Pina Jr.
    É permitida a cópia, distribuição e/ou modificação desde documento
    sob os termos da Licença de Documentação Livre GNU (GNU Free 
    Documentation License) versão 1.3 ou versão subsequente
    publicada pela Fundação de Software Livre (Free Software 
    Foundation); com as seções invariantes sendo Prefácio e 
    Lista de Contribuidores, sem texto de capa ou contra-capa.  
    Uma cópia da licença está disponível em:
    https://www.gnu.org/licenses/licenses.pt-br.html#FDL


.. qnum::
	:prefix: cap03-01
	:start: 1


.. Variáveis, tipos e funções de entrada e saída 

.. Pensando nos Dados: entra, salva, transforma e sai

.. Pensamento e comunicação

Introdução
----------

.. index:: valor, tipo de dados, string, str, str()

.. index:: inteiro, int, int(), real, float, float()

.. index:: input(), print()

.. raw:: html

    <div align="right">
    <p></p>
    <blockquote>
    <p>
    "
    O maior problema com a comunicação é a ilusão</br>
    de que ela foi alcançada.
    "
    </p>
    <p>William H. Whyte</p>
    </blockquote>
    <p></p>
    </div>

Programas precisam se comunicar, ou seja, trocar informações com outros programas ou com pessoas. 
Nesse capítulo vamos ver como os programas recebem e transmitem informações para pessoas.
Vamos estender o conceito de computador como calculadora que cobrimos no capítulo anterior
para computador como **processador de dados**, ou seja, que recebe dados, transforma 
os dados em outros valores e comunica o resultado de alguma maneira.

Nos exemplos interativos desse livro vamos usar as ferramentas CodeLens e ActiveCode
que permitem a troca de mensagens entre o programa e o usuário dentro do próprio navegador.

No seu computador, essa troca de mensagens ocorre em um terminal executando iPython. 
No exercício de laboratório desse capítulo 
você vai ver também como trabalhar com arquivos fonte em Python usando o Spyder.

