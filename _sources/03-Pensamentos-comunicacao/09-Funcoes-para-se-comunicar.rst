
..  Copyright (C) Carlos Hitoshi Morimoto e José Coelho de Pina Jr.
    É permitida a cópia, distribuição e/ou modificação desde documento
    sob os termos da Licença de Documentação Livre GNU (GNU Free 
    Documentation License) versão 1.3 ou versão subsequente
    publicada pela Fundação de Software Livre (Free Software 
    Foundation); com as seções invariantes sendo Prefácio e 
    Lista de Contribuidores, sem texto de capa ou contra-capa.  
    Uma cópia da licença está disponível em:
    https://www.gnu.org/licenses/licenses.pt-br.html#FDL


.. qnum::
	:prefix: cap03-09
	:start: 1



Funções para se comunicar com o usuário
---------------------------------------

Lembre-se que a comunicação do programa com o usuário no terminal deve ser feita usando 
funções de entrada e saída, que permitem que um programa receba uma mensagem do usuário (escrita no terminal) e imprima mensagens de texto (no terminal).
 
Nesse curso, como os programas serão executados no iPython (ou Python Shell), vamos utilizar a função ``input()`` para que o seu programa receba um texto digitado pelo usuário (no terminal) e vamos utilizar a função ``print()`` para que um programa possa imprimir um texto (no terminal). 


Função input()
..............

Para usar a função ``input()`` devemos fazer o seguinte.

.. code-block:: Python

    var = input( mensagem )

A função ``input()`` recebe uma mensagem (uma string) que é exibida no terminal. A função dessa mensagem é explicar ao usuário o que digitar. Ao terminar de digitar, o usuário deve teclar ENTER para enviar a string digitada ao programa. O programa atribui essa string à variável ``var``. 


Função print()
..............

Para usar a função ``print()`` devemos fazer o seguinte.

.. code-block:: Python

    print( lista_de_valores_separada_por_vírgulas )
    
A função ``print()`` recebe uma lista de valores separada por vírgulas e imprime cada valor (transforma o valor para uma string e imprime a string), adicionando um espaço em branco entre duas strings e pulando de linha ao final.

O trecho de código no ActiveCode abaixo mostra como essas funções podem ser usadas. Edite esse código para fazer outras perguntas e respostas. 

.. activecode:: ac03_ola_mundo

    nome = input('Qual o seu nome? ')
    print("olá", nome, "!")
    print()   # um print() sem nada apenas pula uma linha
    print('O tipo da variável nome é:', type(nome))



Observe que a função ``print()`` procura facilitar a impressão e torná-la mais legível, por exemplo, inserindo um espaço automaticamente entre dois valores da lista e pulando para a próxima linha ao final da impressão,
para que cada ``print()`` apareça em uma linha. Um ``print()`` sem nada entre parênteses apenas pula uma linha.

Esse comportamento pode ser modificado definindo alguns parâmetros extras, como ``sep`` e ``end``. 

    * O parâmetro ``sep`` pode ser usado para alterar a string inserida automaticamente entre dois valores consecutivos da lista. O valor padrão é um único espaço em brando. Exemplos:

        .. code-block:: Python

            >>> print(2, 3, 4, sep=' ')   # com um espaço é o comportamento padrão:
            2 3 4
            >>> print(2, 3, 4, sep='')    # com uma string vazia, sem espaço, resulta em:
            234
            >>> print(2, 3, 4, sep='-*-') # com a string '-*-' resulta em:
            2-*-3-*-4

    * O parâmetro ``end`` pode ser usado para alterar a string inserida automaticamente ao final da lista de valores. O valor default é o caractere especial que pula uma linha (representado por ''\n''). Observe os seguintes exemplos (clique em ``Run`` caso você estiver lendo esse texto online usando um navegador).

        .. activecode:: exemplo_print_end_default

            print('primeira', 'linha', end='\n')   # comportamento padrão
            print('segunda', 'linha', end='\n')

        A saída desse trecho de código é 

            primeira linha
            segunda linha

        .. activecode:: exemplo_print_end_pula_duas

            print('primeira', 'linha', end='\n\n')   # pula duas linhas
            print('segunda', 'linha')

        A saída desse trecho de código é 

            primeira linha

            segunda linha
        
        .. activecode:: exemplo_print_end_nao_pula

            print('primeira', 'linha', end=' <<>> ')   # string sem pula linha
            print('segunda', 'linha')

        A saída desse trecho de código é 

            primeira linha <<>> segunda linha


    * Os parâmetros ``sep`` e ``end`` podem ser combinados.

        .. activecode:: exemplo_print_end_nao_pula_com_sep

            print('primeira', 'linha', sep='**', end=' <<>> ')  
            print('segunda', 'linha', sep='%%', end='<<FIM>>')

        A saída desse trecho de código é 

            primeira**linha <<>> segunda%%linha<<FIM>>

