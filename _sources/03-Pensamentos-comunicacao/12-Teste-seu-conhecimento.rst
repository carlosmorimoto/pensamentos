
..  Copyright (C) Carlos Hitoshi Morimoto e José Coelho de Pina Jr.
    É permitida a cópia, distribuição e/ou modificação desde documento
    sob os termos da Licença de Documentação Livre GNU (GNU Free 
    Documentation License) versão 1.3 ou versão subsequente
    publicada pela Fundação de Software Livre (Free Software 
    Foundation); com as seções invariantes sendo Prefácio e 
    Lista de Contribuidores, sem texto de capa ou contra-capa.  
    Uma cópia da licença está disponível em:
    https://www.gnu.org/licenses/licenses.pt-br.html#FDL


.. qnum::
	:prefix: cap03-12
	:start: 1



Teste o seu conhecimento
------------------------

.. mchoice:: questao_tabulacao
    :answer_a: todos os blocos foram digitados corretamente.
    :answer_b: apenas os blocos A e B
    :answer_c: apenas o bloco C
    :answer_d: apenas o bloco D
    :correct: c
    :feedback_a: Incorreto: lembre-se que a tabulação é importante em Python.
    :feedback_b: Incorreto: lembre-se que a tabulação é importante em Python.
    :feedback_c: Correto. As linhas devem começar na primeira coluna.
    :feedback_d: Incorreto: lembre-se que a tabulação é importante em Python.

    Cada bloco corresponde a um programa em Python com apenas 2 linhas. Qual desses programas foi digitado corretamente no editor do Spyder?

    .. code-block:: python

        # Bloco A
        nome = input('Digite seu nome: ')
            print('Olá', nome)

        # Bloco B
            nome = input('Digite seu nome: ')
        print('Olá', nome)

        # Bloco C
        nome = input('Digite seu nome: ')
        print('Olá', nome)

        # Bloco D
            nome = input('Digite seu nome: ')
            print('Olá', nome)


Esse exercício é um típico programa para processar dados.

.. activecode:: ac03_conversor_CF
    
    :caption: Conversor Celsius para Farenheit

    Escreva um programa que leia uma temperatura em 
    graus Celsius e converta para Fahrenheit onde:

    F = C * 9 / 5 + 32

    ~~~~
    # Pense: o programa deve ler um int ou float?
    # 
    # Pense: o programa deve imprimir uma mensagem?
    #
    # Veja que o enunciado não manda imprimir o resultado. 
    # Isso não quer dizer que o programa não imprime uma mensagem
    # mas sem uma definição, a mensagem tem um formato livre.


