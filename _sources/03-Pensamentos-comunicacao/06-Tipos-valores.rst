
..  Copyright (C) Carlos Hitoshi Morimoto e José Coelho de Pina Jr.
    É permitida a cópia, distribuição e/ou modificação desde documento
    sob os termos da Licença de Documentação Livre GNU (GNU Free 
    Documentation License) versão 1.3 ou versão subsequente
    publicada pela Fundação de Software Livre (Free Software 
    Foundation); com as seções invariantes sendo Prefácio e 
    Lista de Contribuidores, sem texto de capa ou contra-capa.  
    Uma cópia da licença está disponível em:
    https://www.gnu.org/licenses/licenses.pt-br.html#FDL


.. qnum::
	:prefix: cap03-06
	:start: 1



Tipos e valores
---------------

Observe que qualquer valor inteiro como ``2`` também pode ser escrito como um real usando a notação ``2.0`` (ou seja, a existência do ponto decimal indica que se trata de um número real, mesmo que a parte fracionária seja zero). Assim, ao escrever seus programas, procure sempre escrever os números reais de forma explícita, incluindo o '.0' para indicar que a grandeza que deseja representar se trata de um real e não um inteiro. 

Ao calcular expressões aritméticas é comum utilizar valores de tipos diferentes. No trecho de código abaixo, a variável ``perimetro`` é calculada multiplicando o inteiro ``2`` por números reais. Execute esse trecho passo-a-passo clicando em ``Next`` para verificar o tipo do valor associado a essa variável, resultado da expressão "2 * pi * raio". 

.. codelens:: cl03_dois_pi_raio_exemplo

    pi = 3.1415   # número real
    raio = 1.0    # número real pois tem '.0'
    perimetro = 2 * pi * raio   # 2 é inteiro. Mas qual o tipo de perimetro?
    print( 'tipo: ', type(perimetro) )

Para manter consistência nos dados, é desejável gerar um resultado que tenha o mesmo tipo dos dados utilizados, como ``2 * 4`` deve resultar no valor ``8`` inteiro. 
Mas existem excessões como a divisão, onde "5 / 2" resulta no ``float`` ``2.5``. 
Quando há mistura de tipos, o Python realiza os cálculos usando o tipo mais "poderoso", ou seja, em expressões com inteiros e reais, o resultado será real (``float``).  

Como o Python considera valores, também não há problema em comparar valores de tipos diferentes. Assim, a expressão ``5 == 5.0`` resulta no valor booleano ``True`` (do tipo ``bool``). 

.. admonition:: Evite comparar igualdade entre floats

    Devido a representação finita de número reais como números em ponto flutuante, números reais em Python são, na verdade, aproximações. 
    
    Por exemplo, considere o número Pi4 que representa o valor da constante matemática Pi usando 4 casas decimais (``3.1415``)
    e o número Pi5 que representa o mesmo valor mas com 5 casas decimais (``3.14159``).
    Qual seria o valor da expressão Pi4 == Pi5?

    Outro problema de usar aproximações ao realizar cálculos é o acumulo de erros. 

    Por exemplo execute o trecho a seguir, procurando pensar no resultado antes de clicar o ``Next``.

    .. codelens:: problema_igualdade_entre_floats

        x = 0.01 + 0.01 + 0.01 + 0.01 + 0.01 + 0.01 
        print( x == 0.06 )
        print( 'x = ', x )

    Se for realmente necessário comparar floats, use um intervalo de precisão epsilon, pequeno o sufiente para a sua aplicação, como no trecho de código abaixo.

    .. codelens:: outr_problema_igualdade_entre_floats

        s = 0.06
        x = 0.01 + 0.01 + 0.01 + 0.01 + 0.01 + 0.01 
        print( x == s ) # x é igual a s

        eps = 0.0001
        print( s - eps < x < s + eps ) ## x está perto o suficiente de s?


.. exercício associe valores x tipos



