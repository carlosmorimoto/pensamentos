
..  Copyright (C) Carlos Hitoshi Morimoto e José Coelho de Pina Jr.
    É permitida a cópia, distribuição e/ou modificação desde documento
    sob os termos da Licença de Documentação Livre GNU (GNU Free 
    Documentation License) versão 1.3 ou versão subsequente
    publicada pela Fundação de Software Livre (Free Software 
    Foundation); com as seções invariantes sendo Prefácio e 
    Lista de Contribuidores, sem texto de capa ou contra-capa.  
    Uma cópia da licença está disponível em:
    https://www.gnu.org/licenses/licenses.pt-br.html#FDL


.. qnum::
	:prefix: cap03-04
	:start: 1



Variáveis e o comando de atribuição
-----------------------------------

.. index:: nome de variável, comando de atribuição

Uma **variável** é simplesmente um **nome** que faz referência a um objeto na memória que corresponde, por exemplo, ao resultado de alguma expressão aritmética. Para criar uma variável, precisamos associá-la a um objeto ou valor usando o **comando de atribuição** que corresponde ao símbolo ``=``, como no trecho abaixo:

.. code-block:: Python

    variável = expressão

Para entender como  utilizar o comando de atribuição para criar e modificar o valor de variáveis, execute o trecho de código Python no CodeLens abaixo, acompanhando a lista de comentários a seguir antes de clicar em ``Next``.

* Com a seta vermelha na Linha 1: pi = 3.14

    - clique em ``Next`` e repare  que abaixo do quadro de saída aparece um outro quadro chamado ``Global frame``. Esse quadro mostra uma tabela com os nomes das variáveis criadas e seus valores associados. O comando de atribuição calcula a expressão do lado direito, nesse caso é o valor ``3.14``, e cria a variável ``pi`` que passa a fazer referência ao valor da expressão. 

* Com a seta na linha 2: print( 'pi = ', pi )

    - clique em ``Next``. A mensagem "pi = 3.14" é impressa na saída e a seta desce para a próxima linha.

* Com a seta na linha 3: novo_pi = pi

    - clique em ``Next``. O valor da expressão do lado direito é calculado. Desse lado, há apenas a variável ``pi``. Para calcular a expressão, todas as variáveis são substituídas pelos valores a que fazem referência e, portanto, o valor da expressão se torna ``3.14``. A variável com nome ``novo_pi`` é criada e faz referência ao valor ``3.14``. 

* Com a seta na linha 4: print( 'novo_pi = ', novo_pi )

    - clique em ``Next``. A mensagem "novo_pi = 3.14" é impressa na saída e a seta desce para a próxima linha.

* Com a seta na linha 5: pi = 2 * pi

    - clique em ``Next``. Veja que o valor a que ``pi`` faz referência é atualizado. Mas observe também que o valor de ``novo_pi`` não é alterado. 

* Com a seta na linha 6: print( 'pi = ', pi )

    - clique em ``Next``. A mensagem "pi = 6.28" é impressa na saída e o programa se encerra (não é mais possível clicar em ``Next``).


.. codelens:: cl03_criacao_de_uma_variavel
    :showoutput:

    pi = 3.14
    print( 'pi = ', pi )
    novo_pi = pi
    print( 'novo_pi = ', novo_pi )
    pi = 2 * pi
    print( 'pi = ', pi )


.. admonition:: **Nomes de variáveis**

   Nesse curso o nome de uma variável será sempre iniciada por uma letra
   minúscula e poderá ser seguida por outras letras minúsculas e números, como
   em: ``contador``, ``ind``, ``i``, ``j``, ``a1``, ``r2d2``, ``bb8`` etc.
   
   Escolha sempre um nome **significativo ou comum**, para facilitar o
   entendimento sobre o que variável representa ou realiza. Por exemplo, ``i``
   e ``j`` são nomes comuns para contar o número de iterações em um laço, mas
   podemos utilizar o nome ``contador`` e as vezes algumas abreviações como
   ``cont`` e ``aux``. Para melhorar a clareza de seus programas, sugerimos
   também o uso de nomes compostos separados pelo caractere `_`, como em
   ``conta_pares`` e ``conta_impares``. Evitaremos também o uso de caracteres
   acentuados para facilitar a compatibilidade com a ferramenta online
   utilizada no curso.
   
   Vamos chamar de **constantes** variáveis que **não devem** mudar de valor 
   após serem inicializadas. Para indicar uma constante utilizaremos nomes
   formados por todas as letras em maiúscula, como por exemplo
   ``MAXIMO_TAMANHO = 100``.

   **Palavras reservadas** como o nome de comandos e funções nativas do Python
    (``if``, ``while``, ``print()`` etc) também não podem ser utilizadas como nome de
    variáveis.


.. admonition:: **Não confunda atribuição com igualdade**

    Nós somos muito treinados a ler o símbolo "=" como "igual". Lembre-se que o operador relacional de igualdade é representado como "==" e a expressão "pi == 3.14" devolve ``True`` ou ``False`` mas **não** cria ou altera o valor da variável ``pi``. 
    
    Um reflexo desse "treinamento" quando começamos a programar é ignorar a ordem dos elementos do comando de atribuição (pois o símbolo é lido como "igual"). A ordem é muito importante, sendo que devemos sempre escrever o nome de uma variável do lado esquerdo e a expressão do lado direito do comando de atribuição "=".

    Finalmente quando usamos construções como 'novo_pi = pi' é comum imaginar que ambas são "a mesma" variável (outro reflexo de "igual"), ou seja, modificando-se o valor associado a uma delas, o valor da outra também é alterada. Isso não é verdade como pudemos observar no exemplo do CodeLens acima. 


