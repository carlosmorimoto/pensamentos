
..  Copyright (C) Carlos Hitoshi Morimoto e José Coelho de Pina Jr.
    É permitida a cópia, distribuição e/ou modificação desde documento
    sob os termos da Licença de Documentação Livre GNU (GNU Free 
    Documentation License) versão 1.3 ou versão subsequente
    publicada pela Fundação de Software Livre (Free Software 
    Foundation); com as seções invariantes sendo Prefácio e 
    Lista de Contribuidores, sem texto de capa ou contra-capa.  
    Uma cópia da licença está disponível em:
    https://www.gnu.org/licenses/licenses.pt-br.html#FDL


.. qnum::
	:prefix: cap03-03
	:start: 1




Comunicação e interação
-----------------------

.. No capítulo anterior vimos como o Python calcula o valor de uma expressão. 
    Basicamente, uma expressão é reduzida um operador por vez, começando dos operadores com maior precedência até que a expressão seja totalmente reduzida a um único valor numérico ou booleano.
    Vimos também que o iPython pode ser utilizado como "calculadora" para resolver expressões. 

.. A maioria das calculadoras são máquinas dedicadas que possuem um teclado reduzido onde 
    o usuário pode entrar com os operandos e operadores, e um tela reduzida também (apenas uma linha) para exibir os valores digitados e os resultados das operações. 

.. Os computadores tipicamente possuem interfaces gráficas que recebem informações de um 
    teclado e mouse e exibem informações em um monitor. Vamos evitar a complexidade de criar interfaces gráficas e nos concentrar em problemas mais simples que podem ser resolvidos em terminais por linhas de comando, como o iPython. 

Diferente dos programas que você deve estar acostumado a usar, que possuem interfaces com elementos gráficos que você pode selecionar com o mouse, os programas que vamos desenvolver nesse curso precisam ser executados dentro de um **terminal**. 

Como vimos, um terminal permite interagir com o computador por meio de linhas de comando. Esse tipo de interface era bastante comum antes das interfaces gráficas e vai nos permitir manter o foco nos algoritmos ao invés da interface. 

Se você ainda tem dúvidas sobre o funcionamento de um terminal, imagine um terminal como um programa de troca de mensagens, como o WhatsApp, Telegram ou Signal, que você usa para "trocar mensagens" com o programa. Você deve escrever alguma mensagem na linha de comando para enviar ao programa, e o programa responde imprimindo uma mensagem no terminal. 
Assim, quando um exercício mencionar que um programa "recebe dados", estamos dizendo que o programa deve ler os dados na forma de textos que devem ser digitados pelo usuário na linha de comando do terminal. 
Depois de ler os dados, o programa deve processar os dados para calcular a resposta e 
imprimir uma mensagem avisando o usuário do resultado.
 
Esse processo é ilustrado pela figura abaixo.

.. figure:: ../Figuras/cap03/arara-shell.png
    :align: center
    :width: 800px
    :alt: Figura 3.1: Arara usando programa no Python Shell.

    Figura 3.1: O usuário usa o iPython (terminal Python ou Python shell) para interagir com o programa Python sendo executado no computador. A função ``input()`` permite que o Python receba um texto digitado pelo usuário e a função ``print()`` permite que o Python imprima textos na janela do Python Shell.

Nesse capítulo você vai aprender a usar a função ``input()`` do Python para ler um texto do teclado e imprimir um texto usando a função ``print()``. Como mostra a figura 3.1, essas duas funções fazem a ponte (entrada e saída) entre o usuário e o programa.

Os dados que entram e saem devem ficar armazenados e são acessados por meio de **variáveis**. O uso de variáveis é ainda fundamental para armazenar resultados intermediários necessários para processar os dados. Antes portanto de introduzir as funções de entrada e saída, vamos discutir alguns conceitos relacionados à criação e uso de variáveis.

.. Para escrever e executar nosso primeiro programa vamos usar o Spyder, um aplicativo chamado de "ambiente integrado de desenvolvimento" que reune várias ferramentas (de forma integrada) que facilitam o desenvolvimento de programas. 


