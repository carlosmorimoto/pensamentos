
..  Copyright (C) Carlos Hitoshi Morimoto e José Coelho de Pina Jr.
    É permitida a cópia, distribuição e/ou modificação desde documento
    sob os termos da Licença de Documentação Livre GNU (GNU Free 
    Documentation License) versão 1.3 ou versão subsequente
    publicada pela Fundação de Software Livre (Free Software 
    Foundation); com as seções invariantes sendo Prefácio e 
    Lista de Contribuidores, sem texto de capa ou contra-capa.  
    Uma cópia da licença está disponível em:
    https://www.gnu.org/licenses/licenses.pt-br.html#FDL


.. qnum::
	:prefix: cap03-13
	:start: 1



Onde chegamos e para onde vamos?
--------------------------------

Nesse capítulo vimos que é muito importante especificar os tipos de dados de entrada e saída para que os programas possam se comunicar. 

Vimos como usar variáveis para poder ter acesso a esses dados e também armazenar valores intermediários necessários para o processamento dos dados. 

Os programas que vamos desenvolver nesse curso devem ser executados em um terminal e a interação com o usuário será por meio de mensagens de texto, usando a função de entrada de texto ``input()`` e a de saída ``print()``. 

Como essas funções só trabalham com textos (strings), muitas vezes é necessário transformar as strings em outros tipos de dados mais apropriados à aplicação, usando as funções de conversão de tipos como ``int()``, ``float()`` e ``bool()``. 

O comando de atribuição, usado para criar e modificar os valores associados a variáveis, é um dos conceitos mais fundamentais desse capítulo. Pelo símbolo utilizado, é muito importante não confundir esse comando com "igualdade".

O próximo capitulo introduz outro conceito fundamental, ainda bastante ligado à lógica matemática, e que vai nos permitir tratar problemas bem mais complexos que os vistos até aqui.






