
..  Copyright (C) Carlos Hitoshi Morimoto e José Coelho de Pina Jr.
    É permitida a cópia, distribuição e/ou modificação desde documento
    sob os termos da Licença de Documentação Livre GNU (GNU Free 
    Documentation License) versão 1.3 ou versão subsequente
    publicada pela Fundação de Software Livre (Free Software 
    Foundation); com as seções invariantes sendo Prefácio e 
    Lista de Contribuidores, sem texto de capa ou contra-capa.  
    Uma cópia da licença está disponível em:
    https://www.gnu.org/licenses/licenses.pt-br.html#FDL


.. qnum::
	:prefix: cap03-10
	:start: 1



Exercício comentado: Como somar 2 números inteiros?
---------------------------------------------------

O objetivo desse exercício é aplicar tudo o que vimos até agora e aproveitar para apresentar algumas características da linguagem Python. 

Vamos começar tentando somar dois números usando apenas variáveis. Para isso execute o script abaixo no ActiveCode: 

.. activecode:: cap3_ex1_1
    :caption: Como somar dois números?
    :nocodelens: 

    Tentativa 0: execute o programa abaixo e veja o que acontece.

    ~~~~

    a = 3
    b = 4
        soma = a + b
        print('A soma de a + b é: soma')

    ====

Clique no botão "Run" para executar esse programa.

Depure o programa
.................

Depurar (às vezes chamado de debugar, do inglês **debug**), é a atividade de  corrigir problemas do programa. 

O primeiro **bug** desse programa é sintático. Para corrigir esse problema basta
corrigir a tabulação e clique em RUN para executar o programa novamente. 

Sem o erro sintático, o Python consegue executar o programa até o final e imprimir na saída a string fornecida à função ``print()``.
Nesse caso, a ``print()`` está imprimindo a string que recebeu, 
onde o **nome** da variável ``soma`` é impresso ao invés de seu valor.

.. activecode:: cap3_ex1_2
    :caption: Como somar dois números?
    :nocodelens: 

    Tentativa 1: execute o programa abaixo e veja o que acontece.

    ~~~~

    a = 3
    b = 4
    soma = a + b
    print('A soma de a + b é:', soma)

Observe que o print recebeu dois **valores** separados por vírgula:

    - uma string (entre apóstrofes) e
    - a variável ``soma`` (ao invés da string 'soma')

A string é impressa diretamente sem apóstrofes (pois esse é o seu **valor**) e, ao invés de imprimir a string 'soma', a ``print()`` imprime o **valor** da variável.

Mas como imprimir os valores de ``a`` e ``b`` ao invés de seus nomes?

.. activecode:: cap3_ex1_3
    :caption: Como somar dois números?
    :nocodelens: 

    Tentativa 2: execute o programa abaixo e veja o que acontece.

    ~~~~

    a = 3
    b = 4
    soma = a + b
    print('A soma de', a, '+', b, 'é:', soma)

       
Podemos quebrar ainda mais a lista de valores passada ao print, com strings e variáveis, para que a mensagem fique mais clara.

Uma forma mais elegante para imprimir mensagens e substituir valores de variáveis é usando uma string formatada. 

.. activecode:: cap3_ex1_3_formatada
    :caption: Como somar dois números?
    :nocodelens: 

    Tentativa 3: execute o programa abaixo e veja o que acontece.

    ~~~~

    a = 3
    b = 4
    soma = a + b
    print( f'A soma de {a} + {b} é: {soma}' )

Uma string formatada começa com o caractere "f". Dentro da string formatada, os elementos entre chaves, como "{a}" e "{b}", indicam nomes de variáveis. Esses elementos são substituídos pelos valores das variáveis correspondentes. 


Mas como fazer com que os valores a serem somados sejam definidos por um usuário?

.. activecode:: cap3_ex1_4
    :caption: Como somar dois números?
    :nocodelens: 

    Tentativa 4: execute o programa abaixo e veja o que acontece.

    ~~~~
    # tudo que vem após o simbolo # até o final da linha é um comentário
    # comentários são mensagens aos programadores 
    # que não são executados pelo Python

    PROMPT_1 = "Digite o primeiro numero: "  # PROMPT_1 é uma constante
    PROMPT_2 = "Digite o segundo numero: "   # PROMPT_2 é outra constante
             
    a = input(PROMPT_1) # veja o texto de PROMPT_1
    b = input(PROMPT_2)
    
    soma = a + b
    print( f'A soma de {a} + {b} é: {soma}' )
                
Utilizamos a função ``input()`` para receber a resposta do usuário à mensagem que colocamos como string passado à função ``input()``. Como a resposta do usuário também é uma string (e não um **número**) a operação ``+`` é realizada com strings, ou seja, seus valores são concatenados.

Precisamos, portanto, de um maneira para converter uma string em um número, para que o Python obtenha a soma desses números.


.. activecode:: cap3_ex1_5
    :caption: Como somar dois números?
    :nocodelens: 

    Tentativa 5: execute o programa abaixo e veja o que acontece.

    ~~~~

    a_str = input("Digite o primeiro numero: ")
    b_str = input("Digite o segundo numero: ")
    a_int = int(a_str) # converte string/texto para inteiro
    b_int = int(b_str) # converte string/texto para inteiro
    soma = a_int + b_int
    print( f'A soma de {a_int} + {b_int} é: {soma}' )

                
A função ``int()`` converte uma string para um número inteiro (se possível, senão fornece uma mensagem de erro). Como não precisamos guardar as respostas na forma de texto, podemos simplificar o programa combinando as funções ``int()`` e ``input()`` da seguinte forma:
    
.. activecode:: cap3_ex1_6
    :caption: Como somar dois números?
    :nocodelens: 

    Tentativa 6: execute o programa abaixo e veja o que acontece.
    ~~~~
    a = int(input("Digite o primeiro numero: "))
    b = int(input("Digite o segundo numero: "))
    soma = a + b
    print( f'A soma de {a} + {b} é: {soma}' )


