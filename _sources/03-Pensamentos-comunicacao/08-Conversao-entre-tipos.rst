
..  Copyright (C) Carlos Hitoshi Morimoto e José Coelho de Pina Jr.
    É permitida a cópia, distribuição e/ou modificação desde documento
    sob os termos da Licença de Documentação Livre GNU (GNU Free 
    Documentation License) versão 1.3 ou versão subsequente
    publicada pela Fundação de Software Livre (Free Software 
    Foundation); com as seções invariantes sendo Prefácio e 
    Lista de Contribuidores, sem texto de capa ou contra-capa.  
    Uma cópia da licença está disponível em:
    https://www.gnu.org/licenses/licenses.pt-br.html#FDL


.. qnum::
	:prefix: cap03-08
	:start: 1



Conversão entre tipos
---------------------

Acabamos de ver que expressões aritméticas podem misturar valores inteiros com reais. 
Assim, quando um operador aritmético encontra um operando inteiro e outro real, o Python promove o inteiro para real para realizar o cálculo. Essas conversões são feitas automaticamente pelo Python, de forma **implícita**.

.. mchoice:: mc03_real_ou_inteiro
    :answer_a: 14
    :answer_b: 14.0
    :answer_c: as alternativas a e b estão corretas
    :answer_d: 20.0
    :correct: b
    :feedback_a: o resultado não pode ser um ``int``.
    :feedback_b: Correto.
    :feedback_c: o resultado não pode ser um ``int``.
    :feedback_d: a multiplicação tem precedência sobre a soma.

    Qual o valor da expressão ``2 + 3 * 4.0``


Ao processar dados é muito comum no entanto precisar converter o tipo de um dado em outro. 
A conversão entre tipos nativos do Python pode ser realizada de forma **explícita** usando as funções de mesmo nome como mostra os exemplos abaixo.

.. code-block:: Python

    >>> 4 / 2
    2.0
    >>> int(4/2)
    2
    >>> float(2 * 4)
    8.0


