
..  Copyright (C) Carlos Hitoshi Morimoto e José Coelho de Pina Jr.
    É permitida a cópia, distribuição e/ou modificação desde documento
    sob os termos da Licença de Documentação Livre GNU (GNU Free 
    Documentation License) versão 1.3 ou versão subsequente
    publicada pela Fundação de Software Livre (Free Software 
    Foundation); com as seções invariantes sendo Prefácio e 
    Lista de Contribuidores, sem texto de capa ou contra-capa.  
    Uma cópia da licença está disponível em:
    https://www.gnu.org/licenses/licenses.pt-br.html#FDL


.. qnum::
	:prefix: cap01-01
	:start: 1

.. shortname:: Introdução
.. description:: Introdução


Introdução
---------- 

.. raw:: html

    <div align="right">
    <p></p>
    <blockquote>
    <p>
    "Sessenta anos atrás eu sabia tudo. </br>
    Agora sei que nada sei. </br>
    A educação é a descoberta progressiva da nossa ignorância."
    </br>

    </p>
    <p><a href="https://pt.wikipedia.org/wiki/Will_Durant">Will Durant</a></p>
    </blockquote>
    <p></p>
    </div>

É possível que, se você está começando a ler esse livro, se pergunte:
para que eu preciso aprender a pensar com Python?

Certamente você já sabe "pensar" e "resolver" problemas, aliás muito
bem já que você escolheu seguir esse livro. No entanto, provavelmente
você deve ter pouca ou nenhuma experiência com computação e, em
particular, com programação de computadores. Se esse é o seu caso e
você deseja aprender um pouco de programação e computação, esse livro
foi escrito pensando em você. Mas se você já sabe programar em alguma
linguagem e quer aprender Python, provavelmente há formas melhores,
como a própria `documentação do Python <https://wiki.python.org/moin/BeginnersGuide>`__.

Portanto, esse não é um livro para aprender Python. Nosso objetivo é
mais fundamental, nós almejamos que, ao final do curso, você seja
capaz de resolver problemas computacionais simples, independente da
linguagem usada para programar suas soluções. Para isso, ao longo do
curso, vamos realizar vários exercícios para refinar algumas das suas
`habilidades cognitivas <https://pt.wikipedia.org/wiki/Habilidade_cognitiva>`__ até que você se sinta confortável na resolução
desses problemas.

Nesse livro vamos chamar essas habilidades de
"`pensamentos <https://pt.wikipedia.org/wiki/Pensamento>`__". Tipicamente, as melhores soluções
são resultado de um profundo conhecimento do problema e larga experiência com as
ferramentas usadas para construir as soluções, além, é claro, de um
pouco de criatividade. Veremos que, em muitos casos, nossas
soluções são incorretas por falta de conhecimento do problema e/ou uso incorreto das ferramentas.

Sabemos que, para quem nunca programou, perceber certos detalhes é
muito difícil e muitas vezes isso nós impede de entender um problema,
ou enxergar uma solução, ou ainda ver um problema na nossa tentativa de
solução.  Por isso, desde já, pedimos sua atenção e paciência para que
possamos retreinar sua habilidade de leitura de textos para leitura de
programas. Um dos primeiros fundamentos que vamos treinar é procurar
entender o que está escrito, como **leitores** "pensando" se o que
está escrito resolve o problema, para que, em seguida, possamos passar
ao papel de **escritores** ou criadores de soluções.

Esse livro também surgiu da possibilidade de criar um material
interativo, onde o texto fica intercalado com exercícios e questões
que você pode resolver dentro do próprio navegador. Vamos dar um
exemplo com o seguinte exercício de ordenação de palavras.

Considere as palavras ``"casa"``, ``"CASA"``, ``"cASa"`` e ``"CasA"``. 
Essas quatro palavras são exatamente as mesmas, correto?
ERRADO!
Cada uma delas tem uma combinação de letras maiúsculas e minúsculas e são consideradas **diferentes** pela linguagem Python.
Considere agora o problema de colocar essas palavras em ordem alfabética crescente.
Para isso, considere que as letras maiúsculas vem antes (tem precedência) das letras minúsculas. 

            
.. mchoice:: ordene_as_casas

   Ordene as palavras ``"casa"``, ``"CASA"``, ``"cASa"`` e ``"CasA"`` em ordem crescente, considerando que
   letras maiúsculas tem precedência sobre minúsculas. Dizer que maiúsculeas têm **precedência** sobre minúsculas significa que ``"A"`` deve aparece antes de ``"a"`` e que uma *palavra* como ``"aBa"`` deve aparecer antes de uma palavra como ``"aba"``.

   -    ``"casa"``, ``"CASA"``, ``"cASa"``, ``"CasA"``

        - ``'C'`` tem precedência sobre ``'c'``
         
   -    ``"casa"``, cASa, ``"Casa"``, ``"CASA"``

        - ``'C'``  tem precedência sobre ``'c'`` minúscula
       
   -    ``"CASA"``, ``"Casa"``, ``"casa"``, ``"casa"``

        + parabéns, correto!

   -    ``"CASA"``, ``"casa"``, ``"Casa"``, ``"casa"``

        - ``'C'`` tem precedência sobre ``'c'``



.. admonition:: Spoiler Alert!

    Além de questões de múltipla escolha, o livro está repleto de outros tipos de atividades interativas. Você poderá inclusive escrever e executar trechos de código em Python, de forma interativa, sem sair desse livro.

    Ao final de cada capítulo existe também uma seção com exercícios de **laboratório**. O objetivo dos laboratórios é que você aprenda a usar o Python de forma plena, fora das limitações impostas pelo navegador. Os laboratórios exploram outras ferramentas que facilitam o desenvolvimento de programas mais complexos.

.. figure:: ../Figuras/00-prefacio/Le_Penseur.jpg
    :width: 300
    :align: center

    `Wikipedia <https://en.wikipedia.org/wiki/The_Thinker/>`__: O Pensador no Jardim do Museu Rodin, Paris. 

..
   .. mchoice:: ordene_as_``"casa"``s
             :answer_a: ``"casa"``, ``"CASA"``, ``"casa"``, ``"Casa"``
             :answer_b: ``"casa"``, ``"casa"``, ``"Casa"``, ``"CASA"``
             :answer_c: ``"CASA"``, ``"Casa"``, ``"casa"``, ``"casa"``
             :answer_d: ``"CASA"``, ``"casa"``, ``"Casa"``, ``"casa"``
             :correct: c 
             :feedback_a: lembre-se que 'C' maiúscula tem precedência sobre 'c' minúscula
             :feedback_b: lembre-se que 'C' maiúscula tem precedência sobre 'c' minúscula
             :feedback_c: parabéns, correto!
             :feedback_d: lembre-se que 'C' maiúscula tem precedência sobre 'c' minúscula

             Ordene as palavras "``"casa"``", "``"CASA"``", "``"casa"``' e "``"Casa"``" em ordem cresente, considerando que letras maiúsculas tem precedência sobre minúsculas.

   
..


      ..  .. parsonsprob:: casas_ordenadas
          Coloque as palavras em ordem alfabética, arrastando-as para o quadro à direita.
          ----
          casa
          ====
          CASA
          ====
          CasA
          ====
          cASa

            
