
..  Copyright (C) Carlos Hitoshi Morimoto e José Coelho de Pina Jr.
    É permitida a cópia, distribuição e/ou modificação desde documento
    sob os termos da Licença de Documentação Livre GNU (GNU Free 
    Documentation License) versão 1.3 ou versão subsequente
    publicada pela Fundação de Software Livre (Free Software 
    Foundation); com as seções invariantes sendo Prefácio e 
    Lista de Contribuidores, sem texto de capa ou contra-capa.  
    Uma cópia da licença está disponível em:
    https://www.gnu.org/licenses/licenses.pt-br.html#FDL


.. qnum::
	:prefix: cap01-04
	:start: 1




Computadores e calculadoras
---------------------------

.. index:: computador, arquitetura de von Neumann, ULA, UCP

A habilidade de representar quantidades e manipulá-las (como "fazer conta" de mais e de menos) é uma necessidade básica para muitas atividades humanas. O ábaco e a régua de cálculo são exemplos de instrumentos inventados para nos ajudar a "fazer contas". 

A primeira calculadora mecânica foi desenvolvida por Blaise Pascal em 1642. A *Pascaline* (`<https://en.wikipedia.org/wiki/Pascal%27s_calculator>`_) utilizava um complexo sistema de engrenagens para realizar operações de adição e subtração.

Apesar dos computadores serem capazes de realizar operações matemáticas, os computadores são máquinas diferentes de calculadoras pois podem ser **programadas**. Os programas de computador permitem a repetição automática de cálculos muitas e muitas vezes, como por exemplo, calcular o pagamento de milhares de funcionários de uma companhia baseado no cargo e no número de horas trabalhadas por cada um.

Os primeiros computadores eletrônicos foram desenvolvidos na década de 1940 e o rápido e contínuo desenvolvimento tecnológico resultou nos computadores modernos, que tem desempenho muitas vezes superior a esses primeiros computadores, com tamanho e consumo de energia muitas vezes menor. Apesar de serem máquina complexas, entender como um computador funciona de forma genérica nos ajuda a usá-lo de forma mais eficaz e nos fornece um contexto mais claro para definir o que é computação. 

Vamos basear nossa explicação na arquitetura de um computador proposta por John von Neumann (`<https://en.wikipedia.org/wiki/Von_Neumann_architecture>`_), ilustrada na figura 1.1 Essa arquitetura é composta por uma unidade central de processamento (UCP), um bloco de "memória" para armazenar dados, e dispositivos de entrada e saída. Esses componentes são interligados por uma via de comunicação de dados. 

.. figure:: ../Figuras/cap01/vonNeumann.png
    :width: 800
    :align: center
    :alt: Figura 1.1: Arquitetura de von Neumann.
    
    Figura 1.1: Arquitetura de um computador proposta por `John von Neumann <https://en.wikipedia.org/wiki/Von_Neumann_architecture>`_.


Hoje em dia, a primeira coisa que vemos quando olhamos para um computador de mesa são os dispositivos de entrada e saída, como teclado, mouse e monitor, necessários para que o computador se comunique com outros computadores e pessoas. Dessa forma, os dispositivos de entrada permitem que o computador receba dados do usuário por meio do teclado e mouse (ou da Internet etc) e transmita os resultados para o monitor ou outros dispositivos de saída como impressoras, auto-falantes ou para a Internet.

Esses dados ficam armazenados na memória do computador em formato digital, ou seja, na forma de dígitos binários. Um **dígito binário** assume apenas 2 valores (como zero ou um) e é mais adequado para representar eletronicamente que um dígito decimal pois é possível utilizar uma chave eletrônica (aberta ou fechada) ou um capacitor (carregado ou descarregado). Um único dígito binário é chamado de *bit* e 8 (oito) bits formam um *byte*. A capacidade da memória é comumente representada em *kilobyte* ou KB (cerca de mil bytes), *megabytes* ou MB (cerca de um milhão de bytes), *gigabytes* ou GB (um bilhão) e assim por diante. 

Hoje é comum encontrar discos com mais de 1 *terabyte* (um trilhão de bytes) de capacidade. Para se ter uma ideia, um filme de cerca de 2 horas de duração gravado em alta resolução pode ser armazenado usando cerca de 2 GB no formato mp4, enquanto todos os textos contidos na Enciclopédia Britânica (`<https://websitebuilders.com/how-to/glossary/terabyte/>`_) no ano 2010 (último ano quando foi publicada com 32 volumes) pode ser armazenado em apenas 300 MB (300 milhões de caracteres ou cerca de 50 milhões de palavras). 

Além de dados, a memória do computador também armazena programas.
Um *programa de computador* é uma sequência de instruções que deve ser executada *uma de cada vez* pela UCP. Os programas também ficam armazenados na memória em código binário (também chamado de código de máquina ou em linguagem de máquina).
Uma instrução pode ser de natureza *aritmética* ou *lógica*, que basicamente permite ao computador executar operações matemáticas como uma calculadora, ou de *controle*.

As instruções de controle permitem, entre outras coisas, movimentar dados e desviar a execução do programa. A UCP contém um pouco de memória local (ou um grupo de registradores) para trabalhar os dados internamente. A evolução dos computadores tem resultado no tamanho dos dados processados em relação ao número de bits ou bytes que a UCP é capaz de processar de cada vez. As UCPs atuais tipicamente processam dados de 64 bits (ou 8 bytes). Assim, caso você precise instalar algum software para fazer os exercícios desse livro, provavelmente é seguro escolher a versão de 64 bits desse software. Computadores mais antigos (com 10 anos ou mais) processavam dados de 32 bits, os muito antigos processavam dados de 16 bits, e assim por diante. 

