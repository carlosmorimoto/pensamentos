Pensamentos
:::::::::::

.. toctree::
	:maxdepth: 1

	01-Introducao.rst
	02-Para-onde-vamos.rst
	03-Pensamento-Computacional.rst
	04-Computadores-calculadoras.rst
	05-Programas-Linguagens-Pensamentos.rst
	06-Um-pouco-mais-sobre.rst
	07-Computador-x-Computacao.rst
	08-Laboratorio.rst
	09-Teste-seus-conhecimentos.rst
	10-Onde-chegamos-para-onde.rst

