
..  Copyright (C) Carlos Hitoshi Morimoto e José Coelho de Pina Jr.
    É permitida a cópia, distribuição e/ou modificação desde documento
    sob os termos da Licença de Documentação Livre GNU (GNU Free 
    Documentation License) versão 1.3 ou versão subsequente
    publicada pela Fundação de Software Livre (Free Software 
    Foundation); com as seções invariantes sendo Prefácio e 
    Lista de Contribuidores, sem texto de capa ou contra-capa.  
    Uma cópia da licença está disponível em:
    https://www.gnu.org/licenses/licenses.pt-br.html#FDL


.. qnum::
	:prefix: cap01-10
	:start: 1



Onde chegamos e para onde vamos?
--------------------------------

Esse capítulo introduziu alguns conceitos básicos de computação sob um brevíssimo contexto histórico.
Nesse contexto, o Python é uma linguagem de programação de alto nível altamente portável (roda praticamente em qualquer computador, até mesmo em telefones celulares) e flexível (pode ser usada para diversos tipos de aplicação). O Anaconda fornece um pacote com várias ferramentas integradas, facilitando a instalação e manutenção do ambiente de desenvolvimento. 

Nos próximos capítulos vamos utilizar o IPython para ilustrar alguns comandos do Python e o IDE Spyder para escrever programas em Python. 





.. Referências

.. Para saber mais
    ---------------
    * `Como Pensar Como um Cientista da Computação - Aprendendo com Python: Versão Interativa <https://panda.ime.usp.br/pensepy/static/pensepy/index.html>`_, tradução do livro interativo produzido no projeto `Runestone Interactive <http://runestoneinteractive.org/>`_ por Brad Miller and David Ranum. 
    * [versão interativa em inglês]  `How to Think Like a Computer Scientist: Interactive Edition <https://runestone.academy/runestone/books/published/thinkcspy/index.html>`_, livro interativo produzido no projeto `Runestone Interactive <http://runestoneinteractive.org/>`_ por Brad Miller and David Ranum. 
    * [versão original em inglês] `How to Think Like a Computer Scientist: Learning with Python <https://open.umn.edu/opentextbooks/textbooks/how-to-think-like-a-computer-scientist-learning-with-python>`_, por Allen Downey, Franklin W. Olin, Jeff Elkner, and Chris Meyers.
    * Vídeos
        - `Introdução à Computação com Python <https://www.coursera.org/learn/ciencia-computacao-python-conceitos>`_ do professor Fábio Kon do Departamento de Computação do IME-USP no Coursera.        
        - `Python para Zumbis <https://www.pycursos.com/python-para-zumbis/>`_ do professor Fernando Masanori da FATEC de São José dos Campos. 
    * sobre Computação e computadores:
        - `Breve história da computação <http://www.ime.usp.br/~macmulti/historico/>`_
        - `O que é um Computador <http://pt.wikipedia.org/wiki/Computador>`_


..  Glossário
    ---------
    - Calculadora: máquina que apenas realiza cálculos, sem ou com uma capacidade limitada de programação.
    - Computador: 
    - Hardware:
    - Software:
    - Número binário:
    - bit:
    - byte:
    - ULA: unidade lógica e aritmética.
    - UCP: unidade central de processamento.
    - memória:
    - programa:
    - computação:
    - linguagem de máquina
    - linguagem de alto nível
    - linguagem de baixo nível
    - linguagem de montagem


