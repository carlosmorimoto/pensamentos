
..  Copyright (C) Carlos Hitoshi Morimoto e José Coelho de Pina Jr.
    É permitida a cópia, distribuição e/ou modificação desde documento
    sob os termos da Licença de Documentação Livre GNU (GNU Free 
    Documentation License) versão 1.3 ou versão subsequente
    publicada pela Fundação de Software Livre (Free Software 
    Foundation); com as seções invariantes sendo Prefácio e 
    Lista de Contribuidores, sem texto de capa ou contra-capa.  
    Uma cópia da licença está disponível em:
    https://www.gnu.org/licenses/licenses.pt-br.html#FDL


.. qnum::
	:prefix: cap01-05
	:start: 1



Programas, linguagens e mais pensamentos
----------------------------------------

Dizemos que os programas que ficam na memória de um computador para serem executados (portanto em formato binário) estão em código ou **linguagem de máquina**, por serem as linguagens "compreendidas" pelas máquinas.
Muito cedo na história dos computadores eletrônicos se descobriu que é muito difícil programar usando linguagem de máquina e assim foram surgindo várias linguagens de programação, cada uma com propostas diferentes para tratar certos problemas computacionais. 

De uma forma genérica porém, o desenvolvimento das linguagens de programação buscou se aproximar de alguma linguagem natural que usamos para nos comunicar, como o inglês e o português. Essas linguagens, chamadas de **linguagens de alto nível** por serem mais fáceis de usar por humanos, vem se tornando mais poderosas ao apresentar conjuntos de instruções mais extensos e recursos mais apropriados a aplicações modernas como computação gráfica, jogos eletrônicos e inteligência artificial. De forma similar, quanto mais próximas às linguagens de máquina, mais de **baixo nível** elas se tornam. 
 
Observe que cada máquina (ou microprocessador) possui um conjunto diferente de instruções que definem a sua própria "linguagem". 
Para que um programa escrito em uma linguagem de alto nível possa ser executado pela máquina (que só é capaz de executar código da máquina) o programa precisa ser traduzido por um programa chamado de **compilador**. 
Vamos chamar de **código fonte** o programa escrito pelos programadores e que fica armazenado em um **arquivo fonte**. 
Assim, um compilador recebe um arquivo fonte e gera um **arquivo executável** em linguagem de máquina. 
Programas escritos em linguagens compiladas tendem a ser bastante eficientes pois, após traduzidos, são executados em código nativo da máquina.

A figura 1.2 ilustra o processo de desenvolvimento de software usando linguagens de alto nível compiladas. O topo da figura mostra algumas habilidades que você vai desenvolver nesse curso. O **design** envolve o processo de criação e refinamento da solução, que requer um conhecimento profundo do problema. A **codificação** concretiza a solução na forma de um programa escrito na linguagem de alto nível. O programador usa um software de edição de textos para escrever e salvar o arquivo fonte. Esse arquivo é transformado em um arquivo executável usando o compilador. Finalmente, para **testar** se a solução resolve o problema, é comum usar um **terminal de entrada e saída** de texto (que pode ser chamado também de console ou shell). 

O terminal é usado para "interagir" com programas que não possuem interfaces gráficas que, infelizmente, é o caso dos programas que vamos escrever nesse curso. Nós vamos interagir com esses programas por meio de mensagens de texto, ou seja, nossos programas vão receber mensagens de texto que os usuários digitam usando o teclado (sempre digitando "Enter" ou "Return" para terminar a mensagem), e os programas podem também imprimir mensagens no terminal. As mensagens recebidas pelo terminal são basicamente comandos que ele tenta executar. Assim, para executar o programa após ser compilado, o programador deve digitar o nome do arquivo no terminal. O terminal carrega e passa a executar o programa. 
 
.. figure:: ../Figuras/cap01/compiladores.png
    :align: center
    :width: 800px
    :alt: Figura 1.2: desenvolvimento de software usando linguagens compiladas.

    Figura 1.2: Processo de desenvolvimento de software usando linguagens compiladas.


No entanto, a compilação de programas grandes e complexos pode ser um processo demorado. Uma alternativa é criar uma **máquina virtual** capaz de "entender" as instruções da linguagem de alto nível. Essa máquina virtual funciona como um tradutor instantâneo, um outro programa sendo executado pela máquina hospedeira. Diz-se que essas linguagens são **interpretadas** pois cada instrução é traduzida e executada pela máquina hospedeira. O Python é uma linguagem interpretada mas que apresenta um bom desempenho pois seu código é traduzido para a máquina virtual Python. É claro que essa tradução não deixa de ser uma compilação. No entanto, como a máquina virtual não roda código nativo, há sempre uma perda de desempenho se compararmos às linguagens compiladas para código nativo. 
Uma vantagem no caso do Python é que, por não serem compiladas (para código nativo ao menos), o desenvolvimento de programas se torna mais ágil devido a rapidez e facilidade para executar testes. 

A figura 1.3 ilustra a desenvolvimento de software usando linguagens interpretadas. Observe que não há mais um arquivo executável. As linguagens interpretadas modernas (como o Python) em geral usam um compilador "instantâneo" ou JIT ("*Just in Time*"), que transformam o código fonte em código "executável pela maquina virtual" a medida que o código vai sendo executado. Uma outra vantagem das linguagens interpretadas é que elas podem ficar "integradas" ao terminal e por isso o terminal foi aparece mais próximo ao programador. No caso do Python, vamos usar o `iPython <https://ipython.readthedocs.io/en/stable/>`_ para interagir e trocar mensagens diretamente com o Python, como se não houvesse um tradutor, o que agiliza o processo de testes.

.. figure:: ../Figuras/cap01/interpretadores.png
    :align: center
    :width: 800px
    :alt: Figura 1.3: desenvolvimento de software usando linguagens interpretadas.

    Figura 1.3: Processo de desenvolvimento de software usando linguagens interpretadas.


.. admonition:: Arquivos em Python

    No caso do Python, vamos usar a extensão '.py' para indicar os arquivos fonte em Python. Da primeira vez que esses arquivos são executados, o Python cria arquivos correspondentes com a extensão '.pyc'. Das próximas vezes que o programa for executado, o Python usa os arquivos '.pyc'. Mas, diferente de linguagens compiladas, mesmo que você deletar os arquivos '.pyc', os seus programas vão continuar sendo executados. 

A **portabilidade** de uma linguagem se reflete no número de compiladores ou interpretadores disponíveis, ou seja, quanto maior a portabilidade, maior será o número de máquinas onde um programa escrito na linguagem pode ser executado. O Python por exemplo é uma linguagem facilmente portável pois há máquinas virtuais para todos os principais sistemas operacionais modernos como Linux, Mac OS, Windows, Android, iOS etc.

Os programas muito simples em Python, como os primeiros programas que você vai escrever nesse curso, são chamados de **scripts**. 
Em geral, para desenvolver scripts, não precisamos de ferramentas muito poderosas. No entanto, o Python é uma linguagem poderosa que permite o desenvolvimento de softwares bem complexos. Para projetos maiores, usamos ferramentas mais poderosas chamadas de **IDE** (*Integrated Development Environment*) ou ambiente integrado de desenvolvimento de programas. Esses ambientes reúnem diversas ferramentas como ilustrado nas figuras 1.2 e 1.3, como editores de texto, terminais para teste dos programas, depuradores etc. Nesse curso, vamos adotar o IDE Spyder que acompanha o pacote Anaconda. O uso do Spyder é tratado no laboratório do capítulo 3 desse livro.


.. Além do tamanho dos dados, é importante também saber que cada dado é armazenado em um *endereço* numérico. De forma simplificada, vamos assumir que uma "instrução do programa" é um dado na memória, e essas instruções estão em endereços sequenciais. 

.. ideia do computador a papel

.. Para ilustrar como um computador baseado na arquitetura de von Neumann executa um programa, vamos fazer uma simulação passo-a-passo de um programa que soma uma sequência de números. Esse exemplo é baseado no `Computador à Papel <>`_ do Prof. Setzer. 

..    Exemplo de um programa
    .......................
    Vamos considerar que o computador tenha 16 posições de memória numeradas de 0 a 15. Vamos indicar cada posição usando dois algarismos precedidos pela letra "M", como `M00`, `M01`, até `M15`.
    Lembre-se em que cada posição podemos armazenar dados ou instruções do programa. 
    Vamos usar colchetes como em `[M03]` para indicar o **conteúdo** armazenado na posição `M03` da memória.  
    Um registrador importante da UCP que possibilita a execução da sequência correta de instruções é chamado de *contador de programa* (CP). O CP simplesmente armazena o local da memória (endereço) que contém a próxima instrução a ser executada. Dizemos que o CP "aponta" para a próxima instrução a ser executada.
    Dessa forma a unidade de controle da UCP "busca" a instrução indicada pelo CP na posição correspondente na memória, a decodifica e decide se a executa ou passa para a unidade lógica e aritmética (ULA) executar. Após terminar de executar a instrução, o CP é atualizado para que a unidade de controle busque a próxima instrução. O computador fica simplesmente executando esse ciclo:
    --> busca -> executa -> atualiza CP -> busca -> ...
    ininterruptamente. Apesar de cada instrução ser relativamente simples, um computador pessoal moderno é capaz de executar bilhões de instruções por segundo, que lhe confere o poder de realizar tarefas bastante complexas.
    .. Esse poder tem permitido que computadores substituam pessoas em tarefas repetitivas e entediantes, permitindo que as pessoas tenham mais tempo livre para as atividades criativas.
    Para simplificar nossa notação, vamos assumir que a UCP tem um registrador `AC` (de acumulador) que é utilizado para realizar cálculos. Vamos indicar o conteúdo do acumulador usando colchetes também, como em `[AC]`.
    A tabela abaixo mostra um programa (lista de instruções) armazenadas nas posições de memória de `M00` a `M15`.
    | end   | conteúdo   | abreviatura |
    | :--- | :--- | :--- |
    | `00`    | **carregue** 0 no `AC`                     | `AC  <-- 0` | 
    | `01`    | **armazene** o `[AC]` no `M15`             | `M15 <-- [AC]` | 
    | `02`    | **leia** um número e armazena no `M14`     | `M14 <-- leia` | 
    | `03`    | **escreva** `[M14]`                        | **escreva**  `[M14]` |
    | `04`    | **carregue** no `AC` o `[M14]`             | `AC <-- [M14]` |
    | `05`    | **desvie** para `M12` se `[AC]=0`          | **desvie** para `M12` se `[AC]=0` |
    | `06`    | **carregue** no `AC` o [M15]               | `AC <-- [M15]` |
    | `07`    | **some** `[AC]` ao `[M4]` e                | `AC <-- [AC] + [M]` |
    | `07`    | **some** `[AC]` ao `[M5]` e                | `AC <-- [AC] * [M]` |
    |         | **armazene** resultado no `AC`             |                          |
    | `08`    | **armazene** `[AC]` no `M15`               | `M15 <-- [AC]` |  
    | `09`    | **leia** um número e armazene no `M14`     | `M14 <--` **leia** |
    | `10`    | **escreva** `[M14]`                        | **escreva** `[M14]` |
    | `11`    | **desvie** para o `M04`                    | **desvie** para o `M04` | 
    | `12`    | **escreva** `[M15]`                        | **escreva** `[M15]` |
    | `13`    | **pare**                                   | **pare** |
    | `14`    | `??????`                                   |          |
    | `15`    | `??????`                                   |          |

..    Simulação do Programa
    ......................
    Vamos tentar entender o que esse programa faz, simulando o seu comportamento para uma sequência de números como 2, 3, 4, 5 e 0.
    Ao iniciar a execução, considere que o registrador CP esteja apontando para `M00`. O computador busca a instrução na memória e ao decodificá-la, identifica que a instrução corresponde a "carregue 0 no AC". A coluna da direita é uma abreviação onde o sentido da seta `<--` indica que o valor 0 (zero) está sendo movido para `AC`. 
    Após executar essa instrução, o conteúdo de AC deve ser zero e CP é atualizado para apontar para `M01`. 

