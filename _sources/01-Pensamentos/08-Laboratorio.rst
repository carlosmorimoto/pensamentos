
..  Copyright (C) Carlos Hitoshi Morimoto e José Coelho de Pina Jr.
    É permitida a cópia, distribuição e/ou modificação desde documento
    sob os termos da Licença de Documentação Livre GNU (GNU Free 
    Documentation License) versão 1.3 ou versão subsequente
    publicada pela Fundação de Software Livre (Free Software 
    Foundation); com as seções invariantes sendo Prefácio e 
    Lista de Contribuidores, sem texto de capa ou contra-capa.  
    Uma cópia da licença está disponível em:
    https://www.gnu.org/licenses/licenses.pt-br.html#FDL


.. qnum::
	:prefix: cap01-08
	:start: 1



Laboratório: Como instalar Python em seu computador
---------------------------------------------------

O desenvolvimento do raciocínio computacional requer muita
prática. Para isso, cada capítulo traz uma seção de exercícios em
laboratório que você deve realizar utilizando um computador para
praticar.  Nesse primeiro capítulo, vamos ajudar você a arrumar o seu
ambiente de programação e testá-lo, para que você possa fazer os
exercícios de laboratório dos próximos capítulos.  A instalação também
é um bom exercício inicial para as alunas e alunos sem prévia
experiência com computadores e serve para que eles e elas comecem a se
habituar com o uso de computadores e das ferramentas de
desenvolvimento de programas que serão utilizadas nos exercícios.


Instalando o Python
...................

.. index:: Anaconda

Vamos utilizar a distribuição `Anaconda <https://www.anaconda.com/>`__ (`Wikipedia <https://en.wikipedia.org/wiki/Anaconda_(Python_distribution)>`__)
que reúne várias ferramentas úteis para programação científica e ciência de dados.
Como historicamente a maioria de nossas alunas e alunos costumam utilizar um computador com o sistema operacional Windows,
as instruções a seguir são voltadas para essa plataforma.
Caso você use um outro sistema operacional, como Linux ou MacOS,
você pode seguir as instruções específicas para o seu sistema na
`página de instalação da Anaconda  <https://docs.anaconda.com/anaconda/install/>`__.

.. figure:: ../Figuras/anaconda/anaconda_pagina_inicial.png
   :width: 600
   :align: center
   :name: org
          
   Página inicial da distribuição Anaconda.

Antes de instalar, é preciso baixar os programas da distribuição `
Anaconda <https://en.wikipedia.org/wiki/Anaconda_(Python_distribution)>`__.
Para isso, no seu navegador favorito, Firefox, Chrome, Edge, ...,  siga para a página de *download* da Anaconda `<https://www.anaconda.com/distribution>`_).
Dependendo do seu navegador ou da versão do seu navegados, as mensagens apresentadas podem ser diferentes das mostradas nas imagens a seguir.
Sugerimos ainda que você salve o arquivo para
evitar fazer o download novamente caso algo de errado aconteça durante
a instalação ``;-)``.


Inicialmente clique no botão ``Download`` para baixar o *programa instalador* da distribuição da Anaconda,
que contém o ``Python`` e outros programas que serão úteis no futuro.

.. figure:: ../Figuras/anaconda/anaconda_inicio.png
   :width: 600
   :align: center
   :name: comsalvar
          
   Clique no botão ``Download``.
            
Em seguida, selecione a distribuição para o seu sistema, Windows, MacOS ou Linux.
No nosso exemplo selecionamos Windows.

.. figure:: ../Figuras/anaconda/anaconda_sistemas.png
   :width: 600
   :align: center

   Em Windows clique em ``64-Bit Graphical Installer (457 MB)``.

O tempo gasto para baixar o instalador junto com os demais programas da Anaconda depende da velocidade da sua conexão e pode demorar alguns minutos.
Vá tomar um café.
Em geral, os navegadores apresentam quanto já foi baixado ou uma estimativa do tempo que resta para finalizar o processo.
A imagem a seguir mostra o início do processo no Chrome. no canto inferior esquerdo mostra que foram baixados ``2.7 MB`` de ``457 MB``.

.. figure:: ../Figuras/anaconda/anaconda_downloading.png
   :width: 600
   :align: center

   Arquivo de instalação ``anaconda3-2020.11-Windows-x86_64.exe`` sendo baixado pelo Chrome.

Depois que terminar de baixar, clique no arquivo que foi baixado para executar que o instalador comece a trabalhar.
A imagem a seguir mostra a janela de execução exibida pelo Firefox.

.. figure:: ../Figuras/anaconda/anaconda_mozilla.png
   :width: 400
   :align: center

   Janela de execução do ``anaconda3-2020.11-Windows-x86_64.exe`` no Firefox.


Deste ponto você passará a se comunicar com o programa instalador.
Siga as opções sugeridas pelo instalador.

Na janela de boas-bindas apresentada pelo instalador, clique em ``Next``.

.. figure:: ../Figuras/anaconda/anaconda_boas_vindas.png
   :width: 400
   :align: center

   Boas-vindas do instalador.


Para prosseguir é necessário que estejamos de acordo com os termos da  licença da Anaconda,
clique em ``I agree``.


.. figure:: ../Figuras/anaconda/anaconda_licenca.png
   :width: 400
   :align: center

   Licença da Anaconda.

 
Agora chegou o momento de selecionarmos o tipo da instalação.
Siga a recomendação do instalador para instalar apenas para vocês, selecione ``Just me (recommended)`` e clique em ``Next``.

   
.. figure:: ../Figuras/anaconda/anaconda_tipos_instalacao.png
   :width: 400
   :align: center

   Tipos de instalação.        

Desta vez o instalador está recomendando o nome da pasta em que a Anaconda será instalada.
Sugerimos que você aceite a recomendação do instalador e clique em ``Next``.
Para selecionar uma outra localização, clique em ``Browser``, navegue pelo seu computador a procura da pasta destino. 
Depois de escolher o nome da pasta, clique em ``Next``.

.. figure:: ../Figuras/anaconda/anaconda_destino.png
   :width: 400
   :align: center

   Pasta em que a Anaconda será instalada.        


A recomendação agora é que o ``Python 3.8`` da Anaconda ser o ``Python`` usado pelo seu computador.
Clique em ``Intall`` para que a instalação ser iniciada. 

.. figure:: ../Figuras/anaconda/anaconda_opcoes.png
   :width: 400
   :align: center

   Opções avançadas.        

A instalação pode demorar alguns minutos. Vá tomar outro café.
Ao final, quando a instalação o processo de instalação tiver sido completado, o instalador apresentará a janela abaixo.
Clique em ``Next``.

.. figure:: ../Figuras/anaconda/anaconda_complete.png
   :width: 400
   :align: center

   Janela mostrando que a instalação está completa.

Pronto!
O instalador fará mais algumas recomendações de programas a serem instalações.
Você pode continuar a clicar em ``Next`` até fechar o instalador da Anaconda.

.. figure:: ../Figuras/anaconda/anaconda_jupyter.png
   :width: 400
   :align: center

   Mais recomendações do instalador.
   
O instalador agradece, dá mais alguma dicas de informações para serem examinadas mais tarde.
Clique em ``Finish`` para terminar.
Ufa!

.. figure:: ../Figuras/anaconda/anaconda_finish.png
   :width: 400
   :align: center

   Mais dicas do instalador.


Teste a sua instalação usando o Anaconda Prompt e o IPython
...........................................................

.. index:: Anaconda prompt, terminal, IPython

Clique na barra de pesquisa do Windows e digite ``Anaconda Prompt``. Dentre as primeiras opções deve estar o 
``Anaconda Prompt (Anaconda 3)`` que você acabou de instalar, como é mostrado na imagem abaixo.

.. figure:: ../Figuras/anaconda/anaconda.pesquisa.png
   :width: 600
   :align: center

   Procura pelo ``Anaconda prompt``        
           
           
Clique nessa opção ``Anaconda Prompt`` para abrir uma janela 
como a mostrada na imagem abaixo.
Essa janela é comumente chamada de *terminal* ou *shell* da Anaconda.

.. figure:: ../Figuras/anaconda/anaconda_shell.png
   :width: 600
   :align: center

   Terminal ou shell da Anaconda.
   
No terminal, digite ``ipython`` para abrir o `IPython <https://ipython.readthedocs.io/en/stable/>`_ (*Interactive Python interpreter*).
O ``IPython``  é um outro  terminal ou shell que permite você interativamente execute comandos em ``Python``.

.. figure:: ../Figuras/anaconda/anaconda_ipython_prompt.png
   :width: 600
   :align: center

   Terminal ou shell do ``IPython`` logo após ter sido iniciado.

Após iniciar o ``IPython`` o programa apresentará, além de outras informação, o seu ``prompt`` deixando claro que está preparado ou *pronto* para
receber comandos a serem interpretados.
Na imagem acima o ``prompt`` é o texto ``In [1]:``.
A palavra *interativamente* significa para cada comando digitado o ``IPython`` apresentará imediatamente uma resposta.
O ``IPython`` é muito útil para testar o comportamento de comandos e também serve como uma calculadora.
Por exemplo, digitando a expressão ``2 + 2`` ou qualquer operação aritmética que desejar, seguida por ``ENTER`` o resultado é o apresentado na próxima imagem.

.. figure:: ../Figuras/anaconda/anaconda_ipython_soma.png
   :width: 600
   :align: center

   Resultado da operação ``2 + 2`` no ``IPython``.

Após mostrar a resposta ``Out [1]: 4`` para ``In [1]: 2 + 2``, o ``IPython`` exibe novamente o seu ``prompt``,  avisando que está preparado para
a próxima tarefa. Como essa será a segunda tarefa desta sessão, o novo ``prompt`` que o ``IPython`` exibirá é ``In [2]:``.
É esse o comportamento de um terminal ou um shell.
Toda vez que você digita algo após o ``prompt`` do ``IPython`` e em seguida tecla ``ENTER``,
o novo comando será  interpretado e a resposta ou resultado do comando será apresentada.

Se você enviar um comado incorreto ou incompleto, como ``2 *`` seguido de ``ENTER``, o
``IPython`` como todo bom terminal, exibirá uma mensagem avisando do erro.

.. figure:: ../Figuras/anaconda/anaconda_ipython_erro.png
   :width: 600
   :align: center

   Erro resultante de ``2 *`` no ``IPython``.


No caso a mensagem é ``SyntaxError: invalid sintax``, que  indica que o comando foi escrito de forma errada e que, portanto,
não foi compreendido. 
.. Note que é importante você não inserir espaços em branco ou tabs no início da linha para evitar erros de sintaxe (veremos mais tarde a razão).
Para interpretar os comandos, o  ``IPython`` é diretamente auxiliado pelo ``Python``.
O ``IPython`` trabalha, digamos, como um garçon que leva os pedidos feitos pelos clientes para que a chefe de cozinha ``Python`` prepare os pratos.
Se a chefe ``Python`` não entende algum pedido, diz isso ao garçon ``IPython`` que por sua vez avisará o cliente sobre o pedido incompreensível.

Exploraremos muito mais o ``IPython`` e o seu modo *interativo* como uma *calculadora*  no laboratório do próximo capítulo.



