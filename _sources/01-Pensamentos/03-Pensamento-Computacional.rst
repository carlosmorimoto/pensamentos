
..  Copyright (C) Carlos Hitoshi Morimoto e José Coelho de Pina Jr.
    É permitida a cópia, distribuição e/ou modificação desde documento
    sob os termos da Licença de Documentação Livre GNU (GNU Free 
    Documentation License) versão 1.3 ou versão subsequente
    publicada pela Fundação de Software Livre (Free Software 
    Foundation); com as seções invariantes sendo Prefácio e 
    Lista de Contribuidores, sem texto de capa ou contra-capa.  
    Uma cópia da licença está disponível em:
    https://www.gnu.org/licenses/licenses.pt-br.html#FDL


.. qnum::
	:prefix: cap01-03
	:start: 1



Pensamento computacional
------------------------

É difícil imaginar hoje um ramo da atividade humana que não tenha se beneficiado diretamente dos avanços da computação. Seus impactos são tão notáveis que não precisamos ir longe para perceber seus efeitos.  A começar talvez pelos dispositivos móveis como os telefones celulares rodando inúmeros aplicativos que, para o bem ou para o mal, vem transformando comportamentos pessoais, sociais e até criando alguns novos comportamentos. Esse exemplo de computação móvel mostra que um dispositivo "comum" como um telefone, ao se tornar digital e conectado à Internet, pode se tornar coisas bem diferentes. Por ser programável, a flexibilidade e utilidade desses dispositivos aumenta a cada nova aplicação que instalamos nessa "coisa", ou seja, a coisa telefone vira outra coisa, como agenda, tv, ou vídeo game.

.. Uma outra tecnologia que começa a ser notada a nível pessoal é a Internet das Coisas ou IoT (do inglês *Internet of Things*). A IoT vem permitindo que "coisas" se tornem mais "inteligentes", desde uma lâmpada que pode ser controlada remotamente pela Internet, até a integração de "coisas" em toda uma cidade. Dessa forma, as cidades inteligentes poderão otimizar melhor seus recursos, como por exemplo melhoria no trânsito, maior conservação de energia e redução de poluentes. 

Assim, à medida que mais coisas se tornam digitais, conectadas à Internet e programáveis, abrem-se infinitas novas oportunidades de uso, tanto de coisas agindo individualmente, quanto coisas agindo em conjunto com outras coisas. Você provavelmente já faz uso de muitas dessas coisas. Aprender um pouco de computação pode até ajudar você a usar essas coisas de forma mais eficaz, mas o principal objetivo desse livro é que você será capaz de construir suas próprias soluções computacionais (coisas) para problemas simples. Para isso vamos orientar você na aquisição de conhecimentos e desenvolvimento de habilidades que vamos chamar de **pensamento computacional**. 


Mas o que é pensamento computacional?
.....................................

Tradicionalmente, cursos de introdução a computação tem grande foco no aprendizado de uma linguagem de programação. Nosso curso vai ser um pouco diferente no sentido de usar apenas recursos básicos da linguagem para resolver problemas computacionais inicialmente simples e que irão se tornando cada vez mais complexos. Nosso propósito é remover recursos "mágicos" oferecidos pelas linguagens modernas de programação para exercitar e desenvolver sua habilidade de resolver problemas usando o seu próprio raciocínio, ao invés, por exemplo, de buscar uma solução "pronta" na Internet.
Para isso nos concentraremos no desenvolvimento do `pensamento computacional <http://en.wikipedia.org/wiki/Computational_thinking>`_, que é um certo raciocínio aplicado na formulação e resolução de problemas computacionais.

A descrição da resolução de problemas depende de uma linguagem de programação. 
Como nosso foco não é na linguagem em si, poderíamos a princípio adotar qualquer linguagem para desenvolver o pensamento, mas 
adotamos a linguagem Python por ser moderna, portável, fácil de instalar, de sintaxe simples e ainda poderosa para lhe ser útil na resolução de seus próprios problemas computacionais.

A construção das habilidades cada vez mais complexas só se tornam sólidas quando apoiadas em outras habilidades sólidas e já bem fundamentadas. Por isso muitos exercícios que você verá nesse curso são para desenvolver ferramentas e recursos antes de aplicá-las, para exercitar as ideias, mesmo quando alguns desses recurso já estejam "prontos" e disponíveis na linguagem. Ao exercitar o pensamento para remover a mágica desses recursos, você ganha em conhecimento e competência.

Frequentemente é possível identificar alguns dos seguintes componentes nesse raciocínio ou pensamento:

* **decomposição**: quebrar um problema complexo em subproblemas que sejam idealmente mais simples;
* **reconhecimento de padrões**: buscar similaridades entre os subproblemas que permitam que estes sejam resolvidos de formas semelhantes;
* **representação dos dados**: determinar as características do problema necessárias para sua solução; e
* **algoritmos**: sequência de passos para resolver o problema.

Em particular, quebrar um problema em subproblemas mais simples e a procura de similaridades não são estratégias somente utilizadas em programação. Essas são rotinas aplicadas na solução diária de problemas de diversas origens e não são as únicas.
A criatividade de cada uma e cada um nos leva frequentemente a soluções muito engenhosas.

Não se trata de acertar ou errar.
Se trata de nos tornarmos melhores pensadores.
Para treinar nosso pensamento começaremos sempre da análise e compreensão de um problema.
Passaremos à construção de um roteiro para sua solução.
Esse roteiro ou *script* é o nosso programa que, ao ser executado em um computador, nos leva a uma
solução.


.. colocar figura? 
    ```
    +-----------+              +------------+             +----------+            
    |           |              |            |             |          |  
    | Problema  | -----------> | Computador | ----------> | Solução  |  
    |           |              |            |             |          |  
    +-----------+              +------------+             +----------+  
                                    ^                      
                                    |
                                    |
                            programa/roteiro/script
                                    ^ 
                                    |
                                    |
                                raciocício
                                    ^
                                    |
                                    |
                                    nóis
    programa = sequência de passos/ordens 
    Ato de programar é uma ferramenta útil
    ```


Na próxima seção vamos introduzir outros conceitos sobre computadores e computação para que possamos entender melhor a natureza dos problemas computacionais e das suas soluções.


.. Aprender a resolver problemas programar novos usos de essas coisas é certamente um desafio que, quem sabe, você mesmo vai ajudar a resolver usando algumas das habilidades que esse livro pode ajudar você a desenvolver.



.. Mas não precisamos ser tão ambiciosos. Para alunos e alunas de engenharia e ciências exatas por exemplo, as ferramentas computacionais cobertas nesse livro podem ajudar a entender conceitos de cálculo e estatística, visualizar dados multidimensionais e simular modelos. Alunas e alunos das áreas humanas e biológicas também podem utilizar as habilidades de pensamento computacional desenvolvidas nesse livro para começar a explorar seus dados e testar seus próprios modelos. Mas seja qual for a sua área de interesse, desenvolver um raciocínio computacional vai ajudar você a desenvolver melhores programas assim como pode ajudar você a utilizar programas existentes de forma mais eficaz.


