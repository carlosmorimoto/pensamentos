
..  Copyright (C) Carlos Hitoshi Morimoto e José Coelho de Pina Jr.
    É permitida a cópia, distribuição e/ou modificação desde documento
    sob os termos da Licença de Documentação Livre GNU (GNU Free 
    Documentation License) versão 1.3 ou versão subsequente
    publicada pela Fundação de Software Livre (Free Software 
    Foundation); com as seções invariantes sendo Prefácio e 
    Lista de Contribuidores, sem texto de capa ou contra-capa.  
    Uma cópia da licença está disponível em:
    https://www.gnu.org/licenses/licenses.pt-br.html#FDL


.. qnum::
	:prefix: cap01-06
	:start: 1



Um pouco mais sobre design
--------------------------

A palavra **design** é usada (abusada?) por diferentes áreas do conhecimento. Nesse livro, vamos usar **design** para indicar o processo que envolve um pouco de arte, de ciência, de técnica, de experiência, de criatividade, de exploração de alternativas etc, 
na busca de uma solução algorítmica para um problema computacional.

O processo de design se inicia pelo entendimento do problema. Muitos dos problemas que veremos nesse curso possuem uma entrada e saída bem definidos. Isso facilita o design pois isso nos permite focar em processos que transformam os dados de entrada na saída.
Em geral, há vários processos distintos possíveis que levam ao resultado correto. Ao longo do curso vamos trabalhar com o conceito de eficiência computacional para medir o uso de recursos (como tempo e memória) necessários para executar cada algoritmo.

A escolha e definição do algoritmo a ser implementado (codificado usando uma linguagem de programação) é o objetivo do design. 
Por enquanto, vamos usar a palavra **algoritmo** para indicar uma sequência lógica das etapas que conduzem ao resultado correto. 
Os algoritmos são tipicamente hierárquicos, ou seja, cada etapa pode ser descrita com um nível de detalhes diferente. 

Uma forma comum para desenvolver um algoritmo é conhecido como *top-down* (de cima para baixo). 
Para pensar de forma top-down começamos desenhando a solução usando "módulos grandes" e vamos decompondo cada "módulo" até ficar claro que sabemos codificar aquele módulo. 

Algumas vezes é possível chegar na solução de forma *bottom-up* (de baixo para cima). Costumamos pensar desse jeito quando sabemos resolver certos pedaços e queremos usar esses pedaços na solução. Ao usar essa forma de pensamento, tome muito cuidado para que esse desejo não se torne obsessão pois, muitas vezes, forçar o uso de algum pedaço pode conduzi-lo a um caminho errado.

Um outro problema comum que encontramos em programadores menos experientes mas que acontece com programadores experientes também é a empolgação por um certo design que foge do problema. Talvez porque algumas vezes a gente se apaixona por certas ideias, seja pela beleza, elegância, criatividade etc, que torna difícil abandoná-las. Por isso os testes são fundamentais para nos manter com os pés no chão e no caminho correto. 

Para você que é programador iniciante no entanto, o maior desafio está na codificação. Para vencer esse desafio vamos codificar bastante para que a sintaxe da linguagem (Python) se torne natural, e vamos também fazer exercícios usando as ferramentas de programação como o editor, o terminal, o depurador etc, que constituem o IDE. 

