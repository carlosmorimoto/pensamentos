
..  Copyright (C) Carlos Hitoshi Morimoto e José Coelho de Pina Jr.
    É permitida a cópia, distribuição e/ou modificação desde documento
    sob os termos da Licença de Documentação Livre GNU (GNU Free 
    Documentation License) versão 1.3 ou versão subsequente
    publicada pela Fundação de Software Livre (Free Software 
    Foundation); com as seções invariantes sendo Prefácio e 
    Lista de Contribuidores, sem texto de capa ou contra-capa.  
    Uma cópia da licença está disponível em:
    https://www.gnu.org/licenses/licenses.pt-br.html#FDL


.. qnum::
	:prefix: cap01-09
	:start: 1



Teste seus conhecimentos
------------------------

Indique todas as alternativas corretas

.. mchoice:: questao_dois_kilobytes
    :answer_a: aproximadamente dois mil bits
    :answer_b: aproximadamente quatro mil bits
    :answer_c: aproximadamente oito mil bits
    :answer_d: aproximadamente dezesseis mil bits
    :answer_e: nenhuma das alternativas anteriores 
    :correct: d
    :feedback_a: Incorreto: 1 byte possui 8 bits
    :feedback_b: Incorreto: 1 byte possui 8 bits
    :feedback_c: Incorreto: 1 byte possui 8 bits
    :feedback_d: Correto: 1 byte possui 8 bits
    :feedback_e: Incorreto: 1 byte possui 8 bits

    Quantos bits de informação podemos armazenar em 2 KB (dois kilobytes)?


.. mchoice:: questao_nivel_linguagem_python
    :answer_a: uma linguagem de programação de baixo nível
    :answer_b: uma linguagem de programação de alto nível
    :answer_c: uma linguagem de máquina
    :answer_d: uma linguagem de programação interpretada
    :correct: b, d
    :feedback_a: Incorreto: uma linguagem de baixo nível usa código que roda diretamente (ou quase) na máquina.
    :feedback_b: Correto: um programa em Python parece um texto em inglês.
    :feedback_c: Incorreto: os computadores não conseguem executar diretamente programas em Python. 
    :feedback_d: Correto: um programa em Python não é compilado para linguagem da máquina hospedeira.

    O Python é:


.. mchoice:: questao_python_shell
    :answer_a: uma interface que permite escrever comandos do Python e ver o resultado.
    :answer_b: um ambiente integrado de desenvolvimento de programas em Python.
    :answer_c: uma ferramenta que permite a execução de comandos em Python e pode ser integrada com outras ferramentas.
    :answer_d: uma casca para proteger o Python conta ataques.
    :answer_e: uma concha onde o Python fica armazenado.
    :correct: a, c
    :feedback_a: Correto
    :feedback_b: Incorreto: o Python shell não é um IDE.
    :feedback_c: Correto: como ocorre no Spyder
    :feedback_d: Incorreto: o shell se refere a uma interface por linha de comando.
    :feedback_e: Incorreto: o shell se refere a uma interface por linha de comando.

    O IPython é:



