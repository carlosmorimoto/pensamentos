
..  Copyright (C) Carlos Hitoshi Morimoto e José Coelho de Pina Jr.
    É permitida a cópia, distribuição e/ou modificação desde documento
    sob os termos da Licença de Documentação Livre GNU (GNU Free 
    Documentation License) versão 1.3 ou versão subsequente
    publicada pela Fundação de Software Livre (Free Software 
    Foundation); com as seções invariantes sendo Prefácio e 
    Lista de Contribuidores, sem texto de capa ou contra-capa.  
    Uma cópia da licença está disponível em:
    https://www.gnu.org/licenses/licenses.pt-br.html#FDL


.. qnum::
	:prefix: cap01-07
	:start: 1

 

Computador x computação
-----------------------

.. index:: hardware, software, linguagem de máquina, linguagem de baixo nível, linguagem de alto nível, código fonte


.. raw:: latex

    \begin{flushright}
    {
    "O computador está para a computação assim como o telescópio está para a astronomia." \\
    \href{https://en.wikiquote.org/wiki/Computer\_science}{Edsger Dijkstra} \\
    }
    \end{flushright}
    \vspace{10mm}


.. raw:: html

    <div align="right">
    <p></p>
    <blockquote>
    <p>"O computador está para a computação assim como o telescópio está para a astronomia."</p>
    <p><a href="https://en.wikiquote.org/wiki/Computer_science">Edsger Dijkstra</a></p>
    </blockquote>
    <p></p>
    </div>


Um computador é uma máquina que precisa de hardware e software para funcionar. Chamamos de **hardware** a parte física dos computadores. A parte digital responsável pelo funcionamento da parte física (os programas de uma forma geral) é chamada de **software**. O objetivo de programar é criar software para resolver algum problema computacional, que pode ser um simples cálculo matemático ou algo um pouco mais complexo como controlar a navegação de um foguete até a Lua. 
A atividade de desenvolver esses programas é chamada de **programação**. 

Como vimos, tradicionalmente os cursos de introdução à computação tem um foco grande em alguma linguagem de programação. 
Mas a (ciência da) computação vai além da programação. Como ciência, ela estuda os fundamentos teóricos da informação (dados) e da computação (transformação dos dados) e as considerações práticas para aplicar essa teoria em sistemas computacionais. Por exemplo, nesse curso vamos estudar formas para representar diferentes tipos de dados e algoritmos eficazes para manipular esses dados. Consideramos o estudo dessas estruturas e algoritmos uma excelente forma de desenvolver um raciocínio computacional, algo parecido com estudar partidas entre grandes mestres para aprender a jogar xadrez. 

