
..  Copyright (C) Carlos Hitoshi Morimoto e José Coelho de Pina Jr.
    É permitida a cópia, distribuição e/ou modificação desde documento
    sob os termos da Licença de Documentação Livre GNU (GNU Free 
    Documentation License) versão 1.3 ou versão subsequente
    publicada pela Fundação de Software Livre (Free Software 
    Foundation); com as seções invariantes sendo Prefácio e 
    Lista de Contribuidores, sem texto de capa ou contra-capa.  
    Uma cópia da licença está disponível em:
    https://www.gnu.org/licenses/licenses.pt-br.html#FDL


.. qnum::
	:prefix: cap01-02
	:start: 1



Para onde vamos
---------------

Nesse primeiro capítulo, vamos apresentar e discutir rapidamente alguns conceitos sobre computação e sua relação com computadores e linguagens de programação. 
Ao final desse capítulo você deve ser capaz de:

- explicar como funciona um computador baseado na arquitetura de von Neumann;
- descrever como dados e programas são armazenados na memória e definir o que é um bit, byte, kilobyte, megabyte etc.
- identificar linguagens de programação de baixo e de alto nível;
- descrever a diferença entre linguagens compiladas de linguagens interpretadas;   
- definir os conceitos de hardware, software, IDE, contexto de um programa, programação e computação.
- explicar o que é pensamento computacional e por que isso é diferente de programar.


