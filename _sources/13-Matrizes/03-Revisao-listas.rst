
..  Copyright (C) Carlos Hitoshi Morimoto e José Coelho de Pina Jr.
    É permitida a cópia, distribuição e/ou modificação desde documento
    sob os termos da Licença de Documentação Livre GNU (GNU Free 
    Documentation License) versão 1.3 ou versão subsequente
    publicada pela Fundação de Software Livre (Free Software 
    Foundation); com as seções invariantes sendo Prefácio e 
    Lista de Contribuidores, sem texto de capa ou contra-capa.  
    Uma cópia da licença está disponível em:
    https://www.gnu.org/licenses/licenses.pt-br.html#FDL


.. qnum::
	:prefix: cap13-03
	:start: 1



Revisão de listas
-----------------

O que é impresso pelo seguinte trecho de código:

.. activecode:: aula11_apelido_0

    def main(): 
        a = [0, 1, 2, 3, 4]
        b = a # b e apelido para a 
        b[1] = 7 
        print("a = ", a)
        print("b = ", b)  

    main()


Vejamos o código acima pela ótica do *codelens*:

.. codelens:: aluna11_apelido_1

    def main(): 
        a = [0, 1, 2, 3, 4]
        b = a # b e apelido para a 
        b[1] = 7 

    main()


No código acima ``b`` é  uma *referência* ou apelido (= *alias*) para a mesma lista a que ``a`` está se referenciando.
    
O trecho de código a seguir cria um *cópia* (= *clone*) de ``a`` e ``b`` será uma referência a essa cópia.

.. activecode:: aula11_clone_0

    def main():
        a = [0, 1, 2, 3, 4]
        b = a[:]
        b[1] = 7
        print("a = ", a)
        print("b = ", b)  
  
    main()

Vejamos o que o *codelens* tem a dizer sobre o trecho de código acima.


.. codelens:: aula11_clone_1

    def main():
        a = [0, 1, 2, 3, 4]
        b = a[:]
        b[1] = 7

    main()

A fatia ``a[:]`` é uma cópia de ``a``, uma *fatia* com todos os elementos de ``a``.
Lembre-se que o ``:`` é utilizado para definir uma fatia da lista.
Por exemplo,  ``a[1:3]`` produz a lista ``[1,2]``.
    

Criação de matrizes
...................

Matrizes são estruturas bidimensionais (tabelas) com ``m`` linhas por
``n`` colunas muito importantes na matemática, utilizadas por exemplo
para a resolução de sistemas de equações e transformações lineares.

Em Python, uma matriz pode ser representada como uma lista de listas,
onde um elemento da lista contém uma linha da matriz, que por sua vez
corresponde a uma lista com os elementos da coluna da matriz.

.. admonition:: NumPy - Numerical Python

    Dizem que, quando você usa Python, as baterias vem incluídas, pois há vários módulos a sua disposição. Um deles é o [NumPy](http://www.numpy.org/), que facilita operações com matrizes e torna o seu processamento mais eficaz. Mas usar matrizes como listas de listas é um recurso muito útil, inclusive para aprender mais sobre listas.

Qual o problema do seguinte pedaço de código para criação de uma
matriz ``A`` com 5 linhas e 5 colunas com o valor 2 na posição ``[1][1]``
e zero nas demais posições?

.. codelens:: crie_matriz_0

     linha_com_zeros = [0]*5
     A = [ linha_com_zeros ] * 5
     A[1][1] = 2

A variável ``linha_com_zeros`` contém uma referência
à lista ``[0, 0, 0, 0, 0]``.
No trecho de código acima, na tentativa de cria uma matriz ``A``, 
essa mesma referência é copiada 5 vezes. Assim, todas as linhas apontam para a mesma lista!

Para criarmos uma matriz é necessário criarmos 5 listas diferentes (uma para cada linha) como por exemplo:

.. codelens:: crie_matriz_1

    A = []
    for i in range(5):
        A = A + [[0]*5] # cria uma nova lista [0]*5
    A[1][1] = 2

Podemos ainda criar uma matriz de zeros utilizando uma função como 

.. codelens:: crie_matriz_2

    def crie_matriz(n_linhas, n_colunas, valor):
        ''' (int, int, valor) -> matriz (lista de listas)

        Cria e retorna uma matriz com n_linhas linha e n_colunas
        colunas em que cada elemento é igual ao valor dado.
        '''

        matriz = [] # lista vazia
        for i in range(n_linhas):
            # cria a linha i
            linha = [] # lista vazia
            for j in range(n_colunas):
                linha += [valor]

            # coloque linha na matriz
            matriz += [linha]

        return matriz

    #-----------------------
    A = crie_matriz(5,5,0)
    A[1][1] = 2

Veja a seguir uma versão **errada** da função ``crie_matriz()``.
Nessa versão cada linha da matriz é um apelido de uma *mesma* 
lista.


.. codelens:: crie_matriz_errada

    def crie_matriz_errada(n_linhas, n_colunas, valor):
        ''' (int, int, valor) -> matriz (lista de listas)

        Cria e retorna uma matriz com n_linhas linha e n_colunas
        colunas em que cada elemento é igual ao valor dado.
        '''

        matriz = [] # lista vazia
        # cria uma linha com valor

        linha = [] # lista vazia
        for j in range(n_colunas):
            linha += [valor]

        for i in range(n_linhas):

            # coloque linha na matriz
            matriz += [linha]

        return matriz

    #-----------------------
    A = crie_matriz_errada(3,4,0)
    A[1][1] = 2


