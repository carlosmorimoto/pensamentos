
..  Copyright (C) Carlos Hitoshi Morimoto e José Coelho de Pina Jr.
    É permitida a cópia, distribuição e/ou modificação desde documento
    sob os termos da Licença de Documentação Livre GNU (GNU Free 
    Documentation License) versão 1.3 ou versão subsequente
    publicada pela Fundação de Software Livre (Free Software 
    Foundation); com as seções invariantes sendo Prefácio e 
    Lista de Contribuidores, sem texto de capa ou contra-capa.  
    Uma cópia da licença está disponível em:
    https://www.gnu.org/licenses/licenses.pt-br.html#FDL


.. qnum::
	:prefix: cap13-02
	:start: 1



Vídeos
------

    - `Matrizes <https://www.youtube.com/watch?v=c9yjwWNiNQw&list=PLcoJJSvnDgcKpOi_UeneTNTIVOigRQwcn&index=31>`__
    - `Soma de matrizes <https://www.youtube.com/watch?v=x_hNj2C-kVs&list=PLcoJJSvnDgcKpOi_UeneTNTIVOigRQwcn&index=37>`__
    - `Multiplicação de matrizes <https://www.youtube.com/watch?v=gCKza8cYdJY&index=38&list=PLcoJJSvnDgcKpOi_UeneTNTIVOigRQwcn>`__
  
.. do capítulo 
   `Listas <https://python.ime.usp.br/pensepy/static/pensepy/09-Listas/>`__ 
   do livro interativo 
   `Como pensar como um Cientísta da Computação <https://python.ime.usp.br/pensepy/static/pensepy/index.html>`__.



