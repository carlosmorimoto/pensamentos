
..  Copyright (C) Carlos Hitoshi Morimoto e José Coelho de Pina Jr.
    É permitida a cópia, distribuição e/ou modificação desde documento
    sob os termos da Licença de Documentação Livre GNU (GNU Free 
    Documentation License) versão 1.3 ou versão subsequente
    publicada pela Fundação de Software Livre (Free Software 
    Foundation); com as seções invariantes sendo Prefácio e 
    Lista de Contribuidores, sem texto de capa ou contra-capa.  
    Uma cópia da licença está disponível em:
    https://www.gnu.org/licenses/licenses.pt-br.html#FDL


.. qnum::
	:prefix: cap13-04
	:start: 1



Exercícios
----------

Exercício 1
...........

Escreva a função a ``leia_matriz()`` a seguir.
Abaixo esta um exemplo com um passo a passo da execução da função.

.. sourcecode:: python

    >>> a = leia_matriz()
    Digite o número de linhas: 3
    Digite o número de colunas: 4
    matriz = []
    linha 0 = []
    Digite o elemento (0,0): 1
    linha 0 = [1]
    Digite o elemento (0,1): 2
    linha 0 = [1, 2]
    Digite o elemento (0,2): 3
    linha 0 = [1, 2, 3]
    Digite o elemento (0,3): 4
    linha 0 = [1, 2, 3, 4]
    matriz = [[1, 2, 3, 4]]
    linha 1 = []
    Digite o elemento (1,0): 5
    linha 1 = [5]
    Digite o elemento (1,1): 6
    linha 1 = [5, 6]
    Digite o elemento (1,2): 7
    linha 1 = [5, 6, 7]
    Digite o elemento (1,3): 8
    linha 1 = [5, 6, 7, 8]
    matriz = [[1, 2, 3, 4], [5, 6, 7, 8]]
    linha 2 = []
    Digite o elemento (2,0): 9
    linha 2 = [9]
    Digite o elemento (2,1): 10
    linha 2 = [9, 10]
    Digite o elemento (2,2): 11
    linha 2 = [9, 10, 11]
    Digite o elemento (2,3): 12
    linha 2 = [9, 10, 11, 12]
    matriz = [[1, 2, 3, 4], [5, 6, 7, 8], [9, 10, 11, 12]]
    >>> a
        
.. activecode:: aula_matriz_ex1_tentativa

    #-------------------------------------------------------
    def leia_matriz():
        '''(None) -> matriz (lista de listas)

        Funcao que le do teclado o numero n_linhas de linhas
        e o numero n_colunas de colunas e os elementos de
        uma matriz de inteiros dimensao n_linha x n_colunas. 

        A funcao cria e retorna a matriz lida.
        '''
        print("Vixe! Ainda nao fiz a funcao!")

    #-----------------------------------------------------
    # teste
    a_mat = leia_matriz()
    print(a_mat)
 

Exercício 2
...........

Escreva a função a abaixo:

.. activecode:: aula_matriz_ex2_tentativa

    #-----------------------------------------------------
    def imprima_matriz(matriz):
        '''(matriz) -> None

        Recebe e imprime uma matriz de inteiros.

        >>> a = [[1,2,3],[2,1,4],[3,4,1]]
        >>> a
        [[1, 2, 3], [2, 1, 4], [3, 4, 1]]
        >>> imprima_matriz(a)
        Matriz: 3 x 3
             1     2     3
             2     1     4
             3     4     1

        '''
        print("Vixe! Ainda nao fiz a funcao!")

    #-----------------------------------------------------
    # testes
    a = [[1,2,3],[2,1,4],[3,4,1]]
    imprima_matriz(a)

Exercício 3
...........

Escreva a função a abaixo:

.. activecode:: aula_matriz_ex3_tentativa

    #-----------------------------------------------------
    def simetrica(matriz):
        '''(matriz) -> bool

        Recebe uma matriz e returna True se a matriz for simetrica,
        em caso contrario retorna False.

        Pre-condicao: a funcao supoe que a matriz e quadrada

        >>> a = [[1,2,3],[2,1,4],[3,4,1]]
        >>> a
        [[1, 2, 3], [2, 1, 4], [3, 4, 1]]
        >>> imprima_matriz(a)
        Matriz: 3 x 3
             1     2     3
             2     1     4
             3     4     1
        >>> simetrica(a)
        True
        '''
        print("Vixe! Ainda nao fiz a funcao!")

    #-----------------------------------------------------
    # testes
    a = [[11, -3, 4, 8], [-3, 12, 6, 11], [4, 6, 5, 13], [8, 11, 13, 5]]
    if simetrica(a):
        print("Passou no primeiro teste! :-)")
    else:
        print("Nao passou no primeiro teste! :-(")


Exercício 4
...........

Escreva um programa que lê ``n`` e uma matriz ``A`` de inteiros 
de dimensão ``n x n``, e verifica se ``A`` é simétrica.

.. sourcecode:: python

          0     1     2     3
       +-----+-----+-----+-----+
    0  | 11  | -3  |  4  |  8  |
       +-----+-----+-----+-----+
    1  | -3  | 12  |  6  | 11  |
       +-----+-----+-----+-----+
    2  |  4  |  6  |  5  | 13  |
       +-----+-----+-----+-----+
    3  |  8  | 11  | 13  |  5  |
       +-----+-----+-----+-----+

Sugestões: utilize as funções ``leia_matriz()``, 
``imprima_matriz()`` e ``simetrica()`` dos exercícios anteriores.         


.. activecode:: aula_matriz_ex4_tentativa

    #-----------------------------------------------------
    def main():
        '''
        Programa que le n e uma matriz de inteiros n x n
        e verifica se a matriz e simetrica.
        '''
        # escreva o seu programa abaixo e remova o print()
        print("Vixe! Ainda nao fiz o exercicio!")

    #-----------------------------------------------------
    main()
         
     

Exercício 5
...........

Escreva um programa que leia inteiros positivos ``m`` e ``n`` e  
os elementos de uma matriz ``A`` de números inteiros de 
dimensão ``m x n``
e conta o número de linhas e colunas que tem apenas zeros.

.. sourcecode:: python

    Matriz: 4 x 5
         0     0     0     0     1
         0     0     0     0     0
         0     1     0     0     0
         0     0     0     0     0
    Linhas  nulas = 2
    Colunas nulas = 3
        

.. activecode:: aula_matriz_ex5_tentativa

    def main():
        '''
        Programa que le uma matriz de inteiros
        com m linha e n colunas e imprime o numero de linhas
        e de coluna nulas da matriz.
        '''
        # escreva o seu programa abaixo e remova o print()
        print("Vixe! Ainda nao fiz o exercicio!")

    #-----------------------------------------------
    main()
        

Exercício 6
...........

Escreva a função a abaixo:

.. activecode:: aula_matriz_ex6_tentativa

    def multiplica_matriz(a_mat, b_mat):
        '''(matriz,matriz) -> matriz

        Recebe duas matriz a_mat e b_mat e cria e retorna 
        a matriz produto  de a_mat por b_mat.

        Pre-condicao: a funcao supoe que o numero
           de coluna de a_mat e igual ao numero de linhas
           de b_mat
        '''
        print("Vixe! Ainda nao fiz a funcao!")

    #----------------------------------------------
    # teste
    a = [ [1, 2, -1], [0, 3, 2] ]
    b = [ [1, -1], [2, 0], [3, 2] ]
    c = multi_matriz(a,b)
    resultado = [ [2, -3], [12, 4] ]
    if c == resultado:
        print("Passou no primeiro teste! :-)")
    else:
        print("Nao passou no primeiro teste! :-(")

    # colocar mais testes abaixo


        

Exercício 7
..............

Escreva um programa que leia duas matrizes, a matriz ``A`` de dimensão ``m x n``
e ``B`` de dimensão ``n x p`` e
imprime a matriz ``C`` de dimensão ``m x p`` que é 
o produto de ``A`` por ``B``.

Sugestões: utilize as funções ``leia_matriz()``, 
``imprima_matriz()`` e ``multiplica_matriz()`` dos exercícios anteriores.         


.. activecode:: aula_matriz_ex7_tentativa

    #-----------------------------------------------
    def main():
        '''
        Dados uma matriz A e uma matriz B calcula a matriz 
        produto de A por B.
        '''
        # escreva o seu programa abaixo e remova o print()
        print("Vixe! Ainda nao fiz o exercicio!")

    #-----------------------------------------------
    main()



Exercício 8
...........

Nesse exercício, vamos continuar a explorar a multiplicação de matrizes, mas
dessa vez explorando o coneito de produto escalar de duas listas, que correspondem a
uma linha e uma coluna das matrizes multiplicadas.

**Parte A**

Escreva uma função que recebe um inteiro ``lin`` e uma matriz real ``A`` e
outro inteiro ``col`` e uma matriz real ``B``,
e calcula a soma do produto entre os elementos da linha
``lin`` de ``A`` e com os respectivos elementos da coluna ``col``
de ``B``.
Você deve considerar que o número de colunas de ``A`` é o mesmo número
de linhas de ``B``.

.. activecode:: aula_matriz_ex8a_tentativa

    def produto_lincol(lin, a_mat, col, b_mat):
        '''(int, matriz, int, matriz) -> float

        Recebe duas matriz a_mat e b_mat e dois inteiros lin e col
	e calcula a soma o produto entre a linha lin de a_mat com
	a coluna col de b_mat

	Pre-condicao: a funcao supoe que o numero
           de colunas de a_mat e igual ao numero de linhas
           de b_mat
        '''
	
        print("Vixe! Ainda nao fiz a funcao!")

    # teste
    A = [ [1, 2, 1],
          [2, 2, 2],
	  [1, 3, 2]]
    B = [ [1, 1],
          [2, 0],
	  [0, 1] ] 
    produto_lincol(1, A, 0, B)



**Parte B**

Utilizando a função do item anterior, escreva um programa que
leia duas matrizes, a matriz ``A`` de dimensão ``m x n`` e ``B``
de dimensão ``n x p`` e imprime a matriz ``C`` de dimensão ``m x
p`` que é o produto de ``A`` por ``B``. 

.. activecode:: aula_matriz_ex8b_tentativa

    #-----------------------------------------------
    def main():
        '''
	Le duas matrizes A e B e calcula o produto
        '''
        # escreva o seu programa abaixo e remova o print()
        print("Vixe! Ainda nao fiz o exercicio!")

    #-----------------------------------------------
    main()


    
Exercício 9
...........

**Parte A**

Escreva uma função ``MAX`` que recebe como entrada
uma matriz inteira ``A`` e devolve três inteiros:
``k``, ``lin`` e ``col``. O inteiro ``k`` é um maior elemento de
``A`` e é igual a ``A[lin,col]``. 

.. activecode:: aula_matriz_ex9a_tentativa

    def acha_max(A):
        '''(matriz) -> int, int, int
	
        Recebe uma matriz A e devolve 3 inteiros:
	k (um maior valor), e sua posicao lin, col.
        '''
        print("Vixe! Ainda nao fiz a funcao!")

    # teste
    A = [ [3, 7, 1], [1, 2, 8], [5, 3, 4]]
    acha_max(A)
    

Exemplo:

.. sourcecode:: python
		
    A = [ [3, 7, 1], [1, 2, 8], [5, 3, 4]]

a função deve devolver

.. sourcecode:: python
		
    8, 1, 2

Obs.: Se o elemento máximo ocorrer mais de uma vez, indique em ``lin`` e
``col`` qualquer uma das possíveis posições.

**Parte B**

Escreva um programa que, dado dois inteiros ``nl`` e ``nc`` e uma
matriz quadrada de dimensão ``nl x nc``, cujos elementos são todos
inteiros, imprime uma tabela onde os elementos são listados
em ordem decrescente, acompanhados da indicação de linha e coluna a
que pertencem. Havendo repetições de elementos na matriz, a ordem é
irrelevante. Utilize obrigatoriamente o procedimento da parte A,
mesmo que você não o tenha feito.

Ex.: No caso da matriz acima, a saída poderia ser:

.. sourcecode:: python
		
    Elem   Linha  Coluna
      8	     1 	    2
      7	     0      1
      5      2      0
      4      2      2
      3      0      0
      3      2      1
      2      1      1
      1      0      2 
      1      1      0


.. activecode:: aula12_ex122b_tentativa

    #-----------------------------------------------
    def main():
        '''
	Le uma matriz de inteiros de dimensao nl x nc,
	e imprime os seus valores em ordem decrescente.
        '''
        # escreva o seu programa abaixo e remova o print()
        print("Vixe! Ainda nao fiz o exercicio!")

    #-----------------------------------------------
    main()


Exercício 10
............

`Campo Minado <http://pt.wikipedia.org/wiki/Campo_minado>`__ é um
jogo que se tornou muito popular por acompanhar o sistema operacional
Microsoft Windows.

Nesse jogo, o campo minado pode ser representado por uma matriz
retangular. O jogador deve revelar todas as posições livres (sem
bomba) da matriz, clicando em uma posição com conteúdo desconhecido.
O jogo acaba quando o jogador clicar em uma posição com bomba, ou
quando todas as posições livres forem abertas. 

Nesse exercício, você deve implementar algumas funções que podem ser
utilizadas na implementação desse jogo.

**Parte A**

Escreva uma função que recebe como parâmetros uma matriz inteira ``A``
e uma posição (lin, col) da matriz, e conta quantas posições ao redor
da posição (lin, col) contém o valor -1 (valor adotado para
representar uma bomba)

.. admonition:: Uso de constantes
		
    O uso de constantes é uma boa prática de programação. No caso do
    campo minado, ao invés de utilizar o valor -1, podemos criar uma
    constante (variável que nunca muda de valor após criada) chamada
    BOMBA com o valor -1.

.. activecode:: campo_minado_1

    BOMBA = -1 

    def conta_bomba(A, lin, col):
        '''
        Recebe uma matriz inteira A e uma posicao (lin, col), e
	retorna o numero de bombas ao redor de (lin, col)
        '''
        # escreva o seu programa abaixo e remova o print()
        print("Vixe! Ainda nao fiz o exercicio!")

	# exemplo para usar a constante BOMBA
	# if A[lin][col] != BOMBA:


**Parte B**

Escreva um programa que lê uma matriz A de 0's (posições livres) e -1's (bomba).
Utilizando a função do item anterior, o programa deve computar
e imprimir a quantidade de bombas ao redor de cada posição livre da matriz.

.. activecode:: campo_minado_2

    BOMBA = -1
    LIVRE =  0

    def main():
        '''
        Le uma matriz A com 0's e 1's e calcula a quantidade de
	bombas ao redor de cada posição livre.
        '''
        # escreva o seu programa abaixo e remova o print()
        print("Vixe! Ainda nao fiz o exercicio!")

    #-----------------------------------------------
    main()


    
Exercício 11
............

Problema das `n` rainhas.


**Parte A**

Escreva uma função `marque_atacadas(tab)` que recebe uma tabuleiro
`tab` de xadrez (uma matriz 8x8 de caracteres) contendo rainhas
(posições com `R`) e marca as regiões atacadas com um `X`, como abaixo::

    +---+---+---+---+---+---+---+---+
    | X |   |   | X |   |   | X |   |
    +---+---+---+---+---+---+---+---+
    |   | X |   | X |   | X |   |   |
    +---+---+---+---+---+---+---+---+
    |   |   | X | X | X |   |   |   |
    +---+---+---+---+---+---+---+---+
    | X | X | X | R | X | X | X | X |
    +---+---+---+---+---+---+---+---+
    |   |   | X | X | X |   |   |   |
    +---+---+---+---+---+---+---+---+
    |   | X |   | X |   | X |   |   |
    +---+---+---+---+---+---+---+---+
    | X |   |   | X |   |   | X |   |
    +---+---+---+---+---+---+---+---+
    |   |   |   | X |   |   |   | X |
    +---+---+---+---+---+---+---+---+


.. activecode:: aula16_ex02a_tentativa

    def marque_atacadas(tab):
        """(tab) -> None
	   Altera tab marcando as posicoes atacadas por R com X
	"""
	# escreva a sua funcao abaixo
    
	print("Vixe! Ainda nao fiz esse exercicio!")

    ####
    # codigo para testar a funcao
    tabuleiro = [ list('        '),
		  list('        '),
		  list('        '),
		  list('   R    '),
		  list('        '),
		  list('        '),
		  list('        '),
		  list('        ') ]
    marque_atacadas(tabuleiro)
    for lin in tabuleiro:
        print(lin)


**Parte B**

Usando a função do item anterior,
escreva um programa que leia `n` e a posição de `n` rainhas,
e imprima o tabuleiro (com moldura) mostrando a posição inicial
das rainhas (tabuleiro só com `R`)
e depois mostrando a posição das rainhas e as posições
atacadas (com `R` e `X`).

.. activecode:: aula16_ex02b_tentativa

    # escreva o seu programa abaixo e remova o print()
    print("Vixe! Ainda nao fiz esse exercicio!")
