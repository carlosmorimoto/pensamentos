
..  Copyright (C) Carlos Hitoshi Morimoto e José Coelho de Pina Jr.
    É permitida a cópia, distribuição e/ou modificação desde documento
    sob os termos da Licença de Documentação Livre GNU (GNU Free 
    Documentation License) versão 1.3 ou versão subsequente
    publicada pela Fundação de Software Livre (Free Software 
    Foundation); com as seções invariantes sendo Prefácio e 
    Lista de Contribuidores, sem texto de capa ou contra-capa.  
    Uma cópia da licença está disponível em:
    https://www.gnu.org/licenses/licenses.pt-br.html#FDL


.. qnum::
	:prefix: cap13-01
	:start: 1

..  shortname:: Matrizes
..  description:: Matrizes e listas de listas


Matrizes
========

.. index:: Matrizes

Tópicos
-------
    - Apelidos e clones de listas
    - Matrizes
    - `Funções que produzem listas <https://python.ime.usp.br/pensepy/static/pensepy/09-Listas/listas.html#funcoes-que-produzem-listas>`__;
    - `Lista aninhadas <https://python.ime.usp.br/pensepy/static/pensepy/09-Listas/listas.html#listas-aninhadas>`__.

..    - `Função pura <https://python.ime.usp.br/pensepy/static/pensepy/09-Listas/listas.html#funcao-pura>`__.

