
..  Copyright (C) Carlos Hitoshi Morimoto e José Coelho de Pina Jr.
    É permitida a cópia, distribuição e/ou modificação desde documento
    sob os termos da Licença de Documentação Livre GNU (GNU Free 
    Documentation License) versão 1.3 ou versão subsequente
    publicada pela Fundação de Software Livre (Free Software 
    Foundation); com as seções invariantes sendo Prefácio e 
    Lista de Contribuidores, sem texto de capa ou contra-capa.  
    Uma cópia da licença está disponível em:
    https://www.gnu.org/licenses/licenses.pt-br.html#FDL


.. qnum::
	:prefix: cap15-03
	:start: 1




Links Interessantes
-------------------

    - `Animação de algoritmos de ordenação <http://nicholasandre.com.br/sorting/>`__ de Nicholas André Pinho de Oliveira.
    - `Sorting Algorithms Animation <http://www.sorting-algorithms.com/>`__ por David R. Martin.
      

