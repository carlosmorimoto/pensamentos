Algoritmos Elementares de Ordenação
:::::::::::::::::::::::::::::::::::

.. toctree::
	:maxdepth: 1

	01-Objetivo.rst
	02-Topicos.rst
	03-Links-Interessantes.rst
	04-Algoritmos-Elementares-Ordenacao.rst

