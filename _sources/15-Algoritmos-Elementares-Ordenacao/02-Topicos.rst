
..  Copyright (C) Carlos Hitoshi Morimoto e José Coelho de Pina Jr.
    É permitida a cópia, distribuição e/ou modificação desde documento
    sob os termos da Licença de Documentação Livre GNU (GNU Free 
    Documentation License) versão 1.3 ou versão subsequente
    publicada pela Fundação de Software Livre (Free Software 
    Foundation); com as seções invariantes sendo Prefácio e 
    Lista de Contribuidores, sem texto de capa ou contra-capa.  
    Uma cópia da licença está disponível em:
    https://www.gnu.org/licenses/licenses.pt-br.html#FDL


.. qnum::
	:prefix: cap15-02
	:start: 1



Tópicos
-------

    - `Ordenação: algoritmos elementares <http://www.ime.usp.br/~pf/algoritmos/aulas/ordena.html>`__;
    - `Selection Sort <http://interactivepython.org/runestone/static/pythonds/SortSearch/TheSelectionSort.html>`__
    - `Insertion Sort <http://interactivepython.org/runestone/static/pythonds/SortSearch/TheInsertionSort.html>`__
    - `Buble Sort <http://interactivepython.org/runestone/static/pythonds/SortSearch/TheBubbleSort.html>`__

    
