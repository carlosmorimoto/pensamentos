
..  Copyright (C) Carlos Hitoshi Morimoto e José Coelho de Pina Jr.
    É permitida a cópia, distribuição e/ou modificação desde documento
    sob os termos da Licença de Documentação Livre GNU (GNU Free 
    Documentation License) versão 1.3 ou versão subsequente
    publicada pela Fundação de Software Livre (Free Software 
    Foundation); com as seções invariantes sendo Prefácio e 
    Lista de Contribuidores, sem texto de capa ou contra-capa.  
    Uma cópia da licença está disponível em:
    https://www.gnu.org/licenses/licenses.pt-br.html#FDL


.. qnum::
	:prefix: cap02-09
	:start: 1



Laboratório: iPython
--------------------

Para realizar esse laboratório, você vai precisar de um computador com Python instalado. 
Vamos assumir que você tenha seguido corretamente e com sucesso as instruções de instalação do laboratório do capítulo anterior.

.. Também vamos assumir que você esteja usando um computador com Microsoft Windows. 
    Caso você use outro sistema operacional, 
    abra um terminal e chame o iPython a partir da linha de comando, de forma semelhante a que fazemos no Windows. 

Para abrir um terminal no Windows, 
clique na barra de pesquisa do Windows e digite ``Anaconda Prompt``. 
Dentre as primeiras opções deve estar a "Anaconda Prompt (Anaconda 3)".

Ao clicar na opção "Anaconda Prompt" o seu computador deve abrir uma janela que chamamos de terminal. 
No terminal, digite "ipython" para começar a executar o programa **iPython**.

O iPython é outro terminal (ou shell) que nos permite interagir diretamente com o interpretador Python, ou seja, podemos digitar comandos em Python diretamente na linha de comando do terminal que contém o **prompt** do iPython como ilustrado abaixo:

.. code-block:: Python

    In [1]: 

O número entre colchetes indica o número de comandos que você digitou até agora. Experimente digitar alguns dos programas que vimos nesse capítulo, como:


.. code-block:: Python

    In [1]: print( '2 + 3 * 4 = ', 2 + 3 * 4 )
    2 + 3 * 4 =  14

    In [2]: 

Observe que a função ``print()`` é executada, imprimindo a mensagem resultante na linha seguinte ao comando e, ao terminar de executar o comando, o terminal exibe um novo prompt, indicando que está pronto para receber outro comando. 

O iPython é muito útil para testar um comando quando você não tiver certeza de como ele funciona e também serve como uma **calculadora**.
Por exemplo, digite a expressão: ``2 + 2``, ou qualquer operação que desejar,  e em seguida tecle ``ENTER`` para ver o resultado.

.. code-block:: Python

    In [2]: 2 + 3 * 4
    Out[2]: 14

    In [3]: print(2 + 3 * 4)
    14

    In [4]:

Para entender o que está acontecendo,  
toda vez que você teclar ``ENTER`` você está enviando ao iPython um comando,
para que ele o interprete e execute. Ao executar uma expressão como na linha ``In [2]``, a expressão retorna o valor ``14`` e esses resultados podem ser reutilizados depois.

No caso da  ``print()`` na linha ``In [3]``, essa função imprime a mensagem ``14`` mas não tem um valor que pode ser reutilizado e, por isso, o iPython não mostra um ``Out[3]``. Vamos voltar a discutir esse comportamento após conhecer melhor outros recursos da linguagem Python. Por hora, vamos apenas nos limitar às expressões lógicas, relacionais e aritméticas que vimos nesse capítulo.

Experimente escrever expressões no iPython (lembrando de pensar antes de clicar) para treinar seus conhecimentos. 
Provavelmente você vai encontrar situações que pareçam ambíguas no início e, nesses casos, sugerimos que você utilize o iPython para verificar qual o comportamento do Python nessas situações. 

Experimente também criar expressões incorretas, como:

.. code-block:: Python

    In [4]: 2 + 3 * 4 %
      File "<ipython-input-10-31d0386dedc1>", line 1
        2 + 3 * 4 %
                  ^
    SyntaxError: invalid syntax
  
Nesse caso, ele tenta resolver a expressão até encontrar um erro ao tentar executar o operador ``%``. Observe que o iPython tenta indicar exatamente onde o erro de sintaxe aconteceu na linha que contém o símbolo '^'. 

O iPython é uma das ferramentas que estão integradas ao IDE Spyder, como veremos no próximo laboratório.


