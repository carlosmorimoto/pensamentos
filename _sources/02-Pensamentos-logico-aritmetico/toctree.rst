Pensamentos lógico e aritmético
:::::::::::::::::::::::::::::::

.. toctree::
	:maxdepth: 1

	01-Introducao.rst
	02-Para-onde-vamos.rst
	03-Computador-como-Calculadora.rst
	04-Expressoes-aritmeticas.rst
	05-Tipos-inteiro-real.rst
	06-Expressoes-relacionais.rst
	07-Expressoes-logicas.rst
	08-Exercicios.rst
	09-Laboratorio.rst
	10-Onde-chegamos-para-onde.rst

