
..  Copyright (C) Carlos Hitoshi Morimoto e José Coelho de Pina Jr.
    É permitida a cópia, distribuição e/ou modificação desde documento
    sob os termos da Licença de Documentação Livre GNU (GNU Free 
    Documentation License) versão 1.3 ou versão subsequente
    publicada pela Fundação de Software Livre (Free Software 
    Foundation); com as seções invariantes sendo Prefácio e 
    Lista de Contribuidores, sem texto de capa ou contra-capa.  
    Uma cópia da licença está disponível em:
    https://www.gnu.org/licenses/licenses.pt-br.html#FDL


.. qnum::
	:prefix: cap02-05
	:start: 1



Tipos inteiro e real
--------------------

Até aqui vimos que o computador como calculadora obedece as regras de precedência e associatividade para calcular os resultados de expressões aritméticas até chegar em um valor que não dá mais para ser reduzido. Então,  qual o resultado da expressão ``5 / 2``?

Essa pergunta pode parecer óbvia a princípio, mas pare um pouco para pensar no tipo dos operandos ``5`` e ``2``.
Em Python, podemos usar a função ``type()`` para descobrir o tipo desses valores. Execute o programa para ver os tipos de ``5`` e ``2``.

.. codelens:: cl02_Usando_type

    print( 'Tipo de 5: ', type(5) )
    print( 'Tipo de 2: ', type(2) )


Observe que podemos concatenar funções em Python, como fazemos em matemática, como por exemplo "tangente( raiz_quadrada( 0.5 ) ) ". Assim, o resultado da função ``type()`` é impresso pela função ``print()`` na saída.

A mensagem ``<class 'int'>`` indica que ``5`` e ``2`` pertecem à classe dos números inteiros, onde `int` é uma abreviação de `integer` que significa inteiro em inglês. Ao invés de escrever ``<class 'int'>``, nesse livro vamos abreviar também e dizer que ``5`` é do tipo ``int``. 

Sabemos que o valor "verdadeiro" da expressão ``5 / 2`` é o real ``2.5`` (vamos usar um ponto ``.`` para representar números reais para seguir a mesma notação do Python). Por princípio, a linguagem Python procura calcular os valores da mesma forma como nós humanos o fazemos. Confira a seguir qual o valor de ``5 / 2`` segundo o Python:

.. codelens:: cl02_quanto_é_5_por_2

    print( '5 / 2 = ', 5 / 2 )

    print( 'Tipo de 2.5: ', type(2.5) )

Observe que ``2.5`` é da classe de números ``float`` (vamos dizer que é do tipo ``float``), 
onde a palavra ``float`` é uma abreviação de ``floating point`` (`ponto flutuante <ponto flutuante <https://pt.wikipedia.org/wiki/V%C3%ADrgula_flutuante>`_ em inglês), que corresponde a forma de representação de números reais em Python. Na verdade, como números reais podem possuir um número infinito de dígitos (como o número Pi = 3.141595426... ) e a memória de um computador é finita, um número do tipo ``float`` é na verdade um subconjunto dos números reais limitado a um certo número de bits (como 32, 64 ou 128 bits por exemplo). Essa limitação na representação de valores é, em geral, verdadeira na representação de qualquer coisa em um computador. Uma exceção em Python são os números inteiros. Veremos em capítulos futuros que o Python permite representar números inteiros arbitrariamente grandes. 

Vamos deixar de lado a forma de representação e trabalhar o pensamento matemático segundo as propriedades ``valor`` e ``tipo`` apenas. Assim, vamos dizer que o valor ``2.5`` é do tipo ``float``, e o valor ``2`` é do tipo ``int``. 

Muitas vezes é conveniente "forçar" que o resultado da divisão seja inteiro. 
Para isso, o Python oferece o operador de divisão inteira ``//`` (duas barras de divisão). 
Esse operador "trunca" o resultado, ou seja, mantem apenas a parte inteira e desfaz a parte fracionárias que vem após o ponto. 
Assim, enquanto o valor da expressão ``68/7`` é algo como ``9.714..``, o valor da expressão ``68//7`` é apenas ``9``. 
Outro operador bastante utilizado com inteiros é o operador resto da divisão ``%``. Por exemplo, sabemos que 25 dividido por 7 resulta no inteiro 3 com resto 4. Experimente escrever algumas expressões usando os operadores ``//`` e ``%`` usando o ActiveCode abaixo e tente prever o resultado antes de executar seu teste.

.. activecode:: ac02_testando_operadores_div_resto

    print( '25 // 7 = ', 25 // 7 )

    print( '25 % 7 = ', 25 % 7 )


Teste o seu conhecimento
........................
    

.. mchoice:: questao_expressao_aritmetica_07
    :answer_a: 2
    :answer_b: 2.2
    :answer_c: 2.5 
    :answer_d: 3
    :correct: b
    :feedback_a: Incorreto: não é o resultado da divisão "verdadeira", mas da divisão inteira.
    :feedback_b: Correto: o operador `/` resulta no float 2.2
    :feedback_c: Incorreto: quanto é 2.5 * 5?
    :feedback_d: Incorreto: quanto é 3 * 5?

    Qual o valor da expressão: ``11 / 5``


.. mchoice:: questao_expressao_aritmetica_08
    :answer_a: 2
    :answer_b: 2.2
    :answer_c: 2.5 
    :answer_d: 3
    :correct: a
    :feedback_a: Correto: resultado da divisão inteira.
    :feedback_b: Incorreto: o operador `//` faz divisão com resultado inteiro.
    :feedback_c: Incorreto: quanto é 2.5 * 5?
    :feedback_d: Incorreto: quanto é 3 * 5?

    Qual o valor da expressão: ``11 // 5``


.. mchoice:: questao_expressao_aritmetica_09
    :answer_a: 1
    :answer_b: 2
    :answer_c: 3
    :answer_d: 6
    :correct: a
    :feedback_a: Correto: resultado do resto da divisão 11 por 5
    :feedback_b: Incorreto: qual o resto da divisão de 11 por 5?
    :feedback_c: Incorreto: qual o resto da divisão de 11 por 5?
    :feedback_d: Incorreto: 6 é maior que 5. O resto é sempre menor que o divisor.

    Qual o valor da expressão: ``11 % 5``


