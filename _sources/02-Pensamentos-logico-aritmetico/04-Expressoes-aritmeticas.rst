
..  Copyright (C) Carlos Hitoshi Morimoto e José Coelho de Pina Jr.
    É permitida a cópia, distribuição e/ou modificação desde documento
    sob os termos da Licença de Documentação Livre GNU (GNU Free 
    Documentation License) versão 1.3 ou versão subsequente
    publicada pela Fundação de Software Livre (Free Software 
    Foundation); com as seções invariantes sendo Prefácio e 
    Lista de Contribuidores, sem texto de capa ou contra-capa.  
    Uma cópia da licença está disponível em:
    https://www.gnu.org/licenses/licenses.pt-br.html#FDL


.. qnum::
	:prefix: cap02-04
	:start: 1



Expressões aritméticas
----------------------

.. index:: operador, operando, precedência de operadores, associatividade de operadores.

Uma expressão aritmética é formada por números (chamados de **operandos**) e **operadores** (como soma, multiplicação etc). 
Na sua forma mais simples, uma expressão contém apenas um número. 
Assim, o **valor de uma expressão** com apenas um número é o próprio número. É comum também chamarmos esse valor de resultado da expressão. 

.. Veja o que acontece quando digitamos apenas um número na linha de comando do Python shell:

Veja o que acontece quando mandamos o Python imprimir apenas um número:

.. codelens:: cl02_exemplo_calculadora_03

    print( 5 )
    print( -21 )

Quando uma expressão contém um operador como ``+`` (soma), a "calculadora" deve realizar 
a operação usando os operandos fornecidos como no exemplo:

.. codelens:: cl02_exemplo_calculador_04

    print( '5 + 2 = ', 5 + 2 )
    print( '-21 / 3 = ', -21 / 3 )

Podemos dizer que o Python reduz uma expressão a um valor (resultado da expressão). No caso de um número, não há o que ser reduzido, e o resultado é o próprio número. No caso de uma soma como ``5 + 2``, os dois números são somados e *reduzidos* ao valor ``7``. Como a calculadora (computador) só consegue reduzir um operador por vez, você saberia prever o resultado de uma expressão com vários operadores como ``2 + 3 * 4``? 

Teste o seu conhecimento
........................

Procure responder prevendo o resultado das expressões antes de testá-las no Python ou em uma calculadora.

.. mchoice:: mc02_questao_expressao_aritmetica_01
    :answer_a: -1
    :answer_b: -5
    :answer_c:  9
    :answer_d: 15
    :correct: c
    :feedback_a: Incorreto: existe um padrão para cálculo de expressões que é obedecido pelos computadores onde a multiplicação tem precedência sobre a adição e subtração.  
    :feedback_b: Incorreto: existe um padrão para cálculo de expressões que é obedecido pelos computadores onde a multiplicação tem precedência sobre a adição e subtração. 
    :feedback_c: Correto.
    :feedback_d: Incorreto: existe um padrão para cálculo de expressões que é obedecido pelos computadores onde a multiplicação tem precedência sobre a adição e subtração. 

    Qual o valor da expressão: ``2 + 3 * 4 - 5``


.. mchoice:: mc02_questao_expressao_aritmetica_02
    :answer_a: 15
    :answer_b:  9
    :answer_c: -5
    :answer_d: -1
    :correct: d
    :feedback_a: Incorreto: é necessário calcular a expressão entre parênteses primeiro.
    :feedback_b: Incorreto: é necessário calcular a expressão entre parênteses primeiro.
    :feedback_c: Incorreto: é necessário calcular a expressão entre parênteses primeiro.
    :feedback_d: Correto.

    Qual o valor da expressão: ``2 + 3 * (4 - 5)``


Para conferir esses resultados, 
clique no botão ``Salvar & Executar`` da ferramenta ActiveCode abaixo.

.. activecode:: ac02_teste_de_expressões_aritméticas

    print( 'questão 1: ', 2 + 3 * 4 - 5 )
    print( 'questão 2: ', 2 + 3 * (4 - 5) )

Esperamos que você tenha acertado essas questões pois o Python segue as mesmas 
regras para cálculo de expressões que você aprendeu nos cursos de matemática.

Observe que as expressões foram digitadas previamente para você no campo de edição do ActiveCode. Diferente do CodeLens, o ActiveCode permite que você edite o programa. Experimente modificar as expressões e tente prever os resultados, ou inclua outras linhas com prints de outras expressões.

Note ainda que você pode inclusive executar o seu programa no CodeLens, clicando no botão ``Mostrar em CodeLens``.


.. admonition:: Erros de sintaxe no ActiveCode

    Lembre-se que o Python é interpretado. Ao receber uma sequência de comandos (um programa) o Python tenta executar os comandos na sequência. Caso o Python não entenda um comando ou ocorra algum problema na execução (como divisão por zero), o Python para de executar e imprime uma mensagem de erro com alguma dica sobre o problema. Um tipo comum de erro é o erro de sintaxe (*SyntaxError*). Execute o código no ActiveCode abaixo e confira a mensagem de erro "SyntaxError: bad input on line 1". As mensagens são, infelizmente em inglês, mas em geral dá para ver que o erro ocorreou perto da linha 1. Você pode usar o `translate da Google <https://translate.google.com/>`_ para traduzir a mensagem para o português. Há vários problemas no código abaixo. 
    Procure corrigir um erro de cada vez e clique em ``Salvar & Executar`` para ver o comportamento do Python.

    .. activecode:: ac02_teste_de_expressões_aritméticas_com_erro

        print( '1: ', 2 + 3 * 4 - )
        print( '2: ', 2 +  * (4 - 5) )

        2 +

Assim, para prever o resultado de uma expressão com vários operadores, além de conhecer o que cada operador faz (como soma e multiplicação), é necessário conhecer as regras de **precedência dos operadores** e também sua **associatividade**. 

Exemplos para entender o que é precedência e associatividade
............................................................

As regras de precedência indicam qual operador é calculado primeiro.
Por exemplo, qual o resultado da expressão ``2 - 3 * 4``? 

Como a multiplicação tem maior prioridade que a subtração, o produto ``3 * 4`` é reduzido ao valor ``12`` e a seguir se calcula o valor da subtração ``2 - 12``, resultando em ``-10``. 

As regras de associatividade indicam a ordem dos cálculos para operadores que tenham a mesma precedência. 
Por exemplo, qual o resultado da expressão ``2 - 3 + 4``? 

Como a soma tem a mesma prioridade que a subtração, precisamos aplicar a regra de associatividade. 
Em Python, a maioria dos operadores binários (que usam dois operandos) tem associatividade "da esquerda para a direita" (ou seja, as operações são realizadas na mesma ordem de leitura). 
A expressão portanto é primeiramente reduzida a ``-1 + 4`` resolvendo a subtração e depois reduzida ao valor ``3`` resolvendo a soma. Observe que, caso a soma ``3 + 4`` fosse calculada primeiro, o resultado final seria ``-5 = 2 - (3 + 4)``.


A tabela a seguir mostra a associatividade das principais operações aritméticas em Python, 
em ordem decrescente de precedência (da maior para a menor):


.. table:: Tabela de precedência e associatividade de operadores aritméticos
    :align: left

    +-------------+--------------------------------------------------+-------------------------------+
    | Operador    |  descrição                                       | Associatividade               |
    +=============+==================================================+===============================+
    | ()          | parênteses                                       |  da esquerda para a direita   |
    +-------------+--------------------------------------------------+-------------------------------+
    | \*\*        | potência                                         |  da direita para a esquerda   |
    +-------------+--------------------------------------------------+-------------------------------+
    | +, -        | positivo e negativo unário                       |  da direita para a esquerda   |
    +-------------+--------------------------------------------------+-------------------------------+
    |\*, /, //, % | multiplicação , divisão, divisão inteira e resto |  da esquerda para a direita   |
    +-------------+--------------------------------------------------+-------------------------------+
    | +, -        | soma e subtração                                 |  da esquerda para a direita   |
    +-------------+--------------------------------------------------+-------------------------------+

Observações:

* Níveis de precedência:
    - A tabela mostra que há grupos de operadores com o mesmo nível, como soma e subtração.
    - Quanto mais "alto" o nível na tabela, maior a precedência.
 
* Operadores com mesmo nível de precedência são resolvidos segundo a sua associatividade.
    - exemplo: para reduzir a expressão ``12 / 2 / 3 / 4``, o Python calcula ``12/2``, e segue dividindo o resultado por 3 e depois por 4. 

* Os operadores unários (``+`` e ``-``) tornam explícitos o sinal do operando. 
    - experimente colocar uma sequência de operadores unários como ``-+-+3`` para ver o que acontece (será que resulta em erro de sintaxe?).

* Use parênteses caso deseje alterar a precedência ou torná-la explícita
    - exemplo: ``12 / 2 / 3 / 4`` => ``0.5``
    - exemplo: ``(12 / 2) / ( 3 / 4)`` => ``8.0``


Teste o seu conhecimento
........................

Qual o valor resultante das seguintes expressões:

.. mchoice:: questao_expressao_aritmetica_03
    :answer_a: 0
    :answer_b: 1
    :answer_c: 4 
    :answer_d: 2.4
    :answer_e: nenhuma das alternativas anteriores
    :correct: c
    :feedback_a: Incorreto: A expressão é calculada da esquerda para a direita como (12 * 2) % 10
    :feedback_b: Incorreto: A expressão é calculada da esquerda para a direita como (12 * 2) % 10
    :feedback_c: Resposta correta.
    :feedback_d: Incorreto. O operador `%` calcula o resto da divisão.
    :feedback_e: Incorreto, ao menos uma das alternativas anteriores está correta.

    Qual o valor da expressão: ``12 * 2 % 10``

.. mchoice:: questao_expressao_aritmetica_04
    :answer_a: 3
    :answer_b: 3.6
    :answer_c: 14 
    :answer_d: 14.4
    :answer_e: nenhuma das alternativas anteriores
    :correct: a
    :feedback_a: Resposta correta.
    :feedback_b: Incorreto: O operador `//` faz divisão inteira.
    :feedback_c: Incorreto: O operador `**` tem precedência maior que multiplicação e divisão.
    :feedback_d: Incorreto. O operador `//` faz divisão inteira e sua precedência é menor que `**`.
    :feedback_e: Incorreto, ao menos uma das alternativas anteriores está correta.

    Qual o valor da expressão: ``4 * 3 ** 2 // 10``

.. mchoice:: questao_expressao_aritmetica_05
    :answer_a: 512
    :answer_b: 256
    :answer_c: 128 
    :answer_d:  64
    :answer_e: nenhuma das alternativas anteriores
    :correct: a
    :feedback_a: Correto: como a associatividade de `**` é da direita para esquerda, o resultado é equivalente a 2 ** (3 ** 2) 
    :feedback_b: Incorreto: veja a associatividade de `**`
    :feedback_c: Incorreto: veja a associatividade de `**`
    :feedback_d: Incorreto: veja a associatividade de `**`
    :feedback_e: Incorreto, ao menos uma das alternativas anteriores está correta.

    Qual o valor da expressão: ``2 ** 3 ** 2``

.. mchoice:: questao_expressao_aritmetica_06
    :answer_a: é igual a (-2) ** 4
    :answer_b: é igual a -(2 ** 4)
    :answer_c:  8 
    :answer_d:  16
    :correct: b
    :feedback_a: Incorreto: verifique qual a precedência entre `**` e o `-` unário.
    :feedback_b: Correto: o operador `**` tem precedência.
    :feedback_c: Incorreto: verifique qual a precedência entre `**` e o `-` unário.
    :feedback_d: Incorreto: verifique qual a precedência entre `**` e o `-` unário.

    Qual o valor da expressão: ``-2 ** 4``


Use o ActiveCode abaixo para testar outras expressões.

.. activecode:: ac02_teste_de_outras_expressões_aritméticas

    # Essa linha é um comentário.
    # um comentário em Python começa com o caractere '#'
    # Comentários servem para serem lidos porleitores humanos 
    # mas são ignorados pelo Python.

    print(2 + 2)  #### comentário: deve ser 4


.. admonition:: O que comentar?

    É uma boa prática de computação incluir comentários no código fonte,
    para ajudar outros programadores que gostariam de reusar o código. 

    Desde o início, evite comentários redundantes como: 
    
    print( 2 + 2 ) # soma dois com dois

    Um bom uso de comentários é a descrição de blocos como:

    # Testes de soma
    print( 2 + 3 )
    print( 2 + 3 + 4 )

    # Testes de subtração
    print( 3 - 2 )
    print( -3 - 2 - 5 )

    Ao longo do livro, outras boas práticas de programação serão discutidas.


