
..  Copyright (C) Carlos Hitoshi Morimoto e José Coelho de Pina Jr.
    É permitida a cópia, distribuição e/ou modificação desde documento
    sob os termos da Licença de Documentação Livre GNU (GNU Free 
    Documentation License) versão 1.3 ou versão subsequente
    publicada pela Fundação de Software Livre (Free Software 
    Foundation); com as seções invariantes sendo Prefácio e 
    Lista de Contribuidores, sem texto de capa ou contra-capa.  
    Uma cópia da licença está disponível em:
    https://www.gnu.org/licenses/licenses.pt-br.html#FDL


.. qnum::
	:prefix: cap02-08
	:start: 1



Exercícios
----------

* **Exercício 1**: 
    Escreva uma expressão em função de uma incógnita `nota` que resulte em ``True`` caso `nota` esteja no intervalo aberto (3.0, 5.0), e resulte em ``False`` caso contrário. A expressão deve usar apenas operadores aritméticos, lógicos e/ou relacionais.

    .. admonition:: Dica

        A seguinte expressão em função de `nota` resulta em ``True`` para uma nota maior ou igual a 5

        .. code-block:: Python

            nota >= 5

* **Exercício 2**: 
    Um `ano bissexto <https://pt.wikipedia.org/wiki/Ano_bissexto>`_ ocorre aproximadamente a cada 4 anos. 
    Escreva uma expressão (em função de uma incógnita `ano`) que resulte em ``True`` caso `ano` seja bissexto e ``False`` caso contrário.
    Para ser bissexto, o valor de `ano` precisa ser múltiplo de 4, exceto múltiplos de 100 que não são múltiplos de 400.
    Assim o ano de 2020 é bissexto pois satisfaz todas essas condições. 

    A expressão deve usar apenas operadores aritméticos, lógicos e/ou relacionais.

    .. admonition:: Dica

        Use o operador `%` para verificar se `ano` é um múltiplo de algum número, como por exemplo
        
        .. code-block:: Python
        
            ano % 4 == 0 
         
        Se `ano=12`, a expressão ``12 % 4 == 0`` resulta em ``True`` pois como 12 é múltiplo de 4 o resto da divisão (``12%4``) é zero.

