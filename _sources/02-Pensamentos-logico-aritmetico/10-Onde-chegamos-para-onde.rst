
..  Copyright (C) Carlos Hitoshi Morimoto e José Coelho de Pina Jr.
    É permitida a cópia, distribuição e/ou modificação desde documento
    sob os termos da Licença de Documentação Livre GNU (GNU Free 
    Documentation License) versão 1.3 ou versão subsequente
    publicada pela Fundação de Software Livre (Free Software 
    Foundation); com as seções invariantes sendo Prefácio e 
    Lista de Contribuidores, sem texto de capa ou contra-capa.  
    Uma cópia da licença está disponível em:
    https://www.gnu.org/licenses/licenses.pt-br.html#FDL


.. qnum::
	:prefix: cap02-10
	:start: 1



Onde chegamos e para onde vamos?
--------------------------------

Nesse capítulo vimos como o Python calcula o resultado de expressões aritméticas, relacionais e lógicas. Com o que vimos até aqui, você já pode usar o Python Shell como uma poderosa calculadora.  

Talvez por serem super aprendidas, alguns detalhes no processamento dessas expressões pode lhe passar desapercebidos. Na verdade esse conhecimento matemático é bastante sutil e pode ser até entediante, mas é um passo importante para desenvolver seu pensamento computacional a partir de fundamentos matemáticos sólidos, sempre lembrando que o computador faz apenas uma única operação de cada vez, até chegar em um valor que não dá mais para ser reduzido. 

Vimos que cada **valor** em Python possui um determinado **tipo**, que pode ser:

* tipo ``int`` para representar valores inteiros; 
* tipo ``float`` para representar valores reais;
* tipo ``bool`` para representar booleanos ``True`` ou ``False``.

O Python oferece ainda, de forma nativa, vários outros tipos de dados que serão cobertos nos próximos capítulos.

Cada tipo tem comportamentos que são definidos pelo conjunto de operadores associados ao seu tipo. 

Operadores aritméticos
......................

No caso de valores inteiros e reais temos as operações aritméticas. Vimos que é conveniente ter dois tipos diferentes de divisão, uma com resultado do tipo ``float`` (usando uma barra ``/``) e outra do tipo ``int`` (com duas barras ``//``). Outro operador bastante importante é o operador ``%`` que calcula o resto da divisão. 

Operadores relacionais
......................

Além de calcular o valor de expressões ao escrever programas como uma calculadora comum, ao escrever programas vamos precisamos também comparar valores. Isso é possível por meio dos operadores relacionais como ``==``, ``<``, ``>=`` etc, cuja precedência é convenientemente menor que os operadores aritméticos.
Saber ler e escrever expressões relacionais é um dos fundamentos que vamos precisar para começar a introduzir comandos básicos do Python para controlar a execução de programas.

Operadores lógicos
..................

Se os operadores relacionais permitem comparar valores, os operadores lógicos permitem combinar relações por meio dos operadores ``and``, ``or`` e ``not``. Veremos que o uso de expressões lógicas é um recurso bastante poderoso que pode simplificar e tornar seus programas mais legíveis, fáceis de entender e de encontrar erros.


.. Para saber mais
    ---------------
    * Leituras
        - `Capítulo 2: Variáveis, expressões e comandos <https://panda.ime.usp.br/pensepy/static/pensepy/02-Conceitos/conceitos.html>`__ do livro Como Pensar Como um Cientista da Computação.
        - Expressões em Python `<https://docs.python.org/3/reference/expressions.html>`_.
        - `Valores booleanos e expressões booleanas <https://python.ime.usp.br/pensepy/static/pensepy/06-Selecao/selecao.html>`__ do livro Como Pensar Como um Cientista da Computação. 
    * Vídeos
        - `Tipos booleanos e precedência de operadores <https://www.youtube.com/watch?v=sgfmuFRZuWs&list=PLcoJJSvnDgcKpOi_UeneTNTIVOigRQwcn&index=9>`__ 
        - `Expressões lógicas (bool) [Zumbis] <https://www.youtube.com/watch?v=d6XyTLkTYJo#t=41>`__.


