
..  Copyright (C) Carlos Hitoshi Morimoto e José Coelho de Pina Jr.
    É permitida a cópia, distribuição e/ou modificação desde documento
    sob os termos da Licença de Documentação Livre GNU (GNU Free 
    Documentation License) versão 1.3 ou versão subsequente
    publicada pela Fundação de Software Livre (Free Software 
    Foundation); com as seções invariantes sendo Prefácio e 
    Lista de Contribuidores, sem texto de capa ou contra-capa.  
    Uma cópia da licença está disponível em:
    https://www.gnu.org/licenses/licenses.pt-br.html#FDL


.. qnum::
	:prefix: cap02-03
	:start: 1



Computador como Calculadora
---------------------------

Vimos no capítulo anterior que uma parte importante do funcionamento de um computador é a sua capacidade de realizar cálculos, semelhante a uma calculadora. O pensamento matemático que envolve a representação ou tipo de um elemento (como inteiro, real, complexo, etc) e as operações (ou aritmética) que definem como esses elementos podem ser combinados é um dos fundamentos do pensamento computacional. 

Assim, antes de escrever programas, vamos relembrar como essa "calculadora" funciona e treinar o seu uso até conseguirmos usá-la muito bem, ou seja, até que nosso raciocínio matemático esteja desenvolvido o bastante para que possamos prever o valor devolvido pelo computador para expressões complexas. 

..  Vamos ver também que o computador precisa trabalhar com tipos de dados além de números 
    e cada tipo de dado tem um conjunto distinto de operações que definem seu comportamento.


Uma regra básica é que o computador se comporta de forma **determinística**, ou seja, toda expressão calculada pelo computador é sempre resolvida da mesma maneira e a ordem das operações é previsível para que o computador sempre devolva o mesmo resultado.

Nesse capítulo vamos começar a utilizar outras ferramentas interativas do `projeto Runestone <https://runestone.academy/runestone/default/user/login?_next=/runestone/default/index>`__ que permitem você executar trechos de código em Python dentro do seu próprio navegador. O exemplo abaixo usa a ferramenta CodeLens e possui apenas uma linha de código em Python que usa a função ``print()`` para imprimir o resultado da expressão ``2 + 3``. 

Para executar essa função na linha 1 (indicada pela seta vermelha na coluna da esquerda) basta clicar no botão verde ``Next`` para que a linha seja executada. O resultado da expressão escrita entre os parênteses da função ``print()`` devem aparecer na coluna da direita onde se lê "Print output", que vamos chamar de quadro de saída ou, simplesmente, de saída. Você pode clicar e arrastar o canto inferior direito do quadro de saída para alterar seu tamanho. 

.. codelens:: cl02_exemplo_calculadora_01
    :showoutput:

    print( 2 + 3 )

Após clicar no botão ``Next``, o valor da expressão (no caso, ``5``) deve aparecer na saída. Observe que é o valor da expressão (``5``) e não a expressão em si (``2 + 3``) que é impresso na saída. Para imprimir a expressão também, podemos colocar a expressão entre aspas (") ou apóstrofes ('), e separar os objetos a serem impressos por vírgula como:

.. code-block:: Python

    print( '2 + 3 = ', 2 + 3 )


No exemplo a seguir incluímos uma segunda linha com a expressão ``2 + 3 * 4``.  
Novamente, ao clicar em ``Next`` uma primeira vez o valor ``5`` deve ser impresso na saída. 
Observe que a seta vermelha passa a apontar para a instrução seguinte.

Agora, **antes** de clicar uma segunda vez em ``Next``, procure pensar qual vai ser o valor impresso. 
Por exemplo, esse valor poderia ser ``20`` caso a soma ``2 + 3`` seja calculada antes da multiplicação. 
No entanto, felizmente o Python segue as regras de precedência dos operadores que nós estamos acostumados e o resultado na saída  é ``14``. Clique agora uma segunda vez e confira!

.. codelens:: cl02_exemplo_calculadora_02
    :showoutput:

    print( '2 + 3 = ', 2 + 3 )
    print( '2 + 3 * 4 = ', 2 + 3 * 4 )


Agora que você entendeu como usar o CodeLens com a função ``print()``, vamos revisar um pouco mais sobre expressões aritméticas em Python.

.. admonition:: Pense antes de clicar!

    A partir desse capítulo, procure criar o hábito de "pensar antes de clicar" e tente imaginar o resultado de cada instrução. Caso o resultado não seja o que você pensou, volte e procure entender por que você está "pensando" diferente do Python. 

    Preste muito atenção no código. Observe que as mensagens impressas pela ``print()`` são de inteira responsabilidade do programador.
    Por exemplo, qual o problema do seguinte comando:

    .. code-block:: Python

        print( '2 + 3 = ', 2 * 3 )

    O problema é que a mensagem está inconsistente, pois o resultado esperado para o primeiro termo ``'2 + 3 ='`` é diferente do segundo termo ``2 * 3``. Esses detalhes parecem óbvios ao serem apontados, mas encontrar esses erros pode ser uma tarefa árdua e frustrante pois muitas vezes passam despercebidos. A atenção a detalhes é uma habilidade que lhe será bem util nesse início de jornada.


..  Uma dificuldade inicial para se aprender uma língua desconhecida
    é perceber e diferenciar os sons, para depois perceber as palavras,
    antes de finalmente conseguir entender as frases.
    Em geral também o entendimento da leitura e escuta de uma língua 
    ocorre antes do domínio da escrita e da fala.
    Acreditamos que o aprendizado de uma linguagem de programação
    não seja muito diferente e, embora mais simples que uma linguagem
    natural, como o português, é necessário praticar muito a leitura para
    que consigamos desenvolver a habilidade de criar programas nessa nova língua.

..  Muitos programadores experientes conseguem escrever um programa
    diretamente em uma linguagem de programação. Ao final desse curso
    gostaríamos que você fosse capaz de escrever alguns programas
    simples em Python. Entretanto, diferente de aprender a ler e escrever em uma
    outra língua, um programa depende de uma solução que precisa ser *pensada* 
    antes do programa ser escrito. Isso porque um programa, em geral, resolve um
    determinado problema. Como cada problema pode ter várias soluções
    possíveis, é sempre recomendado que pensemos em algumas
    soluções distintas antes de escolhermos uma para ser implementada.

..  Embora soluções distintas possam levar a mesma solução correta, 
    cada solução pode ter desempenhos diferentes (ser mais ou menos rápida)
    e consumir recursos diferentes (como mais ou menos memória). 
    Nesse curso, mais que
    programar, vamos desenvolver *boas práticas* de programação
    que ajudam na construção de bons programas.


