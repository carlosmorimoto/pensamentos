
..  Copyright (C) Carlos Hitoshi Morimoto e José Coelho de Pina Jr.
    É permitida a cópia, distribuição e/ou modificação desde documento
    sob os termos da Licença de Documentação Livre GNU (GNU Free 
    Documentation License) versão 1.3 ou versão subsequente
    publicada pela Fundação de Software Livre (Free Software 
    Foundation); com as seções invariantes sendo Prefácio e 
    Lista de Contribuidores, sem texto de capa ou contra-capa.  
    Uma cópia da licença está disponível em:
    https://www.gnu.org/licenses/licenses.pt-br.html#FDL


.. qnum::
	:prefix: cap02-06
	:start: 1



Expressões relacionais
----------------------

.. index:: operadores relacionais

Além de "fazer contas" com números inteiros e reais, o Python permite comparar valores usando os seguintes **operadores relacionais**:

.. table:: Tabela dos operadores relacionais

    +----------+--------------------------------+---------------+---------------+
    | Operador |  Descrição                     | Exemplo       | Resultado     |
    +==========+================================+===============+===============+
    | ==       |        igualdade               | 2 == 3        | False         |
    +----------+--------------------------------+---------------+---------------+
    | !=       |        desigualdade            | 2 != 3        | True          |
    +----------+--------------------------------+---------------+---------------+
    | >        |           maior                | 3 > 3         | False         |
    +----------+--------------------------------+---------------+---------------+
    | >=       |           maior ou igual       | 3 >= 3        | True          |
    +----------+--------------------------------+---------------+---------------+
    | <        |           menor                | 2 < 3         | True          |
    +----------+--------------------------------+---------------+---------------+
    | <=       |           menor ou igual       | 4 <= 3        | False         |
    +----------+--------------------------------+---------------+---------------+

Operadores relacionais **comparam** dois valores e o resultado pode ser ``False`` (falso) ou ``True`` (verdadeiro). 
Esse dois valores são chamados de valores **booleanos** em homenagem ao matemático George Boole (`<https://pt.wikipedia.org/wiki/George_Boole>`_). 

Assim como dizemos que as expressões aritméticas são reduzidas a um valor numérico inteiro ou real, 
as expressões relacionais são reduzidas a um valor booleano (ou seja, ``True`` ou ``False``). 
As expressões relacionais podem conter expressões aritméticas, como no seguinte trecho no ActiveCode:

.. activecode:: ac02_exemplo_expressão_relacional_01

    print( 'expressão 1: ', 2 + 3 == 3 + 2 * 1 )

    print( 'tipo de type( 1 == 3 ): ', type( 1 == 3 ) )  
    # resultado esperado: <class 'bool'>

Esse exemplo mostra que o resultado da expressão ``2 + 3 == 3 + 2 * 1`` é o valor booleano ``True``, que pertence ao tipo `bool`, 
que é uma abreviação de ``boolean`` (booleano em inglês).

O resultado da expressão é ``True`` pois a precedência dos operadores relacionais é menor que a dos operadores aritméticos, ou seja, as operações aritméticas são reduzidas primeiro, que resulta na comparação ``5 == 5``, 
e depois a expressão relacional é reduzida (no caso o operador `==`), resultando em ``True``. 



Teste o seu conhecimento
........................
    

.. mchoice:: questao_expressao_relacional_01
    :answer_a: True
    :answer_b: False
    :answer_c: Erro
    :answer_d: 0
    :correct: b
    :feedback_a: Incorreto: compare o valor de 11/5 com 11//5
    :feedback_b: Correto: 11/5 é maior que 11//5
    :feedback_c: Incorreto: não há erro de sintaxe na expressão 
    :feedback_d: Incorreto: o resultado é um booleano

    Qual o valor da expressão: ``11 / 5 <= 11 // 5``


.. mchoice:: questao_expressao_relacional_02
    :answer_a: True
    :answer_b: False
    :answer_c: 1
    :answer_d: 0
    :correct: a
    :feedback_a: Correto: lembre-se que são valores comparados, que são iguais, apesar de tipos diferentes.
    :feedback_b: Incorreto: Observe que 2.000 é o ``float`` dois, não o inteiro dois mil.
    :feedback_c: Incorreto: o resultado é um booleano
    :feedback_d: Incorreto: o resultado é um booleano

    Qual o valor da expressão que compara um real com um inteiro: ``2.000 == 2``


