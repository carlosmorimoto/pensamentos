
..  Copyright (C) Carlos Hitoshi Morimoto e José Coelho de Pina Jr.
    É permitida a cópia, distribuição e/ou modificação desde documento
    sob os termos da Licença de Documentação Livre GNU (GNU Free 
    Documentation License) versão 1.3 ou versão subsequente
    publicada pela Fundação de Software Livre (Free Software 
    Foundation); com as seções invariantes sendo Prefácio e 
    Lista de Contribuidores, sem texto de capa ou contra-capa.  
    Uma cópia da licença está disponível em:
    https://www.gnu.org/licenses/licenses.pt-br.html#FDL


.. qnum::
	:prefix: cap02-01
	:start: 1

..  shortname:: Aula 2
..  description:: Primeiros passos
      

.. Expressões aritméticas, relacionais e lógicas

.. Pensamentos lógico e aritmético

Introdução
----------

.. index:: expressão aritmética, expressão lógica, expressão aritmética


.. raw:: html

    <div align="right">
    <p></p>
    <blockquote>
    <p>
    "
    Não basta possuir uma mente boa:</br>
    o principal é usá-la bem.
    "
    </p>
    <p>René Descartes</p>
    </blockquote>
    <p></p>
    </div>

A lógica e aritmética são fundamentos para o pensamento computacional. 
Vamos iniciar esse capítulo com uma breve revisão de operadores aritméticas e suas propriedades e gostaríamos de chamar a sua atenção para que você observe como o computador resolve expressões aritméticas.

Cada um de nós pensa de uma forma diferente (ufa!). Por exemplo, sabemos que há várias maneiras distintas de calcular o resultado de uma expressão como (2+3)*4/2, sendo todas elas corretas no sentido de alcançar o mesmo resultado numérico. Por exemplo, podemos primeiro resolver a divisão 4/2 e depois a soma 2+3 que está entre parênteses, ou vice-versa, calculando primeiro a soma e depois a divisão.  

No caso de um computador, gostaríamos de frisar desde já que ele sempre realiza os cálculos da mesma maneira, de forma previsível e **determinística**, um operador de cada vez. Esse é um conceito importante pois a ordem de aplicação desses operadores altera o resultado e aprender a pensar nessa ordem nos ajuda a desenvolver um raciocínio lógico que vamos explorar na computação. Além de expressões aritméticas, vamos aprender também a escrever e prever os resultados de expressões lógicas e relacionais. 

.. admonition:: Spoiler Alert!

    O laboratório desse capítulo mostra como utilizar o iPython, um terminal que aceita comandos em Python. 
    Ao longo do capítulo, vamos usar a ferramenta CodeLens para mostrar o resultado de expressões usando a função ``print()`` do Python.



