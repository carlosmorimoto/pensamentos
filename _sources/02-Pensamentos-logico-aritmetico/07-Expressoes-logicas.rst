
..  Copyright (C) Carlos Hitoshi Morimoto e José Coelho de Pina Jr.
    É permitida a cópia, distribuição e/ou modificação desde documento
    sob os termos da Licença de Documentação Livre GNU (GNU Free 
    Documentation License) versão 1.3 ou versão subsequente
    publicada pela Fundação de Software Livre (Free Software 
    Foundation); com as seções invariantes sendo Prefácio e 
    Lista de Contribuidores, sem texto de capa ou contra-capa.  
    Uma cópia da licença está disponível em:
    https://www.gnu.org/licenses/licenses.pt-br.html#FDL


.. qnum::
	:prefix: cap02-07
	:start: 1



Expressões lógicas
------------------

As expressões lógicas são construídas usando operadores lógicos sobre valores (operandos) booleanos.
A tabela a seguir mostra a precedência dos operadores lógicos usados em Python:

.. table:: Precedência dos operadores lógicos

    +----------+--------------------------------+----------------+---------------+
    | Operador |  Descrição                     | Exemplo        | Resultado     |
    +==========+================================+================+===============+
    | ``not``  |  negação lógica                | not True       | False         |
    +----------+--------------------------------+----------------+---------------+
    | ``and``  |  E  lógico                     | True and False | False         |
    +----------+--------------------------------+----------------+---------------+
    | ``or``   |  OR lógico                     | True or False  | True          |
    +----------+--------------------------------+----------------+---------------+

Expressões lógicas (ou booleanas) combinam valores booleanos com operadores lógicos.

O operador ``not`` troca o valor do operando, ou seja, troca o valor booleano de ``True`` para ``False`` e de ``False`` para ``True``.

O operador ``and`` devolve ``True`` apenas quando seus dois operandos são ``True`` e devolve ``False`` caso contrário, quando ao menos um dos operandos é ``False``, como mostra a tabela a seguir. 

.. table:: Tabela do operador lógico ``and``

    +------------+------------+-----------+
    | ``X and Y``| X = True   | X = False |
    +------------+------------+-----------+
    | Y = True   | True       | False     | 
    +------------+------------+-----------+
    | Y = False  | False      | False     | 
    +------------+------------+-----------+


Já o operador ``or`` devolve ``False`` apenas quando seus dois operandos são ``False`` e devolve ``True`` caso contrário (quando ao menos um dos operandos é ``True``. 

.. table:: Tabela verdade do operador lógico ``or``

    +------------+------------+-----------+
    | ``X or Y`` | X = True   | X = False |
    +------------+------------+-----------+
    | Y = True   | True       | True      | 
    +------------+------------+-----------+
    | Y = False  | True       | False     | 
    +------------+------------+-----------+

Como o resultado das comparações usando operadores relacionais é um booleano, os operadores lógicos podem ser utilizados para combinar os resultados relacionais. 
Por exemplo, considere ``x`` um valor real qualquer. A expressão ``x >= -1 and x <= 1`` pode ser usado para testar se x pertence ao intervalo [-1, 1]. 
Assim, para ``x = 0`` temos que a expressão é ``True`` pois as duas expressões relacionais se tornam verdadeiras. 
Mas quando ``x = 2``, a comparação ``x <= 1`` se torna falsa e portanto o resultado do operador ``and`` é ``False``. 

Para esse caso em particular o Python permite a notação ``a <= x <= b`` para verificar se o valor ``x`` está no intervalo [a, b]. 
Embora essa notação seja mais compacta e simples de entender, observe que, se cada operador relacional for reduzido um por vez, 
como ``(a <= x) <= b`` ou ``a <= (x <= b)``, os termos entre parênteses corresponderiam a valores booleanos que são incompatíveis 
com valores numéricos. Por exemplo, seja ``a=10``, ``b=20``, ``x=5`` e a expressão ``(a <= x) <= b``. 
Nesse caso, o valor entre parênteses seria ``False``, resultado de (10 <= 5), e não teríamos como resolver 
``False <= 20`` dado que são valores de tipos incompatíveis para comparação.

Observe que o uso de incógnitas como ``a``, ``b`` e ``x`` em expressões é muito útil. 
Em computação, elementos como esses são chamados de **variáveis** e são objeto do próximo capítulo.

