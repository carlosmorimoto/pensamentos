
..  Copyright (C) Carlos Hitoshi Morimoto e José Coelho de Pina Jr.
    É permitida a cópia, distribuição e/ou modificação desde documento
    sob os termos da Licença de Documentação Livre GNU (GNU Free 
    Documentation License) versão 1.3 ou versão subsequente
    publicada pela Fundação de Software Livre (Free Software 
    Foundation); com as seções invariantes sendo Prefácio e 
    Lista de Contribuidores, sem texto de capa ou contra-capa.  
    Uma cópia da licença está disponível em:
    https://www.gnu.org/licenses/licenses.pt-br.html#FDL


.. qnum::
	:prefix: cap02-02
	:start: 1



Para onde vamos
---------------

Nesse capítulo vamos entender como o Python calcula o resultado de expressões lógicas, relacionais e aritméticas para que você alinhe seu pensamento com essas regras e seja capaz de escrever expressões corretas em seus programas.
Ao final desse capítulo você deverá ser capaz de:

* Ler e calcular o resultado de expressões 
    - aritméticas;
    - relacionais; e
    - lógicas.
* Escrever expressões corretas usando:
    - operadores aritméticos;
    - operadores lógicos;
    - operadores relacionais; e
    - combinações desses operadores.
* Usar o iPython para testar expressões aritméticas, lógicas e relacionais.
              
