#!/bin/bash 

echo "cd ../scripts"
cd ../scripts

echo "python migra2021.py"
python migra2021.py

echo "bash sync_sources.sh"
bash sync_sources.sh

echo "cd ../pensamentos"
cd ../pensamentos

[ -d ./build ] && rm -rf ./build && echo "limpei build"
[ -d ./published ] && rm -rf ./published && echo "limpei published"

runestone build
chown www-data:www-data build -R
runestone deploy
chown www-data:www-data published -R

[ -f /home/www-data/runeserver/restart_server.sh ] && sh /home/www-data/runeserver/restart_server.sh
