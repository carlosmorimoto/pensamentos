..  shortname:: Tópico: funções
..  description:: Como escrever funções em Python

Funções
=======

.. index:: funções, parâmetros, variáveis locais, def, docstring


Tópicos
-------

    - `Funções <https://panda.ime.usp.br/pensepy/static/pensepy/05-Funcoes/funcoes.html>`__;
    - `Variáveis locais
      <https://panda.ime.usp.br/pensepy/static/pensepy/05-Funcoes/funcoes.html#variaveis-e-parametros-sao-locais>`__;  
    - documentação: *docstring*

.. break


Vídeos
------

    - `Funções
      <https://www.youtube.com/watch?v=AMUxtcJ8yfc&index=17&list=PLcoJJSvnDgcKpOi_UeneTNTIVOigRQwcn>`__;
    - `Exercício resolvido -- coeficiente binomial
      <https://www.youtube.com/watch?v=se9JcK05PGw&list=PLcoJJSvnDgcKpOi_UeneTNTIVOigRQwcn&index=18>`__;
    - `Exercício resolvido -- função éPrimo
      <https://www.youtube.com/watch?v=Wcug8mjU8Sg&index=26&list=PLcoJJSvnDgcKpOi_UeneTNTIVOigRQwcn>`__. 
      
.. break


Exercício 1 de motivação
------------------------

O número de combinações possíveis de ``m`` elementos em grupos de ``n``
elementos (n <= m) é dada pela fórmula de combinação
``m!/((m-n)!n!)``. 

Escreva um programa que lê dois inteiros ``m`` e ``n`` e calcula a
combinação de ``m``, ``n`` a ``n``.

.. activecode:: aula_funcao_ex01_tentativa

    # Escreva o seu programa
    
    def main():

    #-----------------------------------------------------            
    if __name__ == '__main__': # chamada da funcao principal 
        main() 

Clique `aqui <exercicios/funcao01.html>`__ para ver uma solução.

Observe que o pedaço de código que calcula o fatorial é repetido
várias vezes. Uma importante ferramenta computacional é a capacidade
de abstrair partes relevantes e/ou que se repetem mais de uma vez
na forma de **funções**.

.. break

Funções em Python
-----------------

Para declarar uma função em Python faça o seguinte:


.. sourcecode:: python

    def nome_da_função ( parâmetros ):
        '''
        docstring contendo comentários sobre a função.
	Embora opcionais são fortemente recomendados. Os comentários
	devem descrever o papel dos parâmetros e o que a função faz.
        '''
        # corpo da função
        |
        | bloco de comandos
        |

.. break

Esqueleto de um programa em Python
----------------------------------

Para nos prevenir de alguns problemas (que serão explicados mais
tarde), vamos adotar nesse curso o seguinte esqueleto para escrever
programas em Python com funções. Nesse esqueleto, a primeira função
sempre será chamada de ``main`` e corresponderá a função principal do
programa, ou seja, aquela que resolve o problema. As demais funções
auxiliares devem ser definidas, em qualquer ordem, após a definição da
``main`` e, por fim, para executar o programa, a função ``main``
precisa ser chamada.

.. sourcecode:: python

    # função  principal  
    def main():
        ''' 
        Função principal, será a primeira a ser executado e
        será a responsável pela chamada de outras funções que 
        por sua vez podem ou não chamar outras funções que 
        por sua vez ...
        '''
        # corpo da função main
        |
        | bloco de comandos     
        | 

    # Declaração das funções 
    def f( parâmetros_de_f ):
        '''
        docstring da função f
        '''
        # corpo da função f
        |
        | bloco de comandos     
        | 

    def g( parâmetros_de_g ):
        '''
        docstring da função g
        '''
        # corpo da função g
        |
        | bloco de comandos     
        | 

    [...]

    # início da execução do programa
    #-----------------------------------------------------            
    if __name__ == '__main__': # chamada da funcao principal 
        main() # chamada da função main

	
O uso do "if __name__ == '__main__'" para a chamada da função main
permite que esse arquivo contendo uma ou várias funções seja incluído em
outros programas (usando "import" e suas variações) sem a necessidade
de reescrever ou copiar o código.

.. break
	
Exercícios
----------

Exercício 2
...........

Complete a função ``fatorial`` abaixo,
que recebe como parâmetro um número 
inteiro ``k``, ``k >= 0``, e retorna k!.

Escreva apenas o corpo da função.
Observe que o código já inclui
chamadas para a função ``fatorial``, para que
você possa testar a função.


.. activecode:: aula_funcao_ex02_tentativa

    def main():
        ''' testes da função fatorial '''
        print("0! =", fatorial(0))
        print("1! =", fatorial(1))
        print("5! =", fatorial(5))   
        print("17! =", fatorial(17))   
		
    #-----------------------------------------------------
    
    def fatorial(k):
        '''(int) -> int

        Recebe um inteiro k e retorna o valor de k!

        Pre-condição: supõe que k é um número inteiro não negativo. 
        '''

        k_fat = 1

	# COMPLETE ESSA FUNÇÃO
	
        return k_fat 

    #-----------------------------------------------------
    
    if __name__ == '__main__': # chamada da funcao principal 
        main() # chamada da função main

Clique `aqui <exercicios/funcao02.html>`__ para ver uma solução.

Exercício 3
...........

Usando a função do exercício 6.2, escreva uma função que recebe
dois inteiros, ``m`` e ``n``, como parâmetros e retorna a combinação
``m!/((m-n)!n!)``. 

.. activecode:: aula_funcao_ex03_tentativa

    def main():
        ''' Testes da função combincao '''
	print("Combinacao(4,2) =", combinacao(4,2))
	print("Combinacao(5,2) =", combinacao(5,2))
	print("Combinacao(10,4) =", combinacao(10,4))
		
    #-----------------------------------------------------
    
    def combinacao(m, n):
        '''(int, int) -> int
        Recebe dois inteiros m e n, e retorna o valor de m!/((m-n)! n!)
        '''

	# COMPLETE ESSA FUNÇÃO E MUDE O RETURN ABAIXO

        return "o resultado"

    #-----------------------------------------------------
    
    if __name__ == '__main__': # chamada da funcao principal 
        main() 


Clique `aqui <exercicios/funcao03.html>`__ para ver uma solução.


Exercício 4
...........

Usando as funções ``fatorial`` e ``combinacao`` dos exercícios anteriores,
escreva um programa que lê um inteiro ``n``, ``n >= 0`` e imprime 
os coeficientes da expansão de ``(x+y)`` elevado a ``n``.

Lembre-se de utilizar o esqueleto de programa com funções em Python.

.. activecode:: aula_funcao_ex04_tentativa

    # Escreva o seu programa usando o esqueleto sugerido
    
    def main():
        ''' Escreva aqui alguns testes '''

	print("Vixe! Ainda não fiz esse exercício.")

    #-----------------------------------------------------            
    if __name__ == '__main__': # chamada da funcao principal 
        main() 


Clique `aqui <exercicios/funcao04.html>`__ para ver uma solução.



