
# inclusão da biblioteca random, utilizada na classe Simulador
import random

# CONSTANTES
CAP_MIN = 10
CAP_MAX = 51  # Capacidade máxima, já ajustada com +1
SORT_MIN = 1
SORT_MAX = 11 # sorteio máximo, já ajustado com +1
        
# ----------------------------------------------------------------------------

def main():
    ''' A função main controla a entrada e saída,
        e a classe Simulador mantém o estado da simulação e
        os procedimentos para simular. Note que a classe
        Simulador "esconde" o módulo random da função main.
    '''
    # leitura do nome do usuário 
    usuário = input("Qual é o seu nome: ")
    #leitura da semente para criar o simulador
    semente = int(input("%s, por favor, digite a semente do gerador aleatório: " %usuário))
    s = Simulador(semente)
    desisto = False

    print("\nInício da simulação")
    while not s.fim and not desisto:
        print()
        print(s)
        # sorteia o próximo valor
        vol = s.sorteia()
        # decisão do usuário
        escolha = input("Volume sorteado: %d\nDeseja adicionar? (s/n) : " %vol)
        if escolha == 's':
            meio = s.adiciona(vol)
            if meio:
                print("O volume do balde atingiu/passou a metade.")
        else:
            desisto = True

    s.finaliza(vol)
                
# ----------------------------------------------------------------------------

class Recipiente:
    '''
        A classe Recipiente modela um recipente com capacidade cap e
        volume atual vol. Ela armazena também o volume derramado e
        indica se está cheio.
    '''
    def __init__(self, min, max):
        self.cap = random.randrange(min, max)
        self.vol = 0
        self.derramado = 0
        self.cheio = False

    def get(self):
        return self.vol, self.cap, self.derramado
    
    def deposita(self, v):
        '''
            Deposita um volume de água v e atualiza o estado do Recipiente.
        '''
        self.vol += v
        if self.vol >= self.cap:
            self.cheio = True
            self.derramado = self.vol - self.cap
            self.vol = self.cap
        return self.derramado

    def __repr__(self):
        if self.vol == self.cap:
            return "*%2d*"%self.vol
        else:
            return "[%2d]"%self.vol

# ----------------------------------------------------------------------------

class Simulador:
    ''' Classe que implementa o simulador para encher o balde.
        Encapsula random.
    '''
    def __init__(self, semente):
        random.seed(semente)
        # capacidade entre 10 e 50.
        self.rec = Recipiente(CAP_MIN, CAP_MAX)
        self.fim = False
        self.avisou = False

    def __repr__(self):
        return "Volume atual: %d"%(self.rec.vol)

    def sorteia(self):
        '''Retorna um número aleatório'''
        return random.randrange(SORT_MIN, SORT_MAX)

    def adiciona(self, v):
        ''' adiciona um volume v ao balse.
            A simulação termina quando o balde estiver cheio.
            Retorna True apenas da 1a vez que volume passa da metade da capacidade'''
        derramado = self.rec.deposita( v )
        
        if derramado > 0:
            self.fim = True

        if self.rec.vol >= self.rec.cap/2 and not self.avisou:
            self.avisou = True
            return True
        else:
            return False

    def finaliza(self, ult):
        '''(ult) -> None
           recebe o último volume sorteado e imprime as mensagens finais
        '''
        vol, cap, der = self.rec.get()
        print("\nFim da simulação")
        print("Capacidade do recipiente: %d" % cap)

        if der > 0:
            print("Volume final: %d" % vol)
            print("Recipiente está cheio e houve derramamento de água")
            print("Volume derramado foi: %d" %(der))
        else:
            print("Volume final: %d" % vol)
            if cap == vol:
                print("Recipiente está cheio e não houve derramamento de água")
            elif cap - vol >= ult:
                print("Recipiente não está cheio.")
                print("Havia espaço para o último volume sorteado: %d" % ult)
            else:
                print("Recipiente não está cheio.")
                print("Não havia espaço para o último volume sorteado: %d" % ult)
        
                                            
# ----------------------------------------------------------------------------

main()
