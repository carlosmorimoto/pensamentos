'''
Operações com números complexos

ao invés de fazer overload de +, -, /, *
estou usando add, sub, div, e mul

Uso método conjugado para calcular div

'''

def main():
    c1 = Complexo(1,2)
    c2 = Complexo(3,3)
    c3 = Complexo(1,-2)

    print(c1, c2, c3)
    
    print(c3, 'conjugado = ', c3.conjugado())
    print(c1, 'add', c3, '=', c1.add(c2))
    c4 = c1.mul( c2 )
    print(c4, '=', c1, 'mul', c2)
    print(c1, 'div', c1, '=', c1.div(c3))
    print(c1, 'div', c3, '=', c1.div(c3))
    print(c2, 'sub', c3, '=', c2.sub(c3))
    print(c2, 'sub', c2, '=', c2.sub(c2))


class Complexo:
    
    def __init__(self, r, i):
        self.real = r
        self.imag = i

    def __str__(self):
        return "%f%+fj"%(self.real, self.imag)

    def conjugado(self):
        return Complexo(self.real, -self.imag)
        
    def add(self, other):
        r = self.real + other.real
        i = self.imag + other.imag
        return Complexo(r, i)
    
    def sub(self, other):
        r = self.real - other.real
        i = self.imag - other.imag
        return Complexo(r, i)

    def mul(self, other):
        r = self.real * other.real - self.imag * other.imag
        i = self.imag + other.real + self.real * other.imag 
        return Complexo(r, i)

    def div(self, other):
        c = other.conjugado()
        d = other.mul(c)
        n = self.mul(c)
        return Complexo(n.real/d.real, n.imag/d.real)

main()
        

