# inclusão da biblioteca utilizada para o sorteio de números
import random

MAX_VOL = 51
MIN_VOL = 20

def main():
    # cria um objeto Simulacao usando uma semente
    semente = int(input("Semente: "))
    sim  = Simulacao(semente)

    # cabeçalho de início de uma simulação
    print("--------------------------------")
    print("Início da simulação\n")
    print("Em cada iteração as opções possíveis são: ")
    print("    0 para abandonar")
    print("    1 para depositar no recipiente 1")
    print("    2 para depositar no recipiente 2")
    print("    3 para depositar no recipiente 3")
    print("    4 para descartar a quantidade de água\n")
    
    while not sim.fim:
        vol = sim.sorteia()
        print("    Quantidade disponibilizada: %d" %vol)
        escolha = int(input("    Opção desejada: "))

        if escolha == 0:
            sim.fim = True
        elif escolha == 4:
            sim.descarta(vol)
        else:
            sim.deposita(escolha, vol)

    sim.finaliza()
    
#--------------------------------------------------------

class Recipiente:
    def __init__(self, min, max):
        self.cap = random.randrange(min, max)
        self.vol = 0
        self.derramado = 0
        self.cheio = False

    def deposita(self, v):
        self.vol += v
        if self.vol >= self.cap:
            self.cheio = True
            self.derramado = self.vol - self.cap
            self.vol = self.cap
        return self.derramado

    def __repr__(self):
        if self.vol == self.cap:
            return "*%2d*"%self.vol
        else:
            return "[%2d]"%self.vol
        
#--------------------------------------------------------

class Simulacao:
    def __init__(self, semente):
        random.seed(semente)
        
        self.r1 = Recipiente(MIN_VOL, MAX_VOL)
        self.r2 = Recipiente(3, self.r1.cap)
        self.r3 = Recipiente(1, self.r2.cap)
        self.max_sorteio = (self.r1.cap+self.r2.cap+self.r3.cap)//4+1
        
        self.fim = False
        self.pontos = 0
        self.rodada = 0
        self.excedente = 0
        self.descartes = 0 # até 3 descartes são permitidos
        
    def __repr__(self):
        return "    Volumes ocupados : %s %s %s"%(self.r1, self.r2, self.r3)

    def sorteia(self):
        ''' Cada rodada sorteia um certo volume de água'''
        self.rodada += 1
        # cabeçao antes da opção 
        print("    . . . . . . . . . . . . . . . . . . . . .")
        print("    Iteração  : %d" %self.rodada)
        print("    Descartes : %d" %self.descartes)
        print(self)
        return  random.randrange(1, self.max_sorteio)

    def descarta(self, vol):
        self.descartes += 1
        if self.descartes == 3:
            self.fim = True

    def deposita(self, escolha, vol):
        exc = vol
        haEspaco  = True
        
        while exc > 0 and haEspaco:
            if escolha==1:
                exc = self.r1.deposita(exc)
            elif escolha==2:
                exc = self.r2.deposita(exc)
            else:
                exc = self.r3.deposita(exc)

            print(self)

            if self.r1.cheio and self.r2.cheio and self.r3.cheio:
                haEspaco = False
            elif exc>0 : # se há excedente
                escolha = int(input("    Escolha recipiente para depositar excedente de %d: " %exc))

        self.excedente = exc
        if not haEspaco:
            self.fim = True

    def finaliza(self):
        ''' FIM das iterações de uma simulação '''
        cap_tot = self.r1.cap + self.r2.cap + self.r3.cap
        vol_tot = self.r1.vol + self.r2.vol + self.r3.vol
        pontos_ganhos = 0
        
        print()
        print("Fim da simulação")

        print(self)
        print("    Capacidades      :  %2d    %2d    %2d" %(self.r1.cap,self.r2.cap,self.r3.cap))
        print("    Capacidade total : %d" %(cap_tot))
        print("    Volume ocupado   : %d" %(vol_tot))
        print("    Volume livre     : %d" %(cap_tot - vol_tot))
        print("    Volume derramado : %d" %self.excedente)
        print("    Descartes        : %d" %self.descartes)
        print("--------------------------------\n")
        
#--------------------------------------------------------

main()
