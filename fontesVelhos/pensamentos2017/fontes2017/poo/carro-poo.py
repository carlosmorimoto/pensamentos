def main():
    pri = Carro('brasília', 1968, 'amarela', 80)
    seg = Carro('fuscão', 1981, 'preto', 95)

    pri.acelera(40)
    seg.acelera(50)
    pri.acelera(80)
    seg.para()
    seg.acelera(100)
    
class Carro:
    def __init__(self, m, a, c, vm):
        self.modelo = m
        self.ano    = a
        self.cor    = c
        self.vel    = 0
        self.maxV   = vm

    def __str__(self):
        if self.vel == 0: # parado dá para ver o ano
            return "%s %s %d"%(self.modelo, self.cor, self.ano)	
        elif self.vel < self.maxV:
            return "%s %s indo a %d Km/h"%(self.modelo, self.cor, self.vel)
        else:
            return "%s %s indo muito raaaaaapiiiiiiiidoooooo!"%(self.modelo, self.cor)
	
    def acelera(self, vel):
        self.vel = vel
        if self.vel > self.maxV:
            self.vel = self.maxV
        print(self)

    def para(self):
        self.vel = 0
        print(self)

main()
