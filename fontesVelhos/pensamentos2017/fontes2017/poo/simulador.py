'''
Módulo Simulador mantem o estado da simulacao e
os procedimentos para simular. Note que a classe
Simulador "esconde" o modulo random da funcao main.
'''

import random
from balde import Balde

# definicao de constantes
CAP_MIN = 10
CAP_MAX = 51 # ja ajustado ao random
VOL_MIN = 1
VOL_MAX = 11 # ja ajustado ao random

class Simulador:
    def __init__(self, semente):
        random.seed(semente)
        capacidade = random.randrange(CAP_MIN, CAP_MAX)
        self.rec = Balde(capacidade)
        self.vol = 0
        self.avisou = False

    def sorteia(self):
        self.vol = random.randrange(VOL_MIN, VOL_MAX)
        print()
        print("Volume atual   : ", self.rec.vol)
        print("Volume sorteado: ", self.vol)
        return self.vol

    def deposita(self):
        ''' adiciona o ultimo volume sorteado self.vol
            ao balde e retorna True se o
            balde estiver cheio e False caso contrario.
            '''
        self.rec.deposita( self.vol )

        if self.rec.vol >= self.rec.cap/2 and not self.avisou:
            self.avisou = True
            print("O volume do balde atingiu/passou a metade.")

        return self.rec.cheio

    def finaliza(self):
        print("\nFim da simulacao")
        print("Capacidade do balde: %d" % self.rec.cap)
        print("Volume final: %d" % self.rec.vol)

        if self.rec.der > 0:
            print("Balde esta cheio e houve derramamento de agua")
            print("Volume derramado foi: %d" %(self.rec.der))
        else:
            if self.rec.cheio:
                print("Balde esta cheio e nao houve derramamento de agua")
            elif self.rec.cap - self.rec.vol >= self.vol:
                print("Balde nao esta cheio.")
                print("Havia espaco para o ultimo volume sorteado: %d" % self.vol)
            else:
                print("Balde nao esta cheio.")
                print("Nao havia espaco para o ultimo volume sorteado: %d" % self.vol)


if __name__ == "__main__":
    s = Simulador(123)
    v1 = s.sorteia()
    r1 = s.deposita()
    print(v1, r1)
    v2 = s.sorteia()
    r2 = s.deposita()
    print(v2, r2)
    s.finaliza()

