'''
Operações com números racionais

Aqui eu estou fazendo overload dos operadores:
+ __add__
- __sub__
* __mul__
/ __truediv__ (__div__ em Python2.x é ambíguo)

Uso a função mdc para simplificar numerador e denominador.

'''
def main():
    r1 = Racional(1,4)
    r2 = Racional(2,3)
    r3 = Racional(2,6)

    print(r1, r2, r3)
    r3.simplifica()
    print(r2, '+', r3, '=', r2+r3)
    r4 = r1 * r2
    print(r4, '=', r1, '*', r2)
    print(r1, '/', r3, '=', r1 / r3)
    print(r2, '-', r3, '=', r2 - r3)
    print(r2, '-', r2, '=', r2 - r2)

def mdc(a, b):
    while a%b > 0:
        a, b = b, a%b
    return b

class Racional:
    def __init__(self, n, d):
        self.num = n
        self.den = d

# provavelmente seria melhor definir __repr__ para racionais
#    def __repr__(self):        
    def __str__(self):
        return '%d/%d'%(self.num, self.den)

    def simplifica(self):
        m = mdc(self.num, self.den)
        self.num //= m
        self.den //= m
        return self

    def __add__(self, other):
        n = self.num * other.den + self.den * other.num
        d = self.den * other.den
        return Racional(n, d).simplifica()

    def __sub__(self, other):
        n = self.num * other.den - self.den * other.num
        d = self.den * other.den
        return Racional(n, d).simplifica()
        
#    def __div__(self, other):  # classic division from Python 2.x
###         truediv vira padrao com from __future__ import division
#    def __fllordiv__(self, other):  # esse corresponde ao // 
    def __truediv__(self, other):  # true div standard in Python 3.x
        n = self.num * other.den
        d = self.den * other.num
        return Racional(n, d).simplifica()

    def __mul__(self, other):
        n = self.num * other.num
        d = self.den * other.den
        return Racional(n, d).simplifica()

main()
        

