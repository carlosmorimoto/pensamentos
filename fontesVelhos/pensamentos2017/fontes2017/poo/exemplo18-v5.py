class Racional:
    def __init__(self, n=0, d=1):
        self.put(n, d)

    def __str__(self):
        return "%d/%d"%(self.num, self.den)

    def get(self):
        return self.num, self.den

    def put(self, n=0, d=1):
        self.num, self.den = n, d

    def __mul__(self, other):
        n = self.num * other.num
        d = self.den * other.den
        return Racional(n, d)

    # escreva aqui o seu codigo para os metodos
    # __truediv__
    # __add__
    # __sub__
    # e ao menos um teste para cada metodo


# testes
r1 = Racional(2)
r2 = Racional(1,5)
print(r1, '*', r2, '=>', r1 * r2)
# teste do div:
# print(r1, '/', r2, '=>', r1 / r2)
#
# outros testes

