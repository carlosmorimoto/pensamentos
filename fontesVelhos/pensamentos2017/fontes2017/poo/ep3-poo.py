

#
#-----------------------------------------------------------------------
# ======================================================================
# DEFINIÇÃO DAS CONSTANTES
# ======================================================================
#-----------------------------------------------------------------------
#

CAIXA     = '$'   # Caixa sobre o piso
CAIXA_M   = '*'   # Caixa sobre uma marca
JOGADOR   = '@'   
JOGADOR_M = '+'   # Jogador sobre uma marca
MARCA     = '.'   # Marca sem jogador ou caixa
PAREDE    = '#'
VAZIO     = ' '   # Piso vazio

# movimentos e opções do jogo
BAIXO    = 'b'
CIMA     = 'c'
DIR      = 'd'
ESQ      = 'e'
SAIR     = 's'
VOLTAR   = 'v'

BAIXO_CX = 'B'
CIMA_CX  = 'C'
DIR_CX   = 'D'
ESQ_CX   = 'E'

def main():
    jogo = Sokoban()
#    nome_arq = input("Digite o nome de um arquivo sokoban: ")
    nome_arq = "puzzle01.txt"
    if jogo.carrega_mapa(nome_arq) is None:
        print("Não dá para continuar ;-(")
        return
        
    print(jogo)

    desisto = False
    fim = False
      
    while not desisto and not fim:
        print()
        print(        "--------------------------------------------------------------------")
        print()
        opcao = input("Digite b|c|d|e|v|s [baixo, cima, dir, esq, voltar, sair]: ")
        for i in list(opcao.strip()):
            pula = False ## ignora comandos inválidos
            if i in [BAIXO, CIMA, DIR, ESQ]:
                fim = jogo.move_jogador(i)
                if fim:
                    break
            elif i == VOLTAR:
                jogo.voltar()
            elif i == SAIR:
                desisto = True
                break
            else:
                pula = True
            if not pula:
                print()
                print(jogo)
    if fim:
        print()
        print(jogo)
        print("Parabéns")
    else:
        print("Você desistiu antes de acabar a partida")

    jogo.imprima_jogadas()
    
    
# ======================================================================
    
class Sokoban():
    def __init__(self):
        self.cena = Cena()
        self.jog  = Jogador()

    def move_jogador(self, mov):
        self.jog.mova(mov, self.cena)
        if self.cena.resolvida():
            return True
        return False
            
    def imprima_jogadas(self):
        self.jog.imprima_jogadas()

    def voltar (self):
        self.jog.undo(self.cena)
        
    def carrega_mapa(self, nome):
        '''(string) -> list

        Função que carrega um mapa de um arquivo no formato TXT,
        montando a cena e o jogador.
        '''
        try:
            arq = open(nome, 'r')
        except IOError:
            print("Não deu para abrir o arquivo: ", nome)
            return None
        else:
            mapa = []
            for linha in arq:
                mapa.append(list(linha.rstrip()))
            arq.close()
            
            jl, jc, js = self.cena.carrega_mapa(mapa)
            if jl is None:
                return None
            self.jog.set(jl, jc, js)
            return jl, jc, js
                
    def __str__(self):
        # cria e copia cena para um mapa quadrado
        mapa = []
        nlin = self.cena.nlin
        ncol = self.cena.ncol
        for lin in range(nlin):
            mapa.append( [' ']*ncol )
            for col in range(ncol):
                mapa[lin][col] = self.cena.data[lin][col]
        # coloca o jogador no mapa

        jl, jc, js = self.jog.get()
        mapa[jl][jc] = js

        # gera o string com o mapa para impressão
        mapalindo = '\n'
        s = '    '  # cria um espaço para os indices das linhas
        for col in range(ncol):
            s += '  %d '%col
        s += '\n'
        mapalindo += s
        
        moldura = '+---' * ncol
        moldura = '    ' + moldura + '+\n'
        mapalindo += moldura

        for lin in range(nlin):
            s = ' %2d '%lin
            for col in range(ncol):
                s+= "| %s "%(mapa[lin][col])
            s+='|\n'
            mapalindo += s
            mapalindo += moldura

        jx, jy, js = self.jog.get()
        mapalindo += "\nJogador indicado por %s está em (%d, %d)\n"%(js,jx,jy)
    
        return mapalindo

    
class Cena():
    def __init__(self):
        self.data = []
        self.nlin, self.ncol = 0, 0
        self.ncaixas = 0
        self.nmarcas = 0

    def resolvida(self):
        cont = 0
        for linha in self.data:
            for c in linha:
                if c == CAIXA_M:
                    cont += 1

        print(cont == self.ncaixas, cont , self.ncaixas)
        if cont == self.ncaixas:
            return True
        return False
    
    def carrega_mapa(self, mapa):
        '''(None) -> jlin, jcol
        '''
        self.data = mapa
        self.nlin = len(mapa)
        self.ncol = len(mapa[0])
        jl = jc = js = 0
        jogs = marcas = caixas = 0

        for lin in range(self.nlin):
            if self.ncol < len(mapa[lin]):
                self.ncol = len(mapa[lin])
                
            for col in range(len(mapa[lin])):
                if mapa[lin][col] == JOGADOR:
                    mapa[lin][col] = VAZIO
                    jl, jc, js = lin, col, JOGADOR
                    jogs += 1
                elif mapa[lin][col] == JOGADOR_M:
                    mapa[lin][col] = MARCA
                    marcas += 1
                    jl, jc, js = lin, col, JOGADOR_M
                    jogs += 1
                elif mapa[lin][col] == MARCA:
                    marcas += 1
                elif mapa[lin][col] == CAIXA:
                    caixas += 1
                elif mapa[lin][col] == CAIXA_M:
                    caixas += 1
                    marcas += 1
        
        if jogs == 0:
            print("Mapa inválido - não encontrei um jogador")
            return None, None, None
        elif jogs != 1:
            print("Mapa inválido - não encontrei mais de um jogador")
            return None, None, None
        elif caixas == 0 or marcas == 0 or caixas != marcas:
            print("Mapa inválido - o número de caixas precisa ser maior que zero e igual ao número de marcas")
            print("    Achei %d caixas e %d marcas"%(caixas, marcas))
            return None, None, None

        self.ncaixas = caixas
        self.nmarcas = marcas
        return jl, jc, js

    def get(self, lin, col):
        return self.data[lin][col]

    def set(self, lin, col, x):
        self.data[lin][col] = x

    def move_coisa(self, lin, col, mov):
        ''' move a coisa em lin, col se possível
            retorna true/false e a coisa que havia na posicao
            antes do movimento
        '''

        coisa = self.data[lin][col]
        
        if coisa == PAREDE:
            return False, coisa
        elif coisa == VAZIO or coisa == MARCA:
            return True, coisa

        ## é caixa, será que dá para mover?
        if mov == BAIXO:
            dl, dc = +1, +0
        elif mov == CIMA:
            dl, dc = -1, +0
        elif mov == DIR:
            dl, dc =  0, +1
        elif mov == ESQ:
            dl, dc =  0, -1
        else:
            return None, None

        if self.data[lin+dl][col+dc] == VAZIO:
            self.data[lin+dl][col+dc] = CAIXA
        elif self.data[lin+dl][col+dc] == MARCA:
            self.data[lin+dl][col+dc] = CAIXA_M
        else:
            return False, coisa

        if coisa == CAIXA:
            self.data[lin][col] = VAZIO
        else:
            self.data[lin][col] = MARCA
        return True, coisa
        
            
class Jogador:
    def __init__(self, l=0, c=0, s=0):
        self.jogadas = []
        self.set(l, c, s)

    def set(self, l, c, s):
        self.posl = l
        self.posc = c
        self.simb = s

    def __str__(self):
        return "\nJogador indicado por %s está em (%d, %d)\n"%(self.simb,self.posl,self.posc)
    
    def get(self):
        return (self.posl, self.posc, self.simb)
        
    def imprima_jogadas(self):
        print("As jogadas foram:")

        jogs = ''
        for j in self.jogadas:
            jogs += j
        print(jogs)
        
    def mova(self, mov, cena):
        if mov == BAIXO:
            dl, dc =  1, 0
        elif mov == CIMA:
            dl, dc = -1, 0
        elif mov == DIR:
            dl, dc =  0, 1
        elif mov == ESQ:
            dl, dc =  0,-1
        else:
            return None
        
        valido, coisa = cena.move_coisa(self.posl+dl, self.posc+dc, mov)
        
        if valido:
            self.posl += dl
            self.posc += dc

            if cena.get(self.posl, self.posc) == VAZIO:
                self.simb = JOGADOR
            else:
                self.simb = JOGADOR_M
                
            if coisa == CAIXA or coisa == CAIXA_M:
                self.jogadas.append(mov.upper())
            else:
                self.jogadas.append(mov)
            
        elif coisa == CAIXA or coisa == CAIXA_M:
            print( "Caixa está presa")
        else:
            print( "Bati na parede")

    def undo(self, cena):
        if len(self.jogadas) == 0:
            return
        
        mov = self.jogadas.pop()
        if mov in [BAIXO, CIMA, DIR, ESQ]:
            caixa = False
        else:
            caixa = True
            mov = mov.lower()
        
        if mov == BAIXO:
            dl, dc = 1, 0
            desfaz = CIMA
        elif mov == CIMA:
            dl, dc = -1, 0
            desfaz = BAIXO
        elif mov == DIR:
            dl, dc = 0, 1
            desfaz = ESQ
        else: # ESQ
            dl, dc = 0, -1
            desfaz = DIR

        if caixa:
            cena.move_coisa(self.posl+dl, self.posc+dc, desfaz)
        self.mova(desfaz, cena)
        self.jogadas.pop() ## remove a jogada do undo
        
main()        
        
