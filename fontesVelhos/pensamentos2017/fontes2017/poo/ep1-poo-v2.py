
# inclusão da biblioteca random, utilizada na classe Simulador
import random

# CONSTANTES
CAP_MIN = 10
CAP_MAX = 51 # Capacidade máxima, já ajustada com +1
VOL_MIN = 1
VOL_MAX = 11 # sorteio máximo, já ajustado com +1
        
# ----------------------------------------------------------------------------

def main():
    ''' A função main controla a entrada e saída,
        e a classe Simulador mantém o estado da simulação e
        os procedimentos para simular. Note que a classe
        Simulador "esconde" o módulo random da função main.
    '''
    #leitura da semente para criar o simulador
    semente = int(input("Digite a semente do gerador aleatório: "))
    jogo    = Simulador(semente)
    continua= True
    cheio   = False

    print("\nInício da simulação")
    while not cheio and continua:
        # sorteia o próximo valor
        jogo.sorteia()
        opcao = input("Deseja adicionar? (s/n) : ")
        if opcao == 's':
            cheio = jogo.deposita()
        else:
            continua = False
    jogo.finaliza()
                
# ----------------------------------------------------------------------------

class Recipiente:
    '''
        A classe Recipiente modela um recipente com capacidade cap e
        volume atual vol. Ela armazena também o volume derramado e
        indica se está cheio.
    '''
    def __init__(self, c):
        self.cap = c
        self.vol = 0  # volume atual
        self.der = 0  # volume derramado
        self.cheio = False

    def deposita(self, v):
        '''
            Deposita um volume de água v e atualiza o estado do Recipiente.
        '''
        self.vol += v
        if self.vol >= self.cap:
            self.cheio = True
            self.der = self.vol - self.cap
            self.vol = self.cap

    def __repr__(self):
        if self.vol == self.cap:
            return "*%2d*"%self.vol
        else:
            return "[%2d]"%self.vol

# ----------------------------------------------------------------------------

class Simulador:
    ''' Classe que implementa o simulador para encher o balde.
        Encapsula random.
    '''
    def __init__(self, semente):
        random.seed(semente)
        capacidade = random.randrange(CAP_MIN, CAP_MAX)
        self.rec = Recipiente(capacidade)
        self.vol = 0
        self.avisou = False

    def sorteia(self):
        self.vol = random.randrange(VOL_MIN, VOL_MAX)
        print()
        print("Volume atual   : ", self.rec.vol)
        print("Volume sorteado: ", self.vol)
        
    def deposita(self):
        ''' adiciona o ultimo volume sorteado self.vol
            ao recipiente e retorna True se o
            recipiente estiver cheio e False caso contrário.
            '''
        self.rec.deposita( self.vol )
        
        if self.rec.vol >= self.rec.cap/2 and not self.avisou:
            self.avisou = True
            print("O volume do recipiente atingiu/passou a metade.")

        return self.rec.cheio

    def finaliza(self):
        '''(ult) -> None
           recebe o último volume sorteado e imprime as mensagens finais
        '''
        print("\nFim da simulação")
        print("Capacidade do recipiente: %d" % self.rec.cap)
        print("Volume final: %d" % self.rec.vol)

        if self.rec.der > 0:
            print("Recipiente está cheio e houve derramamento de água")
            print("Volume derramado foi: %d" %(self.rec.der))
        else:
            if self.rec.cheio:
                print("Recipiente está cheio e não houve derramamento de água")
            elif self.rec.cap - self.rec.vol >= self.vol:
                print("Recipiente não está cheio.")
                print("Havia espaço para o último volume sorteado: %d" % self.vol)
            else:
                print("Recipiente não está cheio.")
                print("Não havia espaço para o último volume sorteado: %d" % self.vol)
        
                                            
# ----------------------------------------------------------------------------

main()
