
def main():
    pri = Carro('brasília', 1968, 'amarela')
    seg = Carro('fuscão', 1981, 'preto')

    print(pri)
    print(seg)

    s1 = str(pri).split()
    s2 = str(seg).split()
    print("Eu gosto de %s %s e %s %s"%(s1[0], s1[1], s2[0], s2[1])) 

class Carro:
    def __init__(self, m, a, c):
        self.modelo = m
        self.ano    = a
        self.cor    = c

    def __str__(self):
        return "%s %s %d"%(self.modelo, self.cor, self.ano)

main()
