"""
EP4 de MAC2166 - 2015 S1

Calculadora Polonesa

- `var` e `fim` são palavras reservadas e nao devem ser utilizadas como nome de variaveis.

"""

BINARIOS   = "+-*/%^="
UNARIOS    = "!"
BRANCOS    = [' ', '\n', '\t']
DIGITOS    = "0123456789."

# tipos de tokens
REAL   = 0  # número real
OP_UNI = 1  # operador unário !
OP_BIN = 2  # operador binário * / % + - ^ = 
NOME   = 3  # nome de variável: sequência de caracteres sem dígitos, brancos ou operadores

############################################################
def main():
    """Programa principal"""
    calc  = Calculadora()
    menu()
    exp = input("> ")
    
    while exp != "fim":
        if exp == "var":
            print(calc.escopo)
        else:
            res, mensagem = calc.valor(exp)
            if res is not None:
                print("=> ", res)
            else:
                print(mensagem)                
        exp = input("> ")

############################################################
def menu():
    """(None) -> None
       mostra o menu de opções da calculadora
    """
    print()
    print('Digite: ')
    print('  - uma expressão em notação polonesa como "2 3 + A ="; ou')
    print('  - "var" para imprimir a lista de variáveis no escopo atual; ou')
    print('  - "fim" para terminar.')
    print()
    print('Operações possíveis:')
    print('  - as cinco operações aritméticas básicas: * / % + -')
    print('  - potência: ^')
    print('  - negação : ! (troca de sinal)')
    print('  - atribuição: = ')
    print()

############################################################

class Token:
    """ Token é um elemento da expressão booleana,
        decoficado, que pode ser empilhado
        """
    def __init__(self, t=None, v=None):
        self.tipo  = t
        self.valor = v

    def __str__(self):
        t = self.tipo
        if t is not None:
            if t == REAL:
                return "Tipo REAL com valor %f"%(self.valor)
            elif t == NOME:
                return "Tipo NOME com valor %s"%(self.valor)
            elif t == OP_BIN:
                return "Tipo OP_BIN com valor %s"%(self.valor)
            else:
                return "Tipo OP_UNI com valor %s"%(self.valor)
        return ""
    

############################################################

class Escopo:
    """A classe escopo mantem e manipula as variáveis"""
    def __init__(self):
        self.Vars = []
        self.Vals = []

    def __str__(self):
        n = len(self.Vars)
        s = "Lista das %d variáveis definidas no escopo atual: \n"%n
        for i in range(n):
            s+="%s\t=%.6f\n"%(self.Vars[i], self.Vals[i])
        return s

    def acha(self, nome):
        n = len(self.Vars)
        i = 0
        while i < n:
            if self.Vars[i] == nome:
                return i
            i += 1
        return None
                
    def armazena(self, nome, valor):
        i = self.acha(nome)
        if i is None:
            self.Vars += [nome]
            self.Vals += [valor]
        else:
            self.Vals[i] = valor

    def valor(self, nome):
        i = self.acha(nome)
        if i is None:
            return None
        else:
            return(self.Vals[i])

    def calcula(self, op, ult, penult=None):
        val1 = ult.valor
        if ult.tipo == NOME:
            val1 = self.valor(val1)
            if val1 is None:
                print("ERRO: Não dá para fazer a operação %s pois a variável %s não definida."%(op.valor, ult.valor))
                return None

        if op.valor == '!': # muda sinal de ult
            return -val1
        elif op.valor == '=': # armazena
            self.armazena(penult.valor, val1)
            return val1
        
        # AGORA SÃO OS DEMAIS OPERADORES BINÁRIOS
        if penult is not None:
            val2 = penult.valor
            if penult.tipo == NOME:
                val2 = self.valor(val2)
                if val2 is None:
                    print("ERRO: Não dá para fazer a operação %s pois a variável %s não definida."%(op.valor, penult.valor))
                    return None

        if op.valor == '+':
            return val1 + val2
        elif op.valor == '-':
            return val1 - val2
        elif op.valor == '*':
            return val1 * val2
        elif op.valor == '/':
            return val1 / val2
        elif op.valor == '^':
            return val1 ** val2
        elif op.valor == '%':
            return val1 % val2
        else:
            return None
        
############################################################
class Calculadora:
    def __init__(self):
        self.pilha  = []
        self.escopo = Escopo()
        
    def valor(self, exp):
        valida = True
        self.pilha = []
        sep = self.separa(exp)
        tokens = self.tokeniza(sep)

        for t in tokens:
            if t.tipo == OP_UNI:
                val1 = self.pilha.pop()
                if val1 == None:
                    return None, "ERRO: expressão inválida a partir de %s "%t
                else:
                    res = self.escopo.calcula(t, val1)
                    if res is not None:
                        self.pilha.append(Token(REAL, res))                
            elif t.tipo == OP_BIN:
                val1 = self.pilha.pop()
                if val1 == None:
                    return None, "ERRO: expressão inválida a partir de %s "%t
                else:
                    val2 = self.pilha.pop()
                    if val2 == None:
                        return None, "ERRO: expressão inválida a partir de %s "%t
                    else:
                        res = self.escopo.calcula(t, val2, val1)
                        if res is not None:
                            self.pilha.append(Token(REAL, res))
            else:
                self.pilha.append(t)

        tam = len(self.pilha)
        if tam == 0:
            return None, None
        elif tam == 1:
            t = self.pilha.pop()
            if t.tipo == REAL:
                return t.valor, ""
            elif t.tipo == NOME:
                val = self.escopo.valor(t.valor)
                if val is None:
                    return None, "ERRO: variável %s ainda não foi inicializada"%t.valor
                else:
                    return val, ""
            else:
                return None, "ERRO: expressão resultou em valor inválido na pilha"
        else:
            return None, "ERRO: expressão resultou em pilha de tamanho > 1"
 
    def separa(self, exp): 
        """(self, str) -> list"""
        sai = []
        tok = ''
        i = 0
        while i < len(exp):
            if exp[i] in UNARIOS or exp[i] in BINARIOS:
                if tok != '':
                    sai += [tok]
                    tok = ''
                sai += [exp[i]]
                i+=1
            elif exp[i] in BRANCOS:
                if tok != '':
                    sai += [tok]
                    tok = ''
                i+=1
            elif exp[i] in DIGITOS:
                if tok != '':
                    sai += [tok]
                    tok = ''
                while i<len(exp) and exp[i] in DIGITOS:
                    tok += exp[i]
                    i += 1
                sai+=[tok]
                tok = ''
            else:
                tok+=exp[i]
                i += 1
        if tok != '':
            sai += [tok]
        return sai

    def tokeniza(self, tokens):
        """(self, list) -> list"""
        sai = []
        for token in tokens:
            if token in UNARIOS:
                sai += [ Token(OP_UNI, token) ]
            elif token in BINARIOS:
                sai += [ Token(OP_BIN, token) ]            
            elif token[0] in DIGITOS:
                sai += [ Token(REAL, float(token)) ]
            else:
                sai += [ Token(NOME, token) ]
        return sai                 
        
        
############################################################
    
main()

