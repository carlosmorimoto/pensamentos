
.. qnum::
    :prefix: intro-2-
    :start: 1
 
Calculadoras e Computadores
---------------------------

.. index:: computador, arquitetura de von Neumann, ULA, UCP

A habilidade de representar quantidades e manipulá-las (como "fazer conta" de mais e de menos) é uma necessidade básica para muitas atividades humanas. O ábaco e a régua de cálculo são exemplos de instrumentos inventados para nos ajudar a "fazer contas". 

A primeira calculadora mecânica foi desenvolvida por Blaise Pascal em 1642. A *Pascaline* (`<https://en.wikipedia.org/wiki/Pascal%27s_calculator>`_) utilizava um complexo sistema de engrenagens para realizar operações de adição e subtração.

Além de realizar operações matemáticas como fazem as calculadoras, os computadores são máquinas que podem ser programadas. Os programas de computador permitem a repetição de cálculos repetidas vezes, como por exemplo, calcular o calcular o pagamento de cada funcionário baseado no número de horas trabalhadas.

Os primeiros computadores eletrônicos (usando válvulas) foram desenvolvidos na década de 1940 e o rápido e contínuo desenvolvimento tecnológico resultou nos computadores modernos, que tem desempenho muitas vezes superior a esses primeiros computadores, com tamanho e consumo de energia muitas vezes menor. A figura 1 mostra um diagrama de blocos com os componentes básicos de um computador: a unidade central de processamento (UCP), a memória, e os dispositivos de entrada e saída. Esses dispositivos são interligados por uma via de comunicação de dados. 

.. image:: Figuras/vonNeumann.png
    :width: 600
    :align: center

Esse diagrama ilustra a arquitetura proposta por John von Neumann (`<https://en.wikipedia.org/wiki/Von_Neumann_architecture>`_).

A primeira coisa que vemos quando olhamos para um computador são os dispositivos de entrada e saída, necessários para que o computador se comunique com outros computadores e pessoas. Dessa forma, os dispositivos de entrada permitem que o computador receba os dados a serem processados (por meio de dispositivos como teclado, mouse, disco ou Internet) e transmita os resultados para um dispositivo de saída como uma impressora ou um monitor.

Esses dados ficam armazenados na memória do computador em formato digital, ou seja, na forma de dígitos binários. Um **dígito binário** assume apenas 2 valores (como zero ou um) e são mais adequados para representar eletronicamente que um dígito decimal pois é possível utilizar uma chave eletrônica (aberta ou fechada) ou um capacitor (carregado ou descarregado). Um único dígito binário é chamado de *bit* e 8 (oito) bits formam um *byte*. A capacidade da memória é comumente representada em *kilobyte* ou KB (cerca de mil bytes), *megabytes* ou MB (cerca de um milhão de bytes), *gigabytes* ou GB (um bilhão) e assim por diante. Hoje é comum encontrar discos com mais de 1 *terabyte* (um trilhão de bytes) de capacidade. Para se ter uma ideia, um filme de cerca de 2 horas de duração gravado em alta resolução pode ser armazenado usando cerca de 2 GB no formato mp4, enquanto todos os textos contidos na Enciclopédia Britânica (`<https://websitebuilders.com/how-to/glossary/terabyte/>`_) no ano 2010 (último ano quando foi publicada com 32 volumes) pode ser armazenado em apenas 300 MB (300 milhões de caracteres ou cerca de 50 milhões de palavras). 

Um *programa de computador* é uma sequência de instruções que deve ser executada uma de cada vez pela UCP. Os programas também ficam armazenados na memória em código binário (também chamado de código de máquina ou em linguagem de máquina).
Uma instrução pode ser de natureza aritmética ou lógica, que basicamente permite ao computador executar operações matemáticas como uma calculadora, ou de controle. As instruções de controle permitem, entre outras coisas, movimentar dados e desviar a execução do programa. A UCP contém um pouco de memória local (ou um grupo de registradores) para trabalhar os dados internamente. 

Um registrador importante que possibilita a execução da sequência correta de instruções é chamado de *contador de programa* (CP). O CP simplesmente aponta para o local da memória (endereço) que contém a próxima instrução  a ser executada. A unidade de controle da UCP busca a instrução indicada pelo CP, a decodifica e decide se a executa ou passa para a unidade lógica e aritmética (ULA) executar. Após terminar de executar a instrução, o CP é atualizado para que a unidade de controle busque a próxima instrução. O computador fica simplesmente executando esse ciclo (busca -> executa -> atualiza CP -> busca -> ...) ininterruptamente. Apesar de cada instrução ser bastante simples, um computador pessoal moderno é capaz de executar bilhões de instruções por segundo, que lhe confere o poder de realizar tarefas bastante complexas com desempenho superior a de um ser humano. Em geral essas tarefas são repetitivas e entediantes, permitindo que as pessoas tenham mais tempo livre para as atividades criativas.

