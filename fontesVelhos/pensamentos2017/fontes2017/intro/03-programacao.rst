
.. qnum::
    :prefix: intro-3-
    :start: 1
 
Programação e Computação
------------------------

.. index:: hardware, software, linguagem de máquina, linguagem de baixo nível, linguagem de alto nível, código fonte

A parte física dos computadores é chamada costumeiramente de **hardware**. A parte digital responsável pelo funcionamento da parte física (os programas de uma forma geral) é chamada de **software**. O objetivo de programar é criar software para resolver algum problema computacional, que pode ser um simples cálculo matemático ou algo um pouco mais complexo como controlar a navegação de um foguete até a Lua.
A atividade de desenvolver esses programas é chamada de programação. 

Um conceito que devemos relembrar para começar a programar é que um computador comum executa apenas uma instrução por vez e, portanto, a ordem das instruções é muito importante.
Observe que, como cada instrução pode modificar o conteúdo da memória, cada nova instrução executada utiliza os dados **atualizados**, resultantes da execução passo-a-passo das instruções anteriores. A cada instante, dizemos que o **contexto** de um programa é formado pelo conjunto de valores que o programa pode acessar, armazenados na memória naquele instante.

Um programa portanto é uma sequência de instruções que, ao ser executada, vai "transformando" (processando por meio de cálculos) os dados de entrada até chegar no resultado desejado na saída. 
Podemos ainda considerar que cada instrução gera um resultado intermediário (dado processado) que pode ser armazenado na memória e cada resultado intermediário seguinte fica mais perto do resultado final. Resultados armazenados na memória podem ser acessados por meio de **variáveis**, que são "nomes" que fazem referência aos dados.

Considere por exemplo o problema de multiplicar 2 por 5. Usando apenas instruções de somar, podemos começar com o resultado de 2 + 2 (ou seja 4) e armazenar em uma variável "soma". A seguir adicionamos 2 ao valor associado a "soma" (resultando no valor intermediário 6) e mais 2 e finalmente mais 2, chegando no resultado final 10.

.. admonition:: Exemplo de cálculos intermediários até o resultado final

    soma = 2 + 2       -> variável soma recebe o 1o resultado intermediário, com valor 4

    soma = soma + 2    -> soma recebe o resultado de soma + 2, ou (2 + 2) + 2

    soma = soma + 2    -> soma recebe o resultado de soma + 2, ou ((2 + 2) + 2) + 2

    soma = soma + 2    -> soma recebe o resultado de soma + 2, ou (((2 + 2) + 2) + 2) + 2

Como vimos, um programa com instruções que uma UCP é capaz de executar é escrito em **linguagem de máquina** ou **linguagem de baixo nível**, por serem as linguagens "compreendidas" pelas máquinas. Muito cedo na história dos computadores eletrônicos se descobriu que é muito difícil programar usando linguagem de máquina e assim foram surgindo várias linguagens de programação, cada uma com propostas diferentes para tratar certos problemas computacionais.

De uma forma genérica porém, o desenvolvimento das linguagens de programação buscou se aproximar de alguma linguagem natural que usamos para nos comunicar, como o inglês e o português. Essas linguagens, chamadas de **linguagens de alto nível** por serem mais fáceis de usar por humanos, vem se tornando mais poderosas ao apresentar conjuntos de instruções mais extensos e recursos mais apropriados a aplicações modernas como computação gráfica, jogos eletrônicos e inteligência artificial. 

Como os computadores não entendem as linguagens de alto nível, é necessário traduzir ou interpretar esses programas antes de serem executados pela máquina. Vamos chamar de **código fonte** o programa escrito pelos programadores e que fica armazenado em um **arquivo fonte**. 
Os programas que traduzem um arquivo fonte em  linguagem de alto nível e geram um arquivo executável em linguagem de máquina são chamados de **compiladores**. Ao invés de traduzir o programa e gerar um arquivo executável, algumas linguagens **interpretam**  e executam cada comando como se rodassem diretamente na linguagem de alto nível. Esse esforço para interpretar cada instrução durante a execução pode prejudicar um pouco o desempenho das linguagens interpretadas, mas tendem a agilizar o desenvolvimento de programas por evitarem a compilação.

A (ciência da) computação vai além da programação. Como ciência, ela estuda os fundamentos teóricos da informação (dados) e da computação (transformação dos dados) e as considerações práticas para aplicar essa teoria em sistemas computacionais. Por exemplo, nesse curso vamos estudar formas para representar diferentes tipos de dados e algoritmos eficazes para manipular esses dados. Consideramos o estudo dessas estruturas e algoritmos uma excelente forma de desenvolver um raciocínio computacional, algo parecido com estudar partidas entre grandes mestres para aprender a jogar xadrez. 


Aplicações
----------

É difícil imaginar hoje um ramo da atividade humana que não tenha se beneficiado diretamente dos avanços da computação. 
Seus impactos são tão notáveis que não precisamos ir longe para perceber seus efeitos. 
A começar talvez pelos dispositivos móveis como os telefones celulares rodando inúmeros aplicativos que, para o bem ou para o mal, vem transformando comportamentos pessoais, sociais e até criando alguns novos.
Esse exemplo mostra que um dispositivo "comum" como um telefone, ao se tornar digital e conectado à Internet, se torna uma coisa muito diferente e, por ser programável, sua utilidade aumenta a cada nova aplicação que instalamos nessa coisa (a coisa vira outra coisa!).

Uma outra tecnologia que começa a ser notada a nível pessoal é a Internet das Coisas ou IoT (do inglês *Internet of Things*). A IoT vem permitindo que "coisas" se tornem mais "inteligentes", desde uma lâmpada que pode ser controlada remotamente pela Internet, até a integração de "coisas" em toda uma cidade. Dessa forma, as cidades inteligentes poderão otimizar melhor seus recursos, como por exemplo melhoria no trânsito, maior conservação de energia e redução de poluentes. 

Assim, a medida que mais coisas se tornam digitais, conectadas à Internet e programáveis, abrem-se infinitas novas oportunidades de uso, tanto de coisas agindo individualmente, quanto coisas agindo em conjunto com outras coisas. Imagine agora a quantidade de dados produzidos por essas coisas. Saber programar novos usos dessas coisas é certamente um desafio que, quem sabe, você mesmo vai ajudar a resolver usando algumas das habilidades que você vai desenvolver nesse curso. 

Mas não precisamos ser tão ambiciosos. Para alunos e alunas de cursos de engenharia e ciências exatas por exemplo, 
as ferramentas computacionais vistas nesse curso podem ajudar a entender conceitos de cálculo e estatística, visualizar dados multidimensionais e simular modelos. Alunas e alunos das áreas humanas e biológicas também podem utilizar as habilidades desenvolvidas nesse curso para começar a explorar seus dados e testar seus próprios modelos. Mas seja qual for a sua área de interesse, desenvolver um raciocínio computacional vai ajudar você a desenvolver melhores programas assim como pode ajudar você a utilizar programas existentes de forma mais eficaz.



Laboratório: Como instalar Python em seu computador
---------------------------------------------------

O desenvolvimento do raciocínio computacional requer muita prática. Para isso, cada capítulo traz uma seção de exercícios em laboratório que você deve realizar utilizando um computador para praticar.
Nesse primeiro capítulo, vamos ajudar você a arrumar o seu ambiente de programação e testá-lo, 
para que você possa fazer os exercícios dos próximos capítulos.
A instalação também é um bom exercício inicial para os alunos e alunas sem prévia experiência com computadores e serve para que eles e elas comecem a se habituar com o uso de computadores e das ferramentas de desenvolvimento de programas que serão utilizadas nos exercícios.


Instalando o Python
...................

.. index:: Anaconda

Vamos utilizar o pacote `Anaconda <https://www.anaconda.com>`__ que reúne várias ferramentas úteis para desenvolver programas científicos. Como historicamente a maioria de nossas alunas e alunos costumam utilizar um computador com o sistema operacional Windows, as instruções a seguir são voltadas para essa plataforma. Caso você use um outro sistema operacional, como Linux ou Mac OS, você pode seguir as instruções específicas para o seu sistema na página de instalação do Anaconda (`<https://docs.anaconda.com/anaconda/install/>`_).


Antes de instalar, é preciso baixar (gratuitamente) o software da Anaconda. 
Para isso, dirija o seu navegador favorito para a página de *download* da Anaconda (`<https://www.anaconda.com/distribution>`_).
Clique no botão para fazer o download da versão 3.x do Python (que na figura corresponde a versão 3.7). Sugerimos que você salve o arquivo para evitar fazer o download novamente caso algo de errado aconteça durante a instalação. 

.. image:: Figuras/anaconda/anaconda.com.salvar.png
    :width: 600
    :align: center

Depois que terminar o download (que pode demorar um pouco dependendo da velocidade da sua conexão e por ser um arquivo meio grande), abra o arquivo que você baixou (clique nele) para executar o instalador. Isso deve abrir uma janela como

.. image:: Figuras/anaconda/anaconda01.png
    :width: 600
    :align: center

e depois basta "concordar" com todas as sugestões, como na sequência de figuras abaixo.

- clique em "I agree"

.. image:: Figuras/anaconda/anaconda02.png
    :width: 600
    :align: center

- selecione "Just me (recommended)"

.. image:: Figuras/anaconda/anaconda03.png
    :width: 600
    :align: center

- selecione a pasta de instalação e clique em "Next"

.. image:: Figuras/anaconda/anaconda04.png
    :width: 600
    :align: center

- selecione a opção "Register Anaconda as my default Python 3.7" e clique "Install".

.. image:: Figuras/anaconda/anaconda05.png
    :width: 600
    :align: center

Aguarde o final da instalação, que pode demorar vários minutos.

.. image:: Figuras/anaconda/anaconda06.png
    :width: 600
    :align: center

e pronto! O navegador pode sugerir para instalar outras ferramentas, mas você não precisa fazer mais nada e pode continuar clicando em "Next" até fechar o instalador do Anaconda.


Teste a sua instalação usando o Anaconda Prompt
...............................................

.. index:: Anaconda prompt, terminal, Python shell

Clique na barra de pesquisa do Windows e digite "Anaconda Prompt". Dentre as primeiras opções deve estar o 
"Anaconda Prompt (Anaconda 3)" que você acabou de instalar, como ilustrado abaixo:

.. image:: Figuras/anaconda/anaconda.pesquisa.png
    :width: 600
    :align: center

Clique nessa opção ("Anaconda Prompt") para abrir uma janela 
como essa mostrada na figura abaixo, que vamos chamar de "terminal" (para ficar mais compatível com outras instalações).

.. image:: Figuras/anaconda/anaconda.prompt01.png
    :width: 600
    :align: center

No terminal, digite "python" para abrir o **Python Shell**:

.. image:: Figuras/anaconda/anaconda.prompt02.png
    :width: 600
    :align: center

O Python Shell faz parte do ambiente Python que permite você digitar comandos de forma interativa, ou seja, para cada comando que você envia o Python te dá uma resposta. O Python Shell é muito útil para testar um comando quando você não tiver certeza de como funciona e também serve como uma calculadora.
Por exemplo, digite a expressão: "2 + 2" seguida por "ENTER" (ou qualquer operação que desejar) e veja o resultado.

Toda vez que você aperta ENTER você está enviando ao Python Shell um comando, que ele vai tentar interpretar. Se você enviar um comado incorreto ou incompleto, como 2 * ENTER, o Shell vai apontar um *SyntaxError* (Erro de Sintaxe) que significa que o Python não conseguiu entender o comando.

.. image:: Figuras/anaconda/anaconda.prompt03.png
    :width: 600
    :align: center



Teste a sua instalação usando o Spyder
......................................

.. index:: IDE (Integrated Development Environment), Spyder

O Spyder é um ambiente integrado de desenvolvimento (IDE do inglês *Integrated Development Environment*)
que faz parte do Anaconda. Ele é chamado de ambiente integrado pois reúne várias ferramentas em um só lugar, dentro de uma mesma interface gráfica. Dentre as principais ferramentas que vamos utilizar estão o editor de texto, o Python Shell, e o depurador de programas (ou *debugger* em inglês). 

Para abrir o Spyder, digite "spyder" na área de busca do Windows (como você fez anteriormente para abrir o Anaconda Prompt). 
A figura a seguir ilustra a janela com a interface do Spyder.

.. image:: Figuras/anaconda/spyder01.png
    :width: 600
    :align: center

A janela no canto inferior direito funciona como um Python shell. Você pode clicar nessa janela e digitar uma expressão aritmética qualquer, como "2 + 3 * 4 ENTER" e ver o que acontece. 

O Python shell fica esperando um comando do usuário. Assim que o usuário aperta "ENTER", o Python shell lê o texto que foi digitado pelo usuário na linha de comando e tenta executá-lo. Caso seja um comando válido, o Python shell mostra o resultado na saída. Caso seja um comando inválido, o Python shell mostra uma mensagem de erro que descreve o problema com o comando recebido. 

Para escrever programas, mesmo que simples, devemos usar o editor de texto para escrever todos os comandos, salvar o programa em um arquivo e depois executar o programa. Copie o seguinte programa para o editor do Spyder no seu computador. 

.. code-block:: python

    # IMPORTANTE: todas as linhas abaixo devem começar na 1a coluna
    nome = input("Digite o seu nome: ")
    print("olá", nome, "!")
    print("Parabéns! Seu 1o programa em Python está funcionando!")

O texto do seu programa precisa estar **exatamente igual** a esse exemplo.
Em Python, a tabulação de cada linha é muito importante e força uma estrutura visual que ajuda a identificar os blocos de um programa. Como esse exemplo é um programa com apenas 1 bloco, 
verifique se todas as linhas do seu programa estão alinhadas na 1a coluna no editor do Spyder. 
Caso não estejam, remova todos os espaços iniciais para que cada linha comece exatamente na 1a coluna do editor.

Salve esse programa clicando na opção de menu no canto superior da janela 

.. code-block::  

    Arquivo -> Salvar como

em alguma pasta no disco do seu computador. 
Use um nome qualquer com "teste.py". Evite usar espaços e caracteres acentuados no nome e lembre-se de sempre incluir a extensão ".py" para indicar que se trata de um programa em Python. 
Depois de salvo, clique na opção de menu

.. code-block::
    
    Executar -> Executar 

(ou simplesmente aperte a tecla "F5") para executar esse programa ou no triângulo verde na barra de ferramentas localizada abaixo da linha de menu no topo da janela.

Ao ser executado, o programa é executado instrução por instrução pelo Python, mas **sem o shell**. Em inglês, *shell* significa concha ou casca. Na janela do Python shell, essa casca corresponde a uma camada de interface entre o Python e o usuário. 
O shell permite a interação com o Python por meio da **linha de comando**, ou seja, a linha que, quando digitamos algum comando e apertamos "ENTER", o shell passa o comando para o Python. O shell também recebe a resposta e a exibe na tela. Ou seja, o Python, como linguagem, não tem uma interface fixa. Sem o shell, os programas precisam especificar como a informação entra e sai do programa. Vários programas funcionam sem o shell. Por exemplo, um programa em Python pode controlar o tráfego em um site na Internet, simplesmente recebendo e enviando dados de uma placa de rede. 

Esse exemplo usa as funções *input()* e *print()* do Python para receber dados e imprimir mensagens. 
Nesse caso, vamos usar a janela do Python shell como casca dos nossos programas.
Assim, ao executar o programa observe que a mensagem "Digite o seu nome: " aparece na janela do Python shell. Clique nessa janela para ativá-la e digite o seu nome usando o teclado. Aperte a tecla "ENTER" para que o shell envie sua resposta ao programa sendo executado. Finalmente, após recebida sua mensagem, o programa continua sua execução enviando ao Python shell as mensagens finais a serem impressas no shell.


Resumo 
------

Esse capítulo introduziu alguns conceitos básicos de computação sob um brevíssimo contexto histórico.
Nesse contexto, o Python é uma linguagem de programação de alto nível altamente portável (roda praticamente em qualquer computador, até mesmo em telefones celulares) e flexível (pode ser usada para diversos tipos de aplicação). O Anaconda fornece um pacote com várias ferramentas integradas, facilitando a instalação e manutenção do ambiente de desenvolvimento. 

Nos próximos capítulos vamos utilizar o Python shell para ilustrar alguns comandos do Python e o Spyder para escrever programas em Python. 


Exercícios
----------

Indique todas as alternativas corretas

.. mchoice:: questao_python_shell
    :answer_a: uma interface que permite escrever comandos do Python e ver o resultado.
    :answer_b: um ambiente integrado de desenvolvimento de programas em Python.
    :answer_c: uma ferramenta que permite a execução de comandos em Python e pode ser integrada com outras ferramentas.
    :answer_d: uma casca para proteger o Python conta ataques.
    :answer_e: uma concha onde o Python fica armazenado.
    :correct: a, c
    :feedback_a: Correto
    :feedback_b: Incorreto: o Python shell não é um IDE.
    :feedback_c: Correto: como ocorre no Spyder
    :feedback_d: Incorreto: o shell se refere a uma interface por linha de comando.
    :feedback_e: Incorreto: o shell se refere a uma interface por linha de comando.

    O Python shell é:


.. mchoice:: questao_dois_kilobytes
    :answer_a: aproximadamente dois mil bits
    :answer_b: aproximadamente quatro mil bits
    :answer_c: aproximadamente oito mil bits
    :answer_d: aproximadamente dezesseis mil bits
    :answer_e: nenhuma das alternativas anteriores 
    :correct: d
    :feedback_a: Incorreto: 1 byte possui 8 bits
    :feedback_b: Incorreto: 1 byte possui 8 bits
    :feedback_c: Incorreto: 1 byte possui 8 bits
    :feedback_d: Correto: 1 byte possui 8 bits
    :feedback_e: Incorreto: 1 byte possui 8 bits

    Quantos bits de informação podemos armazenar em 2 KB (dois kilobytes)?

.. mchoice:: questao_nivel_linguagem_python
    :answer_a: uma linguagem de programação de baixo nível
    :answer_b: uma linguagem de programação de alto nível
    :answer_c: uma linguagem de máquina
    :answer_d: uma linguagem de programação interpretada
    :correct: b, d
    :feedback_a: Incorreto: uma linguagem de baixo nível usa código que roda diretamente (ou quase) na máquina.
    :feedback_b: Correto: um programa em Python parece um texto em inglês.
    :feedback_c: Incorreto: os computadores não conseguem executar diretamente programas em Python. 
    :feedback_d: Correto: um programa em Python não é compilado para linguagem da máquina hospedeira.

    O Python é:

.. mchoice:: questao_tabulacao
    :answer_a: todos os blocos foram digitados corretamente.
    :answer_b: apenas os blocos A e B
    :answer_c: apenas o bloco C
    :answer_d: apenas o bloco D
    :correct: c
    :feedback_a: Incorreto: lembre-se que a tabulação é importante em Python.
    :feedback_b: Incorreto: lembre-se que a tabulação é importante em Python.
    :feedback_c: Correto. As linhas devem começar na primeira coluna.
    :feedback_d: Incorreto: lembre-se que a tabulação é importante em Python.

    Cada bloco corresponde a um programa em Python com apenas 2 linhas. Qual desses programas foi digitado corretamente no editor do Spyder?

    .. code-block:: python

        # Bloco A
        nome = input('Digite seu nome: ')
            print('Olá', nome)

        # Bloco B
            nome = input('Digite seu nome: ')
        print('Olá', nome)

        # Bloco C
        nome = input('Digite seu nome: ')
        print('Olá', nome)

        # Bloco D
            nome = input('Digite seu nome: ')
            print('Olá', nome)



Para saber mais
---------------

* `Como Pensar Como um Cientista da Computação - Aprendendo com Python: Versão Interativa <https://panda.ime.usp.br/pensepy/static/pensepy/index.html>`_, tradução do livro interativo produzido no projeto `Runestone Interactive <http://runestoneinteractive.org/>`_ por Brad Miller and David Ranum. 

    * [versão interativa em inglês]  `How to Think Like a Computer Scientist: Interactive Edition <https://runestone.academy/runestone/books/published/thinkcspy/index.html>`_, livro interativo produzido no projeto `Runestone Interactive <http://runestoneinteractive.org/>`_ por Brad Miller and David Ranum. 


    * [versão original em inglês] `How to Think Like a Computer Scientist: Learning with Python <https://open.umn.edu/opentextbooks/textbooks/how-to-think-like-a-computer-scientist-learning-with-python>`_, por Allen Downey, Franklin W. Olin, Jeff Elkner, and Chris Meyers.


* Vídeos

    - `Introdução à Computação com Python <https://www.coursera.org/learn/ciencia-computacao-python-conceitos>`_ do professor Fábio Kon do Departamento de Computação do IME-USP no Coursera.
    
    - `Python para Zumbis <https://www.pycursos.com/python-para-zumbis/>`_ do professor Fernando Masanori da FATEC de São José dos Campos. 

* sobre Computação e computadores:

    - `Breve história da computação <http://www.ime.usp.br/~macmulti/historico/>`_

    - `O que é um Computador <http://pt.wikipedia.org/wiki/Computador>`_

..  Glossário
    ---------
    - Calculadora: máquina que apenas realiza cálculos, sem ou com uma capacidade limitada de programação.
    - Computador: 
    - Hardware:
    - Software:
    - Número binário:
    - bit:
    - byte:
    - ULA: unidade lógica e aritmética.
    - UCP: unidade central de processamento.
    - memória:
    - programa:
    - computação:
    - linguagem de máquina
    - linguagem de alto nível
    - linguagem de baixo nível
    - linguagem de montagem


