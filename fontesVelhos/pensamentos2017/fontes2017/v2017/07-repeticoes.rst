. -*- coding: utf-8 -*-

..  shortname:: Repetições encaixadas
..  description:: Repetições encaixadas

Repetições encaixadas
=====================


Tópicos e objetivos
-------------------

    - identificar situações onde é necessário utilizar repetições encaixadas;
    - utilizar repetições encaixadas em seus programas;
    - utilizar indicadores de passagem em repetições encaixadas;
    - simular programas com repetições encaixadas.


.. break

Introdução
----------

Uma característica importante dos computadores é executar ações repetitivas inúmeras vezes sem se cansar, sempre com a mesma precisão e desempenho.
Ações complexas precisam ser decompostas para que possam ser mais facilmente descritas na forma de um algoritmo. A habilidade de resolver problemas computacionais depende muito da habilidade de decompor problemas em problemas mais simples e, uma vez que as soluções dos problemas mais simples forem desenvolvidas, precisamos compor essas soluções para resolver os problemas mais complexos. 

Tanto os comandos de execução condicional quanto os de repetição podem ser "encaixados", ou seja, um comando de repetição pode ser colocado dentro de outra repetição, como por exemplo:

.. code-block:: Python

    while condição_1:
        # bloco do while 1
        
        while condição_2:
            # bloco do while 2
             
            while condição_3:
                # bloco do while 3                
                
            # instruções após fim do while condição_3
        # instruções após fim do while condição_2
    # instruções após fim do while condição_1


.. break


Problema de motivação
---------------------

Considere a função :math:`y + x y - x^2`.

Uma forma computacional para estimar o máximo dessa função dentro de um intervalo é testar vários pares (x,y), "procurando" pela solução. 
A ideia é escrever um programa que gera todos os pares (x,y) para encontrar o par que corresponde ao máximo da função.

Exemplo
........

Vamos considerar apenas os números inteiros no intervalo 0 <= x <= 2
e 0 <= y <= 3.
Para gerar todos os pares (x,y) nesse intervalo devemos varrer todos os valores de x e, para cada valor de x, varrer todos os valores de y, algo como:
       # varrer todos os valores de x
    x = 0
    while x <= 2:
        print('para x igual a', x)

        # varrer os valores de y, para cada valor de x
        y = 0
        while y <= 3:
            print('    (', x, ',', y, ')'  )
            y = y + 1

        x = x + 1

Execute esse código no *activecode*. A saída é mostrada abaixo:

.. code-block:: Python

    para x igual a 0
        ( 0 , 0 )
        ( 0 , 1 )
        ( 0 , 2 )
        ( 0 , 3 )
    para x igual a 1
        ( 1 , 0 )
        ( 1 , 1 )
        ( 1 , 2 )
        ( 1 , 3 )
    para x igual a 2
        ( 2 , 0 )
        ( 2 , 1 )
        ( 2 , 2 )
        ( 2 , 3 )

Estude o código acima e verifique que o bloco de comandos que controla a 
varredura dos valores de x é muito parecido com o de y, como no trecho a seguir.

.. code-block:: Python

    # comandos que controlam a varredura dos valores de x
    x_inicial = 0
    x_final   = 2

    x = x_inicial
    while x <= x_final:

        x = x + 1

Calculando o ponto de máximo
~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Agora que sabemos varrer todos os pares (x,y), para determinar o ponto de máximo podemos usar a seguinte ideia:

    * No início, não sabemos qual é o ponto de máximo. 
        - Assim, vamos assumir que o máximo é um ponto qualquer
        - como o ponto (0,0)
    * O valor máximo é iniciado com o valor da função nesse ponto inicial
    * Varremos agora todos os pontos, um de cada vez 
        - calculamos o valor da função nesse ponto (x,y)
        - se o valor no ponto for maior que o valor visto até agora
            - atualizamos o valor com esse novo valor de máximo 

Em pseudo código, mais perto de Python, podemos escrever:

.. code-block:: Python

    # Vamos usar o ponto (0,0) como ponto máximo inicial
    x_max = 0
    y_max = 0
    v_max = valor da função no ponto inicial (x_max, y_max)

    para cada novo par (x,y) no intervalo:
        valor = valor da função em (x,y)
        se valor > máximo:
            v_max = valor 
            x_max = x
            y_max = y

    imprima o valor v_max e as coordenadas x_max e y_max

Lembre-se agora que, para varrer os pontos, usamos a repetição encaixada como vimos anteriormente e obtemos o seguinte código em Python:

.. activecode:: exemplo_estima_ponto_de_maximo_final

    # Inicialização: intervalo da varredura
    x_ini = 0
    x_fim = 2

    y_ini = 0
    y_fim = 3

    # Inicialização: cálculo do ponto de máximo
    x_max = 0
    y_max = 0
    v_max = y_ini + x_ini * y_ini - x_ini * x_ini

    # Varredura dos pontos no intervalo 
    x = x_ini
    while x <= x_fim:
        # varrer os valores de y, para cada valor de x
        y = y_ini
        while y <= y_fim:
            valor = y + x*y - x*x
            if valor > v_max:
                v_max = valor
                x_max = x
                y_max = y

            y = y + 1
        x = x + 1

    print('Valor máximo: ', v_max)
    print('Obtido no ponto: (', x_max,  ',', y_max, ')')

      
.. break

Exercícios
----------

Exercício 1
...........

    Exercício 6 da `lista sobre repetições encaixadas <http://www.ime.usp.br/~macmulti/exercicios/repetenc/index.html>`__.

Dado um número inteiro ``n``, ``n > 1``, imprimir sua decomposição 
em fatores primos, indicando também a mutiplicidade de cada fator.
   

    fator 2 multiplicidade 3
    fator 3 multiplicidade 1
    fator 5 multiplicidade 2


.. activecode:: aula_repeticoes_ex01_tentativa

    # escreva o seu programa abaixo e remova o print()
    print("Vixe! Ainda nao fiz este exercício!")
                

Clique `aqui <exercicios/repeticao01.html>`__ para ver uma solução.


Exercício 2
...........

    Exercício 7 da `lista sobre repetições encaixadas <http://www.ime.usp.br/~macmulti/exercicios/repetenc/index.html>`__. 

Dados um número inteiro ``n > 0`` e uma sequência com
``n`` números inteiros maiores do que zero,
determinar o máximo divisor comum entre eles.

Por exemplo, para a sequência 

.. sourcecode:: python

    3    42    105      

o seu programa deve escrever o número ``3``.


.. activecode:: aula_repeticoes_ex02_tentativa

    # escreva o seu programa abaixo e remova o print()
    print("Vixe! Ainda nao fiz este exercício!")
                
        
Clique `aqui <exercicios/repeticao02.html>`__ para ver uma solução.


Exercício 3
...........

Dados um número inteiro ``n``, ``n > 0``, e uma sequência com
``n`` números inteiros maiores do que zero,
determinar o fatorial de cada número da sequência.
   

.. activecode:: aula_repeticoes_ex03_tentativa

    # escreva o seu programa abaixo e remova o print()
    print("Vixe! Ainda nao fiz este exercício!")
                
    
Clique `aqui <exercicios/repeticao03.html>`__ para ver uma solução.


Exercício 4
...........

Dada uma sequência de números inteiros maiores que um, terminada por
um zero, determinar quantos números primos há na sequência.

.. activecode:: aula_repeticao__ex04_tentativa

    # escreva o seu programa abaixo e remova o print()
    print("Vixe! Ainda nao fiz este exercício!")
                
                
Clique `aqui <exercicios/repeticao04.html>`__ para ver uma solução.


Exercício 5
...........

    Exercício 5 da `lista sobre repetições encaixadas <http://www.ime.usp.br/~macmulti/exercicios/repetenc/index.html">`__. 
    
Sabe-se que cada número da forma ``n**3`` é igual a soma de ``n``
ímpares consecutivos.
Por exemplo,

.. sourcecode:: Python

    1**3 = 2
    2**3 = 3 + 5
    3**3 = 7 + 9 + 11
    4**3 = 13 + 15 + 17 + 19

Dado um número inteiro ``m``, ``m > 0``, determinar os ímpares
consecutivos cuja soma é igual a ``n**3``, para n
assumindo valores de 1 a ``m``.
 
.. activecode:: aula_repeticoes_ex05_tentativa
		
    # escreva o seu programa abaixo e remova o print()
    print("Vixe! Ainda nao fiz este exercício!")
                
    
.. break

Para saber mais
---------------

* Vídeos

    - `Depuração
      <https://www.youtube.com/watch?v=Aa8-39qwbUU&list=PLcoJJSvnDgcKpOi_UeneTNTIVOigRQwcn&index=14>`__;
    - `Repetições encaixadas <https://www.youtube.com/watch?v=dRCDOH1lugs&list=PLcoJJSvnDgcKpOi_UeneTNTIVOigRQwcn&index=23>`__;
    - `Repetições encaixadas - exercício fatoriais
      <https://www.youtube.com/watch?v=dRCDOH1lugs&index=21&list=PLcoJJSvnDgcKpOi_UeneTNTIVOigRQwcn>`__;
    - `Repetições encaixadas - exercício fatoração <https://www.youtube.com/watch?v=9AUu2xjOACI&index=25&list=PLcoJJSvnDgcKpOi_UeneTNTIVOigRQwcn>`__.  
