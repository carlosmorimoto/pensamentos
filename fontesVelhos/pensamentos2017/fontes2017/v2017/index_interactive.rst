.. Problem Solving with Algorithms and Data Structures documentation master file, created by
   sphinx-quickstart on Thu Oct 27 08:17:45 2011.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

.. meta::
   :description: Uma versão interativa do livro "Pensamento Computacional: uma introdução usando Python".
   :keywords: pensamento computacional, ciência da computação python

.. toc_version: 2

.. _t_o_c:

.. raw:: html

   <div style="text-align:center" class="center-block">
   <h1>Pensamento Computacional: uma introdução com Python</h1>
   <h3>-- versão interativa --</h3>

   <p>por <a href="https://www.ime.usp.br/~coelho">José Coelho de Pina Jr.</a> e
   <a href="https://www.ime.usp.br/~hitoshi">Carlos Hitoshi Morimoto</a>
   </p>
   <style>
   button.reveal_button {
       margin-left: auto;
       margin-right: auto;
   }
   </style>
   </div>

Prefácio
::::::::

.. toctree::
   :maxdepth: 1

   00-Prefacio/toctree.rst

Índice
::::::

.. toctree::
   :numbered:
   :maxdepth: 3

   01-Introducao/toctree.rst
   02-Expressoes-aritmeticas-relacionais-logicas/toctree.rst
   03-Variaveis-tipos-funcoes-entrada/toctree.rst
   04-Comando-repeticao/toctree.rst
   05-Execucao-condicional-alternativas/toctree.rst
   06-Indicadores-passagem/toctree.rst
   07-Repeticoes-encaixadas/toctree.rst
   08-Numeros-reais/toctree.rst
   09-Funcoes/toctree.rst
   10-Lista/toctree.rst
   11-Funcoes-com-listas/toctree.rst
   12-Strings-Arquivos//toctree.rst
   13-Matrizes/toctree.rst
   14-Algoritmos-Busca/toctree.rst
   15-Algoritmos-Elementares-Ordenacao/toctree.rst


Indices and tables
::::::::::::::::::

* :ref:`genindex`
* :ref:`search`


.. raw:: html

   <div style="width: 500px; margin-left: auto; margin-right: auto;">
   <a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/">
   <img alt="Creative Commons License" style="border-width:0; display:block; margin-left: auto; margin-right:auto;" src="https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png" />
   </a><br />
   <span xmlns:dct="http://purl.org/dc/terms/" href="http://purl.org/dc/dcmitype/InteractiveResource" property="dct:title" rel="dct:type">Pensamento Computacional: uma introdução usando Python</span><br /> 
   por <span xmlns:cc="http://creativecommons.org/ns#" property="cc:attributionName">José Coelho de Pina Jr. e Carlos Hitoshi Morimoto</span> <br />
   está sob a licença <a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/">Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License</a>.</div>
