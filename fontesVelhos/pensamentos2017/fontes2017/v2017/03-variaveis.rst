
Variáveis, tipos e funções de entrada e saída 
=============================================

.. index:: valor, tipo de dados, string, str, str()

.. index:: inteiro, int, int(), real, float, float()

.. index:: input(), print()

Tópicos e Objetivos
-------------------

Ao final desse capítulo você deverá ser capaz de:

    - identificar variáveis em programas em Python;
    - usar o operador de atribuição `=` para criar e manipular variáveis;
    - usar a função input();
    - usar a função print();
    - usar valores das classes int, float, str, e bool;
    - usar as função de conversão int(), float(), bool() e str();
    - simular um programa simples em Python.

.. break

Introdução
----------

No capítulo anterior vimos como o Python calcula o valor de uma expressão. 
Basicamente, uma expressão é reduzida um operador por vez, começando dos operadores com maior 
precedência até que a expressão seja totalmente reduzida a um único valor numérico ou booleano.
Vimos também que o Python shell pode ser utilizado como "calculadora" para resolver expressões. 

Nessa aula vamos ver como armazenar os valores das expressões e manipular esses valores utilizando **variáveis**. 
Para isso vamos continuar utilizando o Python shell mas dessa vez dentro do Spyder. Além de começar a introduzir 
esse ambiente, vamos usar o Spyder para escrever e executar nossos primeiros programas.

.. break

Não tenha medo do Spyder
------------------------

Como vimos, o Spyder `<https://www.spyder-ide.org/>`__ é um ambiente integrado de desenvolvimento de programas em Python que vem junto com o Anaconda. 
Apesar da interface do Spyder parecer um tanto complexa 
por reunir muitas ferramentas, não se assuste. Uma vez que você entenda o 
funcionamento e o propósito de cada uma, elas serão muito úteis no desenvolvimento dos seus programas.

Se você puder, acompanhe essa aula junto de um computador e execute os exemplos no Spyder.
Mesmo que você não puder, recomendamos a leitura dessa seção para que você conheça melhor
alguns recursos do Spyder.

Caso você não tenha ainda instalado Python em seu computador siga as instruções na 
página `<https://panda.ime.usp.br/cc110/static/cc110/01-intro.html>`__. Quando terminar de instalar, inicie o Spyder. 

A figura abaixo mostra a tela inicial do Spyder. 
Ela deve aparecer com essas 3 partes (que vamos chamar de janelas). 
Caso você não esteja vendo alguma dessas partes, clique na opção
"Tools" da barra de menu (topo da janela) e depois em 
"Reset Spyder to factory defaults" para que o Spyder volte a ter essa
configuração.

.. image:: Figuras/spyder/spyder01.png
    :alt: Figura 3.1: Tela inicial do Spyder.
    :align: center

A janela na parte superior direita dessa configuração inicial tem 3 "tabs":
"Variable explorer", "File explorer" e "Help". Selecione (clique) o tab "Variable explorer" para que o Spyder exiba os valores das variáveis que você utilizar (como na figura). O tab "Help" possui informações sobre o Spyder e o tab "File explorer" ajuda você a achar arquivos na sua máquina.

A janela na parte inferior direita tem 2 tabs. Selecione o tab "IPython console". O IPython console (que vamos chamar simplesmente de IPython) é também um Python shell, muito semelhante ao "Anaconda Prompt", com uma interface melhorada. A principal mudança é o prompt, que indica o número de comandos executados ao invés de ">>>". 

.. break

Variáveis e o comando de atribuição ``=``
-----------------------------------------

.. index:: nome de variável, comando de atribuição

Uma **variável** é simplesmente um **nome** que faz referência a um objeto na memória que corresponde, por exemplo, ao resultado de alguma expressão aritmética. Para criar uma variável, precisamos associá-la a um objeto ou valor usando o **comando de atribuição** que corresponde ao símbolo ``=``, como no trecho abaixo:

.. code-block:: Python

    variável = expressão

Para entender como  utilizar esse comando, observe a sequência de comandos digitados na janela do IPython na figura 3.2 a seguir, que mostra as janelas "Variable explorer" e "IPython console" do Spyder (as duas janelas do lado direito) após a execução dos comandos. Procure executar no Spyder esses comandos para entender melhor o que está acontecendo.

.. image::  Figuras/spyder/console01.png
    :alt: Figura 3.2: Janelas "Variable explorer" e "IPython console" do Spyder.
    :align: center

Quando o Python é iniciado nenhuma variável está definida. Assim ao digitarmos a palavra (nome da variável) ``pi``, o IPython devolve o erro ``NameError`` indicando que o nome ``pi`` ainda não foi definido. 

O comando de atribuição digitado na linha [2] faz com que a variável com nome ``pi`` seja criada e associada ao valor da expressão do lado direito do comando, nesse caso, 3.14. Se você estiver usando o Spyder, note que na janela "Variable explorer" a variável ``pi`` é criada com esse valor. 

Observe que o comando de atribuição na linha [2] não gera uma saída, ou linha com ``Out[2]``. Lembre-se que o shell interpreta e executa comandos. No caso de uma atribuição, uma variável é criada ou atualizada (modificada). Sem o comando de atribuição ``=``, o Python tenta entender o nome como alguma variável conhecida e resolver uma expressão usando o seu valor associado. 
Observe que, ao digitarmos novamente o nome ``pi`` como na linha [3], o IPython reconhece o nome e resolve a expressão substituindo o nome pelo valor associado (armazenado na memória como mostrado na janela Variable explorer). 

A linha [4] é semelhante à linha [2] e cria uma nova variável de nome ``novo_pi``. Como a expressão do lado direito do símbolo ``=`` contém apenas a variável ``pi``, o **valor** referenciado por ``pi`` é atribuído à ``novo_pi`` (veja o que acontece na janela "Variable explorer" após a execução de cada comando de atribuição). 

A linha [5] mostra como uma variável pode ser atualizada. Primeiro a expressão à direita é calculado usando o valor atual de ``pi`` e o novo valor (6.28) é atribuído a própria variável. Observe que a mesma variável ``pi`` tem um valor associado **antes** da atribuição, quando a expressão é calculada, e um valor diferente **após** a execução do comando.

A linha [6] mostra o novo valor de ``pi`` (ou calcula o valor da expressão, que nesse caso é o valor associado a ``pi``) e a linha [7] mostra  que a variável ``novo_pi`` **não é alterada**, mesmo tendo recebido o valor associado a ``pi`` na linha [4].
Ou seja, cada variável faz referência ao último valor que lhe é atribuído e, portanto, os valores antigos são perdidos a cada nova atribuição. 


.. admonition:: **Nomes de variáveis**

   Nesse curso o nome de uma variável será sempre iniciada por uma letra
   minúscula e poderá ser seguida por outras letras minúsculas e números, como
   em: ``contador``, ``ind``, ``i``, ``j``, ``a1``, ``r2d2``, ``bb8`` etc.
   
   Escolha sempre um nome **significativo ou comum**, para facilitar o
   entendimento sobre o que variável representa ou realiza. Por exemplo, ``i``
   e ``j`` são nomes comuns para contar o número de iterações em um laço, mas
   podemos utilizar o nome ``contador`` e as vezes algumas abreviações como
   ``cont`` e ``aux``. Para melhorar a clareza de seus programas, sugerimos
   também o uso de nomes compostos separados pelo caractere `_`, como em
   ``conta_pares`` e ``conta_impares``. Evitaremos também o uso de caracteres
   acentuados para facilitar a compatibilidade com a ferramenta online
   utilizada no curso.
   
   Vamos chamar de **constantes** variáveis que **não devem** mudar de valor 
   após serem inicializadas. Para indicar uma constante utilizaremos nomes
   formados por todas as letras em maiúscula, como por exemplo
   ``MAXIMO_TAMANHO = 100``.

   **Palavras reservadas** como o nome de comandos e funções nativas do Python
    (``if``, ``while``, ``print()`` etc) também não podem ser utilizadas como nome de
    variáveis.


.. admonition:: **Não confunda atribuição com igualdade**

    Nós somos muito treinados a ler o símbolo "=" como "igual". Lembre-se que o operador relacional de igualdade é representado como "==" e a expressão "pi == 3.14" devolve ``True`` ou ``False`` mas **não** cria ou altera o valor da variável ``pi``. 
    
    Um reflexo desse "treinamento" quando começamos a programar é ignorar a ordem dos elementos do comando. 
    É importante escrever o nome do lado esquerdo e a expressão do lado direito do comando "=".

.. break

Tipos
-----

.. index:: int, float, bool

Vimos que as expressões aritméticas reduzem a expressão a um valor numérico. Esse valor numérico pode ser do tipo inteiro ou real. Internamente, cada tipo de dado (que vamos chamar genericamente de **classe**) é representado de forma diferente e possue um conjunto distinto de operações. Por serem numéricos, as operações com inteiros e reais são semelhantes e podemos até misturar inteiros com reais nas expressões aritméticas. Nesse caso, o resultado é dado usando o tipo mais genérico (no caso, como um número real). 

A linha [8] na janela do IPython da figura 3.2 mostra a função `type()` do Python que devolve o tipo (ou classe) de um objeto, no caso o tipo do  valor associado a ``pi`` é um ``float``. 

Em Python, números inteiros são da classe `int` e números reais são da classe `float` (pois a representação de números reais é realizada em notação com 
`ponto flutuante <https://pt.wikipedia.org/wiki/V%C3%ADrgula_flutuante>`__ 
ou *floating point*). 
Já os valores booleanos são da classe `bool`.  

Como dissemos anteriormente, o comportamento do IPython Console é um pouco diferente do Python shell, que vai além do ">>>" como prompt. A sessão de Python shell a seguir ilustram como ele se comporta ao usarmos a função `type()` (experimente!). 

.. code-block:: python

    >>> type(2)
    <class 'int'>
    >>> type(2.0)
    <class 'float'>
    >>> type(True)
    <class 'bool'>
    >>> dois = 2.0
    >>> type(dois)
    <class 'float'>

Por simplicidade e para evitar alguma confusão que as linhas numeradas do IPython possam gerar, vamos utilizar frequentemente o Python shell para ilustrar alguns exemplos desse livro. No entanto, continuamos a recomendar que você utilize o Spyder (e o IPython no Spyder) para desenvolver os seus programas.


Tipo string (str)
.................

Um outro tipo que vamos utilizar bastante é chamado de "string" e representado por ``str``. Uma string é uma sequência de 0 ou mais caracteres que representam um texto. Para indicar uma string no Python shell, devemos escrevê-lo entre apóstrofes (') ou aspas ("). Essa dualidade facilita a inclusão do caractere apóstrofe ou aspas dentro de uma string, como ilustra a sequência de comandos abaixo:

.. code-block:: python

    >>> carmem = "Carme Miranda"
    >>> carmem
    'Carme Miranda'
    >>> lata = "d'água"
    >>> lata
    "d'água"
    >>> seq = ' 1, "dois", 3'
    >>> seq
    ' 1, "dois", 3'
    >>> type(seq)
    <class 'str'>

É possível **concatenar** duas strings usando o operador ``+``. Por exemplo:

.. code-block:: Python

    >>> p = "Pedro"
    >>> a = "Álvares"
    >>> c = "Cabral"
    >>> p + a
    'PedroÁlvares'
    >>> nome = p + ' ' + a + c
    >>> nome
    'Pedro ÁlvaresCabral'

Observe que os caracteres em branco são parte da string.
Assim, a concatenação de ``p + a`` resulta em 'PedroÁlvares' pois 
nem ``p`` nem ``a`` foram definidas com espaço. 
Observe que na expressão (com strings!) que cria a variável ``nome`` apenas um espaço
foi definido entre os dois primeiros nomes, mas não há espaço entre os
dois últimos nomes.

.. admonition:: Concatenação ou Soma?

    O operador ``+`` é utilizado tanto para concatenar strings quando os dois operandos são do tipo ``str`` como para somar quando os dois operadores são números. No entanto, quando um operador é string e o outro for um número, o Python indica um erro como mostrado abaixo. 

    .. code-block:: Python

        >>> 'Pedro' + 3
        Traceback (most recent call last):
        File "<stdin>", line 1, in <module>
        TypeError: can only concatenate str (not "int") to str
        >>> 'Pedro ' * 3
        'Pedro Pedro Pedro'

    No entanto, é possível realizar múltiplas concateções usando o operador ``*``. 



Conversão entre tipos
.....................

O capítulo anterior mostrou como o Python calcula valores de expressões aritméticas, lógicas e relacionais, que resultam em valores dos tipos: ``int``, ``float`` ou ``bool``, e vimos agora o tipo ``str``. 

Durante o cálculo de uma expressão, o Python precisa decidir também o tipo do valor calculado.
Por exemplo, qual o tipo do valor resultante de "4 * 2"? 
De forma geral, podemos dizer que o Python procura manter o tipo dos operandos. Assim, o resultado teria valor 8 do tipo ``int``.  

No entanto a expressão "4 / 2" tem o valor 2.0 do tipo ``float``, como mostra a seguinte sessão do Python shell:

.. code-block:: Python

    >>> 4 * 2
    8
    >>> 4 / 2
    2.0
    >>> 4 // 2
    2
    >>> 2 + 3 * 4
    14
    >>> 2 + 3 * 4 / 1
    14.0

O resultado de "4/2" é ``float`` devido ao uso do operador de divisão `/`. Observe que usando o operador `//` o resultado é ``int``. Uma regra geral é que, quando os operandos são de tipos distintos como ``int`` e ``float``, o operando de tipo "menor" é promovido ao tipo "maior" (ou mais abrangente, no caso o ``float``) e a operação é realizado no tipo maior.

A conversão entre tipos nativos do Python pode ser realizada usando as funções de mesmo nome como mostra os exemplos abaixo.

.. code-block:: Python

    >>> 4 / 2
    2.0
    >>> int(4/2)
    2
    >>> float(2 * 4)
    8.0

.. break

Funções de entrada e saída de texto
-----------------------------------

Funções de entrada e saída permitem que programas troquem mensagens entre si ou que programas troquem mensagens com os usuários. Nesse curso, como os programas serão executados no Python shell (ou IPython), vamos utilizar a função ``input()`` para que o seu programa receba um texto digitado no shell pelo usuário e vamos utilizar a função ``print()`` para que um programa possa imprimir um texto no shell. 


Função input()
..............

Para usar a função ``input()`` devemos fazer o seguinte.

.. code-block:: Python

    var = input( mensagem )

A função ``input()`` recebe uma mensagem (uma string) que é exibida no shell. A função dessa mensagem é explicar ao usuário o que digitar. Ao terminar de digitar, o usuário deve teclar ENTER para enviar a string digitada ao programa. O programa atribui essa string à variável ``var``. 


Função print()
..............

Para usar a função ``print()`` devemos fazer o seguinte.

.. code-block:: Python

    print( lista_de_valores_separada_por_vírgulas )
    
A função ``print()`` recebe uma lista de valores separada por vírgulas e imprime cada valor (transforma o valor para uma string e o imprime a string), adicionando um espaço em branco entre duas strings e pulando de linha ao final.

A figura 3.3 ilustra como utilizar as funções ``input()`` e ``print()`` 
no IPython. 

.. image::  Figuras/spyder/console02.png
    :alt: Figura 3.3: Uso das funções input() e print()
    :align: center

Observe que a função ``print()`` procura facilitar a impressão e torná-la mais legível, por exemplo, 
inserindo um espaço automaticamente entre dois valores da lista e pulando para a próxima linha ao final da impressão,
para que cada ``print()`` apareça em uma linha. 

Esse comportamento pode ser modificado definindo alguns parâmetros extras, como ``sep`` e ``end``. 

    * O parâmetro ``sep`` pode ser usado para alterar a string inserida automaticamente entre dois valores consecutivos da lista. O valor padrão é um único espaço em brando. Exemplos:

        .. code-block:: Python

            >>> print(2, 3, 4, sep=' ')   # com um espaço é o comportamento padrão:
            2 3 4
            >>> print(2, 3, 4, sep='')    # com uma string vazia, sem espaço, resulta em:
            234
            >>> print(2, 3, 4, sep='-*-') # com a string '-*-' resulta em:
            2-*-3-*-4

    * O parâmetro ``end`` pode ser usado para alterar a string inserida automaticamente ao final da lista de valores. O valor default é o caractere especial que pula uma linha (representado por ''\n''). Observe os seguintes exemplos (clique em ``Run`` caso você estiver lendo esse texto online usando um navegador).

        .. activecode:: exemplo_print_end_default

            print('primeira', 'linha', end='\n')   # comportamento padrão
            print('segunda', 'linha', end='\n')

        A saída desse trecho de código é 

            primeira linha
            segunda linha

        .. activecode:: exemplo_print_end_pula_duas

            print('primeira', 'linha', end='\n\n')   # pula duas linhas
            print('segunda', 'linha')

        A saída desse trecho de código é 

            primeira linha

            segunda linha
        
        .. activecode:: exemplo_print_end_nao_pula

            print('primeira', 'linha', end=' <<>> ')   # string sem pula linha
            print('segunda', 'linha')

        A saída desse trecho de código é 

            primeira linha <<>> segunda linha


    * Os parâmetros ``sep`` e ``end`` podem ser combinados.

        .. activecode:: exemplo_print_end_nao_pula_com_sep

            print('primeira', 'linha', sep='**', end=' <<>> ')  
            print('segunda', 'linha', sep='%%', end='<<FIM>>')

        A saída desse trecho de código é 

            primeira**linha <<>> segunda%%linha<<FIM>>

.. break 

Laboratório: Usando o editor do Spyder para escrever programas em Python
------------------------------------------------------------------------

Vamos agora escrever um programa simples utilizando o Spyder. O programa deve ler dois inteiros ``a`` e ``b`` e imprimir sua soma.

Uma primeira tentativa pode ser a seguinte:

.. code-block:: Python

    a = input("Digite o valor de a: ")
    b = input("Digite o valor de b: ")
        soma = a + b
        print("A soma de a + b é: ", soma)

Copie esse trecho de código no editor do Spyder e **salve** o programa.

Para salvar o programa em um arquivo no computador que você estiver usando, clique na opção "File -> Save as" na barra de menu do Spyder. O nome precisa terminar com a extensão ".py" para indicar que é um arquivo em Python, como "primeiro_prog.py". 

Depois de salvo, execute o programa utilizando a opção "Run -> Run" do menu (ou use a tecla de atalho 'F5'). O Spyder oferece vários "atalhos" (combinações de teclas) que você pode aprender para agilizar o seu trabalho. Essas combinações de teclas existentes são mostradas no canto direito de cada opção do menu. 

O programa é executado no Python shell ou, nesse caso, no IPython Console que vamos chamar simplesmente de Python.
Se você digitou esse programa exatamente como ilustrado, o Python deve indicar um erro de tabulação (*IndentationError*) no console ou shell. Isso porque a coluna onde cada linha inicia indica ao Python a que bloco a linha pertence. A primeira linha de código **precisa** começar na primeira coluna do editor (ou seja, sem nenhum espaço no início da linha). 

Para corrigir esse problema, remova todos os espaços iniciais de todas as linhas como:

.. code-block:: Python

    a = input("Digite o valor de a: ")
    b = input("Digite o valor de b: ")
    soma = a + b
    print("A soma de a + b é: ", soma)

salve e execute novamente. 

O Python não deve indicar nenhum erro dessa vez. Se para ``a`` digitarmos o 3 e para ``b`` digitarmos 4, a saída do programa nesse caso é 34. 

Isso porque é "esquecemos" que a função ``input()`` devolve uma string, que precisa ser convertida para um inteiro. Isso pode ser corrigido convertendo-se o cada string para inteiro da seguinte maneira:

.. code-block:: Python
    :linenos:

    a_str = input("Digite o valor de a: ")
    a_int = int(a_str)
    b = int(input("Digite o valor de b: "))
    soma = a_int + b
    print("A soma de a + b é: ", soma)

Nesse programa, ``a_str`` recebe uma string que é convertida para um valor inteiro pela função ``int()`` e esse valor inteiro é atribuído para a variável ``a_int``. 
A linha 3 mostra que essas funções podem ser compostas para produzir um código mais compacto mas ainda legível. 

Salve e execute esse programa e verifique se ele se comporta como esperado.

.. break

Exercícios online
-----------------

O objetivo do laboratório foi mostrar como usar o Spyder para escrever os seus programas. Esse deve ser o modo preferível para acompanhar o livro em papel. Caso você esteja acompanhando o livro online, vários dos exercícios programa no restante desse livro  utilizam as ferramentas desenvolvidas pelo  projeto `Runestone <http://runestoneinteractive.org>`__. Essas ferramentas permitem que você resolva os exercícios online, dentro de seu próprio navegador. 

Uma dessas ferramentas online é o *activecode*. Caso você esteja acompanhando esse livro online, use a janela do activecode a seguir para ler e imprimir a soma de dois números como no laboratório.

.. activecode:: cap3_ex1_1
    :caption: Como somar dois números?
    :nocodelens: 

    Tentativa 0: execute o programa abaixo e veja o que acontece.

    ~~~~

    a = 3
    b = 4
        soma = a + b
        print("A soma de a + b é: soma")

    ====

Clique no botão "Run" para executar esse programa.

Depure o programa
.................

Depurar (às vezes chamado de debugar, do inglês **debug**), é a atividade de  corrigir problemas do programa. 

O primeiro **bug** desse programa é sintático. Para corrigir esse problema basta
corrigir a tabulação e clique em RUN para executar o programa. Após corrigido o erro sintático, execute-o novamente e veja que a mensagem de saída é simplesmente todo o texto fornecido à função ``print()``.
Como tudo entre aspas (") define uma *string*, o programa está imprimindo o **nome** e não o valor da variável ``soma``. 


.. activecode:: cap3_ex1_2
    :caption: Como somar dois números?
    :nocodelens: 

    Tentativa 1: execute o programa abaixo e veja o que acontece.

    ~~~~

    a = 3
    b = 4
    soma = a + b
    print("A soma de a + b é:", soma)

Observe que o print recebeu dois **valores** separados por vírgula:
    - uma string (entre aspas) e
    - a variável ``soma`` (ao invés da string "soma")

A string é impressa diretamente sem as aspas (pois esse é o seu **valor**) e, ao invés de imprimir a string "soma", o print imprime o **valor** da variável.

Mas e como imprimir os valores de ``a`` e ``b`` ao invés de seus nomes?

.. activecode:: cap3_ex1_3
    :caption: Como somar dois números?
    :nocodelens: 

    Tentativa 2: execute o programa abaixo e veja o que acontece.

    ~~~~

    a = 3
    b = 4
    soma = a + b
    print("A soma de", a, "+", b, "é:", soma)

       
Podemos quebrar ainda mais a lista de valores passada ao print, com strings e variáveis, para que a mensagem fique mais clara.

Mas como fazer com que os valores a serem somados sejam definidos por um
usuário?

.. activecode:: cap3_ex1_4
    :caption: Como somar dois números?
    :nocodelens: 

    Tentativa 3: execute o programa abaixo e veja o que acontece.

    ~~~~
    # tudo que vem após o simbolo # até o final da linha é um comentário
    # comentários são mensagens aos programadores 
    # que não são executados pelo Python

    PROMPT_1 = "Digite o primeiro numero: "  # PROMPT_1 é uma constante
    PROMPT_2 = "Digite o segundo numero: "   # PROMPT_2 é outra constante
             
    a = input(PROMPT_1) # veja o texto de PROMPT_1
    b = input(PROMPT_2)
    
    soma = a + b
    print("A soma de", a, "+", b, "é:", soma)
                
Utilizamos a função ``input()`` para receber a resposta do usuário
à mensagem que colocamos como string passado à função ``input()``.
Como a resposta do usuário também é um string (e não um **número**)
a operação ``+`` é realizada com strings, ou seja, eles são 
concatenados.

Precisamos, portanto, de um maneira para converter uma string em um
número, para que o Python obtenha a soma desses números.


.. activecode:: cap3_ex1_5
    :caption: Como somar dois números?
    :nocodelens: 

    Tentativa 4: execute o programa abaixo e veja o que acontece.

    ~~~~

    a_str = input("Digite o primeiro numero: ")
    b_str = input("Digite o segundo numero: ")
    a_int = int(a_str) # converte string/texto para inteiro
    b_int = int(b_str) # converte string/texto para inteiro
    soma = a_int + b_int
    print("A soma de", a_int, "+", b_int, "eh igual a", soma)

                
A função ``int()`` converte um dado string para um número inteiro.
Como não precisamos guardar as respostas na forma de texto,
podemos simplificar o programa combinando as funções ``int()``
e ``input()`` da seguinte forma:
    
.. activecode:: cap3_ex1_6
    :caption: Como somar dois números?
    :nocodelens: 

    Tentativa 5: execute o programa abaixo e veja o que acontece.
    ~~~~
    a = int(input("Digite o primeiro numero: "))
    b = int(input("Digite o segundo numero: "))
    soma = a + b
    print("A soma de", a, "+", b, "eh igual a", soma)
    
.. break

Para saber mais
---------------

* Leituras

    - `Capítulo 2: Variáveis, expressões e comandos <https://panda.ime.usp.br/pensepy/static/pensepy/02-Conceitos/conceitos.html>`__ do livro Como Pensar Como um Cientista da Computação

* Vídeos

    - `Variáveis e scripts (programas) em Python <https://www.youtube.com/watch?v=tqF0MjcOqcI&list=PLcoJJSvnDgcKpOi_UeneTNTIVOigRQwcn&index=5>`__ (`<https://www.youtube.com/watch?v=tqF0MjcOqcI&list=PLcoJJSvnDgcKpOi_UeneTNTIVOigRQwcn&index=5>`_);
      
    - `Valores e tipos em Python <https://www.youtube.com/watch?v=UZ7_oudJ150&index=6&list=PLcoJJSvnDgcKpOi_UeneTNTIVOigRQwcn>`__ (`<https://www.youtube.com/watch?v=UZ7_oudJ150&index=6&list=PLcoJJSvnDgcKpOi_UeneTNTIVOigRQwcn>`_);
      
    - `Entrada de dados <https://www.youtube.com/watch?v=ECFj-bWzU8I&list=PLcoJJSvnDgcKpOi_UeneTNTIVOigRQwcn&index=8>`_ (`<https://www.youtube.com/watch?v=ECFj-bWzU8I&list=PLcoJJSvnDgcKpOi_UeneTNTIVOigRQwcn&index=8>`_);
              
    - `Escrita na tela (print) [Zumbis] <https://www.youtube.com/watch?v=dhtEDVw5EFM>`_ (`<https://www.youtube.com/watch?v=dhtEDVw5EFM>`_);

    - `Leitura via teclado (input) [Zumbis] <https://www.youtube.com/watch?v=mubJU5dHyP8>`_ (`<https://www.youtube.com/watch?v=mubJU5dHyP8>`_).

