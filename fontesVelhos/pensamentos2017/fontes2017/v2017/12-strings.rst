..  shortname:: Strings e Arquivos
..  description:: Strings e Arquivos


Strings e Arquivos
==================

.. index:: string, fatia de string, arquivo texto, encoding, representação de caracteres

Tópicos
-------

    - `String (tipo str) <https://panda.ime.usp.br/pensepy/static/pensepy/08-Strings/strings.html>`__
    - `Fatia de string <https://panda.ime.usp.br/pensepy/static/pensepy/08-Strings/strings.html#fatiamento>`__
    - `Arquivo de texto <https://panda.ime.usp.br/pensepy/static/pensepy/10-Arquivos/files.html>`__

.. break

Vídeos
------      

    - `Strings <https://www.youtube.com/watch?v=DdhNltkI_hE&list=PLcoJJSvnDgcKpOi_UeneTNTIVOigRQwcn&index=33>`__
    - `Comparação de strings <https://www.youtube.com/watch?v=EWQTdbtCtKw&index=34&list=PLcoJJSvnDgcKpOi_UeneTNTIVOigRQwcn>`__

.. break

Introdução
----------

Um string (=tipo ``str``) é uma sequência de caracteres. Em Python,
podemos representar um string colando-o entre aspas ou apóstrofes.
O uso desses 2 marcadores é útil quando queremos utilizar um deles
dentro do string, como:

.. sourcecode:: python

    >>> texto = "Esse string usa 'aspas' como demarcador"
    >>> print(texto)
    Esse string usa 'aspas' como demarcador
    >>> novo_texto = 'Esse string usa "apóstrofes" como demarcador'
    >>> print(novo_texto)
    Esse string usa "apóstrofes" como demarcador
    >>> 

Por ser uma sequência de caracteres, podemos fazer uma analogia de strings com listas em Python. Assim como listas, um caractere de um string pode ser acessado por meio de índices, o comprimento pode ser dado pela função ``len()``, e os caracteres podem ser fatiados e concatenados.

.. break

Strings: criação, indexação, e comparação
-----------------------------------------

O exemplo a seguir mostrar como criar um string
a partir de um string vazio e a 
concatenação de outros strings.

.. activecode:: strings_criacao

    def main():
        frase = ""   # string vazio
        frase = frase + "Esse string usa "
        meio  = '"apóstrofes" '
        fim   = "como demarcador. "
	frase = frase + meio + fim
        print("A frase: ", frase)
        print("Tem comprimento: ", len(frase))

    main()

Modifique o código para que o programa escreva `aspas` ao invés de `apóstrofes`.
Experimente trocar as " (aspas)  por ' (apóstrofes) no programa todo (de forma
coerente) para se certificar que nada muda.

Assim como os elementos de uma lista, os caracteres de um string
podem ser indexados por um índice (um número inteiro) entre colchetes.
Execute o programa abaixo para 
    
.. activecode:: strings_indexacao

   def main():
        frase = "Eu posso ajudar respondendo perguntas no fórum!"
        sorteio = [5, -4, 3, -8, 11]  # não foi bem um sorteio :)
	acertos = 0

	for i in sorteio: 
	    resposta = input("Qual o caractere de índice %d? "%(i))
		if frase[i] == resposta:
		    print("Parabéns, você acertou!")
		else:
		    print("Você errou. O caractere de índice %d é: %s"%(i, frase[i]))
	print("Você acertou %d de % perguntas."%(acertos, len(sorteio)))

    main()
    
Assim como listas
um string também pode ser fatiado. Lembre-se que para definir uma fatia na sequência de caracteres, é necessário definir o início e o fim da fatia e, se desejar, um passo. Assim como nas fatias de listas, o intervalo definido por início e fim é fechado à esquerda e aberto à direita. Veja o exemplo:

.. activecode:: strings_fatia_intervalos

    def main():
        ini, fim = 3, 7
        texto = "Eu posso ajudar respondendo perguntas no fórum!"
	fatia = texto[ ini : fim ]
	print("Seja o texto:\n", texto)
        resposta = input("Digite o string que corresponde à fatia texto[%d:%d] = "%(ini,fim))
        if resposta == fatia:
	    print("Muito bem, você acertou!!")
	else:
	    print("Você escreveu        : ", resposta)
	    print("mas o valor correto é: ", fatia)

    main()


Modifique os valores das variáveis `ini` e `fim` desse programa, para 5 e 15 respetivamente e veja se você consegue adivinhar qual a fatia resultante. Teste outros valores, inclusive valores negativos, como [-15 : -10], e procure entender o que e Python faz em cada caso.

.. admonition:: Índices negativos não são inválidos!
		    
    Observe que o Python usa índices negativos para percorrer a sequência da direita para a esquerda (fim para o começo) e, portanto, índices negativos não são inválidos. Um índice é **inválido** apenas quando ele cai fora da sequência, que pode ser um número positivo ou negativo maior que o tamnanho da sequência.

Lembre-se também que quando desejamos uma fatia começando do primeiro caractere (índice zero), não é necessário escrever o zero explicitamente, simplificando assim a notação para `fatia = texto[:5]` para o intervalo `[0:5]`. De forma semelhante, não é necessário escrever o valor do último índice caso desejamos uma fatia até o final, como por exemplo `fatia = texto[-6:]` para definir uma fatia com os 6 últimos caracteres do string `texto`.

Nesses exemplos nem consideramos o `passo`, pois assumimos que ele era 1. Veja como usar passos diferentes de 1 usando o exemplo a seguir.

.. activecode:: strings_fatia_passos

    def main():
        ini, fim, passo = 3, 13, 2 
        texto = "Eu posso ajudar respondendo perguntas no fórum!"    
	fatia = texto[ ini : fim : passo ]
        resposta = input("Digite o string que corresponde à fatia texto[%d:%d:%d] = "%(ini,fim,passo))
        if resposta == fatia:
	    print("Muito bem, você acertou!!")
	else:
	    print("Você escreveu        : ", resposta)
	    print("mas o valor correto é: ", fatia)

    main()

Modifique os valores das variáveis `ini`, `fim` e `passo` desse programa, inclusive para valores negativos, e procure entender o que e Python faz em cada caso.


.. break

Strings não são listas!
-----------------------

Apesar de serem criados usando um par de " (aspas) ou ' (apóstrofes) como demarcadores, vimos que a função `len()`, a concatenação, a indexação e o fatiamento de strings são realizados de forma semelhante às listas.

A grande diferença é que strings são **imutáveis**, ou seja, não é possível alterar os seus elementos. Execute o programa abaixo e veja o que o Pytho faz quando tentamos modificar `texto[2]` para `S`. 

.. activecode:: strings_nao_sao_listas

    def main():
        texto = "Esse é um 'string'"
	print(texto)
        texto[2] = 'S'
	print(texto)

    main()

Como strings são imutáveis, caso você precise de um string diferente, é necessário criar um novo utilizando, caso desejar, fatias (partes) de outros strings.

.. activecode:: strings_novo_string

    def main():
        texto = "Esse é um 'string'"
	print(texto)
        novo_texto = 'Mais' + texto[6:]
	print(novo_texto)

    main()


Modifique esse programa para criar e imprimir o texto mudando a palavra `string` 
para `mundo pequeno`.		

.. break

Representação de caracteres
---------------------------

Um conhecimento importante para trabalhar com strings é a conhecer a
sua forma de representação (ou `encoding`). Uma grande diferença entre
as versões 2.X e 3.X do Python é que, na versão 2.X os strings são
codificados em ASCII e na versão 3.X em UTF-8.

No `ASCII (American Standard Code for Information Interchange)
<http://en.wikipedia.org/wiki/ASCII>`__, cada
caractere é representado usando apenas 7 bits, sendo que os caracteres
de 0 a 31 são considerados de controle, e os de 32 a 127 correspondem
aos caracteres comuns de um teclado de computador em inglês.
Execute o seguinte programa para ver os caracteres de 32 a 127:

.. activecode:: aula13_tabela_ascii

    print("Tabela ASCII de 32 a 127: ")
    for i in range(32, 128, 3):
        print("ASCII[ %3d ] = '%s'  |  ASCII[ %3d ] = '%s'  |  ASCII[ %3d ] = '%s'"%
	(i, chr(i), i+1, chr(i+1), i+2, chr(i+2)))

Esse programa usa a função ``chr`` que converte um índice da tabela ASCII em seu caractere correspondente. Observe que a ordem dos caracteres na tabela não é arbitrária. Por exemplo, os caracteres de `a` a `z` (tanto minúsculos quanto maiúsculos) estão agrupados sequencialmente, respeitando a ordem alfabética.
Observe no entanto que não há caracteres acentuados, pois estes não fazem parte da língua inglesa.

O *Unicode* é um conjunto de caracteres que inclui os símbolos de todas as línguas humanas, desde os milhares de caracteres de línguas asiáticas como o chinês, até os caracteres de línguas mortas como os hieróglifos usados pelos egípicios. Para representar os caracteres do Unicode, foram desenvolvidos diversas formas de codificação. Uma delas é a `UTF-32 <http://en.wikipedia.org/wiki/UTF-32>`__ (*U* de Universal Character Set e o *TF* de Transformation Format - usando 32 bits). Essa forma utiliza sempre 32 bits (ou 4 bytes) para codificar um caractere (ou, mais exatamente, para codificar o índice do caractere na tabela Unicode).

O UTF-32 não é muito utilizado hoje pois, apesar de ser simples de decodificar, seus arquivos tornam-se muito grandes. O `UTF-16 <http://ergoemacs.org/emacs/unicode_basics.html>`__ exige ao menos 2 bytes (16 bits) para codificar cada caractere, criando arquivos menores em geral. Esse padrão é utilizado na linguagem Java e em sistemas operacionais como o Microsoft Windows e o Mac OS X. Já o padrão mais utilizado para codificar páginas na Internet atualmente é o `UTF-8 <http://en.wikipedia.org/wiki/UTF-32>`__, que usa pelo menos 8 bits para representar cada caractere. 

O Python oferece vários métodos para operar com strings (consulte a `documentação do Python sobre strings  <https://docs.python.org/3/library/stdtypes.html#text-sequence-type-str>`__). A seguir veremos apenas alguns mais úteis para processar arquivos.

.. break

Arquivos
--------

Utilizamos arquivos para armazenar informação, programas, documentos, etc. 

Para os fins desse curso, vamos trabalhar apenas com arquivos tipo texto, chamados assim porque seu conteúdo pode ser lido facilmente por uma pessoa (com extensões como .txt, .py, .html, etc). Existem também arquivos binários, feitos para serem lidos ou processados por computadores, como arquivos executáveis, arquivos comprimidos (tipo .zip, .gz, etc), arquivos com imagens (tipo .jpg, .gif, .tiff, etc), e muitos outros.


Criação de arquivos
...................

Para criar um arquivo e poder gravar informações nele, vamos utilizar
a seguinte forma:

.. sourcecode:: python

    nome = "poesia.txt"    # poderia ser um input("Digite o nome do arquivo: ")
    
    with open(nome, 'w', encoding='utf-8') as arq:
        # CORPO DO WITH
	arq.write("    O poeta é um fingidor.      \n")
	arq.write("    Finge tão completamente     \n")
	arq.write("    Que chega a fingir que é dor\n")
	arq.write("    A dor que deveras sente.    \n")
	arq.write("                Fernando Pessoa.\n")
	
Utilizamos o comando ``with`` para nos proteger contra alguns problemas que podem acontecer com arquivos (que não iremos detalhar no momento). A função ``open()`` abre o arquivo de nome `poesia.txt` (certifique-se de incluir todo o caminho (``path``) no nome, ou o arquivo será gravado no mesmo diretório do programa sendo executado) para gravação (devido a opção `w` do inglês `write`) e, para garantir que o arquivo será gravado em ``utf-8``, definimos também a forma de codificação pela opção ``encoding``.

Dentro comando ``with`` (corpo do ``with``), devemos fazer todas as operações com o arquivo ``arq``, nesse exemplo, escrever algumas linhas de texto usando o método ``write()``. Observe que o ``\n`` faz a mudança de linha explicitamente.
Uma grande vantagem de utilizar o comando ``with`` é que o arquivo é fechado automaticamente ao final do corpo, não sendo necessário fechar o arquivo explicitamente.


Leitura de arquivos
...................

Agora vamos abrir o arquivo "poesia.txt" para processar o seu conteúdo, utilizando o comando ``with`` novamente:

.. sourcecode:: python

    nome = "poesia.txt"    # poderia ser um input("Digite o nome do arquivo: ")
    
    with open(nome, 'r', encoding='utf-8') as arq_entrada:
        # CORPO DO WITH
        conteudo = arq_entrada.read()
	
    # continue o programa usando conteudo
    print(conteudo)


A saída seria:

.. sourcecode:: python

	O poeta é um fingidor.
        Finge tão completamente     
        Que chega a fingir que é dor
        A dor que deveras sente.    
                    Fernando Pessoa.

	
A função ``open()`` abre o arquivo com nome indicado no string ``nome`` (certifique-se de passar todo o caminho (``path``) no nome, ou o arquivo deve estar no mesmo diretório do programa sendo executado) para leitura (devido a opção `r` do inglês `read`) e usamos a mesma forma de codificação, no caso, `utf-8`.

No corpo do comando ``with`` devemos fazer toda a computação com o arquivo, acessando-o pela variável ``arq_entrada`` (poderia ser qualquer outro nome). O método ``read()`` lê todo o conteúdo de ``arq_entrada`` e o atribui para ``conteudo``. Nesse caso, a variável ``conteudo`` faz referência a um string cujo conteúdo foi carregado do arquivo.
Ao final do corpo do ``with``, esse comando fecha automaticamente o arquivo. 

Ao invés de carregar o arquivo inteiro de uma vez, muitos vezes é mais conveniente processar uma linha de cada vez para converte-lo ou extrair as informações necessárias, e assim evitar de manter o arquivo todo (que pode ser muito grande) na memória do computador.

Para isso, o Python oferece a seguinte forma de processar as linhas de um arquivo:

.. sourcecode:: python

    nome = "poesia.txt"
    
    with open(nome, 'r', encoding='utf-8') as arquivo:
        for linha in arquivo:
	    print(linha)

A saída seria:

.. sourcecode:: python
    
        O poeta é um fingidor.
	
        Finge tão completamente
	
        Que chega a fingir que é dor
	
        A dor que deveras sente.
	
                    Fernando Pessoa.
	
Nesse caso, é como se o arquivo fosse uma lista de linhas.
Ao rodar esse programa no entanto, você deve notar que o espaçamento entre linhas aumentou. Isso porque a linha contém o ``\n`` e a função ``print()`` muda automaticamente de linha.

Como já mencionamos, 
o Python oferece vários métodos para operar com strings (consulte a `documentação do Python sobre strings  <https://docs.python.org/3/library/stdtypes.html#text-sequence-type-str>`__).
O método ``strip()`` é um método para eliminar o ``\n`` e demais caracteres considerados "brancos" (como o espaço e o tab) no início e final de um string.
Experimente agora rodar o programa abaixo:

.. sourcecode:: python

    nome = "poesia.txt"
    
    with open(nome, 'r', encoding='utf-8') as arquivo:
        for linha in arquivo:
            linha_sem_brancos = linha.strip()
	    print(linha_sem_brancos)

e nesse caso a saída seria:

.. sourcecode:: python

    O poeta é um fingidor.
    Finge tão completamente
    Que chega a fingir que é dor
    A dor que deveras sente.
    Fernando Pessoa.

O ``strip()`` é muito útil quando estamos interessados apenas no texto e podemos desconsiderar a formatação. Observe que não apenas o ``\n`` foi desconsiderado, mas também o tab no início de cada linha do poema.

Finalmente, se além do texto queremos separar a frase em palavras, podemos utilizar o método ``split()``, que em seu comportamento padrão separa a frase em palavras usando os caracteres considerados "brancos" como separadores.
Rode o seguinte programa para ver o resultado de ``split()`` nas linhas do poema.

.. activecode:: aula13_exemplo_split

    linhas = [
		"    O poeta é um fingidor.      \n",
		"    Finge tão completamente     \n",
		"    Que chega a fingir que é dor\n",
		"    A dor que deveras sente.    \n",
		"                Fernando Pessoa.\n"
		]

    for linha in linhas:
        linha_sem_brancos = linha.strip()
        print(linha_sem_brancos)
	palavras = linha_sem_brancos.split()
	print(palavras)
   
Experimente rodar esse mesmo programa sem o ``strip()``.

.. break

Exercícios
----------

Exercício 1
..............

**PARTE A**

Escreva uma função separa, que recebe um string texto e um caractere separador. A função "corta" o texto nos separadores, retornando uma lista com as palavras do texto.

Exemplo:

para o texto: ",1,,2,3," a saída deve ser a lista:
['', '1', '', '2', '3', '']
onde '' indica uma palavra vazia (entre 2 separadores consecutivos).


.. activecode:: aula13_ex131a_tentativa

    def separa(texto, sep):
        '''
        (str, str) -> list
	recebe um text e separador, e devolve o texto separado
        '''
        # escreva o seu programa abaixo e remova o print()
        print("Vixe! Ainda nao fiz a funcao!")

**PARTE B**

Usando a função do item anterior, escreva um programa que leia uma linha com palavras separadas por vírgula, e determina o comprimento da maior palavra. A linha pode conter palavras vazias.

.. activecode:: aula13_ex131b_tentativa

    #-----------------------------------------------
    def main():
        '''
	lê uma linha com palavras separadas por vírgula e
	determina o comprimento da maior palavra.
        '''
        # escreva o seu programa abaixo e remova o print()
        print("Vixe! Ainda nao fiz o exercicio!")

    #-----------------------------------------------
    main()

**PARTE C**

Escreva o programa da parte B usando a função ``split()``.


Exercício 2
..............

Escreva um programa que leia vários números reais em uma mesma linha, e imprima a soma.

.. activecode:: aula13_ex132_tentativa

    #-----------------------------------------------
    def main():
        '''
	lê uma linha com numeros reais e calcula a soma dos numeros
        '''
        # escreva o seu programa abaixo e remova o print()
        print("Vixe! Ainda nao fiz o exercicio!")

    #-----------------------------------------------
    main()


Exercício 3
..............

Escreva um programa que leia um arquivo contendo ao menos 1 número real por linha, e para cada linha imprima a sua soma. Ao final, o programa deve imprimir também a soma total. Exemplo: para o arquivo numeros.txt com:

.. sourcecode:: python
		
    5 4 3 2 1
    4 4 4
    2.7
    3.14 2.1
  
a saída deve ser:

.. sourcecode:: python
		
    Digite o nome do arquivo: numeros.txt
    Soma = 15.000000
    Soma = 12.000000
    Soma = 2.700000
    Soma = 5.240000
    Total = 34.940000
  
Dessa vez, use um editor como o Idle no seu computador para escrever o programa.


Exercício 4
...........


**Parte A**

Escreva uma função com protótipo

.. sourcecode:: python

    def insereSeNovo (nome, lista):
        ''' (str, list) -> int
            modifica a lista, inserindo nome na lista.
	    Retorna a posição de nome na lista ou None caso ele já exista.
	'''

que devolve a posição em que o string x ocorre em lista ou, caso x não estiver na lista, insere x no final da lista e devolve o índice dessa posição.

.. activecode:: aula16_ex01a_tentativa

    # escreva a sua função abaixo e remova o print()
    def insereSeNovo (nome, lista):
	print("Vixe! Ainda nao fiz essa funcao!")

    # Codigo para testar a sua funcao
    lista = ["Ronaldo", "Romario", "Rivelino"]
    pos = insereSeNovo("Romario", lista)
    pos = insereSeNovo("Pele", lista)
    pos = insereSeNovo("Rivelino", lista)
    print(lista, " que deve ser Ronaldo, Romario, Rivelino, Pele")

**Parte B**

Escreva um programa que leia uma sequência de nomes e, usando a função do
item anterior, conte quantas vezes cada nome ocorre na sequência.

Exemplo:

    - Para `n = 7` e a sequência `Neimar Messi Kaka Neimar Neimar Messi Zico`
    - a saída deve ser::

        Neimar ocorre 3 vezes
        Messi ocorre 2 vezes
        Kaka ocorre 1 vezes
        Zico ocorre 1 vezes

.. activecode:: aula16_ex01b_tentativa

    # escreva o seu programa abaixo e remova o print()
    print("Vixe! Ainda nao fiz esse exercicio!")

