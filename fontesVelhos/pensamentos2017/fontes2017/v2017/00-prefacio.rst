
..  shortname:: Prefácio
..  description:: Prefácio

Prefácio
========

.. raw:: latex

    \begin{Large}
    \textbf{Prefácio}
    \end{Large}
    \vspace{5mm}

Seja bem-vindo!

Se você chegou até aqui, deve estar interessado em aprender um pouco de 
computação. Mas para que aprender computação? 

Para melhor ou pior, a computação vêm transformando nossa sociedade,
redefinindo como fazemos certas atividades e introduzindo várias outras. 
Com tamanho impacto sobre nossas vidas, aprender um pouco de computação pode 
lhe ser útil, tanto pessoal quanto acadêmica e profissionalmente. 

Essas transformações são possíveis pois a computação nos ajuda a resolver 
certos tipos de problemas, que vamos chamar de "problemas computacionais", 
de formas mais eficazes. 

Historicamente, um primeiro passo para se aprender computação é aprender 
a programar (escrever programas que são executadas por um computador) 
utilizando alguma linguagem de programação. Decidimos nesse livro seguir
um curso um pouco diferente, tirando o foco da linguagem de programação
para a habilidade de resolver problemas computacionais, que chamamos
de `pensamento computacional`. 

DAR mais foco no PC ... 


Esse livro surgiu do nosso desejo de colocar nossas experiências de vários 
anos no ensino de cursos introdutórios de computação que, 
ao longo dos anos, utilizaram diversas linguagens de
programação como Fortran, Pascal e C. 





Nessa edição vamos utilizar a linguagem Python. Para nós
a linguagem de programação é apenas uma ferramenta didática e, 
apesar de ser um bônus concreto de aprendizagem que esperamos lhe seja muito útil no futuro, 
nosso objetivo principal é que você desenvolva um  
`raciocínio aplicado na formulação e resolução de problemas computacionais 
<http://en.wikipedia.org/wiki/Computational_thinking>`__.

Nossa abordagem é desenvolver sua habilidade de resolver problemas computacionais
construindo uma forte base conceitual e experiência com problemas inicialmente simples 
e cada vez mais complexos.
A resolução de um problema passa pelo seu entendimento e conceituação, 
estruturação do problema em problemas menores e mais fáceis de serem tratados,
busca de alternativas de solução que considerem aspectos de desempenho e custo, 
definição de formas adequadas para a representação das informações, 
definição de casos de teste, etc., até a escrita de um código bem documentado. 

Como outras linguagens modernas, o Python oferece várias funções nativas e módulos 
que a tornam muito poderosa. Apesar do domínio dessas ferramentas ser fundamental 
para um programador efetivo, nesse curso vamos utilizar apenas um subconjunto limitado 
dos recursos de Python para que possamos nos concentrar no desenvolvimento do
raciocínio aplicado na formulação e resolução de problemas
computacionais.

Nós esperamos que você utilize essas notas de aula para exercitar a aplicação de cada conceito após uma breve introdução teórica. Cada novo problema vai introduzir novos desafios que, para serem transpostos, 
vai conduzí-lo a novas ideias e práticas de programação.
Para que esses exercícios sejam efetivos no desenvolvimento de seu raciocínio, procure resolver **sozinho** os exercícios antes de ver sua solução. Sua solução deve também utilizar apenas os recursos elementares fornecidos, justamente para treinar o uso desses conceitos. Nossa experiência mostra que, sem essa limitação, muitos alunos gastam mais tempo procurando um "comando mágico" que resolva o exercício ao invés de escrever uma solução. 

Ao final do curso você terá aprendido a resolver problemas computacionais utilizando recursos simples do Python mas disponíveis na maioria das linguagens de programação, como C, C++, Java etc., 
de forma que aprender outras linguagens será uma tarefa mais simples. 

Mas por que aprender computação com *Python*?

Além da linguagem em si, o Python oferece um ambiente interativo, o chamado *Python Shell*, 
que pode ser instalado facilmente em várias plataformas como Linux, Window, Mac OS e Android. 
Esse ambiente é extremamente conveniente e útil para o aprendizado de programação.
Nele é possível testar trechos pequenos de código para testarmos ideias e sanarmos
dúvidas sobre o comportamento de algum comando.

O Python tem uma sintaxe simples e força a escrita de programas estruturados de forma a facilitar
a visualização da solução, identificação de problemas e sua depuração. 
A linguagem oferece também uma ampla variedade de módulos que a tornam muito poderosa, 
facilitando o desenvolvimento de software para várias áreas do conhecimento como matemática,
estatística, processamento de sinais, aprendizagem de máquina e inteligência artificial, entre outras. 

Uma outra ferramenta que tornou possível a criação desse curso interativo em Python
é o projeto `Runestone <http://runestoneinteractive.org>`__, utilizada na criação do livro *online*
`How to Think Like a Computer Scientist - Learning with Python: Interactive Edition <http://interactivepython.org/runestone/static/thinkcspy/index.html>`__.
Deixamos aqui nosso profundo agradecimento aos autores da ferramenta e do livro.

Antes dessas notas, nós ajudamos na tradução do livro 
`Como Pensar como um Cientista da Computação - Aprendendo com Python: Edição interativa <http://panda.ime.usp.br/pensepy/static/pensepy/index.html>`__.
Dessa experiência, decidimos utilizar esses recursos para criar nosso próprio curso interativo em português. Esperamos que esse material ``interativo`` contribua de forma mais efetiva para o desenvolvimento de seu raciocínio computacional pois combinam, em um mesmo lugar, trechos breves de teoria e prática que acompanham e reforçam os conteúdos vistos em aula.  

No que diz respeito à programação em Python, veremos, entre outros possíveis tópicos:

    - funções de entrada e saída: ``input()`` e ``print()``; 
    - tipos nativos de variáveis: ``int``, ``float``, ``str``, e ``bool``;
    - funções de conversão de tipos: ``int()`` e ``float()`` ;
    - operadores aritméticos: ``+``, ``-``, ``*``, ``/``, ``//``, ``%``,...;
    - operadores relacionais: ``<``, ``>``, ``<=``, ``>=``, ``==`` e ``!=``;
    - operadores lógicos ou booleanos: ``and``, ``or`` e ``not``;
    - expressões aritméticas, relacionais e lógicas;
    - execução condicional, alternativa e em cadeia: ``if``, ``if-else``, ``if-elif-else``; 
    - comando de repetição: ``while``;
    - indicadores de passagem (*flags*); 
    - funções;
    - listas;
    - strings; e
    - noções de algoritmos.

Para dar início aos seus estudos e aproveitar melhor os recursos interativos que utilizamos nos exercícios *online*, recomendamos que você leia a seção
`Formas especiais para rodar Python nesse livro <http://panda.ime.usp.br/pensepy/static/pensepy/01-Introducao/introducao.html#formas-especiais-para-rodar-python-nesse-livro>`__
do livro.

Além dos exercícios *online*, recomendamos também que você tente resolver os 
exercícios disponíveis nessa `lista de exercícios
<http://www.ime.usp.br/~macmulti/exercicios>`__.

.. A disciplina é estruturada, em geral, em 3 partes. 
   O material de cada parte foi planejado para ser coberto em cerca de 8 aulas 
   de 100 minutos.
   Cada parte culmina com a aplicação de uma avaliação com duração
   aproximadamente 100 minutos também.
   Para facilitar o estudo individual, esse material foi agrupado em
   tópicos. Sugerimos que cada parte seja coberta em cerca de 1
   mês de estudo.

Há muito mais material disponível, tanto impresso quanto *online*. Mas assim como é improvável aprender a andar de bicicleta apenas estudando livros, desenvolver um raciocínio computacional exige muita prática na resolução de exercícios. Sabemos que, para alguns alunos, esse processo pode ser longo 
e cheio de desafios. 
Esse material foi desenvolvido com esses alunos em mente e esperamos que 
possa ajudá-los na superação desses desafios. 

São Paulo, 17 de março de 2021.

Coelho e Hitoshi
