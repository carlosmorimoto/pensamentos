..  shortname:: Aula 2
..  description:: Primeiros passos
      

Expressões aritméticas, relacionais e lógicas
=============================================

.. index:: expressão aritmética, expressão lógica, expressão aritmética

Tópicos e Objetivos
-------------------

Ao final desse capítulo você deverá ser capaz de:

* calcular o resultado de expressões 
    - aritméticas
    - relacionais
    - lógicas
* escrever expressões corretas usando:
    - operadores aritméticos
    - operadores lógicos
    - operadores relacionais
    - combinações desses operadores
              

.. break

Introdução 
----------

Vimos que uma parte importante do funcionamento de computadores é a sua capacidade de realizar cálculos, semelhante a uma calculadora. 
Antes de escrever programas, precisamos entender como essa "calculadora" funciona e treinar o seu uso até conseguirmos usá-la muito bem, ou seja, até que você consiga prever o valor devolvido pelo computador para expressões complexas. Provavelmente 
você vai encontrar situações que pareçam ambíguas no início e, nesses casos, sugerimos que você utilize o Python shell para verificar qual o comportamento do Python nessas situações. 

Uma regra básica é que toda expressão calculada pelo computador deve sempre devolver o mesmo resultado. Por exemplo, a expressão ``2 + 3 * 4`` poderia devolver o valor ``20`` caso a soma ``2+3`` seja calculada antes da multiplicação. 
Felizmente o Python segue as regras de precedência dos operadores que nós estamos acostumados e o resultado 
devolvido é ``14``. Faça um teste você mesmo digitando essa expressão no Python shell como:

.. code-block:: Python

    >>> 2+3*4
    14

Além de entender como o Python resolve as expressões aritméticas, vamos ver também nesse capítulo como trabalhar com expressões lógicas e relacionais. Vamos começar com uma revisão sobre expressões aritméticas.


..  Uma dificuldade inicial para se aprender uma língua desconhecida
    é perceber e diferenciar os sons, para depois perceber as palavras,
    antes de finalmente conseguir entender as frases.
    Em geral também o entendimento da leitura e escuta de uma língua 
    ocorre antes do domínio da escrita e da fala.
    Acreditamos que o aprendizado de uma linguagem de programação
    não seja muito diferente e, embora mais simples que uma linguagem
    natural, como o português, é necessário praticar muito a leitura para
    que consigamos desenvolver a habilidade de criar programas nessa nova língua.

..  Muitos programadores experientes conseguem escrever um programa
    diretamente em uma linguagem de programação. Ao final desse curso
    gostaríamos que você fosse capaz de escrever alguns programas
    simples em Python. Entretanto, diferente de aprender a ler e escrever em uma
    outra língua, um programa depende de uma solução que precisa ser *pensada* 
    antes do programa ser escrito. Isso porque um programa, em geral, resolve um
    determinado problema. Como cada problema pode ter várias soluções
    possíveis, é sempre recomendado que pensemos em algumas
    soluções distintas antes de escolhermos uma para ser implementada.

..  Embora soluções distintas possam levar a mesma solução correta, 
    cada solução pode ter desempenhos diferentes (ser mais ou menos rápida)
    e consumir recursos diferentes (como mais ou menos memória). 
    Nesse curso, mais que
    programar, vamos desenvolver *boas práticas* de programação
    que ajudam na construção de bons programas.


.. break

Expressões aritméticas
----------------------

.. index:: operador, operando, precedência de operadores, associatividade de operadores.

Uma expressão aritmética é formada por números (chamados de operandos) e operadores. Na sua forma mais simples, uma expressão contém apenas um número. Assim, o valor da expressão com apenas um número é o próprio número. Veja o que acontece quando digitamos apenas um número na linha de comando do Python shell:

.. code-block:: Python

    >>> 5
    5
    >>> 21
    21

Quando uma expressão contém um operador como ``+`` (soma), a "calculadora" deve realizar a operação usando os operandos fornecidos como no exemplo:

.. code-block:: Python

    >>> 5 + 2
    7
    >>> 21 / 3
    7

Podemos dizer que o Python reduz uma expressão a um valor. No caso de um número, não há o que ser reduzido, e o resultado é o próprio número. No caso de uma soma como ``5+2``, os dois números são somados e *reduzidos* ao valor ``7``. 
Como a calculadora (computador) só consegue reduzir um operador por vez, como ela resolve uma expressão com vários operadores como ``2+3*4``? 

Teste o seu conhecimento
........................

Procure prever o resultado das expressões respondendo essas questões antes de testá-las no Python shell.

.. mchoice:: questao_expressao_aritmetica_01
    :answer_a: -1
    :answer_b: -5
    :answer_c:  9
    :answer_d: 15
    :correct: c
    :feedback_a: Incorreto: existe um padrão para cálculo de expressões que é obedecido pelos computadores onde a multiplicação tem precedência sobre a adição e subtração.  
    :feedback_b: Incorreto: existe um padrão para cálculo de expressões que é obedecido pelos computadores onde a multiplicação tem precedência sobre a adição e subtração. 
    :feedback_c: Correto.
    :feedback_d: Incorreto: existe um padrão para cálculo de expressões que é obedecido pelos computadores onde a multiplicação tem precedência sobre a adição e subtração. 

    Qual o valor da expressão: ``2 + 3 * 4 - 5``


.. mchoice:: questao_expressao_aritmetica_02
    :answer_a: 15
    :answer_b:  9
    :answer_c: -5
    :answer_d: -1
    :correct: d
    :feedback_a: Incorreto: é necessário calcular a expressão entre parênteses primeiro.
    :feedback_b: Incorreto: é necessário calcular a expressão entre parênteses primeiro.
    :feedback_c: Incorreto: é necessário calcular a expressão entre parênteses primeiro.
    :feedback_d: Correto.

    Qual o valor da expressão: ``2 + 3 * (4 - 5)``

Digite agora essas expressões no Python shell para conferir o resultado. 
Esperamos que você tenha acertado essas questões pois o Python segue as mesmas 
as regras para cálculo de expressões que você aprendeu nos cursos de matemática.

.. admonition:: Erros de sintaxe

    Lembre-se que o Python shell é um ambiente interativo que espera um ENTER para executar o que foi digitado na linha de comando e imprime o resultado. No caso de expressões aritméticas, uma expressão válida (mesmo que longa) é "reduzida" a um valor. Expressões inválidas geram mensagens de erro como:

    .. code-block:: Python

        >>> 2 + 
        File "<stdin>", line 1
            2 + 
               ^
        SyntaxError: invalid syntax

    Nesse exemplo, como a soma precisa de dois operandos e apenas um foi fornecido, o Python indica um **erro de sintaxe** (*SyntaxError*). Preste bastante atenção nas mensagens pois elas indicam inclusive o local provável onde o erro aconteceu.

Assim, para prever o resultado de uma expressão com vários operadores, além de conhecer o que cada operador faz (como soma e multiplicação), é necessário conhecer as regras de **precedência dos operadores** e também sua **associatividade**. 

Exemplos para entender o que é precedência e associatividade
............................................................

As regras de precedência indicam qual operador é calculado primeiro.
Por exemplo, qual o resultado da expressão ``2 - 3 * 4``? 

Como a multiplicação tem maior prioridade que a subtração, o produto ``3*4`` é reduzido ao valor ``12`` e a seguir se calcula o valor da subtração ``2 - 12``, resultando em ``-10``. 

As regras de associatividade indicam a ordem dos cálculos para operadores que tenham a mesma precedência. 
Por exemplo, qual o resultado da expressão ``2 - 3 + 4``? 

Como a soma tem a mesma prioridade que a subtração, precisamos aplicar a regra de associatividade. 
Em Python, a maioria dos operadores binários (que usam dois operandos) tem associatividade "da esquerda para a direita" (ou seja, as operações são realizadas na mesma ordem de leitura). A expressão portanto é primeiramente reduzida a ``-1 + 4`` resolvendo a subtração e depois reduzida ao valor ``3`` resolvendo a soma. Observe que, caso a soma ``3+4`` fosse calculada primeiro, o resultado final seria ``-5 = 2-(3+4)``.


A tabela a seguir mostra a associatividade das principais operações aritméticas em Python, 
em ordem decrescente de precedência (da maior para a menor):


.. table:: Tabela de precedência e associatividade de operadores aritméticos
    :align: left

    +-------------+--------------------------------------------------+-------------------------------+
    | Operador    |  descrição                                       | Associatividade               |
    +=============+==================================================+===============================+
    | ()          | parênteses                                       |  da esquerda para a direita   |
    +-------------+--------------------------------------------------+-------------------------------+
    | \*\*        | potência                                         |  da direita para a esquerda   |
    +-------------+--------------------------------------------------+-------------------------------+
    | +, -        | positivo e negativo unário                       |  da direita para a esquerda   |
    +-------------+--------------------------------------------------+-------------------------------+
    |\*, /, //, % | multiplicação , divisão, divisão inteira e resto |  da esquerda para a direita   |
    +-------------+--------------------------------------------------+-------------------------------+
    | +, -        | soma e subtração                                 |  da esquerda para a direita   |
    +-------------+--------------------------------------------------+-------------------------------+

Observações:

* Níveis de precedência:
    - A tabela mostra que há grupos de operadores com o mesmo nível, como soma e subtração.
    - Quanto mais "alto" o nível na tabela, maior a precedência.
 
* Operadores com mesmo nível de precedência são resolvidos segundo a sua associatividade.
    - exemplo: para reduzir a expressão ``12 / 2 / 3 / 4``, o Python calcula ``12/2``, e segue dividindo o resultado por 3 e depois por 4. 

* Os operadores unários (``+`` e ``-``) tornam explícitos o sinal do operando. 
    - experimente colocar uma sequência de operadores unários como ``-+-+3`` para ver o que acontece (será que resulta em erro de sintaxe?).

* Use parênteses caso deseje alterar a precedência ou torná-la explícita
    - exemplo: ``12 / 2 / 3 / 4`` => ``0.5``
    - exemplo: ``(12 / 2) / ( 3 / 4)`` => ``8.0``


Teste o seu conhecimento
........................

Qual o valor resultante das seguintes expressões:

.. mchoice:: questao_expressao_aritmetica_03
    :answer_a: 0
    :answer_b: 1
    :answer_c: 4 
    :answer_d: 2.4
    :answer_e: nenhuma das alternativas anteriores
    :correct: c
    :feedback_a: Incorreto: A expressão é calculada da esquerda para a direita como (12 * 2) % 10
    :feedback_b: Incorreto: A expressão é calculada da esquerda para a direita como (12 * 2) % 10
    :feedback_c: Resposta correta.
    :feedback_d: Incorreto. O operador `%` calcula o resto da divisão.
    :feedback_e: Incorreto, ao menos uma das alternativas anteriores está correta.

    Qual o valor da expressão: ``12 * 2 % 10``

.. mchoice:: questao_expressao_aritmetica_04
    :answer_a: 3
    :answer_b: 3.6
    :answer_c: 14 
    :answer_d: 14.4
    :answer_e: nenhuma das alternativas anteriores
    :correct: a
    :feedback_a: Resposta correta.
    :feedback_b: Incorreto: O operador `//` faz divisão inteira.
    :feedback_c: Incorreto: O operador `**` tem precedência maior que multiplicação e divisão.
    :feedback_d: Incorreto. O operador `//` faz divisão inteira e sua precedência é menor que `**`.
    :feedback_e: Incorreto, ao menos uma das alternativas anteriores está correta.

    Qual o valor da expressão: ``4 * 3 ** 2 // 10``

.. mchoice:: questao_expressao_aritmetica_05
    :answer_a: 512
    :answer_b: 256
    :answer_c: 128 
    :answer_d:  64
    :answer_e: nenhuma das alternativas anteriores
    :correct: a
    :feedback_a: Correto: como a associatividade de `**` é da direita para esquerda, o resultado é equivalente a 2 ** (3 ** 2) 
    :feedback_b: Incorreto: veja a associatividade de `**`
    :feedback_c: Incorreto: veja a associatividade de `**`
    :feedback_d: Incorreto: veja a associatividade de `**`
    :feedback_e: Incorreto, ao menos uma das alternativas anteriores está correta.

    Qual o valor da expressão: ``2 ** 3 ** 2``

.. mchoice:: questao_expressao_aritmetica_06
    :answer_a: é igual a (-2) ** 4
    :answer_b: é igual a -(2 ** 4)
    :answer_c:  8 
    :answer_d:  16
    :correct: b
    :feedback_a: Incorreto: verifique qual a precedência entre `**` e o `-` unário.
    :feedback_b: Correto: o operador `**` tem precedência.
    :feedback_c: Incorreto: verifique qual a precedência entre `**` e o `-` unário.
    :feedback_d: Incorreto: verifique qual a precedência entre `**` e o `-` unário.

    Qual o valor da expressão: ``-2 ** 4``


.. break

Expressões relacionais
----------------------

.. index:: operadores relacionais

Além de "fazer contas", o Python permite comparar valores usando os seguintes **operadores relacionais**:

.. table:: Tabela dos operadores relacionais

    +----------+--------------------------------+---------------+---------------+
    | Operador |  Descrição                     | Exemplo       | Resultado     |
    +==========+================================+===============+===============+
    | ==       |        igualdade               | 2 == 3        | False         |
    +----------+--------------------------------+---------------+---------------+
    | !=       |        desigualdade            | 2 != 3        | True          |
    +----------+--------------------------------+---------------+---------------+
    | >        |           maior                | 3 > 3         | False         |
    +----------+--------------------------------+---------------+---------------+
    | >=       |           maior ou igual       | 3 >= 3        | True          |
    +----------+--------------------------------+---------------+---------------+
    | <        |           menor                | 2 < 3         | True          |
    +----------+--------------------------------+---------------+---------------+
    | <=       |           menor ou igual       | 4 <= 3        | False         |
    +----------+--------------------------------+---------------+---------------+

Operadores relacionais comparam dois valores e o resultado pode ser ``False`` (falso) ou ``True`` (verdadeiro). Esse dois valores são chamados de valores **booleanos** em homenagem ao matemático George Boole (`<https://pt.wikipedia.org/wiki/George_Boole>`_). 

Assim como dizemos que as expressões aritméticas são reduzidas a um valor numérico inteiro ou real, as expressões relacionais são reduzidas a um valor booleano (ou seja, ``True`` ou ``False``). As expressões relacionais podem conter expressões aritméticas, como no seguinte exemplo em Python shell:

.. code-block:: Python  

    >>> 2 + 3 == 3 + 2 * 1
    True

Esse exemplo mostra que o resultado da expressão ``2 + 3 == 3 + 2 * 1`` é o valor booleano ``True``. 
Isso porque a precedência dos operadores relacionais é menor que a dos operadores aritméticos, ou seja, as operações aritméticas são reduzidas primeiro, que resulta na comparação ``5 == 5``, e depois a expressão relacional é reduzida (no caso o operador `==`), resultando em ``True``. 

.. break

Expressões lógicas
------------------

As expressões lógicas são construídas usando operadores lógicos sobre valores booleanos.
A tabela a seguir mostra a precedência dos operadores lógicos usados em Python:

.. table:: Precedência dos operadores lógicos

    +----------+--------------------------------+----------------+---------------+
    | Operador |  Descrição                     | Exemplo        | Resultado     |
    +==========+================================+================+===============+
    | ``not``  |  negação lógica                | not True       | False         |
    +----------+--------------------------------+----------------+---------------+
    | ``and``  |  E  lógico                     | True and False | False         |
    +----------+--------------------------------+----------------+---------------+
    | ``or``   |  OR lógico                     | True or False  | True          |
    +----------+--------------------------------+----------------+---------------+

Expressões lógicas (ou booleanas) combinam valores booleanos com operadores lógicos.

O operador ``not`` troca o valor do operando, ou seja, troca o valor booleano de ``True`` para ``False`` e de ``False`` para ``True``.

O operador ``and`` devolve ``True`` apenas quando seus dois operandos são ``True`` e devolve ``False`` caso contrário, quando ao menos um dos operandos é ``False``, como mostra a tabela a seguir. 

.. table:: Tabela do operador lógico ``and``

    +------------+------------+-----------+
    | ``X and Y``| X = True   | X = False |
    +------------+------------+-----------+
    | Y = True   | True       | False     | 
    +------------+------------+-----------+
    | Y = False  | False      | False     | 
    +------------+------------+-----------+


Já o operador ``or`` devolve ``False`` apenas quando seus dois operandos são ``False`` e devolve ``True`` caso contrário (quando ao menos um dos operandos é ``True``. 

.. table:: Tabela verdade do operador lógico ``or``

    +------------+------------+-----------+
    | ``X or Y`` | X = True   | X = False |
    +------------+------------+-----------+
    | Y = True   | True       | True      | 
    +------------+------------+-----------+
    | Y = False  | True       | False     | 
    +------------+------------+-----------+

Como o resultado das comparações usando operadores relacionais é um booleano, os operadores lógicos podem ser utilizados para combinar os resultados relacionais. Por exemplo, considere ``x`` um valor real qualquer. A expressão ``x >= -1 and x <= 1`` pode ser usado para testar se x pertence ao intervalo [-1, 1]. Assim, para ``x = 0`` temos que a expressão é ``True`` pois as duas expressões relacionais se tornam verdadeiras. Mas quando ``x = 2``, a comparação ``x <= 1`` se torna falsa e portanto o resultado do operador ``and`` é ``False``. 

Para esse caso em particular o Python permite a notação ``a <= x <= b`` para verificar se o valor ``x`` está no intervalo [a, b]. 
Embora essa notação seja mais compacta e simples de entender, observe que, se cada operador relacional for reduzido um por vez, como ``(a <= x) <= b`` ou ``a <= (x <= b)``, os termos entre parênteses corresponderiam a valores booleanos que são incompatíveis com valores numéricos. Por exemplo, seja ``a=10``, ``b=20``, ``x=5``  e a expressão ``(a <= x) <= b``. Nesse caso, o valor entre parênteses seria ``False``, resultado de (10 <= 5), e não teríamos como resolver ``False <= 20`` dado que são valores de tipos incompatíveis para comparação.

Observe que o uso de incógnitas como ``a``, ``b`` e ``x`` em expressões é muito útil. Em computação, elementos como esses são chamados de **variáveis** e são objeto do próximo capítulo.

.. break

Exercícios
----------

* **Exercício 1**: 
    Escreva um expressão em função de uma incógnita `nota` que resulte em ``True`` caso `nota` esteja no intervalo aberto (3.0, 5.0), e resulte em ``False`` caso contrário.

    .. admonition:: Dica

        A seguinte expressão em função de `nota` resulta em ``True`` para uma nota maior ou igual a 5

        .. code-block:: Python

            nota >= 5

* **Exercício 2**: 
    Um `ano bissexto <https://pt.wikipedia.org/wiki/Ano_bissexto>`_ ocorre aproximadamente a cada 4 anos. 
    Escreva uma expressão (em função de uma incógnita `ano`) que resulte em ``True`` caso `ano` seja bissexto e ``False`` caso contrário.
    Para ser bissexto, o valor de `ano` precisa ser múltiplo de 4, exceto múltiplos de 100 que não são múltiplos de 400.
    Assim o ano de 2020 é bissexto pois satisfaz todas essas condições. 

    .. admonition:: Dica

        Use o operador `%` para verificar se `ano` é um múltiplo de algum número, como por exemplo
        
        .. code-block:: Python
        
            ano % 4 == 0 
         
        Se `ano=12`, a expressão ``12 % 4 == 0`` resulta em ``True`` pois como 12 é múltiplo de 4 o resto da divisão (``12%4``) é zero.


.. break

Para saber mais
---------------

* Leituras

    - `Capítulo 2: Variáveis, expressões e comandos <https://panda.ime.usp.br/pensepy/static/pensepy/02-Conceitos/conceitos.html>`__ do livro Como Pensar Como um Cientista da Computação.
    - Expressões em Python `<https://docs.python.org/3/reference/expressions.html>`_.
    - `Valores booleanos e expressões booleanas <https://python.ime.usp.br/pensepy/static/pensepy/06-Selecao/selecao.html>`__ do livro Como Pensar Como um Cientista da Computação.

 
* Vídeos

    - `Tipos booleanos e precedência de operadores <https://www.youtube.com/watch?v=sgfmuFRZuWs&list=PLcoJJSvnDgcKpOi_UeneTNTIVOigRQwcn&index=9>`__ e


    - `Expressões lógicas (bool) [Zumbis] <https://www.youtube.com/watch?v=d6XyTLkTYJo#t=41>`__.

