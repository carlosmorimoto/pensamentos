
.. role:: latex(raw)
   :format: latex


:latex:`\Large{UNIVERSIDADE DE SÃO PAULO}\\\\`

.. class:: meuH2

    INSTITUTO DE MATEMÁTICA E ESTATÍSTICA

    DEPARTAMENTO DE CIÊNCIA DA COMPUTAÇÃO

.. raw:: pdf 

    Spacer 0 200


.. class:: meuTitle1

    Introdução à Computação com Python

.. class:: meuTitle2

    um curso interativo

.. raw:: pdf 

    Spacer 0 60

.. class:: meuAutor

    `J.C. de Pina Jr. <http://ime.usp.br/~coelho/>`_ e `C.H. Morimoto <http://ime.usp.br/~hitoshi/>`_

.. raw:: pdf 

    Spacer 0 200

.. class:: meuAutor

    Fevereiro, 2020

