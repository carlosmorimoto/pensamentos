.. -- coding: utf-8 --

    Departamento de Ciência da Computação
    Instituto de Matemática e Estatística

    Fevereiro de 2019:
    Versão 19.02 das aulas de Introdução à Computação com Python.

.. meta::
   :description: Versão interativa das aulas de Introdução à Computação com Python.
   :keywords: python, ciência da computação, programação

.. include::
    ./capa2.rst

.. raw:: pdf

    PageBreak

.. include::
   ../00-introducao.rst

.. raw:: pdf

    SetPageCounter 0 lowerroman

.. contents:: Índice
    :depth: 1

.. include::
    ../01-intro.rst
    ../02-expressoes.rst
    ../02-booleanos.rst
    ../03-while.rst
    ../04-if.rst
    ../05-indicador.rst
    ../06-repeticoes.rst
    ../07-reais.rst
    ../08-funcoes.rst 
    ../09-listas.rst 
    ../10-funcoes-listas.rst
    ../11-strings.rst
    ../12-matrizes.rst
    ../13-busca.rst
    ../14-ordenacao.rst



