.. -*- coding: utf-8 -*-

.. |INSTALAR_PYTHON_URL| replace:: `Como instalar Python em seu computador <https://panda.ime.usp.br/panda/static/data/python/instalacao.html>`__
				   
.. |LIVRO_PEPY_URL| replace:: `Como pensar como um Cientista da Computação <https://panda.ime.usp.br/pensepy/static/pensepy/index.html>`__

.. |PEPY_CAP01_URL| replace:: `Capítulo 1: O Caminho do Programa <https://panda.ime.usp.br/pensepy/static/pensepy/01-Introducao/introducao.html>`__

.. |PEPY_CAP02_URL| replace:: `Capítulo 2: Variáveis, expressões e comandos <https://panda.ime.usp.br/pensepy/static/pensepy/02-Conceitos/conceitos.html>`__

.. |PEPY_SEC_WHILE| replace:: `O comando while <https://panda.ime.usp.br/pensepy/static/pensepy/07-Iteracao/maisiteracao.html#o-comando-while>`__
			      
.. |PEPY_CAP06_URL| replace:: `Decisões e Seleção <https://python.ime.usp.br/pensepy/static/pensepy/06-Selecao/selecao.html#>`__
				
.. |PEPY_CAP13_URL| replace:: `Capítulo 13: Classes e Objetos - Fundamentos <https://python.ime.usp.br/pensepy/static/pensepy/13-Classes/classesintro.html>`__;

.. |PEPY_BOOLEANOS| replace:: `Valores booleanos e expressões booleanas <https://python.ime.usp.br/pensepy/static/pensepy/06-Selecao/selecao.html>`__

.. |PEPY_CONDICIONAL| replace:: `Execução condicional: seleção binária <https://python.ime.usp.br/pensepy/static/pensepy/06-Selecao/selecao.html#execucao-condicional-selecao-binaria>`__

.. |PEPY_SELEÇÃO_UNÁRIA| replace:: `Seleção unária: omissão do else <https://python.ime.usp.br/pensepy/static/pensepy/06-Selecao/selecao.html#selecao-unaria-omissao-do-else>`__

.. |PEPY_TIPOS| replace:: `Variáveis de tipos de dados <https://python.ime.usp.br/pensepy/static/pensepy/02-Conceitos/conceitos.html#variaveis-e-tipos-de-dados>`__

.. |PEPY_CONVERSAO| replace:: `Funções para conversão de valores <https://python.ime.usp.br/pensepy/static/pensepy/02-Conceitos/conceitos.html#funcoes-para-conversao-de-valores>`__

.. |PEPY_OPERADORES| replace:: `Operadores e operandos <https://python.ime.usp.br/pensepy/static/pensepy/02-Conceitos/conceitos.html#operadores-e-operandos>`__
			       
		    
