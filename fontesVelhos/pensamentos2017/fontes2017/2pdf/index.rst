.. Apostila documentation master file, created by
   sphinx-quickstart on Tue Feb 18 17:56:06 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.


.. include:: ../00-prefacio.rst

.. toctree::
    :depth: 2
    :caption: Índice

.. include:: ../01-intro.rst

.. include:: ../02-expressoes.rst

.. include:: ../02-booleanos.rst

.. include:: ../03-while.rst

.. include:: ../04-if.rst

.. include:: ../05-indicador.rst

.. include:: ../06-repeticoes.rst

.. include:: ../07-reais.rst

.. include:: ../08-funcoes.rst 

.. include:: ../09-listas.rst 

.. include:: ../10-funcoes-listas.rst

.. include:: ../11-strings.rst

.. include:: ../12-matrizes.rst

.. include:: ../13-busca.rst

.. include:: ../14-ordenacao.rst



Índices e Tabelas
:::::::::::::::::

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
