.. shortname:: Introdução
.. description:: Introdução


Para onde vamos
---------------

.. raw:: latex

    \begin{flushright}
    {
    "O computador está para a computação assim como o telescópio está para a astronomia." \\
    \href{https://en.wikiquote.org/wiki/Computer\_science}{Edsger Dijkstra} \\
    }
    \end{flushright}
    \vspace{10mm}

.. raw:: html

    <div align="right">
    <p></p>
    <blockquote>
    <p>"O computador está para a computação assim como o telescópio está para a astronomia."</p>
    <p><a href="https://en.wikiquote.org/wiki/Computer_science">Edsger Dijkstra</a></p>
    </blockquote>
    <p></p>
    </div>

.. 

Neste capítulo tratamos rapidamente de vários tópicos, sendo o principal deles, sem sombra de dúvidas, o *pensamento computacional*.
É esse pensamento que estaremos treinando durante todo o tempo.

Como a história da computação está muito relacionada à evolução dos computadores,
máquinas usadas para realizar as computações ou cálculos, vamos começar com uma breve descrição dessas máquinas.
Apresentaremos uma brevíssima *história da computação* para facilitar a introdução de vários conceitos importantes e a motivação de seu estudo.
Você pode encontrar mais detalhes históricos no  `Projeto MAC Multimídia`__.

.. __: https://www.ime.usp.br/~macmulti/historico


É nossa intenção discutir como funciona um computador baseado na arquitetura de `von Neumann <https://en.wikipedia.org/wiki/John_von_Neumann>`_.
Descreveremos como dados e programas são armazenados na memória e definiremos `bit <https://en.wikipedia.org/wiki/Bit>`_, `byte <https://en.wikipedia.org/wiki/Byte>`_, *kilobyte*, *megabyte*, etc. Identificaremos as linguagens de programação de *baixo nível* e de *alto nível* além de descrevermos a diferença entre
linguagens compiladas de linguagens interpretadas.   
Finalmente, falaremos de  *hardware*, *software*, *IDE*, contexto de um programa e programação e computação.
Qualquer conceito discutido aqui, ou em qualquer ponto do curso, será revisitado várias vezes a medida que formos aprofundado nossos estudos.


.. break


Pensamento computacional
------------------------

É comum que cursos de  introdução à computação ou programação tenham uma certa ênfase no aprendizado de uma linguagem de programação.
O nosso foco não será sobre a linguagem de programação, que no caso é Python.
Nos concentraremos no **pensamento computacional** que é um certo
`raciocínio aplicado na formulação e resolução de problemas computacionais <http://en.wikipedia.org/wiki/Computational_thinking>`_.

Treinaremos esse pensamento resolvendo vários pequenos problemas.
Utilizaremos apenas recursos básicos de Python.
A medida que avançarmos, utilizaremos recursos mais elaborados da linguagem.
Entretanto, só utilizaremos esses recursos depois de nós mesmos os construí-los a partir de ferramentas e ideias mais básicas.
De certa forma, do ponto de vista de Python, criaremos ferramentas antes de utilizarmos as ferramentas nativas.
Nosso propósito é remover recursos "mágicos" oferecidos pelas linguagens modernas de
programação para exercitar e desenvolver sua  habilidade de resolver
problemas usando o seu próprio pensamento, ao invés, por exemplo, de buscar alguma solução pronta na Internet.

Frequentemente é possível identificar nesse raciocínio ou pensamento alguns dos seguintes componentes:

* **decomposição**: quebrar um problema complexo em subproblemas que sejam idealmente mais simples;
* **reconhecimento de padrões**: buscar similaridades entre os subproblemas que permitam que estes sejam resolvidos de formas semelhantes;
* **representação dos dados**: determinar as características do problema necessárias para sua solução; e
* **algoritmos**: sequência de passos para resolver o problema.

Em particular, quebrar um problema em subproblemas mais simples e a procura de similaridades não são estratégias somente utilizadas em programação.
Essas são rotinas aplicadas na solução diária de problemas de diversas origens e não são as únicas.
A criatividade de cada uma e cada um nos leva frequentemente a soluções muito engenhosas.

Não se trata de acertar ou errar.
Se trata de nos tornarmos melhores pensadores.
Para treinar nosso pensamento começaremos sempre da análise e compreensão de um problema.
Passaremos a construção de um roteiro para sua solução.
Esse roteiro ou *script* é o nosso programa e ao ser executado em um computador nos leva a uma
solução.

        
Nas próximas seções decreveremos de uma forma superficial alguns conceitos sobre computadores, computação e programa para que possamos entender melhor a natureza
dos problemas computacionais e das suas soluções. Entretanto, a nossa intenção é passar o mais rapidamente para a fase de resolução de problemas.

.. break

   
Computação no dia a dia
------------------------

É difícil imaginar hoje um ramo da atividade humana que não tenha se
beneficiado diretamente dos avanços da computação.  Seus impactos são
tão notáveis que não precisamos ir longe para perceber seus efeitos.
A começar talvez pelos dispositivos móveis como os telefones celulares
rodando inúmeros aplicativos que, para o bem ou para o mal, vem
transformando comportamentos pessoais, sociais e até criando alguns
novos.  Esse exemplo mostra que um dispositivo "comum" como um
telefone, ao se tornar digital e conectado à Internet, se torna uma
coisa muito diferente e, por ser programável, sua utilidade aumenta a
cada nova aplicação que instalamos nessa coisa, *uma coisa vira outra
coisa!*.

Uma outra tecnologia que começa a ser notada é a *Internet das Coisas*
(IoT = *Internet of Things*).  A IoT vem permitindo que "coisas" se
tornem mais "inteligentes", desde uma lâmpada que pode ser controlada
remotamente pela Internet, até a integração de "coisas" em toda uma
cidade.  Dessa forma, as cidades inteligentes poderão otimizar melhor
seus recursos, como por exemplo melhoria no trânsito, maior
conservação de energia e redução de poluentes.

Assim, a medida que mais coisas se tornam digitais, conectadas à
Internet e programáveis, abrem-se infinitas novas oportunidades de
uso, tanto de coisas agindo individualmente, quanto coisas agindo em
conjunto com outras coisas. Imagine agora a quantidade de dados
produzidos por essas coisas. Saber programar novos usos dessas coisas
é certamente um desafio que, quem sabe, você mesmo vai ajudar a
resolver usando algumas das habilidades que você vai desenvolver nesse
curso.

Mas não precisamos ser tão ambiciosos.  Para alunas e alunos de cursos
de engenharia, ciências de exatas, por exemplo, as ferramentas
computacionais vistas nesse curso podem ajudar a entender conceitos de
cálculo e estatística, visualizar dados multidimensionais e simular
modelos. Alunas e alunos das áreas humanas e biológicas também podem
utilizar as habilidades desenvolvidas nesse curso para começar a
explorar seus dados e testar seus próprios modelos. Mas seja qual for
a sua área de interesse, desenvolver um raciocínio computacional vai
ajudar você a desenvolver melhores programas assim como pode ajudar
você a utilizar programas existentes de forma mais eficaz.

A abordagem de programação (`computational thinking <https://en.wikipedia.org/wiki/Computational_thinking>`_)
e as ferramentas que você aprenderá
devem auxiliá-la(o) a resolver vários problemas computacionais que serão encontrados
no restante de seus cursos e na suas vidas profissionais.

.. break

Calculadoras, computadores e programas
--------------------------------------

.. index:: computador, arquitetura de von Neumann, ULA, UCP

A habilidade de representar quantidades e manipulá-las, como "fazer
conta" de adição e de subtração, é uma necessidade básica para muitas
atividades diárias.  O `ábaco <https://en.wikipedia.org/wiki/Abacus>`_
e a `régua de cálculo <https://en.wikipedia.org/wiki/Slide_rule>`_ são
exemplos de instrumentos inventados para nos ajudar a "fazer contas".

A primeira calculadora mecânica foi desenvolvida por Blaise Pascal
em 1642.  A `Pascaline
<https://en.wikipedia.org/wiki/Pascal%27s_calculator>`_ utilizava um
sistema complexo de engrenagens para realizar operações de adição e
subtração.

Além de realizar operações matemáticas como calculadoras, os
computadores são máquinas que podem ser programadas.  Programas
permitem a repetição de cálculos, como por exemplo, o valor do salário
devido a cada funcionário baseado no número de horas trabalhadas.

Os primeiros computadores eletrônicos, que usavam válvulas, foram
desenvolvidos na década de 1940.  O rápido e contínuo desenvolvimento
tecnológico resultou nos computadores modernos, que têm desempenho
muitas vezes superior ao desses primeiros computadores e consumem
muito menos energia.  A figura a seguir mostra um diagrama de blocos
com os componentes básicos de um computador: a unidade central de
processamento (UCP), a memória, e os dispositivos de entrada e saída.
Esses dispositivos são interligados por uma via de comunicação de
dados.

.. image:: ../Figuras/vonNeumann.png
    :width: 600
    :align: center

Esse diagrama ilustra a `arquitetura <https://en.wikipedia.org/wiki/Von_Neumann_architecture>`_ proposta por John von Neumann.

A primeira coisa que notamos quando olhamos para um computador são os dispositivos de entrada e saída,
necessários para que o computador se comunique com outros computadores e pessoas.
Dessa forma, os **dispositivos de entrada** permitem que o computador receba por meio de dispositivos como teclado,
mouse, disco ou Internet os dados a serem processados e transmita os resultados para um **dispositivo de saída** como uma
impressora ou um monitor.

Esses dados ficam armazenados na memória do computador em formato digital, ou seja, na forma de dígitos binários.
Um **dígito binário** assume apenas dois valores, `0`  ou `1`, e são mais adequados para representar eletronicamente.
É possível utilizar uma chave eletrônica aberta para representar o valor `1` ou fechada para valor `0`.
Ainda um capacitor carregado para representar `1` ou descarregado para `0`.
Um único dígito binário é chamado de *bit* e oito bits formam um *byte*.
A capacidade da memória é comumente representada em *kilobyte* (KB) cerca de mil bytes,
*megabytes* (MB) aproximadamente um milhão de bytes,  *gigabytes* (GB) que é um bilhão de bytes e assim por diante.
Hoje é comum encontrar discos com capacidade de mais de um trilhão de bytes, um *terabyte*.
Para se ter uma ideia, um filme de cerca de 2 horas de duração gravado em alta resolução pode ser armazenado usando cerca de 2 GB no formato `mp4`.
Enquanto isso em 2010, último ano em que a `Enciclopédia Britânica <https://websitebuilders.com/how-to/glossary/terabyte/>`_ foi publicada,
todos os textos contidos nos seus 32 volumes podiam ser armazenado em apenas 300 MB,
aproximadamente 300 milhões de caracteres formando cerca de 50 milhões de palavras. 

Um **programa de computador** é uma sequência de instruções que deve
ser executada uma de cada vez pela UCP.  Os programas também ficam
armazenados na memória em código binário, também chamado de **código
de máquina** ou de **linguagem de máquina**.  Uma instrução pode ser
de natureza aritmética ou lógica, que basicamente permite ao
computador executar operações matemáticas como uma calculadora, ou
operações de controle.  As instruções de controle permitem, entre
outras coisas, movimentar dados e desviar o fluxo de execução do
programa para outra trecho do programa.  A UCP contém um pouco de
memória local e um grupo de registradores para trabalhar os dados
internamente.

Um registrador importante que possibilita a execução da sequência
correta de instruções é chamado de **contador de programa** (CP).  O
CP simplesmente indica o endereço ou local da memória que contém a
próxima instrução a ser executada.  A unidade de controle da UCP busca
esta próxima instrução indicada pelo CP, a decodifica e decide se a
executa ou a passa para a unidade lógica e aritmética (ULA) a fim e
ser executada executar.  Após terminar de executar a instrução, o CP é
atualizado para que a unidade de controle passe a buscar a próxima
instrução que deverá ser executada.  O computador simplesmente executa
ininterruptamente o ciclo: busca próxima instrução; executa a
instrução; atualiza CP; busca próxima instrução ...  Cada instrução
executada por um computador é extremamente simples.  O poder dos
computadores moderno reside na capacidade de executar bilhões de
instruções por segundo.  Isso confere a um computador, ou a nós, a
possibilidade de realizar tarefas bastante complexas com desempenho
superior a de um ser humano.  Em geral essas tarefas, apesar de
complexas, são repetitivas e entediantes, permitindo que gastemos
nosso tempo com atividades mais criativas.  Hmm, essa última sentença,
em particular, abre a possibilidade para uma ampla margem de debates.


.. break

Programação
-----------

.. index:: hardware, software, linguagem de máquina, linguagem de baixo nível, linguagem de alto nível, código fonte

A parte física dos computadores é chamada costumeiramente de **hardware**.
A parte digital responsável pelo funcionamento da parte física, os programas de uma forma geral, é chamada de **software**.
O objetivo de programar é criar software para resolver algum problema computacional,
que pode ser um simples cálculo matemático ou algo mais complexo como controlar o
sistema de navegação de veículos ou helicópteros robóticos na superfície de Marte.
A atividade de desenvolver programas é chamada de **programação**. 

Um conceito que devemos ter em mente para começar a programar é que um computador comum executa apenas uma instrução por vez e que a ordem em que das instruções são executadas
é muito importante.
Observe que, como cada instrução pode modificar o conteúdo da memória, cada nova instrução utilizará os dados **atualizados**,
resultantes da execução passo-a-passo das instruções anteriores.
Dizemos que o **contexto** de um programa em um determinado instante é formado pelo conjunto de valores aos quais ele tem acesso.
Valores armazenados na memória naquele instante.

Um programa portanto é uma sequência de instruções que, ao ser executada, vai transformando,
processando por meio de cálculos, os dados de entrada até chegar na saída com o resultado desejado. 
Podemos ainda considerar que cada instrução gera, através do processamento dos dados, valores intermediários que podem ser armazenados na memória para processamentos futuros.
Cada valor intermediário calculado está mais perto do resultado final desejado.
Valores armazenados na memória podem ser acessados por meio de nomes ou apelidos que fazem referência aos valores.
Esses nomes ou apelidos são chamdos de  **variáveis**.
Variáveis são um conceito mundano e fundamental de programação que trataremos em breve e que será revisto em muitas outras ocasiões.

Considere por exemplo o produto de `2` por `5`.
Podemos obter o produto de dois valores usando  adições sucessivas.
Podemos começar com o valor intermediário de `2 + 2`, ou seja `4` 
e dar o nome ou apelidar esse valor de `soma`, a nossa primeira variável.
A seguir adicionamos `2` ao valor associado a `soma`, resultando no novo valor intermediário `6`
e passar a chamar esse valor de `soma`,
Depois de adicionarmos mais `2` ao valor associado a `soma`, rebatizar o valor `8` de `soma`  e finalmente repetir esse processo novamente, adionando mais `2` à `soma`,
chegando no resultado final `10`.

.. admonition:: Exemplo de cálculos intermediários até o resultado final

    soma = 2 + 2       -> variável soma recebe o 1o resultado intermediário, com valor 4

    soma = soma + 2    -> soma recebe o resultado de soma + 2, ou (2 + 2) + 2

    soma = soma + 2    -> soma recebe o resultado de soma + 2, ou ((2 + 2) + 2) + 2

    soma = soma + 2    -> soma recebe o resultado de soma + 2, ou (((2 + 2) + 2) + 2) + 2

Como já foi mencionado, um programa ou  sequência de instruções que uma UCP é capaz de executar é escrito em uma linguagem que é dita **de máquina**
ou **de baixo nível**, por serem as linguagens "compreendidas" pelas máquinas ou computadores.
Muito cedo na história dos computadores eletrônicos se descobriu que é muito difícil programar usando linguagem de máquina
e assim foram surgindo várias linguagens de programação, cada uma com propostas diferentes para tratar certos
problemas computacionais.

De uma forma genérica porém, o desenvolvimento das linguagens de
programação buscou se aproximar de alguma linguagem natural que usamos
para nos comunicar, como o inglês e o português. Essas linguagens,
chamadas de **linguagens de alto nível** por serem mais fáceis de serem usadas 
por nós, humanos, vem se tornando mais poderosas ao apresentar conjuntos de
instruções mais extensos e recursos mais apropriados a aplicações
modernas como computação gráfica, jogos eletrônicos, inteligência
artificial, controlar o sistema de navegação ... 

Como os computadores não foram feitos para entendem diretamente as
linguagens de alto nível, é necessário introduzir um passo
intermediário para *traduzir* ou *interpretar* esses programas para
que possam ser executados pela máquina.  Vamos chamar de **código
fonte** o programa escrito pelos programadores e que fica armazenado
em um arquivo que será chamado de **arquivo fonte**.  Os programas que
traduzem um arquivo fonte em linguagem de alto nível e geram um
arquivo executável em linguagem de máquina são chamados de
**compiladores**.  Ao invés de traduzir o programa e gerar um arquivo
executável, algumas linguagens **interpretam** e executam cada comando
como se rodassem diretamente na linguagem de alto nível.  Esse esforço
para interpretar cada instrução durante a execução pode prejudicar um
pouco o desempenho das linguagens interpretadas, mas tendem a agilizar
o desenvolvimento de programas por evitarem a compilação.

A ciência da computação vai além da programação.  Como ciência, ela
estuda os fundamentos teóricos da informação ou dados, e da computação
realizada na transformação dos dados, além das considerações práticas
para aplicar essa teoria em sistemas computacionais.  Por exemplo,
nesse curso vamos estudar formas para representar diferentes tipos de
dados e processos ou *algoritmos* eficazes para manipular esses dados.
Consideramos o estudo dessas estruturas e algoritmos uma excelente
forma de desenvolver um raciocínio computacional, algo parecido com
estudar partidas entre grandes mestres de xadrez para ser um melhor
jogador de xadrez.

.. break 

.. break

Python no seu computador
------------------------

O desenvolvimento do raciocínio computacional, como qualquer outra atividade,  requer muita prática.
Para isso, cada capítulo traz uma seção de exercícios em laboratório que você deve realizar utilizando um computador para praticar.
Nesse primeiro capítulo, vamos ajudar você a preparar o seu ambiente de programação e testá-lo, 
para que você possa fazer os exercícios dos próximos capítulos.
A instalação também é um bom exercício inicial para os alunas e alunos sem prévia experiência com computadores e serve para que elas e eles
comecem a se habituar com o uso de computadores e das ferramentas de desenvolvimento de programas que serão utilizadas.


Instalando o Python 
...................

.. index:: Anaconda

Vamos utilizar o plataforma `Anaconda <https://www.anaconda.com>`_
que reúne várias ferramentas e bibliotecas úteis para desenvolver programas científicos.
Entre essas  ferramentas e bibliotecas estão
`Spyder <https://www.spyder-ide.org/>`_ (*Scientific PYthon Development EnviRonment*) que é um ambiente interativo de desenvolvimento para a linguagem Python,
`IPython <https://ipython.org/>`_ (*Interactive Python interpreter*),
bibliotecas populares para  Python como
`NumPy <https://numpy.org/>`_ para álgebra linear,
`SciPy <https://www.scipy.org/>`_ para processamento de sinais e imagens,
`matplotlib <https://matplotlib.org/>`_ para desenhos e gráficos  2D/3D e
`pandas <https://pandas.pydata.org/>`_ para análise de dados.

 
Como historicamente a maioria das alunas e alunos costumam
utilizar um computador com o sistema operacional Windows, as
instruções a seguir são voltadas exclusivamente para essa plataforma.
Caso você use um outro sistema operacional, como Linux ou Mac OS, você pode seguir
as instruções específicas para o seu sistema `nesta página de instalação <https://docs.anaconda.com/anaconda/install/>`_ 
da Anaconda.

Antes de instalar, é preciso baixar o software da Anaconda, que é gratuito. 
Para isso, dirija o seu navegador favorito `está página <https://www.anaconda.com/distribution>`_ de *download* da Anaconda.
Clique no botão para fazer o download da versão 3.x do Python, que na figura corresponde a versão 3.7.
Sugerimos que você salve o arquivo para evitar fazer o download novamente caso algo de errado aconteça durante a instalação. 

.. image:: ../Figuras/anaconda/anaconda.com.salvar.png
    :width: 600
    :align: center

Depois que terminar o download (que pode demorar um pouco dependendo da velocidade da sua conexão e por ser um arquivo meio grande), abra o arquivo que você baixou (clique nele) para executar o instalador. Isso deve abrir uma janela como

.. image:: ../Figuras/anaconda/anaconda01.png
    :width: 600
    :align: center

Basta "concordar" com todas as sugestões, como na sequência de figuras abaixo.

Clique em ``I agree``.

.. image:: ../Figuras/anaconda/anaconda02.png
    :width: 600
    :align: center

Selecione ``Just me (recommended)``.

.. image:: ../Figuras/anaconda/anaconda03.png
    :width: 600
    :align: center

Selecione a pasta de instalação e clique em ``Next``.

.. image:: ../Figuras/anaconda/anaconda04.png
    :width: 600
    :align: center

Selecione a opção ``Register Anaconda as my default Python 3.?`` e clique ``Install``.

.. image:: ../Figuras/anaconda/anaconda05.png
    :width: 600
    :align: center

Aguarde o final da instalação, que pode demorar vários minutos.

.. image:: ../Figuras/anaconda/anaconda06.png
    :width: 600
    :align: center

Pronto!
O navegador pode sugerir para instalar outras ferramentas, mas você não precisa fazer mais nada e pode continuar clicando em ``Next`` até fechar o instalador do Anaconda.


Teste a sua instalação com Anaconda Prompt 
............................................

.. index:: Anaconda prompt, terminal, Python shell

Clique na barra de pesquisa do Windows e digite ``Anaconda Prompt``. Dentre as primeiras opções deve estar o 
"Anaconda Prompt (Anaconda 3)" que você acabou de instalar, como ilustrado abaixo:

.. image:: ../Figuras/anaconda/anaconda.pesquisa.png
    :width: 600
    :align: center

Clique nessa opção ("Anaconda Prompt") para abrir uma janela 
como essa mostrada na figura abaixo, que vamos chamar de "terminal" (para ficar mais compatível com outras instalações).

.. image:: ../Figuras/anaconda/anaconda.prompt01.png
    :width: 600
    :align: center

No terminal, digite "python" para abrir o **Python shell**:

.. image:: ../Figuras/anaconda/anaconda.prompt02.png
    :width: 600
    :align: center

O ``Python shell`` faz parte do ambiente Python que permite você digitar comandos de forma interativa, ou seja, para cada comando que você envia o Python te dá uma resposta.
O ``Python shell`` é muito útil para testar um comando quando você não tiver certeza de como funciona e também serve como uma calculadora.
Por exemplo, digite a expressão: ``2 + 2``, ou qualquer operação que desejar,  e em seguida tecle ``ENTER`` e veja o resultado.

Toda vez que você teclar ``ENTER`` você está enviando ao ``Python shell`` um comando,
para que ele o interprete e execute.
Se você enviar um comando incorreto ou incompleto, como ``2 *`` e tecle ``ENTER``, o shell indicará um erro de sintaxe (*SyntaxError*)
que significa que o Python não conseguiu entender o comando.

.. image:: ../Figuras/anaconda/anaconda.prompt03.png
    :width: 600
    :align: center


Teste a sua instalação com o Spyder 
...................................

.. index:: IDE (Integrated Development Environment), ``Spyder``

O `Spyder` é um ambiente integrado de desenvolvimento (IDE = *Integrated Development Environment*)
que faz parte do Anaconda.
O nome ambiente integrado é devido ao fato de reunir várias ferramentas em uma mesma interface gráfica.
Dentre as principais ferramentas que vamos utilizar estão o editor de texto, o ``Python shell``, e o depurador de programas ou *debugger*. 

Para iniciar o ``Spyder``, digite ``spyder`` na área de busca do Windows, da mesma forma que anteriormente você iniciou Anaconda Prompt. 
A figura a seguir ilustra a janela com a interface do Spyder.

.. image:: ../Figuras/anaconda/spyder01.png
    :width: 600
    :align: center

A janela no canto inferior direito é um ``Python shell``.
Você pode clicar nessa janela e digitar uma expressão aritmética qualquer, como ``2 + 3 * 4``  e em seguida tecle ``ENTER`` e observe o resultado. 

O ``Python shell`` fica aguardando um comando do usuário.
Assim que o usuário teclar "ENTER", o ``Python shell`` lê o texto que foi digitado pelo usuário na linha de comando, o interpreta  executa.
Caso seja um comando válido, o ``Python shell`` mostra o resultado na saída.
Caso seja um comando inválido, o ``Python shell`` mostra uma mensagem de erro que descreve o problema encontrado no comando recebido. 

Para escrever programas, mesmo que simples,
devemos usar o editor de texto para escrever todos os comandos,
salvar o programa em um arquivo e depois executar o programa.
Nesse caso é comum chamarmos o programa de um **script**.
Copie o seguinte programa para o editor do Spyder no seu computador. 

.. code-block:: python

    # IMPORTANTE: todas as linhas abaixo devem começar na 1a coluna
    nome = input("Digite o seu nome: ")
    print(f"Olá, {nome}!")
    print("Parabéns! Seu 1o programa em Python está funcionando!")

O texto do seu programa precisa estar **exatamente igual** a esse exemplo.
Em Python, a tabulação de cada linha é muito importante e força uma estrutura visual que ajuda a identificar os blocos, trechos de código,  de um programa.
Como esse exemplo é um programa com apenas 1 bloco, 
verifique se todas as linhas alinhadas na 1a coluna no editor do ``Spyder``. 
Caso não estejam, remova todos os espaços iniciais para que cada linha comece exatamente na 1a coluna do editor.

Salve esse programa clicando na opção de menu no canto superior da janela 

.. code-block::  

    Arquivo -> Salvar como

em alguma pasta no disco do seu computador.  Use um nome qualquer com
"teste.py". Evite usar espaços e caracteres acentuados no nome e
lembre-se de sempre incluir a extensão ".py" para indicar que se trata
de um programa em Python.  Depois de salvo, clique na opção de menu

.. code-block::
    
    Executar -> Executar 

ou simplesmente tecle ``F5`` para executar esse programa ou no triângulo
verde na barra de ferramentas localizada abaixo da linha de menu no
topo da janela.

Ao ser executado, o programa é executado instrução por instrução pelo
Python, mas **sem o shell**.  Em inglês, *shell* significa concha ou
casca.  Na janela do Python shell, essa casca corresponde a uma camada
de interface entre o Python e o usuário.  O shell permite a interação
com o Python por meio da **linha de comando**, ou seja, a linha que,
quando digitamos algum comando e apertamos "ENTER", o shell passa o
comando para o Python. O shell também recebe a resposta e a exibe na
tela. Ou seja, o Python, como linguagem, não tem uma interface
fixa. Sem o shell, os programas precisam especificar como a informação
entra e sai do programa. Vários programas funcionam sem o shell. Por
exemplo, um programa em Python pode controlar o tráfego em um site na
Internet, simplesmente recebendo e enviando dados de uma placa de
rede.

Esse exemplo usa as funções ``input()`` e ``print()`` do Python para
receber dados e exibir mensagens.  Nesse caso, vamos usar a janela
do ``Python shell`` como casca dos nossos programas.  Assim, ao executar o
programa observe que a mensagem ``"Digite o seu nome: "`` aparece na
janela do ``Python shell``. Clique nessa janela para ativá-la e digite o
seu nome usando o teclado. Tecle ``ENTER`` para que o shell
exiba a resposta do programa sendo executado. Finalmente, após
recebida sua mensagem, o programa continua sua execução enviando ao
``Python shell`` as mensagens finais a serem exibidas no shell.


.. break

   
Onde chegamos
-------------

Neste capítulo vimos alguns conceitos básicos de computação sob um
brevíssimo contexto histórico.  Nesse contexto, o Python é uma
linguagem de programação de alto nível altamente portável que pode ser executada
em praticamente em qualquer computador, inclusive em  telefones celulares.
Python é flexível  e pode ser empregada para diversos tipos de aplicações.
A plataforma  Anaconda fornece várias ferramentas integradas,
facilitando a instalação e manutenção do ambiente de desenvolvimento que usaremos.

Nos próximos capítulos vamos utilizar o ``Python shell`` para ilustrar comandos do Python e o ``Spyder``
para escrever programas em ``Python``. 

.. break

   
Teste sua compreensão
---------------------

Indique todas as alternativas corretas.

.. mchoice:: questao_python_shell
    :answer_a: uma interface que permite escrever comandos do Python e ver o resultado.
    :answer_b: um ambiente integrado de desenvolvimento de programas em Python.
    :answer_c: uma ferramenta que permite a execução de comandos em Python e pode ser integrada com outras ferramentas.
    :answer_d: uma casca para proteger o Python conta ataques.
    :answer_e: uma concha onde o Python fica armazenado.
    :correct: a, c
    :feedback_a: Correto
    :feedback_b: Incorreto: o Python shell não é um IDE.
    :feedback_c: Correto: como ocorre no Spyder
    :feedback_d: Incorreto: o shell se refere a uma interface por linha de comando.
    :feedback_e: Incorreto: o shell se refere a uma interface por linha de comando.

    O ``Python shell`` é:


.. mchoice:: questao_dois_kilobytes
    :answer_a: aproximadamente dois mil bits
    :answer_b: aproximadamente quatro mil bits
    :answer_c: aproximadamente oito mil bits
    :answer_d: aproximadamente dezesseis mil bits
    :answer_e: nenhuma das alternativas anteriores 
    :correct: d
    :feedback_a: Incorreto: 1 byte possui 8 bits
    :feedback_b: Incorreto: 1 byte possui 8 bits
    :feedback_c: Incorreto: 1 byte possui 8 bits
    :feedback_d: Correto: 1 byte possui 8 bits
    :feedback_e: Incorreto: 1 byte possui 8 bits

    Quantos bits de informação podemos armazenar em 2 KB (dois kilobytes)?

.. mchoice:: questao_nivel_linguagem_python
    :answer_a: uma linguagem de programação de baixo nível
    :answer_b: uma linguagem de programação de alto nível
    :answer_c: uma linguagem de máquina
    :answer_d: uma linguagem de programação interpretada
    :correct: b, d
    :feedback_a: Incorreto: uma linguagem de baixo nível usa código que roda diretamente (ou quase) na máquina.
    :feedback_b: Correto: um programa em Python parece um texto em inglês.
    :feedback_c: Incorreto: os computadores não conseguem executar diretamente programas em Python. 
    :feedback_d: Correto: um programa em Python não é compilado para linguagem da máquina hospedeira.

    O Python é:

.. mchoice:: questao_tabulacao
    :answer_a: todos os blocos foram digitados corretamente.
    :answer_b: apenas os blocos A e B
    :answer_c: apenas o bloco C
    :answer_d: apenas o bloco D
    :correct: c
    :feedback_a: Incorreto: lembre-se que a tabulação é importante em Python.
    :feedback_b: Incorreto: lembre-se que a tabulação é importante em Python.
    :feedback_c: Correto. As linhas devem começar na primeira coluna.
    :feedback_d: Incorreto: lembre-se que a tabulação é importante em Python.

    Cada bloco corresponde a um programa em Python com apenas 2 linhas. Qual desses programas foi digitado corretamente no editor do Spyder?

    .. code-block:: python

        # Bloco A
        nome = input('Digite seu nome: ')
            print('Olá', nome)

        # Bloco B
            nome = input('Digite seu nome: ')
        print('Olá', nome)

        # Bloco C
        nome = input('Digite seu nome: ')
        print('Olá', nome)

        # Bloco D
            nome = input('Digite seu nome: ')
            print('Olá', nome)


.. break

Para saber mais
---------------

O livro interativo `Como Pensar Como um Cientista da Computação -
Aprendendo com Python: Versão Interativa
<https://panda.ime.usp.br/pensepy/static/pensepy/index.html>`_ é uma
excelente fonte de conceitos sobre programação.  Este livro
é uma tradução do livro `How to Think Like a Computer Scientist:
Interactive Edition
<https://runestone.academy/runestone/books/published/thinkcspy/index.html>`_,
que foi produzido pelo projeto `Runestone Interactive
<http://runestoneinteractive.org/>`_, liderado por Brad Miller e David
Ranum e que conta com muitas e muitos colaboradores.  Por sua vez, o *How to
Think Like a Computer Scientist* foi baseado no `How to Think Like a
Computer Scientist: Learning with Python
<https://open.umn.edu/opentextbooks/textbooks/how-to-think-like-a-computer-scientist-learning-with-python>`_,
escrito Allen Downey, Franklin W. Olin, Jeff Elkner, and Chris Meyers.

Há vários cursos online de Python.
Entre estes temos o `Python para Zumbis <https://www.pycursos.com/python-para-zumbis/>`_ do Fernando
Masanori da FATEC de São José dos Campos.
O Fernando é apaixonado por ensino em geral e em particular pelo ensino de programação com Python.
Temos também o curso `Introdução à Computação com Python. 
<https://www.coursera.org/learn/ciencia-computacao-python-conceitos>`_
do Fábio Kon do Departamento de Computação do IME-USP no Coursera.
 
Sobre computação e computadores você pode ler este `Breve história da computação <http://www.ime.usp.br/~macmulti/historico/>`_ e  
o texto sobre `O que é um Computador <http://pt.wikipedia.org/wiki/Computador>`_.

      
..  Glossário
    ---------
    - Calculadora: máquina que apenas realiza cálculos, sem ou com uma capacidade limitada de programação.
    - Computador: 
    - Hardware:
    - Software:
    - Número binário:
    - bit:
    - byte:
    - ULA: unidade lógica e aritmética.
    - UCP: unidade central de processamento.
    - memória:
    - programa:
    - computação:
    - linguagem de máquina
    - linguagem de alto nível
    - linguagem de baixo nível
    - linguagem de montagem


