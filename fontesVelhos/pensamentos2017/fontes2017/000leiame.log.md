# Arquivo log do livro pensamento

## Esqueleto do livro

* 01 introdução
  
* 02 Pensamento e linguagem

    * computador como calculadora
    * ambiente, shell, entrada, saída e strings

* 03 Pensamento matemático: expressões numéricas

    * tipo int, float
    * variáveis
    * saída formatada usando f"" 

* 04 Pensamento matemático: expressões booleanas

    * comparações e o tipo booleano
    * operadores booleanos
    * comando if, else, elif, etc

* 05 Processamento de uma sequência de dados: repetição

    * história da computação: processamento de dados

* 06 Pensamento computacional

    * tudo junto, de forma estruturada
    * truques com indicadores de passagem
    * malhas encaixadas

* 07 Funções

* 08 Listas

    * comando for
    * funções com lista
        * funções mutadoras
    * fatias
    * vista x clone

* 09 Matrizes

* 10 Strings

* 11 Computação e Matemática (discreta?)

    * números binários
    * mais sobre reais
    * números pseudo aleatórios

* 12 O que é análise de algoritmos?

    * algoritmos de busca

* 13 Algoritmos elementares de ordenação



## 2021-03-20

* Hitoshi: cansei de trabalhar na infra. Vou tentar trabalhar um pouco no texto.

    * ideia: usar "como pensar" para introduzir conceitos e atacar problemas

        * variáveis e tipos númericos
        * condições e booleanos
        * while: repeticao: contar, comparar com anterior

## 2021-03-19

* Hitoshi: trabalhando ainda no:

    * runestone: tem coisas em ingles ainda. Para corrigir desconfio que precisa traduzir o "runestone components". Hack: talvez só mudar strings no runestone.js.
    * panda: Discutir novo layout e estrutura

        * Livro com acompanhamento
        * nível básico 
        * nível intermediário
        * coloquei autores em todos os links





