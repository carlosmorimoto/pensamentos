.. include:: macros.rst

..  shortname:: Booleanos e relacionais
..  description:: Expressões booleanas e relacionais

Expressões lógicas e operadores relacionais
=============================================

.. index::  valores booleanos, True, False

.. index::  operadores lógicos, and, or, expressões lógicas, expressões relacionais

Tópicos
-------

    - tipo ``bool``;
    - valores booleanos:  ``True`` e ``False``;  
    - operadores lógicos ``and``, ``or`` e ``not``;
    - expressões lógicas ou booleanas;
    - operadores relacionais.  

.. Os tópicos a seguir podem ser dadas até a aula 08, em algum momento
   conveniente:
    
    - precedência de operadores;
    - atribuição múltipla: ``i = j = k = 0``, ``x, y = 3, 4``
    - comparações: ``3 <= x <= y < 50``
    - abreviatura: ``+= -= *= /=``


Livro
-----

Sugerimos que seja lido a sessão  |PEPY_BOOLEANOS| do livro Como Pensar Como um Cientista da Computação.

 
Vídeos
------

    - `Tipos booleanos e precedência de operadores <https://www.youtube.com/watch?v=sgfmuFRZuWs&list=PLcoJJSvnDgcKpOi_UeneTNTIVOigRQwcn&index=9>`__ e


    - `Expressões lógicas (bool) [Zumbis] <https://www.youtube.com/watch?v=d6XyTLkTYJo#t=41>`__.

Valores booleanos
-----------------

Em Python, uma variável pode assumir valor booleano ``True``
(= verdadeiro) ou ``False`` (= falso). Esses valores são úteis para
representar, por exemplo, o resultado de uma comparação.

.. activecode:: aula05_exemplo_1
    :caption: Exemplos de expressões relacionais que resultam em valor booleano
    :nocodelens: 

    Qual a saída desse programa? Simule a saída e depois o execute clicando no botão ``Run``. 
    ~~~~

    def main():            
        a = 3
        b = 4
        c = a < b   # c recebe o valor da comparação a < b
        d = a > b   # d recebe o valor da comparação a > b
        e = a == b  # e recebe o valor da comparação a == b

        print("Valor de c: ", c)
        print("Valor de d: ", d)
        print("Valor de e: ", e)
        
    #-----
    main()
                
Operadores e expressões lógicas
-------------------------------

Expressões aritméticas, como ``2 + 3 * 4``, são formadas por valores e operadores
aritméticos ``+``, ``*``, ``-``, ``/``,...
De maneira semelhante, 
aritméticos, , **expressões lógicas** ou **booleanas** são formadas por valores e 
os operadores lógicos tais como ``and``, ``or`` e ``not``. 

Operador ``and``
................

Dados dois valores booleanos ``a`` e ``b``, o operador lógico ``and`` resulta
em ``True`` apenas quando ``a`` e ``b`` foram ambos ``True``, e retorna
``False`` em caso contrário.


.. activecode:: exemplo_op_logico_and
    :caption: Exemplo com o operador lógico ``and``
    :nocodelens: 

    Qual o resultado da função ``print()`` do programa abaixo?
    Altere os valores das variáveis ``a`` e ``b`` e veja o resultado.
    ~~~~

    def main():            
        a = True
        b = False
        print("Valor de 'a and b': ", a and b)
        
    #-----
    main()

A tabela abaixo mostra o resultado de ``and`` para todas as
combinações de a e b.

    +------------+------------+-----------+
    |   ``and``  | a = True   | a = False |
    +------------+------------+-----------+
    | b = True   | True       | False     | 
    +------------+------------+-----------+
    | b = False  | False      | False     | 
    +------------+------------+-----------+
        

Operador ``or``
................

Dados dois valores booleanos ``a`` e ``b``, o operador lógico ``or`` resulta
em ``False`` apenas quando ``a`` e ``b`` foram ambos ``False``, e retorna
``True`` em caso contrário.


.. activecode:: exemplo_op_logico_or
    :caption: Exemplo com o operador lógico ``or``
    :nocodelens: 

    Qual o resultado da função ``print()``?
    Altere os valores de ``a`` e ``b`` no programa abaixo e veja o resultado.

    ~~~~

    def main():            
        a = True
        b = False
        print("Valor de 'a or b': ", a or b)
    
    #-----
    main()
                
A tabela abaixo mostra o resultado de ``or`` para todas as
combinações de ``a`` e ``b``.

    +------------+------------+-----------+
    |   ``or``   | a = True   | a = False |
    +------------+------------+-----------+
    | b = True   | True       | True      | 
    +------------+------------+-----------+
    | b = False  | True       | False     | 
    +------------+------------+-----------+
        

Operador ``not``
................

O operador lógico ``not`` muda o valor de seu argumento, ou seja,
``not True`` é ``False``, e ``not False`` é ``True``.




Expressões Lógicas
------------------

**Expressões lógicas** ou **booleans** são expressões cujo valor 
é verdadeiro (= ``True`` em Python) ou falso (= ``False`` em Python) e 
usam os operadores relacionais:

    ======== ===============
    Operador Descrição 
    ======== ===============
    ==        igual
    !=        diferente
    >         maior
    <         menor
    >=        maior ou igual
    <=        menor ou igual
    ======== ===============
        
Assim, o resultado de uma comparação é um valor `booleano
<http://panda.ime.usp.br/pensepy/static/pensepy/06-Selecao/selecao.html>`__,
``True`` ou ``False``. 

.. activecode:: operador_relacional
    :caption: Teste dos operadores de comparação.
    :nocodelens: 

    Use a janela abaixo para
    experimentar esses operadores.
    ~~~~
    def main():   
        a = 2   # modifique os valores das variáveis a e b
        b = 3  

        print(a, b, b == a + 1)  # modifique a expressão, experimente outros operadores relacionais

    #-----    
    main()
       
Como o resultado de uma comparação é um booleano, comparações podem
ser encadeadas por meio de operadores lógicos. Por exemplo, o trecho
de código abaixo ilustra se uma pessoa está dentro do peso normal
segundo a tabela de `Indice de Massa Corporal <http://www.calculoimc.com.br/tabela-de-imc/>`__.

.. activecode:: operador_relacional_e_logico
    :caption: Programa que calcula o índice de massa corporal
    :nocodelens: 

    Programa para calcular o IMC
    ~~~~
    def main():   
        # modifique esse valor para ver o que o programa imprime
        # altura em metros e peso em Kg
        altura = 1.70
        peso   = 65.3

        # indice de massa corporal
        imc = peso / (altura * altura)

        print("O IMC = ", imc)
        print("Muito abaixo do peso  : ", imc < 17) 
        print("Abaixo do peso normal : ", imc >= 17 and imc < 18.5) 
        print("Peso dentro do normal : ", imc >= 18.5 and imc < 25.0) 
        print("Acima do peso normal  : ", imc >= 25 and imc < 30) 
        print("Muito acima do peso   : ", imc >= 30)

    #-----
    main()
                
.. admonition:: Intervalos em Python

    Para facilitar a legibilidade de intervalos, o Python permite
    escrever expressões relacionais do tipo

.. sourcecode:: python
                    
        17 <= imc < 18.5
        
    ao invés da forma mais *computacional*

.. sourcecode:: python
   
        17 <= imc and imc < 18.5
        
    Experimente reescrevendo alguns trechos de código do exemplo anterior.

		
Exercícios
----------

Exercício 1
...........

Escreva um programa que leia um ano (um número inteiro) e calcula se o ano é bisexto.

Um ano é bisexto deve ser múltiplo de 4 mas não pode ser múltiplo de 100, a menos que seja também múltiplo de 400.

.. activecode:: booleanos_ex1_tentativa
    :caption: Ano bisexto.
    :nocodelens: 

    def main():            
        # Escreva sua solução aqui

    #-----
    main()
                
Exercício 2
...........

O Leonard está procurando uma namorada no site ``alma.gemea.org`` e, para
isso, ele preenche uma ficha com as suas preferências:

   - altura: 155 a 170
   - peso  : 45 a 65
   - idade : 25 a 35
   - cabelo: "loiro"
   - sexo  : "feminino"
   - escolaridade: não "PhD"

O programa abaixo mostra os dados de uma cliente chamada
Penny. Complete o programa abaixo para ver se a Penny é compatível
com o Leonard, imprimindo ``True`` ou ``False``.

.. activecode:: booleanos_ex2_tentativa
    :caption: Ajude Leonard a encontrar uma namorada.
    :nocodelens: 

    def main():            
        # dados da Penny
        nome = "Penny"
        sexo = "feminino"
        altura = 167
        peso   =  52
        idade  =  25
        cabelo = "loiro"
        escolaridade = "medio"
    		
        # Complete a expressão que determina se a Penny é compatível
        # com as preferências do Leonard
        compativel = sexo == 'feminino' and  

        print("Candidata", nome, ":", compativel)

    #-----
    main()
                
Modifique alguns valores para testar a sua expressão.
