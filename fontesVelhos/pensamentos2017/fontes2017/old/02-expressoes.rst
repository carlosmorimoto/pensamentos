.. include:: macros.rst

..  shortname:: Aula 2
..  description:: Primeiros passos
      

Variáveis, expressões e funções de entrada e saída 
==================================================

.. index:: valor, tipo de dados, string, str, inteiro

.. index:: int, int(), while, input(), print() 

Tópicos e Objetivos
-------------------

Ao final dessa aula você deverá ser capaz de:

    - identificar as variáveis de um programa em Python;
    - calcular o resultado de expressões aritméticas;
    - usar o operador de atribuição `=`, com variáveis e expressões;
    - usar a função input();
    - usar a função print();
    - usar strings na função print();
    - usar as função de conversão de tipo int().


Livro
-----

Sugerimos a leitura do |PEPY_CAP02_URL| do livro Como Pensar Como um Cientista da Computação.


Vídeos
------

Recomendamos que sejam assistidos os vídeos:

    - `Variáveis e scripts (programas) em Python <https://www.youtube.com/watch?v=tqF0MjcOqcI&list=PLcoJJSvnDgcKpOi_UeneTNTIVOigRQwcn&index=5>`__;
      
    - `Valores e tipos em Python <https://www.youtube.com/watch?v=UZ7_oudJ150&index=6&list=PLcoJJSvnDgcKpOi_UeneTNTIVOigRQwcn>`__;
      
    - `Entrada de dados <https://www.youtube.com/watch?v=ECFj-bWzU8I&list=PLcoJJSvnDgcKpOi_UeneTNTIVOigRQwcn&index=8>`__;
        

    - `Idle no modo de edição e variáveis (int, str) [Zumbis] <https://www.youtube.com/watch?v=9srd0tYvqv8>`__;
      
    - `Escrita na tela (print) [Zumbis] <https://www.youtube.com/watch?v=dhtEDVw5EFM>`__;

    - `Leitura via teclado (input) [Zumbis] <https://www.youtube.com/watch?v=mubJU5dHyP8>`__.

              
Introdução
----------

Uma dificuldade inicial para se aprender uma língua desconhecida
é perceber e diferenciar os sons, para depois perceber as palavras,
antes de finalmente conseguir entender as frases.
Em geral também o entendimento da leitura e escuta de uma língua 
ocorre antes do domínio da escrita e da fala.
Acreditamos que o aprendizado de uma linguagem de programação
não seja muito diferente e, embora mais simples que uma linguagem
natural, como o português, é necessário praticar muito para
que consigamos desenvolver a habilidade de nos comunicarmos nessa nova língua.

Muitos programadores experientes conseguem escrever um programa
diretamente em uma linguagem de programação. Ao final desse curso
gostaríamos que você fosse capaz de escrever alguns programas
simples em Python. Entretanto, diferente de aprender a se comunicar em uma
outra língua, um programa mais difícil precisa ser *pensado* antes de
ser escrito. Isso porque um programa, em geral, resolve um
determinado problema. Como cada problema pode ter várias soluções
possíveis, é sempre recomendado que pensemos em algumas
soluções antes de escolhermos uma para ser implementada.

Mais que programar, a Ciência da Computação se preocupa em definir
que problemas podem ser resolvidos por meio de computadores, como
eles podem ser modelados, executados, avaliados etc. Assim, mais que
programar, esse curso visa desenvolver *boas práticas* de programação
que levam a construção de bons programas.

Nessa aula, vamos tentar explorar um pouco mais o Python Shell
para começar a **perceber** e **entender** como o Python se comporta
e dessa forma você vai rapidamente aprender a **ler** programas em Python
e começar a escrever alguns programas simples. Antes de prosseguir, sugerimos
a leitura do |PEPY_CAP02_URL|.


Problema
........

Dados dois inteiros ``a`` e ``b``, calcular a sua soma.

.. admonition:: **Nome de variáveis**
	  
   Nesse curso o nome de uma variável será sempre iniciada por uma letra
   minuscula e poderá ser seguida por outras letras mínuscular e números, como
   em: ``contador``, ``ind``, ``i``, ``j``, ``a1``, ``r2d2``, ``bb8``, etc.
   
   Escolha sempre um nome significativo ou comum, para facilitar o
   entendimento sobre o que variável representa ou realiza. Por exemplo, ``i``
   e ``j`` são nomes comuns para contar o número de iterações em um laço, mas
   podemos utilizar o nome ``contador`` e as vezes algumas abreviações como
   ``cont`` e ``aux``. Para melhorar a clareza de seus programas, sugerimos
   também o uso de nomes compostos separados pelo caractere `_`, como em
   ``conta_pares`` e ``conta_impares``. Evitaremos também o uso de caracteres
   acentuados para facilitar a compatibilidade com a ferramenta online
   utilizada no curso.
   
   Vamos chamar de **constantes** variáveis inicializadas uma única vez e que
   nunca mudam de valor. Para indicar uma constante utilizaremos nomes
   formados por todas as letras em maiúscula, como por exemplo
   ``MAXIMO_TAMANHO = 100``.

   **Palavras reservadas** como o nome de comandos e funções nativas do Python
    (``if``, ``while``, ``print()``, etc) também não podem ser utilizadas como nome de
    variáveis.




.. activecode:: aula02_ex1_1
    :caption: Como somar dois números?
    :nocodelens: 

    Tentativa 0: execute o programa abaixo e veja o que acontece.

    ~~~~

    def main():   
       a = 3
       b = 4
       soma = a + b
       print("A soma de a + b eh igual a soma")

    #-----   
    main()    

    ====

    # TODO: add unit tests

Observe que tudo entre aspas (") define um texto ou *string*. Mas queremos
imprimir o **valor** das variáveis e não o **nome** delas.


.. activecode:: aula02_ex1_2
    :caption: Como somar dois números?
    :nocodelens: 

    Tentativa 1: execute o programa abaixo e veja o que acontece.

    ~~~~

    def main():   
        a = 3
        b = 4
        soma = a + b
        print("A soma de a + b eh igual a", soma)

    #-----
    main()

Observe que o print recebeu dois **valores** separados por vírgula:
    - um string (entre aspas) e
    - o nome de uma variável

O texto é impresso diretamente (pois esse é o seu **valor**) e, ao invés de imprimir o nome da
variável soma, o print imprime o seu **valor**.

Mas e como imprimir os valores de ``a`` e ``b`` ao invés de seus nomes?


.. activecode:: aula02_ex1_3
    :caption: Como somar dois números?
    :nocodelens: 

    Tentativa 2: execute o programa abaixo e veja o que acontece.

    ~~~~

    def main():   
        a = 3
        b = 4
        soma = a + b
        print("A soma de", a, "+", b, "eh igual a", soma)

    #-----
    main()
       
Agora sim, a mensagem ficou bem mais clara.
Mas como fazer com que os valores a serem somados sejam definidos por um
usuário?


.. activecode:: aula02_ex1_4
    :caption: Como somar dois números?
    :nocodelens: 

    Tentativa 3: execute o programa abaixo e veja o que acontece.

    ~~~~

    PROMPT_1 = "Digite o primeiro numero: "  # PROMPT_1 é uma constante
    PROMPT_2 = "Digite o segundo numero: "   # PROMPT_2 é outra constante
             
    def main():            
        a = input(PROMPT_1) # veja o texto de PROMPT_1
        b = input(PROMPT_2)
		
        soma = a + b
        print("A soma de", a, "+", b, "eh igual a", soma)

    #-----
    main()
                
Utilizamos a função ``input()`` para receber a resposta do usuário
à mensagem que colocamos como string passado à função ``input()``.
Como a resposta do usuário também é um string (e não um **número**)
a operação ``+`` é realizada com strings, ou seja, eles são "grudados"
(concatenados) 
em vez de adicionados. Não faz sentido adicionar dois strings, mas é 
muito útil *concatenar* dois strings usando o símbolo `+`.

Precisamos, portanto, de um maneira para converter um string em um
número, para que o Python obtenha a soma desses números.


.. activecode:: aula02_ex1_5
    :caption: Como somar dois números?
    :nocodelens: 

    Tentativa 4: execute o programa abaixo e veja o que acontece.

    ~~~~

    def main():
        a_str = input("Digite o primeiro numero: ")
        b_str = input("Digite o segundo numero: ")
        a_int = int(a_str) # converte string/texto para inteiro
        b_int = int(b_str) # converte string/texto para inteiro
        soma = a_int + b_int
        print("A soma de", a_int, "+", b_int, "eh igual a", soma)

    #-----
    main()
                
A função ``int()`` converte um dado string para um número inteiro.
Como não precisamos guardar as respostas na forma de texto,
podemos simplificar o programa combinando as funções ``int()``
e ``input()`` da seguinte forma:
    


.. activecode:: aula02_ex1_6
    :caption: Como somar dois números?
    :nocodelens: 

    Tentativa 5: execute o programa abaixo e veja o que acontece.
    ~~~~
    def main():            
        a = int(input("Digite o primeiro numero: "))
        b = int(input("Digite o segundo numero: "))
        soma = a + b
        print("A soma de", a, "+", b, "eh igual a", soma)

    #-----    
    main()
    
