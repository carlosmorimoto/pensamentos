# verão 1

troco = int(input("Digite o troco: "))

n5 = 0
while n5 * 5 <= troco:
    print(n5, "notas de 5")
    n5 = n5 + 1

print("fim")

# Versão 2
troco = int(input("Digite o troco: "))

n5 = 0
while n5 * 5 <= troco:
    print(n5, "moedas de 5")
    # moedas de 2
    n2 = 0
    while n2 * 2 <= troco:
        print("    com", n2, "moedas de 2")
        n2 = n2 + 1

    n5 = n5 + 1

print("fim")
