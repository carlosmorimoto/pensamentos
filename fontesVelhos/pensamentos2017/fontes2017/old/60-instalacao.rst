.. shortname:: Instalação
.. description:: Como instalar Python 3.x

Como instalar Python 3.x em seu computador usando Anaconda
==========================================================

.. index:: instalação do Python, Anaconda

Para esse curso, vamos utilizar o Python 3.x e os módulos que acompanham o pacote
do `Anaconda <https://www.anaconda.com/distribution/>`__, 
que é fácil de instalar e manter. 

O  `Anaconda <https://www.anaconda.com>`__ é uma plataforma de software provida pela empresa `Anaconda Inc. <https://www.anaconda.com/about-us/>`__ que integra o Python com vários módulos que podem ser úteis no desenvolvimento de aplicações científicas, tais como o Numpy, Scipy e MatPlotLib. 
Essa integração vai facilitar a instalação e a manutenção desse software na sua máquina e, em particular, já vem o `Spyder <https://www.spyder-ide.org/>`__, 
um ambiente integrado de desenvolvimento 
-- `IDE <https://pt.wikipedia.org/wiki/Ambiente_de_desenvolvimento_integrado>`__ -- para Python.

O Anaconda pode ser instalado em computadores rodando Windows, Mac OS X ou Linux. Certifique-se de baixar e instalar a versão do Anaconda apropriada para o seu computador, com Python 3.5 ou mais recente (maior que 3.5). Os arquivos de instalação estão disponíveis em `https://www.anaconda.com/distribution/ <https://www.anaconda.com/distribution/>`__, bem como as instruções para instalação.


Se você está começando a aprender a programar, dê uma olhada nos seguintes vídeos:

    - `Instalação do Python 3.5 em Windows 10 usando Anaconda <https://www.youtube.com/watch?v=tbx2rMJc4n4>`__.

..
    - `Usando o ipython notebook para desenvolver os seus primeiros programas <https://www.youtube.com/watch?v=3bcWDR0Zes0>`__.


Spyder: um ambiente de desenvolvimento integrado para Python
============================================================

O Spyder é uma excelente ambiente de desenvolvimento integrado (IDE = *Integrated development environment*)
para escrever programas e poderá ser utilizado durante todo o curso para
escrever as soluções dos problemas propostos.
Veja essa 
`apresentação sobre o Spyder <https://panda.ime.usp.br/panda/static/data/python/spyder.pdf>`__
para conhecer essa ferramenta.

