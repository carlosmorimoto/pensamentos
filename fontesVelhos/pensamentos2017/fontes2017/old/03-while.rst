.. include:: macros.rst

..  shortname:: while
..  description:: Comando de repetição while

Comando de repetição: ``while``
===============================

.. index:: while, comando de repetição, repetição, expressão relacional


Tópicos
-------

Ao final dessa parte do curso você deverá saber:

    - utilizar comandos de repetição na resolução de problemas computacionais;
    - definir condições, com valores iniciais e de parada, para o comando ``while``;
    - simular o processamento do comando while.

Livro
-----

Será útil a leitura da seção |PEPY_SEC_WHILE| do livro Como Pensar como um Cientísta da Computação.


Vídeos
------

    - `Repetições: comando while <https://www.youtube.com/watch?v=sgfmuFRZuWs&list=PLcoJJSvnDgcKpOi_UeneTNTIVOigRQwcn&index=9>`__;  

    - `Comando de repetição (while) [Zumbis] <https://www.youtube.com/watch?v=ef9LpwS-UHk#t=284>`__. 
      
Introdução
----------

O comando de repetição ``while`` (= enquanto) permite repetir instruções enquanto uma condição for verdadeira.
Para utilizar o comando corretamente você precisa:

    - inicializar as variáveis de controle antes do comando;
    - criar uma condição que usa a variável de controle e se mantenha verdadeira pelo número correto de iterações;
    - modificar a variável de controle para garantir a terminação; e
    - realizar as computações sucessivas para se chegar a resposta correta.



Sintaxe do comando while
........................

A sintaxe do comando ``while`` é a seguinte::
  
  while condição:
      # sequência de comandos executados no corpo do while
      comando_1
      comando_2
      ...
      comando_n

A ``condição`` é em geral definida na forma de uma `expressão
lógica <02-booleanos.html>`__. O
resultado de uma expressão lógica é sempre ``True`` ou ``False``. 
    
A sequência de comandos ``comando_1``, ``comando_2``, ..., ``comando_n``
pode conter qualquer comando do Python como atribuição, entrada ou saída, e outros, até mesmo outro ``while``. 

.. admonition:: Nota sobre **Programação Estruturada**

   Uma característica importante do Python é que ele força a estruturação do programa em blocos que podem ser facilmente visualizados pela **tabulação**. Observe que os comandos *dentro* do ``while``, ou seja, os comandos <comando_1> a <comando_n>, precisam ser deslocados, ou seja, devem começar em uma coluna deslocada para a direita do while. Caso existam comandos a serem executados após o while terminar, esses comandos devem ser escritos usando a mesma tabulação (na mesma coluna) que o while.

    
Descrição do fluxo do programa
..............................

O comando ``while`` repete a sequência de comandos definida em seu corpo enquanto a ``condição`` permanecer verdadeira.

Quando o comando while é executado, a ``condição`` é testada e, caso
verdadeira, o seu corpo é executado um comando de cada vez, ou seja
``comando_1`` primeiro, depois o ``comando_2``, até o ``comando_n``.  Após a
execução do ``comando_n`` a ``condição`` volta a ser testada e, caso
verdadeira, o processo se repete. O while termina quando, ao testar a
``condição``, o resultado do teste for ``False``.


Exemplo
.......

Observe o fluxo do programa abaixo, executando-o passo-a-passo:

.. codelens:: Exemplo_de_while
    :showoutput:
    def main():
                    # inicialização
        fim  = 5        # número de iterações
        cont = 0        # variável de controle

        while cont < fim: 
            # faça alguma coisa, nesse caso, apenas imprima cont
	    print("Iteracao numero: ", cont)

	    cont = cont + 1  # variável de controle precisa ser
	                     # atualizada para garantir o fim do while

    #-----
    main()
   
Esse programa imprime os números de 1 até ``fim``. Procure entender bem as partes desse programa:

    - inicialização das variáveis antes do ``while``
    - condição do ``while``, que define o número de iterações
    - atualização da variável de controle, que garante o fim do ``while``.
      

.. admonition:: Nota sobre **Simulação de um Programa**

   Os programas em Python são constituídos por comandos executados um de cada vez, passo-a-passo. Saber simular um programa é uma habilidade muito importante para entender o que um programa faz, encontrar problemas (`bugs <https://en.wikipedia.org/wiki/Software_bug>`__) e testar soluções.

   Para simular um programa você deve primeiro definir um teste, ou seja, você precisa definir uma entrada e a correspondente saída esperada pelo programa. A "saída" não precisa ser algo impresso mas pode ser uma certa configuração de valores de variáveis. Durante uma simulação devemos executar cada comando e verificar se a evolução dos valores das variáveis está correta.

   Ferramentas como o codelens ajudam, mas você deve aprender a simular um programa manualmente, **sem** o auxílio de um computador. 



Exercícios
---------
   
Exercício 1
...........

Dada uma sequência de números inteiros diferentes de zero,
terminada por um zero, calcular a sua soma. Por exemplo, 
para a sequência: 

.. sourcecode:: python

    12   17   4   -6   8   0      

o seu programa deve escrever o número ``35``. 

Tente escrever a sua solução primeiro e, depois,
clique `aqui <exercicios/while/while_ex1.html>`__ para ver uma.

.. activecode:: aula_while_ex1_tentativa

    def main():            
        # escreva aqui a sua solução

    #-----        
    main()    

    
Exercício 2
...........

.. admonition::

   Exercício 4 da `lista sobre inteiros <http://www.ime.usp.br/~macmulti/exercicios/inteiros/index.html>`__.

Dados números inteiros ``n`` e ``k``, com ``k >= 0``, 
calcular ``n`` elevado a ``k``.
Por exemplo, dados os números ``3`` e ``4`` o seu
programa deve escrever o número ``81``.


Tente escrever a sua solução abaixo primeiro e, depois,
clique `aqui <exercicios/while/while_ex2.html>`__ para ver uma.

.. activecode:: aula_while_ex2_tentativa

    def main():            
        # escreva aqui a sua solução
    
    #-----
    main()

   
Exercício 3
...........

.. admonition::

   Exercício 8 da `lista sobre inteiros <http://www.ime.usp.br/~macmulti/exercicios/inteiros/index.html>`__.

Dado um número inteiro ``n >= 0``, calcular ``n!``. 

Tente escrever a sua solução abaixo primeiro e, depois,
clique `aqui <exercicios/while/while_ex3.html>`__ para ver uma.

.. activecode:: aula_while_ex03_tentativa

    def main():            
        # escreva aqui a sua solução

    #-----        
    main()		


Exercício 4
...........

Nota: Exercício 9 da `lista sobre inteiros <http://www.ime.usp.br/~macmulti/exercicios/inteiros/index.html">`__. 

Dados números inteiros ``n``, ``i`` e ``j``,
todos maiores do que zero,  
imprimir em ordem crescente os  ``n`` primeiros naturais que
são múltiplos de ``i`` ou de ``j`` e ou de ambos.

Por exemplo, para ``n = 6``, ``i = 2`` e ``j = 3`` a saída deverá ser: 

.. sourcecode:: python
 
    0   2   3   4   6   8


.. activecode:: aula_while_ex04_tentativa

    def main():
    # Digite a sua solucao
    
    #-------------------------------------------------
    main()

Clique `aqui <exercicios/multiplos_i_j.html>`__ para ver uma solução.

    
Exercício 5
...........

Nota: Exercício 12 da `lista sobre inteiros <http://www.ime.usp.br/~macmulti/exercicios/inteiros/index.html">`__. 

Dados dois inteiros positivos calcular o máximo divisor
comum entre eles usando o `algoritmo de Euclides <https://pt.wikipedia.org/wiki/Algoritmo_de_Euclides>`__.

.. activecode:: aula_while_ex05_tentativa
		
    def main():
    # Escreva o seu programa
                
    #-------------------------------------------------
    main()

Clique `aqui <exercicios/euclides.html>`__ para ver uma solução.
