.. shortname:: Instalação
.. description:: Como instalar Python 3.x

Como instalar Python 3.x em seu computador
==========================================

.. index:: instalação do Python, IDLE, Anaconda, IDE
	   

Instalando usando Anaconda
--------------------------

Se a sua máquina é relativamente nova (tem menos de 3 anos) e tem espaço em disco sobrando (mais de 3 GB), recomendamos a instalação do `Anaconda <https://www.continuum.io/downloads>`__. Apesar de ocupar mais espaço, esse pacote é fácil de instalar e manter. Caso você não tenha muito espaço, você pode seguir uma das alternativas mais abaixo, usando o miniconda ou instalando Python direto na sua máquina.

O  `Anaconda <https://www.continuum.io>`__ é uma plataforma de software provida pela empresa `Continuum <https://www.continuum.io>`__ que integra o Python com vários pacotes que podem ser úteis no desenvolvimento de aplicações científicas. Essa integração facilita a instalação e a manutenção desse software na sua máquina.

O Anaconda pode ser instalado em computadores rodando Windows, Mac OS X ou Linux. Certifique-se de baixar e instalar a versão do Anaconda apropriada para o seu computador, com Python 3.5 ou mais recente (maior que 3.5). Os arquivos de instalação estão disponíveis em `https://www.continuum.io/downloads <https://www.continuum.io/downloads>`__, bem como as instruções para instalação.

Além de pacotes como o Numpy, Scipy, e MatPlotLib, o Anaconda inclui também ferramentas que podem ajudar você a desenvolver os seus programas em Python, como o iPython (interactive Python) e o Spyder (um ambiente integrado de desenvolviment -- `IDE <https://pt.wikipedia.org/wiki/Ambiente_de_desenvolvimento_integrado>`__ -- para Python).

Se você está começando a aprender a programar, dê uma olhada nos seguintes vídeos:

    - `Instalação do Python 3.5 em Windows 10 usando Anaconda <https://www.youtube.com/watch?v=tbx2rMJc4n4>`__.
    - `Usando o ipython notebook para desenvolver os seus primeiros programas <https://www.youtube.com/watch?v=3bcWDR0Zes0>`__.

Instalando usando miniconda
---------------------------

A instalação do miniconda requer cerca de 400 MB. Além do interpretador Python e o ambiente de desenvolvimento IDLE, miniconda instala também o conda, que ajuda você a gerenciar a instalação e manutenção de outros pacotes do Python. Clique aqui para ver uma rápida introdução ao conda, e assim poder instalar outros pacotes Python. Após instalar o miniconda você pode testar a sua instalação da seguinte maneira:

    - Abra um terminal (janela que recebe comandos via teclado).
        - No Windows, clique no menu "Iniciar" e digite "cmd" ENTER.
    - Após abrir o terminal, digite "python3" para iniciar o Python shell.
    - Digite alguns comando em Python, como "2+2", ou "x=0".
    - Digite "quit()" ou "ctrl-d" para sair do Python shell.

Além de testar o Python 3.x, experimente também chamar o "idle3" a partir do terminal.


Instalando usando python.org
----------------------------

Você pode instalar apenas o Python 3.x seguindo as instruções abaixo, dependendo do tipo do seu computador.

    - `Ubuntu Linux <https://panda.ime.usp.br/panda/static/data/python/ubuntu.html>`__
    - `Mac OS X <https://panda.ime.usp.br/panda/static/data/python/macos.html>`__
    - `Microsoft Windows <https://panda.ime.usp.br/panda/static/data/python/windows.html>`__

Nesse caso, você via precisar gerenciar os pacotes do Python manualmente, ou usando outras ferramentas como pip, easyinstall etc. Para testar a sua instalação:

    - Abra um terminal (janela que recebe comandos via teclado).
        - No Windows, clique no menu "Iniciar" e digite "cmd" ENTER.
    - Após abrir o terminal, digite "python3" para iniciar o Python shell.
    - Digite alguns comando em Python, como "2+2", ou "x=0".
    - Digite "quit()" ou "ctrl-d" para sair do Python shell.

Além de testar o Python 3.x, experimente também chamar o "idle3" a partir do terminal.
