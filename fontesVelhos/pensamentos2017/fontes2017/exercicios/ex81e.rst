.. -*- coding: utf-8 -*-

.. shortname:: Exercício sobre reais
.. description:: Dentro e fora


Solução 5 do Exercício 4 sobre reais
....................................

Nota: Questão 1 da `Prova 1 de 2014 <http://www.ime.usp.br/~mac2166/provas/P1-2014.html">`__.

Na figura, no plano cartesiano, a região sombreada não inclui as
linhas de bordo. Note que o eixo ``y`` cai bem no meio da figura, 
e usamos o lado do quadrado para indicar as ordenadas 
correspondentes.

Escreva na página do desenho um programa que lê as coordenadas
cartesianas ``(x, y)`` de um ponto, ambas do tipo ``float``
e imprime ``dentro`` se esse ponto está na região, e ``fora``
caso contrário.

.. image:: ../../_static/face.png

Solução 5:  utiliza uma variável ``bool`` para indicar em que
parte da face está o ponto: face, boca
olho direito, olho esquerdo, íris do olho direito, íris do olho 
esquerdo. A condição para decidir se o ponto está ou não 
na região hachurada pode parecer complicada, mas é bem elegante.

.. activecode:: aula08_ex81e

    x = float(input("Digite x: "))
    y = float(input("Digite y: "))

    # face == True se (x,y) esta na face, False em caso contrário
    face = -5 < x < 5 and 0 < y < 8 

    # boca == True se (x,y) esta na boca, False em caso contrário
    boca = -3 <= x <= 3 and 1 <= y <= 2

    # olho_esquerdo == True se (x,y) esta no olho esquerdo
    olho_esquerdo = -4 <= x <= -1 and 4 <= y <= 7

    # iris_esquerda == True se (x,y) esta na iris esquerda
    iris_esquerda = -3 <  x <  -2 and 5 <  y <  6

    # olho_direito  == True se (x,y) esta no olho direito
    olho_direito = 1 <= x <= 4 and 4 <= y <= 7

    # iris_direita  == True se (x,y) esta na iris direita
    iris_direita = 2 <  x <  3 and 5 <  y <  6

    # vixe! :-D complicado? certamente elegante...
    if iris_esquerda or iris_direita or face and not (boca or olho_esquerdo or olho_direito):
        print("dentro")
    else:
        print("fora")

    
Clique
    - `aqui <ex81a.html>`__ para ver a 1a solução.
    - `aqui <ex81b.html>`__ para ver a 2a solução.
    - `aqui <ex81c.html>`__ para ver a 3a solução.
    - `aqui <ex81d.html>`__ para ver a 4a solução.
    - `aqui <ex81f.html>`__ para ver a 6a solução.

`Voltar <../08-reais.html#aula08_ex81_tentativa>`__
	
