.. shortname:: Exercício com while
.. description:: Soma de uma sequência	       

		 
Leitura e soma de uma sequência
...............................

Dada uma sequência de números inteiros diferentes de zero,
terminada por um zero, calcular a sua soma. Por exemplo, 
para a sequência: 

.. sourcecode:: python

    12   17   4   -6   8   0      

o seu programa deve escrever o número ``35``. 

.. codelens não funciona com input. Vamos deixar com activecode :(
   
.. activecode:: aula_while_ex1

    def main():            
        num = int(input("Digite um inteiro [0 para terminar]: "))
        soma = 0

        while num != 0:
            soma = soma + num
            num = int(input("Digite um inteiro [0 para terminar]: "))

        print("A soma foi: ", soma)

    #-----
    main()            

`Voltar <../../while.html#while_ex1_tentativa>`__
    
