.. shortname:: Exercício com while
.. description:: Cálculo de fatorial

		 
Fatorial
........

.. admonition:: Nota

   Exercício 8 da `lista sobre inteiros <http://www.ime.usp.br/~macmulti/exercicios/inteiros/index.html>`__.

Dado um número inteiro ``n >= 0``, calcular ``n!``. 

Solução:

.. activecode:: aula02_ex3_active

    def main():            
        n = int(input("Digite o valor de n: "))
        fat = 1
        i = 2
        while i <= n:
            fat = fat*i
            i = i + 1

        print("O valor de %d! eh =" %n, fat)

    #-----
    main()
                
Observe a simulação dessa solução com ``n = 3``
    
.. codelens:: aula02_ex3_code
    :showoutput:

    def main():   
        n = 3
        fat = 1
        i = 2
        while i <= n:
            fat = fat*i
            i = i + 1

        print("O valor de %d! eh =" %n, fat)

    #-----
    main()    


`Voltar <../../while.html#aula_while_ex3_tentativa>`__
