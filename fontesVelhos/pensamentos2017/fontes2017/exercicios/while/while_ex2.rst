.. shortname:: Exercício com while
.. description:: Cálculo de potência


Potência
........

.. admonition:: Nota

   Exercício 4 da `lista sobre inteiros <http://www.ime.usp.br/~macmulti/exercicios/inteiros/index.html>`__.

Dados números inteiros ``n`` e ``k``, com ``k >= 0``, 
calcular ``n`` elevado a ``k``.
Por exemplo, dados os números ``3`` e ``4`` o seu
programa deve escrever o número ``81``.

Solução:

.. activecode:: aula02_ex2_active

    def main():            
        n = int(input("Digite o valor de n: "))
        k = int(input("Digite o valor de k: "))

        pot = 1
        i   = 0
        while i < k:
            pot = pot * n
            i   = i + 1

        print("A potencia eh", pot)  # print mais simples 
        print("O valor de %d elevado a %d eh %d" %(n, k, pot)) # mais elaborado

    #-----        
    main()

Observe a simulação dessa solução para ``n = 2`` e ``k = 3``.

.. codelens:: aula02_ex2_code
    :showoutput:

    def main():
        n = 2
        k = 3

        pot = 1
        i   = 0
        while i < k:
            pot = pot * n
            i   = i + 1

        print("A potencia eh", pot)  # print mais simples 
        print("O valor de %d elevado a %d eh %d" %(n, k, pot)) # mais elaborado


    #-----
    main()

`Voltar <../../while.html#aula_while_ex2_tentativa>`__
