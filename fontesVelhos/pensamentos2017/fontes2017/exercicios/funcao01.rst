.. -*- coding: utf-8 -*-

.. shortname:: Exercício Funcao 1
.. description:: combinacao de m n a n

Solução do exercício 1 sobre funcões
....................................

O número de combinações possíveis de ``m`` elementos em grupos de ``n``
elementos (n <= m) é dada pela fórmula de combinação
``m!/((m-n)!n!)``. 

Escreva um programa que lê dois inteiros ``m`` e ``n`` e calcula a
combinação de ``m``, ``n`` a ``n``.

.. activecode:: aula_funcao_ex01

    # leitura dos valores de entrada 
    m = int(input("Digite m: "))
    n = int(input("Digite n: "))

    # calcula o fatorial de m
    k = m
    k_fat = 1
    cont = 1
    while cont < k:
	cont += 1       # o mesmo que cont = cont + 1
        k_fat *= cont   # o mesmo que k_fat = k_fat * cont
	
    m_fatorial = k_fat
    
    # calcula o fatorial de n
    k = n
    k_fat = 1
    cont = 1
    while cont < k:
	cont += 1       # o mesmo que cont = cont + 1
        k_fat *= cont   # o mesmo que k_fat = k_fat * cont
		
    n_fatorial = k_fat
    
    # calcula o fatorial de m - n
    k = m-n
    k_fat = 1
    cont = 1
    while cont < k:
	cont += 1       # o mesmo que cont = cont + 1
        k_fat *= cont   # o mesmo que k_fat = k_fat * cont
	
    mn_fatorial = k_fat

    print("Comb(",m,",",n,"): ", m_fatorial/(mn_fatorial * n_fatorial))



	
