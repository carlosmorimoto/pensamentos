.. -*- coding: utf-8 -*-

.. shortname:: Euclides
.. description:: mdc usando Euclides


Solução
.......

Nota: Exercício 12 da `lista sobre inteiros <http://www.ime.usp.br/~macmulti/exercicios/inteiros/index.html">`__. 

Dados dois inteiros positivos calcular o máximo divisor
comum entre eles usando o algoritmo de Euclides.

.. activecode:: aula_exercicios_euclides

    def main():

        n = int(input("Digite n: "))
        m = int(input("Digite m: "))

        anterior  = n
        atual     = m
          
        resto = atual % anterior
        while resto != 0:
            resto = anterior % atual;
            anterior = atual;
            atual = resto;
          
        print("MDC(%d,%d)=%d" %(n,m,anterior))
                
    #-------------------------------------------------
    main()


	
