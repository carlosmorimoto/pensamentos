# -*- coding: utf-8 -*-

'''
Uma solução do EP1 usando POO
=============================

Neste exercício você escreverá um programa em Python que simula o enchimento de um recipiente com o maior volume possível de água, sem que haja desperdício por excesso.

Para tornar as coisas mais interessantes, a capacidade do recipiente será um número inteiro desconhecido. Quantidades de água, também representadas por números inteiros, serão sucessivamente depositadas no recipiente. A decisão sobre adicionar ou não mais água no recipiente será de um usuário (= pessoa que está usando o programa).

O objetivo do usuário será encher o recipiente com o máximo volume possível de água, sem deixar que a capacidade do recipiente seja ultrapassada e água seja derramada. A simulação será interrompida por decisão do usuário ou assim que a capacidade do recipiente tenha sido alcançada pela água, havendo ou não derramamento. Para auxiliar o usuário em sua tarefa um aviso será emitido pelo programa assim que a quantidade de água no recipiente tenha alcançado a metade da sua capacidade.

   
Comportamento do programa
-------------------------

O programa deve se comportar da seguinte maneira.

Inicialmente, um número inteiro entre 10 e 50, incluindo o 10 e o 50, deve ser sorteado pelo programa. Esse valor representa a capacidade do recipiente que está inteiramente vazio. A maneira que esse e outros sorteios devem ser feitos pelo programa está descrito mais adiante.

Em seguida, o programa passa a executar os passos descritos abaixo, na ordem em que são apresentados, até que o usuário decida que a simulação deva ser encerrada ou o recipiente esteja cheio de água:
- sorteio do volume de água: um número inteiro entre 1 e 10, inclusive o 1 e o 10, representando um volume de água deve ser sorteado;
- pergunta ao usuário: o usuário deve ser questionado se deseja que esse volume de água seja ou não depositado no recipiente: a resposta do usuário será "s" para sim ou "n" para não;
- resposta sim: indica que o volume de água deve ser depositado no recipiente;
- resposta não: indica que o programa deverá ser encerrado

Além disso, no primeiro momento em que o volume de água no recipiente é de pelo menos metade de sua capacidade, uma mensagem correspondente deve ser emitida pelo programa indicando esse evento.

Ao final, o programa deve imprimir um relatório apresentado:
- a capacidade do recipiente;
- o volume final de água no recipiente;
- uma mensagem indicando se:
-- o recipiente não está cheio e havia espaço para o último volume sorteado; ou
-- o recipiente não está cheio e não havia espaço para o último volume sorteado; ou
-- o recipiente está cheio e não houve derramamento de água; ou
-- o recipiente está cheio e houve derramamento de água e, nesse caso, o volume de água derramada também deve ser apresentado.

'''

import random

def main():
    # leitura do nome do usuário 
    usuario = input("Qual eh o seu nome?: ")

    #leitura da semente para o gerador de números pseudo-aleatórios
    semente = int(input("%s, por favor, digite a semente do gerador aleatório: " %usuario))
    random.seed(semente)    

    volume_sorteado = random.randrange(10, 51)
    # criação de um objeto Balde
    rec = Balde(volume_sorteado)

    print("\nInício da simulação")

    continua = True
    while continua:
        volume_sorteado = random.randrange(1, 11)
        print()
        print("Volume atual: ", rec.volume_atual)
        print("Volume sorteado: ", volume_sorteado)
        opcao = input("Deseja adicionar? (s/n) : ")
        if opcao == 's':
            rec.deposite(volume_sorteado)
            if rec.esta_cheio():
                continua = False
        else:
            continua = False

    print("\nFim da simulação")
    print("Capacidade do recipiente: ", rec.volume_max)
    print("Volume final: ", rec.volume_atual)
    if rec.volume_derramado > 0:
        print("Recipiente está cheio e houve derramamento de água")
        print("Volume derramado foi: ", rec.volume_derramado)
    elif rec.esta_cheio():
        print("Recipiente está cheio e não houve derramamento de água")
    else:
        print("Recipiente não está cheio.")
        if volume_sorteado + rec.volume_atual - rec.volume_max > 0:
            print("Não havia espaço para o último volume sorteado:", volume_sorteado)
        else:
            print("Havia espaço para o último volume sorteado:", volume_sorteado)

         
class Balde:
    def __init__(self, cap=0):
        self.volume_max = cap
        self.volume_atual = 0   # inicialmente está vazio
        self.volume_derramado = 0
        self.passou_metade = False

    def esta_cheio(self):
        return self.volume_max == self.volume_atual

    def deposite(self, vol):
        excesso = vol + self.volume_atual - self.volume_max
        if excesso > 0:
            self.volume_atual = self.volume_max
            self.volume_derramado = excesso
        else:
            self.volume_atual += vol
            
        # if not self.esta_cheio():
        print("Novo volume: ", self.volume_atual)
        if not self.passou_metade and self.volume_atual > self.volume_max / 2:
            self.passou_metade = True
            print("Volume no recipiente atingiu/passou metade de sua capacidade")
            
main()
