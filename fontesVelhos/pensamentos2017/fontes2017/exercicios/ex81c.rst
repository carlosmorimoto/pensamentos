.. -*- coding: utf-8 -*-

.. shortname:: Exercício sobre reais
.. description:: Dentro e fora


Solução 3 do Exercício 4 sobre reais
....................................

Nota: Questão 1 da `Prova 1 de 2014 <http://www.ime.usp.br/~mac2166/provas/P1-2014.html">`__.

Na figura, no plano cartesiano, a região sombreada não inclui as
linhas de bordo. Note que o eixo ``y`` cai bem no meio da figura, 
e usamos o lado do quadrado para indicar as ordenadas 
correspondentes.

Escreva na página do desenho um programa que lê as coordenadas
cartesianas ``(x, y)`` de um ponto, ambas do tipo ``float``
e imprime ``dentro`` se esse ponto está na região, e ``fora``
caso contrário.

.. image:: ../../_static/face.png

Solução 3:  explora a simetria da figura em relação ao eixo y.
A variável x_pos pode ser trocada por x.      
A variável dentro e um indicador de passagem bool.

.. activecode:: aula08_ex81c

    x_pos = x = float(input("Digite x: "))
    y = float(input("Digite y: "))

    if x < 0: # simetria ;-)
        x_pos = -x

    # suponha que (x,y) que esta dentro
    dentro = True

    if x_pos >= 5 or y >= 8 or y <= 0:
        # aqui sabemos que (x,y) esta fora da face
        dentro = False
    elif 0 <= x_pos <= 3 and 1 <= y <= 2:
        # aqui sabemos que (x,y) esta na boca
        dentro = False
    elif 1 <= x_pos <= 4 and 4 <= y <= 7:
        # aqui sabemos que (x,y) esta em um olho
        if not (2 < x < 3 or 5 < y < 6):
            # aqui sabemos que (x,y) esta fora da iris
            dentro = False

    if dentro:
        print("dentro")
    else:
        print("fora")
        
        
Clique
    - `aqui <ex81a.html>`__ para ver a 1a solução.
    - `aqui <ex81b.html>`__ para ver a 2a solução.
    - `aqui <ex81d.html>`__ para ver a 4a solução.
    - `aqui <ex81e.html>`__ para ver a 5a solução.
    - `aqui <ex81f.html>`__ para ver a 6a solução.

`Voltar <../08-reais.html#aula08_ex81_tentativa>`__
	
