.. -*- coding: utf-8 -*-

.. shortname:: Exercício sobre reais
.. description:: Dentro e fora


Solução 1 do Exercício 4 sobre reais
....................................

Nota: Questão 1 da `Prova 1 de 2014 <http://www.ime.usp.br/~mac2166/provas/P1-2014.html">`__.

Na figura, no plano cartesiano, a região sombreada não inclui as
linhas de bordo. Note que o eixo ``y`` cai bem no meio da figura, 
e usamos o lado do quadrado para indicar as ordenadas 
correspondentes.

Escreva na página do desenho um programa que lê as coordenadas
cartesianas ``(x, y)`` de um ponto, ambas do tipo ``float``
e imprime ``dentro`` se esse ponto está na região, e ``fora``
caso contrário.

.. image:: ../../_static/face.png

Solução 1: começa supondo que está dentro e depois...
A variável dentro e um indicador de passagem ``bool``.

.. activecode:: aula08_ex81a

    x = float(input("Digite x: "))
    y = float(input("Digite y: "))

    # suponha que (x,y) esta dentro
    dentro = True

    if x <= -5 or 5 <= x or y <= 0 or 8 <= y:
        # aqui sabemos que (x,y) esta fora da face
        dentro = False
    elif -3 <= x <=  3 and 1 <= y <= 2:
        # aqui sabemos que (x,y) esta na boca
        dentro = False
    elif -4 <= x <= -1 and 4 <= y <= 7:
        # aqui sabemos que (x,y) esta no olho esquerdo
        dentro = False
        if -3 < x < -2 and 5 < y < 6:
            # aqui sabemos que na verdade (x,y) esta na iris esquerda
            dentro = True
    elif  1 <= x <=  4 and 4 <= y <= 7:
        # aqui sabemos que (x,y) esta no olho direito
        dentro = False
        if  2 < x <  3 and 5 < y < 6:
            # aqui sabemos que na verdade (x,y) esta na iris direita
            dentro = True

    if dentro:
        print("dentro")
    else:
        print("fora")


Clique
    - `aqui <ex81b.html>`__ para ver a 2a solução.
    - `aqui <ex81c.html>`__ para ver a 3a solução.
    - `aqui <ex81d.html>`__ para ver a 4a solução.
    - `aqui <ex81e.html>`__ para ver a 5a solução.
    - `aqui <ex81f.html>`__ para ver a 6a solução.

`Voltar <../08-reais.html#aula08_ex81_tentativa>`__
	
