..  shortname:: Aula 2
..  description:: Primeiros passos
      

.. Expressões aritméticas, relacionais e lógicas

.. Pensamentos lógico e aritmético

Introdução
----------

.. index:: expressão aritmética, expressão lógica, expressão aritmética


.. raw:: html

    <div align="right">
    <p></p>
    <blockquote>
    <p>
    "
    Não basta possuir uma mente boa:</br>
    o principal é usá-la bem.
    "
    </p>
    <p>René Descartes</p>
    </blockquote>
    <p></p>
    </div>

A lógica e aritmética são fundamentos para o pensamento computacional. 
Vamos iniciar esse capítulo com uma breve revisão de operadores aritméticas e suas propriedades e gostaríamos de chamar a sua atenção para que você observe como o computador resolve expressões aritméticas.

Cada um de nós pensa de uma forma diferente (ufa!). Por exemplo, sabemos que há várias maneiras distintas de calcular o resultado de uma expressão como (2+3)*4/2, sendo todas elas corretas no sentido de alcançar o mesmo resultado numérico. Por exemplo, podemos primeiro resolver a divisão 4/2 e depois a soma 2+3 que está entre parênteses, ou vice-versa, calculando primeiro a soma e depois a divisão.  

No caso de um computador, gostaríamos de frisar desde já que ele sempre realiza os cálculos da mesma maneira, de forma previsível e **determinística**, um operador de cada vez. Esse é um conceito importante pois a ordem de aplicação desses operadores altera o resultado e aprender a pensar nessa ordem nos ajuda a desenvolver um raciocínio lógico que vamos explorar na computação. Além de expressões aritméticas, vamos aprender também a escrever e prever os resultados de expressões lógicas e relacionais. 

.. admonition:: Spoiler Alert!

    O laboratório desse capítulo mostra como utilizar o iPython, um terminal que aceita comandos em Python. 
    Ao longo do capítulo, vamos usar a ferramenta CodeLens para mostrar o resultado de expressões usando a função ``print()`` do Python.



.. break

Para onde vamos
---------------

Nesse capítulo vamos entender como o Python calcula o resultado de expressões lógicas, relacionais e aritméticas para que você alinhe seu pensamento com essas regras e seja capaz de escrever expressões corretas em seus programas.
Ao final desse capítulo você deverá ser capaz de:

* Ler e calcular o resultado de expressões 
    - aritméticas;
    - relacionais; e
    - lógicas.
* Escrever expressões corretas usando:
    - operadores aritméticos;
    - operadores lógicos;
    - operadores relacionais; e
    - combinações desses operadores.
* Usar o iPython para testar expressões aritméticas, lógicas e relacionais.
              
.. break

Computador como Calculadora
---------------------------

Vimos no capítulo anterior que uma parte importante do funcionamento de um computador é a sua capacidade de realizar cálculos, semelhante a uma calculadora. O pensamento matemático que envolve a representação ou tipo de um elemento (como inteiro, real, complexo, etc) e as operações (ou aritmética) que definem como esses elementos podem ser combinados é um dos fundamentos do pensamento computacional. 

Assim, antes de escrever programas, vamos relembrar como essa "calculadora" funciona e treinar o seu uso até conseguirmos usá-la muito bem, ou seja, até que nosso raciocínio matemático esteja desenvolvido o bastante para que possamos prever o valor devolvido pelo computador para expressões complexas. 

..  Vamos ver também que o computador precisa trabalhar com tipos de dados além de números 
    e cada tipo de dado tem um conjunto distinto de operações que definem seu comportamento.


Uma regra básica é que o computador se comporta de forma **determinística**, ou seja, toda expressão calculada pelo computador é sempre resolvida da mesma maneira e a ordem das operações é previsível para que o computador sempre devolva o mesmo resultado.

Nesse capítulo vamos começar a utilizar outras ferramentas interativas do `projeto Runestone <https://runestone.academy/runestone/default/user/login?_next=/runestone/default/index>`__ que permitem você executar trechos de código em Python dentro do seu próprio navegador. O exemplo abaixo usa a ferramenta CodeLens e possui apenas uma linha de código em Python que usa a função ``print()`` para imprimir o resultado da expressão ``2 + 3``. 

Para executar essa função na linha 1 (indicada pela seta vermelha na coluna da esquerda) basta clicar no botão verde ``Next`` para que a linha seja executada. O resultado da expressão escrita entre os parênteses da função ``print()`` devem aparecer na coluna da direita onde se lê "Print output", que vamos chamar de quadro de saída ou, simplesmente, de saída. Você pode clicar e arrastar o canto inferior direito do quadro de saída para alterar seu tamanho. 

.. codelens:: cl02_exemplo_calculadora_01
    :showoutput:

    print( 2 + 3 )

Após clicar no botão ``Next``, o valor da expressão (no caso, ``5``) deve aparecer na saída. Observe que é o valor da expressão (``5``) e não a expressão em si (``2 + 3``) que é impresso na saída. Para imprimir a expressão também, podemos colocar a expressão entre aspas (") ou apóstrofes ('), e separar os objetos a serem impressos por vírgula como:

.. code-block:: Python

    print( '2 + 3 = ', 2 + 3 )


No exemplo a seguir incluímos uma segunda linha com a expressão ``2 + 3 * 4``.  
Novamente, ao clicar em ``Next`` uma primeira vez o valor ``5`` deve ser impresso na saída. 
Observe que a seta vermelha passa a apontar para a instrução seguinte.

Agora, **antes** de clicar uma segunda vez em ``Next``, procure pensar qual vai ser o valor impresso. 
Por exemplo, esse valor poderia ser ``20`` caso a soma ``2 + 3`` seja calculada antes da multiplicação. 
No entanto, felizmente o Python segue as regras de precedência dos operadores que nós estamos acostumados e o resultado na saída  é ``14``. Clique agora uma segunda vez e confira!

.. codelens:: cl02_exemplo_calculadora_02
    :showoutput:

    print( '2 + 3 = ', 2 + 3 )
    print( '2 + 3 * 4 = ', 2 + 3 * 4 )


Agora que você entendeu como usar o CodeLens com a função ``print()``, vamos revisar um pouco mais sobre expressões aritméticas em Python.

.. admonition:: Pense antes de clicar!

    A partir desse capítulo, procure criar o hábito de "pensar antes de clicar" e tente imaginar o resultado de cada instrução. Caso o resultado não seja o que você pensou, volte e procure entender por que você está "pensando" diferente do Python. 

    Preste muito atenção no código. Observe que as mensagens impressas pela ``print()`` são de inteira responsabilidade do programador.
    Por exemplo, qual o problema do seguinte comando:

    .. code-block:: Python

        print( '2 + 3 = ', 2 * 3 )

    O problema é que a mensagem está inconsistente, pois o resultado esperado para o primeiro termo ``'2 + 3 ='`` é diferente do segundo termo ``2 * 3``. Esses detalhes parecem óbvios ao serem apontados, mas encontrar esses erros pode ser uma tarefa árdua e frustrante pois muitas vezes passam despercebidos. A atenção a detalhes é uma habilidade que lhe será bem util nesse início de jornada.


..  Uma dificuldade inicial para se aprender uma língua desconhecida
    é perceber e diferenciar os sons, para depois perceber as palavras,
    antes de finalmente conseguir entender as frases.
    Em geral também o entendimento da leitura e escuta de uma língua 
    ocorre antes do domínio da escrita e da fala.
    Acreditamos que o aprendizado de uma linguagem de programação
    não seja muito diferente e, embora mais simples que uma linguagem
    natural, como o português, é necessário praticar muito a leitura para
    que consigamos desenvolver a habilidade de criar programas nessa nova língua.

..  Muitos programadores experientes conseguem escrever um programa
    diretamente em uma linguagem de programação. Ao final desse curso
    gostaríamos que você fosse capaz de escrever alguns programas
    simples em Python. Entretanto, diferente de aprender a ler e escrever em uma
    outra língua, um programa depende de uma solução que precisa ser *pensada* 
    antes do programa ser escrito. Isso porque um programa, em geral, resolve um
    determinado problema. Como cada problema pode ter várias soluções
    possíveis, é sempre recomendado que pensemos em algumas
    soluções distintas antes de escolhermos uma para ser implementada.

..  Embora soluções distintas possam levar a mesma solução correta, 
    cada solução pode ter desempenhos diferentes (ser mais ou menos rápida)
    e consumir recursos diferentes (como mais ou menos memória). 
    Nesse curso, mais que
    programar, vamos desenvolver *boas práticas* de programação
    que ajudam na construção de bons programas.


.. break

Expressões aritméticas
----------------------

.. index:: operador, operando, precedência de operadores, associatividade de operadores.

Uma expressão aritmética é formada por números (chamados de **operandos**) e **operadores** (como soma, multiplicação etc). 
Na sua forma mais simples, uma expressão contém apenas um número. 
Assim, o **valor de uma expressão** com apenas um número é o próprio número. É comum também chamarmos esse valor de resultado da expressão. 

.. Veja o que acontece quando digitamos apenas um número na linha de comando do Python shell:

Veja o que acontece quando mandamos o Python imprimir apenas um número:

.. codelens:: cl02_exemplo_calculadora_03

    print( 5 )
    print( -21 )

Quando uma expressão contém um operador como ``+`` (soma), a "calculadora" deve realizar 
a operação usando os operandos fornecidos como no exemplo:

.. codelens:: cl02_exemplo_calculador_04

    print( '5 + 2 = ', 5 + 2 )
    print( '-21 / 3 = ', -21 / 3 )

Podemos dizer que o Python reduz uma expressão a um valor (resultado da expressão). No caso de um número, não há o que ser reduzido, e o resultado é o próprio número. No caso de uma soma como ``5 + 2``, os dois números são somados e *reduzidos* ao valor ``7``. Como a calculadora (computador) só consegue reduzir um operador por vez, você saberia prever o resultado de uma expressão com vários operadores como ``2 + 3 * 4``? 

Teste o seu conhecimento
........................

Procure responder prevendo o resultado das expressões antes de testá-las no Python ou em uma calculadora.

.. mchoice:: mc02_questao_expressao_aritmetica_01
    :answer_a: -1
    :answer_b: -5
    :answer_c:  9
    :answer_d: 15
    :correct: c
    :feedback_a: Incorreto: existe um padrão para cálculo de expressões que é obedecido pelos computadores onde a multiplicação tem precedência sobre a adição e subtração.  
    :feedback_b: Incorreto: existe um padrão para cálculo de expressões que é obedecido pelos computadores onde a multiplicação tem precedência sobre a adição e subtração. 
    :feedback_c: Correto.
    :feedback_d: Incorreto: existe um padrão para cálculo de expressões que é obedecido pelos computadores onde a multiplicação tem precedência sobre a adição e subtração. 

    Qual o valor da expressão: ``2 + 3 * 4 - 5``


.. mchoice:: mc02_questao_expressao_aritmetica_02
    :answer_a: 15
    :answer_b:  9
    :answer_c: -5
    :answer_d: -1
    :correct: d
    :feedback_a: Incorreto: é necessário calcular a expressão entre parênteses primeiro.
    :feedback_b: Incorreto: é necessário calcular a expressão entre parênteses primeiro.
    :feedback_c: Incorreto: é necessário calcular a expressão entre parênteses primeiro.
    :feedback_d: Correto.

    Qual o valor da expressão: ``2 + 3 * (4 - 5)``


Para conferir esses resultados, 
clique no botão ``Salvar & Executar`` da ferramenta ActiveCode abaixo.

.. activecode:: ac02_teste_de_expressões_aritméticas

    print( 'questão 1: ', 2 + 3 * 4 - 5 )
    print( 'questão 2: ', 2 + 3 * (4 - 5) )

Esperamos que você tenha acertado essas questões pois o Python segue as mesmas 
regras para cálculo de expressões que você aprendeu nos cursos de matemática.

Observe que as expressões foram digitadas previamente para você no campo de edição do ActiveCode. Diferente do CodeLens, o ActiveCode permite que você edite o programa. Experimente modificar as expressões e tente prever os resultados, ou inclua outras linhas com prints de outras expressões.

Note ainda que você pode inclusive executar o seu programa no CodeLens, clicando no botão ``Mostrar em CodeLens``.


.. admonition:: Erros de sintaxe no ActiveCode

    Lembre-se que o Python é interpretado. Ao receber uma sequência de comandos (um programa) o Python tenta executar os comandos na sequência. Caso o Python não entenda um comando ou ocorra algum problema na execução (como divisão por zero), o Python para de executar e imprime uma mensagem de erro com alguma dica sobre o problema. Um tipo comum de erro é o erro de sintaxe (*SyntaxError*). Execute o código no ActiveCode abaixo e confira a mensagem de erro "SyntaxError: bad input on line 1". As mensagens são, infelizmente em inglês, mas em geral dá para ver que o erro ocorreou perto da linha 1. Você pode usar o `translate da Google <https://translate.google.com/>`_ para traduzir a mensagem para o português. Há vários problemas no código abaixo. 
    Procure corrigir um erro de cada vez e clique em ``Salvar & Executar`` para ver o comportamento do Python.

    .. activecode:: ac02_teste_de_expressões_aritméticas_com_erro

        print( '1: ', 2 + 3 * 4 - )
        print( '2: ', 2 +  * (4 - 5) )

        2 +

Assim, para prever o resultado de uma expressão com vários operadores, além de conhecer o que cada operador faz (como soma e multiplicação), é necessário conhecer as regras de **precedência dos operadores** e também sua **associatividade**. 

Exemplos para entender o que é precedência e associatividade
............................................................

As regras de precedência indicam qual operador é calculado primeiro.
Por exemplo, qual o resultado da expressão ``2 - 3 * 4``? 

Como a multiplicação tem maior prioridade que a subtração, o produto ``3 * 4`` é reduzido ao valor ``12`` e a seguir se calcula o valor da subtração ``2 - 12``, resultando em ``-10``. 

As regras de associatividade indicam a ordem dos cálculos para operadores que tenham a mesma precedência. 
Por exemplo, qual o resultado da expressão ``2 - 3 + 4``? 

Como a soma tem a mesma prioridade que a subtração, precisamos aplicar a regra de associatividade. 
Em Python, a maioria dos operadores binários (que usam dois operandos) tem associatividade "da esquerda para a direita" (ou seja, as operações são realizadas na mesma ordem de leitura). 
A expressão portanto é primeiramente reduzida a ``-1 + 4`` resolvendo a subtração e depois reduzida ao valor ``3`` resolvendo a soma. Observe que, caso a soma ``3 + 4`` fosse calculada primeiro, o resultado final seria ``-5 = 2 - (3 + 4)``.


A tabela a seguir mostra a associatividade das principais operações aritméticas em Python, 
em ordem decrescente de precedência (da maior para a menor):


.. table:: Tabela de precedência e associatividade de operadores aritméticos
    :align: left

    +-------------+--------------------------------------------------+-------------------------------+
    | Operador    |  descrição                                       | Associatividade               |
    +=============+==================================================+===============================+
    | ()          | parênteses                                       |  da esquerda para a direita   |
    +-------------+--------------------------------------------------+-------------------------------+
    | \*\*        | potência                                         |  da direita para a esquerda   |
    +-------------+--------------------------------------------------+-------------------------------+
    | +, -        | positivo e negativo unário                       |  da direita para a esquerda   |
    +-------------+--------------------------------------------------+-------------------------------+
    |\*, /, //, % | multiplicação , divisão, divisão inteira e resto |  da esquerda para a direita   |
    +-------------+--------------------------------------------------+-------------------------------+
    | +, -        | soma e subtração                                 |  da esquerda para a direita   |
    +-------------+--------------------------------------------------+-------------------------------+

Observações:

* Níveis de precedência:
    - A tabela mostra que há grupos de operadores com o mesmo nível, como soma e subtração.
    - Quanto mais "alto" o nível na tabela, maior a precedência.
 
* Operadores com mesmo nível de precedência são resolvidos segundo a sua associatividade.
    - exemplo: para reduzir a expressão ``12 / 2 / 3 / 4``, o Python calcula ``12/2``, e segue dividindo o resultado por 3 e depois por 4. 

* Os operadores unários (``+`` e ``-``) tornam explícitos o sinal do operando. 
    - experimente colocar uma sequência de operadores unários como ``-+-+3`` para ver o que acontece (será que resulta em erro de sintaxe?).

* Use parênteses caso deseje alterar a precedência ou torná-la explícita
    - exemplo: ``12 / 2 / 3 / 4`` => ``0.5``
    - exemplo: ``(12 / 2) / ( 3 / 4)`` => ``8.0``


Teste o seu conhecimento
........................

Qual o valor resultante das seguintes expressões:

.. mchoice:: questao_expressao_aritmetica_03
    :answer_a: 0
    :answer_b: 1
    :answer_c: 4 
    :answer_d: 2.4
    :answer_e: nenhuma das alternativas anteriores
    :correct: c
    :feedback_a: Incorreto: A expressão é calculada da esquerda para a direita como (12 * 2) % 10
    :feedback_b: Incorreto: A expressão é calculada da esquerda para a direita como (12 * 2) % 10
    :feedback_c: Resposta correta.
    :feedback_d: Incorreto. O operador `%` calcula o resto da divisão.
    :feedback_e: Incorreto, ao menos uma das alternativas anteriores está correta.

    Qual o valor da expressão: ``12 * 2 % 10``

.. mchoice:: questao_expressao_aritmetica_04
    :answer_a: 3
    :answer_b: 3.6
    :answer_c: 14 
    :answer_d: 14.4
    :answer_e: nenhuma das alternativas anteriores
    :correct: a
    :feedback_a: Resposta correta.
    :feedback_b: Incorreto: O operador `//` faz divisão inteira.
    :feedback_c: Incorreto: O operador `**` tem precedência maior que multiplicação e divisão.
    :feedback_d: Incorreto. O operador `//` faz divisão inteira e sua precedência é menor que `**`.
    :feedback_e: Incorreto, ao menos uma das alternativas anteriores está correta.

    Qual o valor da expressão: ``4 * 3 ** 2 // 10``

.. mchoice:: questao_expressao_aritmetica_05
    :answer_a: 512
    :answer_b: 256
    :answer_c: 128 
    :answer_d:  64
    :answer_e: nenhuma das alternativas anteriores
    :correct: a
    :feedback_a: Correto: como a associatividade de `**` é da direita para esquerda, o resultado é equivalente a 2 ** (3 ** 2) 
    :feedback_b: Incorreto: veja a associatividade de `**`
    :feedback_c: Incorreto: veja a associatividade de `**`
    :feedback_d: Incorreto: veja a associatividade de `**`
    :feedback_e: Incorreto, ao menos uma das alternativas anteriores está correta.

    Qual o valor da expressão: ``2 ** 3 ** 2``

.. mchoice:: questao_expressao_aritmetica_06
    :answer_a: é igual a (-2) ** 4
    :answer_b: é igual a -(2 ** 4)
    :answer_c:  8 
    :answer_d:  16
    :correct: b
    :feedback_a: Incorreto: verifique qual a precedência entre `**` e o `-` unário.
    :feedback_b: Correto: o operador `**` tem precedência.
    :feedback_c: Incorreto: verifique qual a precedência entre `**` e o `-` unário.
    :feedback_d: Incorreto: verifique qual a precedência entre `**` e o `-` unário.

    Qual o valor da expressão: ``-2 ** 4``


Use o ActiveCode abaixo para testar outras expressões.

.. activecode:: ac02_teste_de_outras_expressões_aritméticas

    # Essa linha é um comentário.
    # um comentário em Python começa com o caractere '#'
    # Comentários servem para serem lidos porleitores humanos 
    # mas são ignorados pelo Python.

    print(2 + 2)  #### comentário: deve ser 4


.. admonition:: O que comentar?

    É uma boa prática de computação incluir comentários no código fonte,
    para ajudar outros programadores que gostariam de reusar o código. 

    Desde o início, evite comentários redundantes como: 
    
    print( 2 + 2 ) # soma dois com dois

    Um bom uso de comentários é a descrição de blocos como:

    # Testes de soma
    print( 2 + 3 )
    print( 2 + 3 + 4 )

    # Testes de subtração
    print( 3 - 2 )
    print( -3 - 2 - 5 )

    Ao longo do livro, outras boas práticas de programação serão discutidas.


.. break

Tipos inteiro e real
--------------------

Até aqui vimos que o computador como calculadora obedece as regras de precedência e associatividade para calcular os resultados de expressões aritméticas até chegar em um valor que não dá mais para ser reduzido. Então,  qual o resultado da expressão ``5 / 2``?

Essa pergunta pode parecer óbvia a princípio, mas pare um pouco para pensar no tipo dos operandos ``5`` e ``2``.
Em Python, podemos usar a função ``type()`` para descobrir o tipo desses valores. Execute o programa para ver os tipos de ``5`` e ``2``.

.. codelens:: cl02_Usando_type

    print( 'Tipo de 5: ', type(5) )
    print( 'Tipo de 2: ', type(2) )


Observe que podemos concatenar funções em Python, como fazemos em matemática, como por exemplo "tangente( raiz_quadrada( 0.5 ) ) ". Assim, o resultado da função ``type()`` é impresso pela função ``print()`` na saída.

A mensagem ``<class 'int'>`` indica que ``5`` e ``2`` pertecem à classe dos números inteiros, onde `int` é uma abreviação de `integer` que significa inteiro em inglês. Ao invés de escrever ``<class 'int'>``, nesse livro vamos abreviar também e dizer que ``5`` é do tipo ``int``. 

Sabemos que o valor "verdadeiro" da expressão ``5 / 2`` é o real ``2.5`` (vamos usar um ponto ``.`` para representar números reais para seguir a mesma notação do Python). Por princípio, a linguagem Python procura calcular os valores da mesma forma como nós humanos o fazemos. Confira a seguir qual o valor de ``5 / 2`` segundo o Python:

.. codelens:: cl02_quanto_é_5_por_2

    print( '5 / 2 = ', 5 / 2 )

    print( 'Tipo de 2.5: ', type(2.5) )

Observe que ``2.5`` é da classe de números ``float`` (vamos dizer que é do tipo ``float``), 
onde a palavra ``float`` é uma abreviação de ``floating point`` (`ponto flutuante <ponto flutuante <https://pt.wikipedia.org/wiki/V%C3%ADrgula_flutuante>`_ em inglês), que corresponde a forma de representação de números reais em Python. Na verdade, como números reais podem possuir um número infinito de dígitos (como o número Pi = 3.141595426... ) e a memória de um computador é finita, um número do tipo ``float`` é na verdade um subconjunto dos números reais limitado a um certo número de bits (como 32, 64 ou 128 bits por exemplo). Essa limitação na representação de valores é, em geral, verdadeira na representação de qualquer coisa em um computador. Uma exceção em Python são os números inteiros. Veremos em capítulos futuros que o Python permite representar números inteiros arbitrariamente grandes. 

Vamos deixar de lado a forma de representação e trabalhar o pensamento matemático segundo as propriedades ``valor`` e ``tipo`` apenas. Assim, vamos dizer que o valor ``2.5`` é do tipo ``float``, e o valor ``2`` é do tipo ``int``. 

Muitas vezes é conveniente "forçar" que o resultado da divisão seja inteiro. 
Para isso, o Python oferece o operador de divisão inteira ``//`` (duas barras de divisão). 
Esse operador "trunca" o resultado, ou seja, mantem apenas a parte inteira e desfaz a parte fracionárias que vem após o ponto. 
Assim, enquanto o valor da expressão ``68/7`` é algo como ``9.714..``, o valor da expressão ``68//7`` é apenas ``9``. 
Outro operador bastante utilizado com inteiros é o operador resto da divisão ``%``. Por exemplo, sabemos que 25 dividido por 7 resulta no inteiro 3 com resto 4. Experimente escrever algumas expressões usando os operadores ``//`` e ``%`` usando o ActiveCode abaixo e tente prever o resultado antes de executar seu teste.

.. activecode:: ac02_testando_operadores_div_resto

    print( '25 // 7 = ', 25 // 7 )

    print( '25 % 7 = ', 25 % 7 )


Teste o seu conhecimento
........................
    

.. mchoice:: questao_expressao_aritmetica_07
    :answer_a: 2
    :answer_b: 2.2
    :answer_c: 2.5 
    :answer_d: 3
    :correct: b
    :feedback_a: Incorreto: não é o resultado da divisão "verdadeira", mas da divisão inteira.
    :feedback_b: Correto: o operador `/` resulta no float 2.2
    :feedback_c: Incorreto: quanto é 2.5 * 5?
    :feedback_d: Incorreto: quanto é 3 * 5?

    Qual o valor da expressão: ``11 / 5``


.. mchoice:: questao_expressao_aritmetica_08
    :answer_a: 2
    :answer_b: 2.2
    :answer_c: 2.5 
    :answer_d: 3
    :correct: a
    :feedback_a: Correto: resultado da divisão inteira.
    :feedback_b: Incorreto: o operador `//` faz divisão com resultado inteiro.
    :feedback_c: Incorreto: quanto é 2.5 * 5?
    :feedback_d: Incorreto: quanto é 3 * 5?

    Qual o valor da expressão: ``11 // 5``


.. mchoice:: questao_expressao_aritmetica_09
    :answer_a: 1
    :answer_b: 2
    :answer_c: 3
    :answer_d: 6
    :correct: a
    :feedback_a: Correto: resultado do resto da divisão 11 por 5
    :feedback_b: Incorreto: qual o resto da divisão de 11 por 5?
    :feedback_c: Incorreto: qual o resto da divisão de 11 por 5?
    :feedback_d: Incorreto: 6 é maior que 5. O resto é sempre menor que o divisor.

    Qual o valor da expressão: ``11 % 5``


.. break

Expressões relacionais
----------------------

.. index:: operadores relacionais

Além de "fazer contas" com números inteiros e reais, o Python permite comparar valores usando os seguintes **operadores relacionais**:

.. table:: Tabela dos operadores relacionais

    +----------+--------------------------------+---------------+---------------+
    | Operador |  Descrição                     | Exemplo       | Resultado     |
    +==========+================================+===============+===============+
    | ==       |        igualdade               | 2 == 3        | False         |
    +----------+--------------------------------+---------------+---------------+
    | !=       |        desigualdade            | 2 != 3        | True          |
    +----------+--------------------------------+---------------+---------------+
    | >        |           maior                | 3 > 3         | False         |
    +----------+--------------------------------+---------------+---------------+
    | >=       |           maior ou igual       | 3 >= 3        | True          |
    +----------+--------------------------------+---------------+---------------+
    | <        |           menor                | 2 < 3         | True          |
    +----------+--------------------------------+---------------+---------------+
    | <=       |           menor ou igual       | 4 <= 3        | False         |
    +----------+--------------------------------+---------------+---------------+

Operadores relacionais **comparam** dois valores e o resultado pode ser ``False`` (falso) ou ``True`` (verdadeiro). 
Esse dois valores são chamados de valores **booleanos** em homenagem ao matemático George Boole (`<https://pt.wikipedia.org/wiki/George_Boole>`_). 

Assim como dizemos que as expressões aritméticas são reduzidas a um valor numérico inteiro ou real, 
as expressões relacionais são reduzidas a um valor booleano (ou seja, ``True`` ou ``False``). 
As expressões relacionais podem conter expressões aritméticas, como no seguinte trecho no ActiveCode:

.. activecode:: ac02_exemplo_expressão_relacional_01

    print( 'expressão 1: ', 2 + 3 == 3 + 2 * 1 )

    print( 'tipo de type( 1 == 3 ): ', type( 1 == 3 ) )  
    # resultado esperado: <class 'bool'>

Esse exemplo mostra que o resultado da expressão ``2 + 3 == 3 + 2 * 1`` é o valor booleano ``True``, que pertence ao tipo `bool`, 
que é uma abreviação de ``boolean`` (booleano em inglês).

O resultado da expressão é ``True`` pois a precedência dos operadores relacionais é menor que a dos operadores aritméticos, ou seja, as operações aritméticas são reduzidas primeiro, que resulta na comparação ``5 == 5``, 
e depois a expressão relacional é reduzida (no caso o operador `==`), resultando em ``True``. 



Teste o seu conhecimento
........................
    

.. mchoice:: questao_expressao_relacional_01
    :answer_a: True
    :answer_b: False
    :answer_c: Erro
    :answer_d: 0
    :correct: b
    :feedback_a: Incorreto: compare o valor de 11/5 com 11//5
    :feedback_b: Correto: 11/5 é maior que 11//5
    :feedback_c: Incorreto: não há erro de sintaxe na expressão 
    :feedback_d: Incorreto: o resultado é um booleano

    Qual o valor da expressão: ``11 / 5 <= 11 // 5``


.. mchoice:: questao_expressao_relacional_02
    :answer_a: True
    :answer_b: False
    :answer_c: 1
    :answer_d: 0
    :correct: a
    :feedback_a: Correto: lembre-se que são valores comparados, que são iguais, apesar de tipos diferentes.
    :feedback_b: Incorreto: Observe que 2.000 é o ``float`` dois, não o inteiro dois mil.
    :feedback_c: Incorreto: o resultado é um booleano
    :feedback_d: Incorreto: o resultado é um booleano

    Qual o valor da expressão que compara um real com um inteiro: ``2.000 == 2``


.. break

Expressões lógicas
------------------

As expressões lógicas são construídas usando operadores lógicos sobre valores (operandos) booleanos.
A tabela a seguir mostra a precedência dos operadores lógicos usados em Python:

.. table:: Precedência dos operadores lógicos

    +----------+--------------------------------+----------------+---------------+
    | Operador |  Descrição                     | Exemplo        | Resultado     |
    +==========+================================+================+===============+
    | ``not``  |  negação lógica                | not True       | False         |
    +----------+--------------------------------+----------------+---------------+
    | ``and``  |  E  lógico                     | True and False | False         |
    +----------+--------------------------------+----------------+---------------+
    | ``or``   |  OR lógico                     | True or False  | True          |
    +----------+--------------------------------+----------------+---------------+

Expressões lógicas (ou booleanas) combinam valores booleanos com operadores lógicos.

O operador ``not`` troca o valor do operando, ou seja, troca o valor booleano de ``True`` para ``False`` e de ``False`` para ``True``.

O operador ``and`` devolve ``True`` apenas quando seus dois operandos são ``True`` e devolve ``False`` caso contrário, quando ao menos um dos operandos é ``False``, como mostra a tabela a seguir. 

.. table:: Tabela do operador lógico ``and``

    +------------+------------+-----------+
    | ``X and Y``| X = True   | X = False |
    +------------+------------+-----------+
    | Y = True   | True       | False     | 
    +------------+------------+-----------+
    | Y = False  | False      | False     | 
    +------------+------------+-----------+


Já o operador ``or`` devolve ``False`` apenas quando seus dois operandos são ``False`` e devolve ``True`` caso contrário (quando ao menos um dos operandos é ``True``. 

.. table:: Tabela verdade do operador lógico ``or``

    +------------+------------+-----------+
    | ``X or Y`` | X = True   | X = False |
    +------------+------------+-----------+
    | Y = True   | True       | True      | 
    +------------+------------+-----------+
    | Y = False  | True       | False     | 
    +------------+------------+-----------+

Como o resultado das comparações usando operadores relacionais é um booleano, os operadores lógicos podem ser utilizados para combinar os resultados relacionais. 
Por exemplo, considere ``x`` um valor real qualquer. A expressão ``x >= -1 and x <= 1`` pode ser usado para testar se x pertence ao intervalo [-1, 1]. 
Assim, para ``x = 0`` temos que a expressão é ``True`` pois as duas expressões relacionais se tornam verdadeiras. 
Mas quando ``x = 2``, a comparação ``x <= 1`` se torna falsa e portanto o resultado do operador ``and`` é ``False``. 

Para esse caso em particular o Python permite a notação ``a <= x <= b`` para verificar se o valor ``x`` está no intervalo [a, b]. 
Embora essa notação seja mais compacta e simples de entender, observe que, se cada operador relacional for reduzido um por vez, 
como ``(a <= x) <= b`` ou ``a <= (x <= b)``, os termos entre parênteses corresponderiam a valores booleanos que são incompatíveis 
com valores numéricos. Por exemplo, seja ``a=10``, ``b=20``, ``x=5`` e a expressão ``(a <= x) <= b``. 
Nesse caso, o valor entre parênteses seria ``False``, resultado de (10 <= 5), e não teríamos como resolver 
``False <= 20`` dado que são valores de tipos incompatíveis para comparação.

Observe que o uso de incógnitas como ``a``, ``b`` e ``x`` em expressões é muito útil. 
Em computação, elementos como esses são chamados de **variáveis** e são objeto do próximo capítulo.

.. break

Exercícios
----------

* **Exercício 1**: 
    Escreva uma expressão em função de uma incógnita `nota` que resulte em ``True`` caso `nota` esteja no intervalo aberto (3.0, 5.0), e resulte em ``False`` caso contrário. A expressão deve usar apenas operadores aritméticos, lógicos e/ou relacionais.

    .. admonition:: Dica

        A seguinte expressão em função de `nota` resulta em ``True`` para uma nota maior ou igual a 5

        .. code-block:: Python

            nota >= 5

* **Exercício 2**: 
    Um `ano bissexto <https://pt.wikipedia.org/wiki/Ano_bissexto>`_ ocorre aproximadamente a cada 4 anos. 
    Escreva uma expressão (em função de uma incógnita `ano`) que resulte em ``True`` caso `ano` seja bissexto e ``False`` caso contrário.
    Para ser bissexto, o valor de `ano` precisa ser múltiplo de 4, exceto múltiplos de 100 que não são múltiplos de 400.
    Assim o ano de 2020 é bissexto pois satisfaz todas essas condições. 

    A expressão deve usar apenas operadores aritméticos, lógicos e/ou relacionais.

    .. admonition:: Dica

        Use o operador `%` para verificar se `ano` é um múltiplo de algum número, como por exemplo
        
        .. code-block:: Python
        
            ano % 4 == 0 
         
        Se `ano=12`, a expressão ``12 % 4 == 0`` resulta em ``True`` pois como 12 é múltiplo de 4 o resto da divisão (``12%4``) é zero.

.. break

Laboratório: iPython
--------------------

Para realizar esse laboratório, você vai precisar de um computador com Python instalado. 
Vamos assumir que você tenha seguido corretamente e com sucesso as instruções de instalação do laboratório do capítulo anterior.

.. Também vamos assumir que você esteja usando um computador com Microsoft Windows. 
    Caso você use outro sistema operacional, 
    abra um terminal e chame o iPython a partir da linha de comando, de forma semelhante a que fazemos no Windows. 

Para abrir um terminal no Windows, 
clique na barra de pesquisa do Windows e digite ``Anaconda Prompt``. 
Dentre as primeiras opções deve estar a "Anaconda Prompt (Anaconda 3)".

Ao clicar na opção "Anaconda Prompt" o seu computador deve abrir uma janela que chamamos de terminal. 
No terminal, digite "ipython" para começar a executar o programa **iPython**.

O iPython é outro terminal (ou shell) que nos permite interagir diretamente com o interpretador Python, ou seja, podemos digitar comandos em Python diretamente na linha de comando do terminal que contém o **prompt** do iPython como ilustrado abaixo:

.. code-block:: Python

    In [1]: 

O número entre colchetes indica o número de comandos que você digitou até agora. Experimente digitar alguns dos programas que vimos nesse capítulo, como:


.. code-block:: Python

    In [1]: print( '2 + 3 * 4 = ', 2 + 3 * 4 )
    2 + 3 * 4 =  14

    In [2]: 

Observe que a função ``print()`` é executada, imprimindo a mensagem resultante na linha seguinte ao comando e, ao terminar de executar o comando, o terminal exibe um novo prompt, indicando que está pronto para receber outro comando. 

O iPython é muito útil para testar um comando quando você não tiver certeza de como ele funciona e também serve como uma **calculadora**.
Por exemplo, digite a expressão: ``2 + 2``, ou qualquer operação que desejar,  e em seguida tecle ``ENTER`` para ver o resultado.

.. code-block:: Python

    In [2]: 2 + 3 * 4
    Out[2]: 14

    In [3]: print(2 + 3 * 4)
    14

    In [4]:

Para entender o que está acontecendo,  
toda vez que você teclar ``ENTER`` você está enviando ao iPython um comando,
para que ele o interprete e execute. Ao executar uma expressão como na linha ``In [2]``, a expressão retorna o valor ``14`` e esses resultados podem ser reutilizados depois.

No caso da  ``print()`` na linha ``In [3]``, essa função imprime a mensagem ``14`` mas não tem um valor que pode ser reutilizado e, por isso, o iPython não mostra um ``Out[3]``. Vamos voltar a discutir esse comportamento após conhecer melhor outros recursos da linguagem Python. Por hora, vamos apenas nos limitar às expressões lógicas, relacionais e aritméticas que vimos nesse capítulo.

Experimente escrever expressões no iPython (lembrando de pensar antes de clicar) para treinar seus conhecimentos. 
Provavelmente você vai encontrar situações que pareçam ambíguas no início e, nesses casos, sugerimos que você utilize o iPython para verificar qual o comportamento do Python nessas situações. 

Experimente também criar expressões incorretas, como:

.. code-block:: Python

    In [4]: 2 + 3 * 4 %
      File "<ipython-input-10-31d0386dedc1>", line 1
        2 + 3 * 4 %
                  ^
    SyntaxError: invalid syntax
  
Nesse caso, ele tenta resolver a expressão até encontrar um erro ao tentar executar o operador ``%``. Observe que o iPython tenta indicar exatamente onde o erro de sintaxe aconteceu na linha que contém o símbolo '^'. 

O iPython é uma das ferramentas que estão integradas ao IDE Spyder, como veremos no próximo laboratório.


.. break

Onde chegamos e para onde vamos?
--------------------------------

Nesse capítulo vimos como o Python calcula o resultado de expressões aritméticas, relacionais e lógicas. Com o que vimos até aqui, você já pode usar o Python Shell como uma poderosa calculadora.  

Talvez por serem super aprendidas, alguns detalhes no processamento dessas expressões pode lhe passar desapercebidos. Na verdade esse conhecimento matemático é bastante sutil e pode ser até entediante, mas é um passo importante para desenvolver seu pensamento computacional a partir de fundamentos matemáticos sólidos, sempre lembrando que o computador faz apenas uma única operação de cada vez, até chegar em um valor que não dá mais para ser reduzido. 

Vimos que cada **valor** em Python possui um determinado **tipo**, que pode ser:

* tipo ``int`` para representar valores inteiros; 
* tipo ``float`` para representar valores reais;
* tipo ``bool`` para representar booleanos ``True`` ou ``False``.

O Python oferece ainda, de forma nativa, vários outros tipos de dados que serão cobertos nos próximos capítulos.

Cada tipo tem comportamentos que são definidos pelo conjunto de operadores associados ao seu tipo. 

Operadores aritméticos
......................

No caso de valores inteiros e reais temos as operações aritméticas. Vimos que é conveniente ter dois tipos diferentes de divisão, uma com resultado do tipo ``float`` (usando uma barra ``/``) e outra do tipo ``int`` (com duas barras ``//``). Outro operador bastante importante é o operador ``%`` que calcula o resto da divisão. 

Operadores relacionais
......................

Além de calcular o valor de expressões ao escrever programas como uma calculadora comum, ao escrever programas vamos precisamos também comparar valores. Isso é possível por meio dos operadores relacionais como ``==``, ``<``, ``>=`` etc, cuja precedência é convenientemente menor que os operadores aritméticos.
Saber ler e escrever expressões relacionais é um dos fundamentos que vamos precisar para começar a introduzir comandos básicos do Python para controlar a execução de programas.

Operadores lógicos
..................

Se os operadores relacionais permitem comparar valores, os operadores lógicos permitem combinar relações por meio dos operadores ``and``, ``or`` e ``not``. Veremos que o uso de expressões lógicas é um recurso bastante poderoso que pode simplificar e tornar seus programas mais legíveis, fáceis de entender e de encontrar erros.


.. Para saber mais
    ---------------
    * Leituras
        - `Capítulo 2: Variáveis, expressões e comandos <https://panda.ime.usp.br/pensepy/static/pensepy/02-Conceitos/conceitos.html>`__ do livro Como Pensar Como um Cientista da Computação.
        - Expressões em Python `<https://docs.python.org/3/reference/expressions.html>`_.
        - `Valores booleanos e expressões booleanas <https://python.ime.usp.br/pensepy/static/pensepy/06-Selecao/selecao.html>`__ do livro Como Pensar Como um Cientista da Computação. 
    * Vídeos
        - `Tipos booleanos e precedência de operadores <https://www.youtube.com/watch?v=sgfmuFRZuWs&list=PLcoJJSvnDgcKpOi_UeneTNTIVOigRQwcn&index=9>`__ 
        - `Expressões lógicas (bool) [Zumbis] <https://www.youtube.com/watch?v=d6XyTLkTYJo#t=41>`__.


