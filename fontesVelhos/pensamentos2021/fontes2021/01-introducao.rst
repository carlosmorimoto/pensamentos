.. shortname:: Introdução
.. description:: Introdução


Introdução
---------- 

.. raw:: html

    <div align="right">
    <p></p>
    <blockquote>
    <p>
    "Sessenta anos atrás eu sabia tudo. </br>
    Agora sei que nada sei. </br>
    A educação é a descoberta progressiva da nossa ignorância."
    </br>

    </p>
    <p><a href="https://pt.wikipedia.org/wiki/Will_Durant">Will Durant</a></p>
    </blockquote>
    <p></p>
    </div>

É possível que, se você está começando a ler esse livro, se pergunte:
para que eu preciso aprender a pensar com Python?

Certamente você já sabe "pensar" e "resolver" problemas, aliás muito
bem já que você escolheu seguir esse livro. No entanto, provavelmente
você deve ter pouca ou nenhuma experiência com computação e, em
particular, com programação de computadores. Se esse é o seu caso e
você deseja aprender um pouco de programação e computação, esse livro
foi escrito pensando em você. Mas se você já sabe programar em alguma
linguagem e quer aprender Python, provavelmente há formas melhores,
como a própria `documentação do Python <https://wiki.python.org/moin/BeginnersGuide>`__.

Portanto, esse não é um livro para aprender Python. Nosso objetivo é
mais fundamental, nós almejamos que, ao final do curso, você seja
capaz de resolver problemas computacionais simples, independente da
linguagem usada para programar suas soluções. Para isso, ao longo do
curso, vamos realizar vários exercícios para refinar algumas das suas
`habilidades cognitivas <https://pt.wikipedia.org/wiki/Habilidade_cognitiva>`__ até que você se sinta confortável na resolução
desses problemas.

Nesse livro vamos chamar essas habilidades de
"`pensamentos <https://pt.wikipedia.org/wiki/Pensamento>`__". Tipicamente, as melhores soluções
são resultado de um profundo conhecimento do problema e larga experiência com as
ferramentas usadas para construir as soluções, além, é claro, de um
pouco de criatividade. Veremos que, em muitos casos, nossas
soluções são incorretas por falta de conhecimento do problema e/ou uso incorreto das ferramentas.

Sabemos que, para quem nunca programou, perceber certos detalhes é
muito difícil e muitas vezes isso nós impede de entender um problema,
ou enxergar uma solução, ou ainda ver um problema na nossa tentativa de
solução.  Por isso, desde já, pedimos sua atenção e paciência para que
possamos retreinar sua habilidade de leitura de textos para leitura de
programas. Um dos primeiros fundamentos que vamos treinar é procurar
entender o que está escrito, como **leitores** "pensando" se o que
está escrito resolve o problema, para que, em seguida, possamos passar
ao papel de **escritores** ou criadores de soluções.

Esse livro também surgiu da possibilidade de criar um material
interativo, onde o texto fica intercalado com exercícios e questões
que você pode resolver dentro do próprio navegador. Vamos dar um
exemplo com o seguinte exercício de ordenação de palavras.

Considere as palavras ``"casa"``, ``"CASA"``, ``"cASa"`` e ``"CasA"``. 
Essas quatro palavras são exatamente as mesmas, correto?
ERRADO!
Cada uma delas tem uma combinação de letras maiúsculas e minúsculas e são consideradas **diferentes** pela linguagem Python.
Considere agora o problema de colocar essas palavras em ordem alfabética crescente.
Para isso, considere que as letras maiúsculas vem antes (tem precedência) das letras minúsculas. 

            
.. mchoice:: ordene_as_casas

   Ordene as palavras ``"casa"``, ``"CASA"``, ``"cASa"`` e ``"CasA"`` em ordem crescente, considerando que
   letras maiúsculas tem precedência sobre minúsculas. Dizer que maiúsculeas têm **precedência** sobre minúsculas significa que ``"A"`` deve aparece antes de ``"a"`` e que uma *palavra* como ``"aBa"`` deve aparecer antes de uma palavra como ``"aba"``.

   -    ``"casa"``, ``"CASA"``, ``"cASa"``, ``"CasA"``

        - ``'C'`` tem precedência sobre ``'c'``
         
   -    ``"casa"``, cASa, ``"Casa"``, ``"CASA"``

        - ``'C'``  tem precedência sobre ``'c'`` minúscula
       
   -    ``"CASA"``, ``"Casa"``, ``"casa"``, ``"casa"``

        + parabéns, correto!

   -    ``"CASA"``, ``"casa"``, ``"Casa"``, ``"casa"``

        - ``'C'`` tem precedência sobre ``'c'``



.. admonition:: Spoiler Alert!

    Além de questões de múltipla escolha, o livro está repleto de outros tipos de atividades interativas. Você poderá inclusive escrever e executar trechos de código em Python, de forma interativa, sem sair desse livro.

    Ao final de cada capítulo existe também uma seção com exercícios de **laboratório**. O objetivo dos laboratórios é que você aprenda a usar o Python de forma plena, fora das limitações impostas pelo navegador. Os laboratórios exploram outras ferramentas que facilitam o desenvolvimento de programas mais complexos.

.. figure:: ../Figuras/00-prefacio/Le_Penseur.jpg
    :width: 300
    :align: center

    `Wikipedia <https://en.wikipedia.org/wiki/The_Thinker/>`__: O Pensador no Jardim do Museu Rodin, Paris. 

..
   .. mchoice:: ordene_as_``"casa"``s
             :answer_a: ``"casa"``, ``"CASA"``, ``"casa"``, ``"Casa"``
             :answer_b: ``"casa"``, ``"casa"``, ``"Casa"``, ``"CASA"``
             :answer_c: ``"CASA"``, ``"Casa"``, ``"casa"``, ``"casa"``
             :answer_d: ``"CASA"``, ``"casa"``, ``"Casa"``, ``"casa"``
             :correct: c 
             :feedback_a: lembre-se que 'C' maiúscula tem precedência sobre 'c' minúscula
             :feedback_b: lembre-se que 'C' maiúscula tem precedência sobre 'c' minúscula
             :feedback_c: parabéns, correto!
             :feedback_d: lembre-se que 'C' maiúscula tem precedência sobre 'c' minúscula

             Ordene as palavras "``"casa"``", "``"CASA"``", "``"casa"``' e "``"Casa"``" em ordem cresente, considerando que letras maiúsculas tem precedência sobre minúsculas.

   
..


      ..  .. parsonsprob:: casas_ordenadas
          Coloque as palavras em ordem alfabética, arrastando-as para o quadro à direita.
          ----
          casa
          ====
          CASA
          ====
          CasA
          ====
          cASa

            
.. break

Para onde vamos
---------------

Nesse primeiro capítulo, vamos apresentar e discutir rapidamente alguns conceitos sobre computação e sua relação com computadores e linguagens de programação. 
Ao final desse capítulo você deve ser capaz de:

- explicar como funciona um computador baseado na arquitetura de von Neumann;
- descrever como dados e programas são armazenados na memória e definir o que é um bit, byte, kilobyte, megabyte etc.
- identificar linguagens de programação de baixo e de alto nível;
- descrever a diferença entre linguagens compiladas de linguagens interpretadas;   
- definir os conceitos de hardware, software, IDE, contexto de um programa, programação e computação.
- explicar o que é pensamento computacional e por que isso é diferente de programar.


.. break

Pensamento computacional
------------------------

É difícil imaginar hoje um ramo da atividade humana que não tenha se beneficiado diretamente dos avanços da computação. Seus impactos são tão notáveis que não precisamos ir longe para perceber seus efeitos.  A começar talvez pelos dispositivos móveis como os telefones celulares rodando inúmeros aplicativos que, para o bem ou para o mal, vem transformando comportamentos pessoais, sociais e até criando alguns novos comportamentos. Esse exemplo de computação móvel mostra que um dispositivo "comum" como um telefone, ao se tornar digital e conectado à Internet, pode se tornar coisas bem diferentes. Por ser programável, a flexibilidade e utilidade desses dispositivos aumenta a cada nova aplicação que instalamos nessa "coisa", ou seja, a coisa telefone vira outra coisa, como agenda, tv, ou vídeo game.

.. Uma outra tecnologia que começa a ser notada a nível pessoal é a Internet das Coisas ou IoT (do inglês *Internet of Things*). A IoT vem permitindo que "coisas" se tornem mais "inteligentes", desde uma lâmpada que pode ser controlada remotamente pela Internet, até a integração de "coisas" em toda uma cidade. Dessa forma, as cidades inteligentes poderão otimizar melhor seus recursos, como por exemplo melhoria no trânsito, maior conservação de energia e redução de poluentes. 

Assim, à medida que mais coisas se tornam digitais, conectadas à Internet e programáveis, abrem-se infinitas novas oportunidades de uso, tanto de coisas agindo individualmente, quanto coisas agindo em conjunto com outras coisas. Você provavelmente já faz uso de muitas dessas coisas. Aprender um pouco de computação pode até ajudar você a usar essas coisas de forma mais eficaz, mas o principal objetivo desse livro é que você será capaz de construir suas próprias soluções computacionais (coisas) para problemas simples. Para isso vamos orientar você na aquisição de conhecimentos e desenvolvimento de habilidades que vamos chamar de **pensamento computacional**. 


Mas o que é pensamento computacional?
.....................................

Tradicionalmente, cursos de introdução a computação tem grande foco no aprendizado de uma linguagem de programação. Nosso curso vai ser um pouco diferente no sentido de usar apenas recursos básicos da linguagem para resolver problemas computacionais inicialmente simples e que irão se tornando cada vez mais complexos. Nosso propósito é remover recursos "mágicos" oferecidos pelas linguagens modernas de programação para exercitar e desenvolver sua habilidade de resolver problemas usando o seu próprio raciocínio, ao invés, por exemplo, de buscar uma solução "pronta" na Internet.
Para isso nos concentraremos no desenvolvimento do `pensamento computacional <http://en.wikipedia.org/wiki/Computational_thinking>`_, que é um certo raciocínio aplicado na formulação e resolução de problemas computacionais.

A descrição da resolução de problemas depende de uma linguagem de programação. 
Como nosso foco não é na linguagem em si, poderíamos a princípio adotar qualquer linguagem para desenvolver o pensamento, mas 
adotamos a linguagem Python por ser moderna, portável, fácil de instalar, de sintaxe simples e ainda poderosa para lhe ser útil na resolução de seus próprios problemas computacionais.

A construção das habilidades cada vez mais complexas só se tornam sólidas quando apoiadas em outras habilidades sólidas e já bem fundamentadas. Por isso muitos exercícios que você verá nesse curso são para desenvolver ferramentas e recursos antes de aplicá-las, para exercitar as ideias, mesmo quando alguns desses recurso já estejam "prontos" e disponíveis na linguagem. Ao exercitar o pensamento para remover a mágica desses recursos, você ganha em conhecimento e competência.

Frequentemente é possível identificar alguns dos seguintes componentes nesse raciocínio ou pensamento:

* **decomposição**: quebrar um problema complexo em subproblemas que sejam idealmente mais simples;
* **reconhecimento de padrões**: buscar similaridades entre os subproblemas que permitam que estes sejam resolvidos de formas semelhantes;
* **representação dos dados**: determinar as características do problema necessárias para sua solução; e
* **algoritmos**: sequência de passos para resolver o problema.

Em particular, quebrar um problema em subproblemas mais simples e a procura de similaridades não são estratégias somente utilizadas em programação. Essas são rotinas aplicadas na solução diária de problemas de diversas origens e não são as únicas.
A criatividade de cada uma e cada um nos leva frequentemente a soluções muito engenhosas.

Não se trata de acertar ou errar.
Se trata de nos tornarmos melhores pensadores.
Para treinar nosso pensamento começaremos sempre da análise e compreensão de um problema.
Passaremos à construção de um roteiro para sua solução.
Esse roteiro ou *script* é o nosso programa que, ao ser executado em um computador, nos leva a uma
solução.


.. colocar figura? 
    ```
    +-----------+              +------------+             +----------+            
    |           |              |            |             |          |  
    | Problema  | -----------> | Computador | ----------> | Solução  |  
    |           |              |            |             |          |  
    +-----------+              +------------+             +----------+  
                                    ^                      
                                    |
                                    |
                            programa/roteiro/script
                                    ^ 
                                    |
                                    |
                                raciocício
                                    ^
                                    |
                                    |
                                    nóis
    programa = sequência de passos/ordens 
    Ato de programar é uma ferramenta útil
    ```


Na próxima seção vamos introduzir outros conceitos sobre computadores e computação para que possamos entender melhor a natureza dos problemas computacionais e das suas soluções.


.. Aprender a resolver problemas programar novos usos de essas coisas é certamente um desafio que, quem sabe, você mesmo vai ajudar a resolver usando algumas das habilidades que esse livro pode ajudar você a desenvolver.



.. Mas não precisamos ser tão ambiciosos. Para alunos e alunas de engenharia e ciências exatas por exemplo, as ferramentas computacionais cobertas nesse livro podem ajudar a entender conceitos de cálculo e estatística, visualizar dados multidimensionais e simular modelos. Alunas e alunos das áreas humanas e biológicas também podem utilizar as habilidades de pensamento computacional desenvolvidas nesse livro para começar a explorar seus dados e testar seus próprios modelos. Mas seja qual for a sua área de interesse, desenvolver um raciocínio computacional vai ajudar você a desenvolver melhores programas assim como pode ajudar você a utilizar programas existentes de forma mais eficaz.


.. break


Computadores e calculadoras
---------------------------

.. index:: computador, arquitetura de von Neumann, ULA, UCP

A habilidade de representar quantidades e manipulá-las (como "fazer conta" de mais e de menos) é uma necessidade básica para muitas atividades humanas. O ábaco e a régua de cálculo são exemplos de instrumentos inventados para nos ajudar a "fazer contas". 

A primeira calculadora mecânica foi desenvolvida por Blaise Pascal em 1642. A *Pascaline* (`<https://en.wikipedia.org/wiki/Pascal%27s_calculator>`_) utilizava um complexo sistema de engrenagens para realizar operações de adição e subtração.

Apesar dos computadores serem capazes de realizar operações matemáticas, os computadores são máquinas diferentes de calculadoras pois podem ser **programadas**. Os programas de computador permitem a repetição automática de cálculos muitas e muitas vezes, como por exemplo, calcular o pagamento de milhares de funcionários de uma companhia baseado no cargo e no número de horas trabalhadas por cada um.

Os primeiros computadores eletrônicos foram desenvolvidos na década de 1940 e o rápido e contínuo desenvolvimento tecnológico resultou nos computadores modernos, que tem desempenho muitas vezes superior a esses primeiros computadores, com tamanho e consumo de energia muitas vezes menor. Apesar de serem máquina complexas, entender como um computador funciona de forma genérica nos ajuda a usá-lo de forma mais eficaz e nos fornece um contexto mais claro para definir o que é computação. 

Vamos basear nossa explicação na arquitetura de um computador proposta por John von Neumann (`<https://en.wikipedia.org/wiki/Von_Neumann_architecture>`_), ilustrada na figura 1.1 Essa arquitetura é composta por uma unidade central de processamento (UCP), um bloco de "memória" para armazenar dados, e dispositivos de entrada e saída. Esses componentes são interligados por uma via de comunicação de dados. 

.. figure:: ../Figuras/cap01/vonNeumann.png
    :width: 800
    :align: center
    :alt: Figura 1.1: Arquitetura de von Neumann.
    
    Figura 1.1: Arquitetura de um computador proposta por `John von Neumann <https://en.wikipedia.org/wiki/Von_Neumann_architecture>`_.


Hoje em dia, a primeira coisa que vemos quando olhamos para um computador de mesa são os dispositivos de entrada e saída, como teclado, mouse e monitor, necessários para que o computador se comunique com outros computadores e pessoas. Dessa forma, os dispositivos de entrada permitem que o computador receba dados do usuário por meio do teclado e mouse (ou da Internet etc) e transmita os resultados para o monitor ou outros dispositivos de saída como impressoras, auto-falantes ou para a Internet.

Esses dados ficam armazenados na memória do computador em formato digital, ou seja, na forma de dígitos binários. Um **dígito binário** assume apenas 2 valores (como zero ou um) e é mais adequado para representar eletronicamente que um dígito decimal pois é possível utilizar uma chave eletrônica (aberta ou fechada) ou um capacitor (carregado ou descarregado). Um único dígito binário é chamado de *bit* e 8 (oito) bits formam um *byte*. A capacidade da memória é comumente representada em *kilobyte* ou KB (cerca de mil bytes), *megabytes* ou MB (cerca de um milhão de bytes), *gigabytes* ou GB (um bilhão) e assim por diante. 

Hoje é comum encontrar discos com mais de 1 *terabyte* (um trilhão de bytes) de capacidade. Para se ter uma ideia, um filme de cerca de 2 horas de duração gravado em alta resolução pode ser armazenado usando cerca de 2 GB no formato mp4, enquanto todos os textos contidos na Enciclopédia Britânica (`<https://websitebuilders.com/how-to/glossary/terabyte/>`_) no ano 2010 (último ano quando foi publicada com 32 volumes) pode ser armazenado em apenas 300 MB (300 milhões de caracteres ou cerca de 50 milhões de palavras). 

Além de dados, a memória do computador também armazena programas.
Um *programa de computador* é uma sequência de instruções que deve ser executada *uma de cada vez* pela UCP. Os programas também ficam armazenados na memória em código binário (também chamado de código de máquina ou em linguagem de máquina).
Uma instrução pode ser de natureza *aritmética* ou *lógica*, que basicamente permite ao computador executar operações matemáticas como uma calculadora, ou de *controle*.

As instruções de controle permitem, entre outras coisas, movimentar dados e desviar a execução do programa. A UCP contém um pouco de memória local (ou um grupo de registradores) para trabalhar os dados internamente. A evolução dos computadores tem resultado no tamanho dos dados processados em relação ao número de bits ou bytes que a UCP é capaz de processar de cada vez. As UCPs atuais tipicamente processam dados de 64 bits (ou 8 bytes). Assim, caso você precise instalar algum software para fazer os exercícios desse livro, provavelmente é seguro escolher a versão de 64 bits desse software. Computadores mais antigos (com 10 anos ou mais) processavam dados de 32 bits, os muito antigos processavam dados de 16 bits, e assim por diante. 

.. break

Programas, linguagens e mais pensamentos
----------------------------------------

Dizemos que os programas que ficam na memória de um computador para serem executados (portanto em formato binário) estão em código ou **linguagem de máquina**, por serem as linguagens "compreendidas" pelas máquinas.
Muito cedo na história dos computadores eletrônicos se descobriu que é muito difícil programar usando linguagem de máquina e assim foram surgindo várias linguagens de programação, cada uma com propostas diferentes para tratar certos problemas computacionais. 

De uma forma genérica porém, o desenvolvimento das linguagens de programação buscou se aproximar de alguma linguagem natural que usamos para nos comunicar, como o inglês e o português. Essas linguagens, chamadas de **linguagens de alto nível** por serem mais fáceis de usar por humanos, vem se tornando mais poderosas ao apresentar conjuntos de instruções mais extensos e recursos mais apropriados a aplicações modernas como computação gráfica, jogos eletrônicos e inteligência artificial. De forma similar, quanto mais próximas às linguagens de máquina, mais de **baixo nível** elas se tornam. 
 
Observe que cada máquina (ou microprocessador) possui um conjunto diferente de instruções que definem a sua própria "linguagem". 
Para que um programa escrito em uma linguagem de alto nível possa ser executado pela máquina (que só é capaz de executar código da máquina) o programa precisa ser traduzido por um programa chamado de **compilador**. 
Vamos chamar de **código fonte** o programa escrito pelos programadores e que fica armazenado em um **arquivo fonte**. 
Assim, um compilador recebe um arquivo fonte e gera um **arquivo executável** em linguagem de máquina. 
Programas escritos em linguagens compiladas tendem a ser bastante eficientes pois, após traduzidos, são executados em código nativo da máquina.

A figura 1.2 ilustra o processo de desenvolvimento de software usando linguagens de alto nível compiladas. O topo da figura mostra algumas habilidades que você vai desenvolver nesse curso. O **design** envolve o processo de criação e refinamento da solução, que requer um conhecimento profundo do problema. A **codificação** concretiza a solução na forma de um programa escrito na linguagem de alto nível. O programador usa um software de edição de textos para escrever e salvar o arquivo fonte. Esse arquivo é transformado em um arquivo executável usando o compilador. Finalmente, para **testar** se a solução resolve o problema, é comum usar um **terminal de entrada e saída** de texto (que pode ser chamado também de console ou shell). 

O terminal é usado para "interagir" com programas que não possuem interfaces gráficas que, infelizmente, é o caso dos programas que vamos escrever nesse curso. Nós vamos interagir com esses programas por meio de mensagens de texto, ou seja, nossos programas vão receber mensagens de texto que os usuários digitam usando o teclado (sempre digitando "Enter" ou "Return" para terminar a mensagem), e os programas podem também imprimir mensagens no terminal. As mensagens recebidas pelo terminal são basicamente comandos que ele tenta executar. Assim, para executar o programa após ser compilado, o programador deve digitar o nome do arquivo no terminal. O terminal carrega e passa a executar o programa. 
 
.. figure:: ../Figuras/cap01/compiladores.png
    :align: center
    :width: 800px
    :alt: Figura 1.2: desenvolvimento de software usando linguagens compiladas.

    Figura 1.2: Processo de desenvolvimento de software usando linguagens compiladas.


No entanto, a compilação de programas grandes e complexos pode ser um processo demorado. Uma alternativa é criar uma **máquina virtual** capaz de "entender" as instruções da linguagem de alto nível. Essa máquina virtual funciona como um tradutor instantâneo, um outro programa sendo executado pela máquina hospedeira. Diz-se que essas linguagens são **interpretadas** pois cada instrução é traduzida e executada pela máquina hospedeira. O Python é uma linguagem interpretada mas que apresenta um bom desempenho pois seu código é traduzido para a máquina virtual Python. É claro que essa tradução não deixa de ser uma compilação. No entanto, como a máquina virtual não roda código nativo, há sempre uma perda de desempenho se compararmos às linguagens compiladas para código nativo. 
Uma vantagem no caso do Python é que, por não serem compiladas (para código nativo ao menos), o desenvolvimento de programas se torna mais ágil devido a rapidez e facilidade para executar testes. 

A figura 1.3 ilustra a desenvolvimento de software usando linguagens interpretadas. Observe que não há mais um arquivo executável. As linguagens interpretadas modernas (como o Python) em geral usam um compilador "instantâneo" ou JIT ("*Just in Time*"), que transformam o código fonte em código "executável pela maquina virtual" a medida que o código vai sendo executado. Uma outra vantagem das linguagens interpretadas é que elas podem ficar "integradas" ao terminal e por isso o terminal foi aparece mais próximo ao programador. No caso do Python, vamos usar o `iPython <https://ipython.readthedocs.io/en/stable/>`_ para interagir e trocar mensagens diretamente com o Python, como se não houvesse um tradutor, o que agiliza o processo de testes.

.. figure:: ../Figuras/cap01/interpretadores.png
    :align: center
    :width: 800px
    :alt: Figura 1.3: desenvolvimento de software usando linguagens interpretadas.

    Figura 1.3: Processo de desenvolvimento de software usando linguagens interpretadas.


.. admonition:: Arquivos em Python

    No caso do Python, vamos usar a extensão '.py' para indicar os arquivos fonte em Python. Da primeira vez que esses arquivos são executados, o Python cria arquivos correspondentes com a extensão '.pyc'. Das próximas vezes que o programa for executado, o Python usa os arquivos '.pyc'. Mas, diferente de linguagens compiladas, mesmo que você deletar os arquivos '.pyc', os seus programas vão continuar sendo executados. 

A **portabilidade** de uma linguagem se reflete no número de compiladores ou interpretadores disponíveis, ou seja, quanto maior a portabilidade, maior será o número de máquinas onde um programa escrito na linguagem pode ser executado. O Python por exemplo é uma linguagem facilmente portável pois há máquinas virtuais para todos os principais sistemas operacionais modernos como Linux, Mac OS, Windows, Android, iOS etc.

Os programas muito simples em Python, como os primeiros programas que você vai escrever nesse curso, são chamados de **scripts**. 
Em geral, para desenvolver scripts, não precisamos de ferramentas muito poderosas. No entanto, o Python é uma linguagem poderosa que permite o desenvolvimento de softwares bem complexos. Para projetos maiores, usamos ferramentas mais poderosas chamadas de **IDE** (*Integrated Development Environment*) ou ambiente integrado de desenvolvimento de programas. Esses ambientes reúnem diversas ferramentas como ilustrado nas figuras 1.2 e 1.3, como editores de texto, terminais para teste dos programas, depuradores etc. Nesse curso, vamos adotar o IDE Spyder que acompanha o pacote Anaconda. O uso do Spyder é tratado no laboratório do capítulo 3 desse livro.


.. Além do tamanho dos dados, é importante também saber que cada dado é armazenado em um *endereço* numérico. De forma simplificada, vamos assumir que uma "instrução do programa" é um dado na memória, e essas instruções estão em endereços sequenciais. 

.. ideia do computador a papel

.. Para ilustrar como um computador baseado na arquitetura de von Neumann executa um programa, vamos fazer uma simulação passo-a-passo de um programa que soma uma sequência de números. Esse exemplo é baseado no `Computador à Papel <>`_ do Prof. Setzer. 

..    Exemplo de um programa
    .......................
    Vamos considerar que o computador tenha 16 posições de memória numeradas de 0 a 15. Vamos indicar cada posição usando dois algarismos precedidos pela letra "M", como `M00`, `M01`, até `M15`.
    Lembre-se em que cada posição podemos armazenar dados ou instruções do programa. 
    Vamos usar colchetes como em `[M03]` para indicar o **conteúdo** armazenado na posição `M03` da memória.  
    Um registrador importante da UCP que possibilita a execução da sequência correta de instruções é chamado de *contador de programa* (CP). O CP simplesmente armazena o local da memória (endereço) que contém a próxima instrução a ser executada. Dizemos que o CP "aponta" para a próxima instrução a ser executada.
    Dessa forma a unidade de controle da UCP "busca" a instrução indicada pelo CP na posição correspondente na memória, a decodifica e decide se a executa ou passa para a unidade lógica e aritmética (ULA) executar. Após terminar de executar a instrução, o CP é atualizado para que a unidade de controle busque a próxima instrução. O computador fica simplesmente executando esse ciclo:
    --> busca -> executa -> atualiza CP -> busca -> ...
    ininterruptamente. Apesar de cada instrução ser relativamente simples, um computador pessoal moderno é capaz de executar bilhões de instruções por segundo, que lhe confere o poder de realizar tarefas bastante complexas.
    .. Esse poder tem permitido que computadores substituam pessoas em tarefas repetitivas e entediantes, permitindo que as pessoas tenham mais tempo livre para as atividades criativas.
    Para simplificar nossa notação, vamos assumir que a UCP tem um registrador `AC` (de acumulador) que é utilizado para realizar cálculos. Vamos indicar o conteúdo do acumulador usando colchetes também, como em `[AC]`.
    A tabela abaixo mostra um programa (lista de instruções) armazenadas nas posições de memória de `M00` a `M15`.
    | end   | conteúdo   | abreviatura |
    | :--- | :--- | :--- |
    | `00`    | **carregue** 0 no `AC`                     | `AC  <-- 0` | 
    | `01`    | **armazene** o `[AC]` no `M15`             | `M15 <-- [AC]` | 
    | `02`    | **leia** um número e armazena no `M14`     | `M14 <-- leia` | 
    | `03`    | **escreva** `[M14]`                        | **escreva**  `[M14]` |
    | `04`    | **carregue** no `AC` o `[M14]`             | `AC <-- [M14]` |
    | `05`    | **desvie** para `M12` se `[AC]=0`          | **desvie** para `M12` se `[AC]=0` |
    | `06`    | **carregue** no `AC` o [M15]               | `AC <-- [M15]` |
    | `07`    | **some** `[AC]` ao `[M4]` e                | `AC <-- [AC] + [M]` |
    | `07`    | **some** `[AC]` ao `[M5]` e                | `AC <-- [AC] * [M]` |
    |         | **armazene** resultado no `AC`             |                          |
    | `08`    | **armazene** `[AC]` no `M15`               | `M15 <-- [AC]` |  
    | `09`    | **leia** um número e armazene no `M14`     | `M14 <--` **leia** |
    | `10`    | **escreva** `[M14]`                        | **escreva** `[M14]` |
    | `11`    | **desvie** para o `M04`                    | **desvie** para o `M04` | 
    | `12`    | **escreva** `[M15]`                        | **escreva** `[M15]` |
    | `13`    | **pare**                                   | **pare** |
    | `14`    | `??????`                                   |          |
    | `15`    | `??????`                                   |          |

..    Simulação do Programa
    ......................
    Vamos tentar entender o que esse programa faz, simulando o seu comportamento para uma sequência de números como 2, 3, 4, 5 e 0.
    Ao iniciar a execução, considere que o registrador CP esteja apontando para `M00`. O computador busca a instrução na memória e ao decodificá-la, identifica que a instrução corresponde a "carregue 0 no AC". A coluna da direita é uma abreviação onde o sentido da seta `<--` indica que o valor 0 (zero) está sendo movido para `AC`. 
    Após executar essa instrução, o conteúdo de AC deve ser zero e CP é atualizado para apontar para `M01`. 

.. break

Um pouco mais sobre design
--------------------------

A palavra **design** é usada (abusada?) por diferentes áreas do conhecimento. Nesse livro, vamos usar **design** para indicar o processo que envolve um pouco de arte, de ciência, de técnica, de experiência, de criatividade, de exploração de alternativas etc, 
na busca de uma solução algorítmica para um problema computacional.

O processo de design se inicia pelo entendimento do problema. Muitos dos problemas que veremos nesse curso possuem uma entrada e saída bem definidos. Isso facilita o design pois isso nos permite focar em processos que transformam os dados de entrada na saída.
Em geral, há vários processos distintos possíveis que levam ao resultado correto. Ao longo do curso vamos trabalhar com o conceito de eficiência computacional para medir o uso de recursos (como tempo e memória) necessários para executar cada algoritmo.

A escolha e definição do algoritmo a ser implementado (codificado usando uma linguagem de programação) é o objetivo do design. 
Por enquanto, vamos usar a palavra **algoritmo** para indicar uma sequência lógica das etapas que conduzem ao resultado correto. 
Os algoritmos são tipicamente hierárquicos, ou seja, cada etapa pode ser descrita com um nível de detalhes diferente. 

Uma forma comum para desenvolver um algoritmo é conhecido como *top-down* (de cima para baixo). 
Para pensar de forma top-down começamos desenhando a solução usando "módulos grandes" e vamos decompondo cada "módulo" até ficar claro que sabemos codificar aquele módulo. 

Algumas vezes é possível chegar na solução de forma *bottom-up* (de baixo para cima). Costumamos pensar desse jeito quando sabemos resolver certos pedaços e queremos usar esses pedaços na solução. Ao usar essa forma de pensamento, tome muito cuidado para que esse desejo não se torne obsessão pois, muitas vezes, forçar o uso de algum pedaço pode conduzi-lo a um caminho errado.

Um outro problema comum que encontramos em programadores menos experientes mas que acontece com programadores experientes também é a empolgação por um certo design que foge do problema. Talvez porque algumas vezes a gente se apaixona por certas ideias, seja pela beleza, elegância, criatividade etc, que torna difícil abandoná-las. Por isso os testes são fundamentais para nos manter com os pés no chão e no caminho correto. 

Para você que é programador iniciante no entanto, o maior desafio está na codificação. Para vencer esse desafio vamos codificar bastante para que a sintaxe da linguagem (Python) se torne natural, e vamos também fazer exercícios usando as ferramentas de programação como o editor, o terminal, o depurador etc, que constituem o IDE. 

.. break 

Computador x computação
-----------------------

.. index:: hardware, software, linguagem de máquina, linguagem de baixo nível, linguagem de alto nível, código fonte


.. raw:: latex

    \begin{flushright}
    {
    "O computador está para a computação assim como o telescópio está para a astronomia." \\
    \href{https://en.wikiquote.org/wiki/Computer\_science}{Edsger Dijkstra} \\
    }
    \end{flushright}
    \vspace{10mm}


.. raw:: html

    <div align="right">
    <p></p>
    <blockquote>
    <p>"O computador está para a computação assim como o telescópio está para a astronomia."</p>
    <p><a href="https://en.wikiquote.org/wiki/Computer_science">Edsger Dijkstra</a></p>
    </blockquote>
    <p></p>
    </div>


Um computador é uma máquina que precisa de hardware e software para funcionar. Chamamos de **hardware** a parte física dos computadores. A parte digital responsável pelo funcionamento da parte física (os programas de uma forma geral) é chamada de **software**. O objetivo de programar é criar software para resolver algum problema computacional, que pode ser um simples cálculo matemático ou algo um pouco mais complexo como controlar a navegação de um foguete até a Lua. 
A atividade de desenvolver esses programas é chamada de **programação**. 

Como vimos, tradicionalmente os cursos de introdução à computação tem um foco grande em alguma linguagem de programação. 
Mas a (ciência da) computação vai além da programação. Como ciência, ela estuda os fundamentos teóricos da informação (dados) e da computação (transformação dos dados) e as considerações práticas para aplicar essa teoria em sistemas computacionais. Por exemplo, nesse curso vamos estudar formas para representar diferentes tipos de dados e algoritmos eficazes para manipular esses dados. Consideramos o estudo dessas estruturas e algoritmos uma excelente forma de desenvolver um raciocínio computacional, algo parecido com estudar partidas entre grandes mestres para aprender a jogar xadrez. 

.. break

Laboratório: Como instalar Python em seu computador
---------------------------------------------------

O desenvolvimento do raciocínio computacional requer muita
prática. Para isso, cada capítulo traz uma seção de exercícios em
laboratório que você deve realizar utilizando um computador para
praticar.  Nesse primeiro capítulo, vamos ajudar você a arrumar o seu
ambiente de programação e testá-lo, para que você possa fazer os
exercícios de laboratório dos próximos capítulos.  A instalação também
é um bom exercício inicial para as alunas e alunos sem prévia
experiência com computadores e serve para que eles e elas comecem a se
habituar com o uso de computadores e das ferramentas de
desenvolvimento de programas que serão utilizadas nos exercícios.


Instalando o Python
...................

.. index:: Anaconda

Vamos utilizar a distribuição `Anaconda <https://www.anaconda.com/>`__ (`Wikipedia <https://en.wikipedia.org/wiki/Anaconda_(Python_distribution)>`__)
que reúne várias ferramentas úteis para programação científica e ciência de dados.
Como historicamente a maioria de nossas alunas e alunos costumam utilizar um computador com o sistema operacional Windows,
as instruções a seguir são voltadas para essa plataforma.
Caso você use um outro sistema operacional, como Linux ou MacOS,
você pode seguir as instruções específicas para o seu sistema na
`página de instalação da Anaconda  <https://docs.anaconda.com/anaconda/install/>`__.

.. figure:: ../Figuras/anaconda/anaconda_pagina_inicial.png
   :width: 600
   :align: center
   :name: org
          
   Página inicial da distribuição Anaconda.

Antes de instalar, é preciso baixar os programas da distribuição `
Anaconda <https://en.wikipedia.org/wiki/Anaconda_(Python_distribution)>`__.
Para isso, no seu navegador favorito, Firefox, Chrome, Edge, ...,  siga para a página de *download* da Anaconda `<https://www.anaconda.com/distribution>`_).
Dependendo do seu navegador ou da versão do seu navegados, as mensagens apresentadas podem ser diferentes das mostradas nas imagens a seguir.
Sugerimos ainda que você salve o arquivo para
evitar fazer o download novamente caso algo de errado aconteça durante
a instalação ``;-)``.


Inicialmente clique no botão ``Download`` para baixar o *programa instalador* da distribuição da Anaconda,
que contém o ``Python`` e outros programas que serão úteis no futuro.

.. figure:: ../Figuras/anaconda/anaconda_inicio.png
   :width: 600
   :align: center
   :name: comsalvar
          
   Clique no botão ``Download``.
            
Em seguida, selecione a distribuição para o seu sistema, Windows, MacOS ou Linux.
No nosso exemplo selecionamos Windows.

.. figure:: ../Figuras/anaconda/anaconda_sistemas.png
   :width: 600
   :align: center

   Em Windows clique em ``64-Bit Graphical Installer (457 MB)``.

O tempo gasto para baixar o instalador junto com os demais programas da Anaconda depende da velocidade da sua conexão e pode demorar alguns minutos.
Vá tomar um café.
Em geral, os navegadores apresentam quanto já foi baixado ou uma estimativa do tempo que resta para finalizar o processo.
A imagem a seguir mostra o início do processo no Chrome. no canto inferior esquerdo mostra que foram baixados ``2.7 MB`` de ``457 MB``.

.. figure:: ../Figuras/anaconda/anaconda_downloading.png
   :width: 600
   :align: center

   Arquivo de instalação ``anaconda3-2020.11-Windows-x86_64.exe`` sendo baixado pelo Chrome.

Depois que terminar de baixar, clique no arquivo que foi baixado para executar que o instalador comece a trabalhar.
A imagem a seguir mostra a janela de execução exibida pelo Firefox.

.. figure:: ../Figuras/anaconda/anaconda_mozilla.png
   :width: 400
   :align: center

   Janela de execução do ``anaconda3-2020.11-Windows-x86_64.exe`` no Firefox.


Deste ponto você passará a se comunicar com o programa instalador.
Siga as opções sugeridas pelo instalador.

Na janela de boas-bindas apresentada pelo instalador, clique em ``Next``.

.. figure:: ../Figuras/anaconda/anaconda_boas_vindas.png
   :width: 400
   :align: center

   Boas-vindas do instalador.


Para prosseguir é necessário que estejamos de acordo com os termos da  licença da Anaconda,
clique em ``I agree``.


.. figure:: ../Figuras/anaconda/anaconda_licenca.png
   :width: 400
   :align: center

   Licença da Anaconda.

 
Agora chegou o momento de selecionarmos o tipo da instalação.
Siga a recomendação do instalador para instalar apenas para vocês, selecione ``Just me (recommended)`` e clique em ``Next``.

   
.. figure:: ../Figuras/anaconda/anaconda_tipos_instalacao.png
   :width: 400
   :align: center

   Tipos de instalação.        

Desta vez o instalador está recomendando o nome da pasta em que a Anaconda será instalada.
Sugerimos que você aceite a recomendação do instalador e clique em ``Next``.
Para selecionar uma outra localização, clique em ``Browser``, navegue pelo seu computador a procura da pasta destino. 
Depois de escolher o nome da pasta, clique em ``Next``.

.. figure:: ../Figuras/anaconda/anaconda_destino.png
   :width: 400
   :align: center

   Pasta em que a Anaconda será instalada.        


A recomendação agora é que o ``Python 3.8`` da Anaconda ser o ``Python`` usado pelo seu computador.
Clique em ``Intall`` para que a instalação ser iniciada. 

.. figure:: ../Figuras/anaconda/anaconda_opcoes.png
   :width: 400
   :align: center

   Opções avançadas.        

A instalação pode demorar alguns minutos. Vá tomar outro café.
Ao final, quando a instalação o processo de instalação tiver sido completado, o instalador apresentará a janela abaixo.
Clique em ``Next``.

.. figure:: ../Figuras/anaconda/anaconda_complete.png
   :width: 400
   :align: center

   Janela mostrando que a instalação está completa.

Pronto!
O instalador fará mais algumas recomendações de programas a serem instalações.
Você pode continuar a clicar em ``Next`` até fechar o instalador da Anaconda.

.. figure:: ../Figuras/anaconda/anaconda_jupyter.png
   :width: 400
   :align: center

   Mais recomendações do instalador.
   
O instalador agradece, dá mais alguma dicas de informações para serem examinadas mais tarde.
Clique em ``Finish`` para terminar.
Ufa!

.. figure:: ../Figuras/anaconda/anaconda_finish.png
   :width: 400
   :align: center

   Mais dicas do instalador.


Teste a sua instalação usando o Anaconda Prompt e o IPython
...........................................................

.. index:: Anaconda prompt, terminal, IPython

Clique na barra de pesquisa do Windows e digite ``Anaconda Prompt``. Dentre as primeiras opções deve estar o 
``Anaconda Prompt (Anaconda 3)`` que você acabou de instalar, como é mostrado na imagem abaixo.

.. figure:: ../Figuras/anaconda/anaconda.pesquisa.png
   :width: 600
   :align: center

   Procura pelo ``Anaconda prompt``        
           
           
Clique nessa opção ``Anaconda Prompt`` para abrir uma janela 
como a mostrada na imagem abaixo.
Essa janela é comumente chamada de *terminal* ou *shell* da Anaconda.

.. figure:: ../Figuras/anaconda/anaconda_shell.png
   :width: 600
   :align: center

   Terminal ou shell da Anaconda.
   
No terminal, digite ``ipython`` para abrir o `IPython <https://ipython.readthedocs.io/en/stable/>`_ (*Interactive Python interpreter*).
O ``IPython``  é um outro  terminal ou shell que permite você interativamente execute comandos em ``Python``.

.. figure:: ../Figuras/anaconda/anaconda_ipython_prompt.png
   :width: 600
   :align: center

   Terminal ou shell do ``IPython`` logo após ter sido iniciado.

Após iniciar o ``IPython`` o programa apresentará, além de outras informação, o seu ``prompt`` deixando claro que está preparado ou *pronto* para
receber comandos a serem interpretados.
Na imagem acima o ``prompt`` é o texto ``In [1]:``.
A palavra *interativamente* significa para cada comando digitado o ``IPython`` apresentará imediatamente uma resposta.
O ``IPython`` é muito útil para testar o comportamento de comandos e também serve como uma calculadora.
Por exemplo, digitando a expressão ``2 + 2`` ou qualquer operação aritmética que desejar, seguida por ``ENTER`` o resultado é o apresentado na próxima imagem.

.. figure:: ../Figuras/anaconda/anaconda_ipython_soma.png
   :width: 600
   :align: center

   Resultado da operação ``2 + 2`` no ``IPython``.

Após mostrar a resposta ``Out [1]: 4`` para ``In [1]: 2 + 2``, o ``IPython`` exibe novamente o seu ``prompt``,  avisando que está preparado para
a próxima tarefa. Como essa será a segunda tarefa desta sessão, o novo ``prompt`` que o ``IPython`` exibirá é ``In [2]:``.
É esse o comportamento de um terminal ou um shell.
Toda vez que você digita algo após o ``prompt`` do ``IPython`` e em seguida tecla ``ENTER``,
o novo comando será  interpretado e a resposta ou resultado do comando será apresentada.

Se você enviar um comado incorreto ou incompleto, como ``2 *`` seguido de ``ENTER``, o
``IPython`` como todo bom terminal, exibirá uma mensagem avisando do erro.

.. figure:: ../Figuras/anaconda/anaconda_ipython_erro.png
   :width: 600
   :align: center

   Erro resultante de ``2 *`` no ``IPython``.


No caso a mensagem é ``SyntaxError: invalid sintax``, que  indica que o comando foi escrito de forma errada e que, portanto,
não foi compreendido. 
.. Note que é importante você não inserir espaços em branco ou tabs no início da linha para evitar erros de sintaxe (veremos mais tarde a razão).
Para interpretar os comandos, o  ``IPython`` é diretamente auxiliado pelo ``Python``.
O ``IPython`` trabalha, digamos, como um garçon que leva os pedidos feitos pelos clientes para que a chefe de cozinha ``Python`` prepare os pratos.
Se a chefe ``Python`` não entende algum pedido, diz isso ao garçon ``IPython`` que por sua vez avisará o cliente sobre o pedido incompreensível.

Exploraremos muito mais o ``IPython`` e o seu modo *interativo* como uma *calculadora*  no laboratório do próximo capítulo.



.. break

Teste seus conhecimentos
------------------------

Indique todas as alternativas corretas

.. mchoice:: questao_dois_kilobytes
    :answer_a: aproximadamente dois mil bits
    :answer_b: aproximadamente quatro mil bits
    :answer_c: aproximadamente oito mil bits
    :answer_d: aproximadamente dezesseis mil bits
    :answer_e: nenhuma das alternativas anteriores 
    :correct: d
    :feedback_a: Incorreto: 1 byte possui 8 bits
    :feedback_b: Incorreto: 1 byte possui 8 bits
    :feedback_c: Incorreto: 1 byte possui 8 bits
    :feedback_d: Correto: 1 byte possui 8 bits
    :feedback_e: Incorreto: 1 byte possui 8 bits

    Quantos bits de informação podemos armazenar em 2 KB (dois kilobytes)?


.. mchoice:: questao_nivel_linguagem_python
    :answer_a: uma linguagem de programação de baixo nível
    :answer_b: uma linguagem de programação de alto nível
    :answer_c: uma linguagem de máquina
    :answer_d: uma linguagem de programação interpretada
    :correct: b, d
    :feedback_a: Incorreto: uma linguagem de baixo nível usa código que roda diretamente (ou quase) na máquina.
    :feedback_b: Correto: um programa em Python parece um texto em inglês.
    :feedback_c: Incorreto: os computadores não conseguem executar diretamente programas em Python. 
    :feedback_d: Correto: um programa em Python não é compilado para linguagem da máquina hospedeira.

    O Python é:


.. mchoice:: questao_python_shell
    :answer_a: uma interface que permite escrever comandos do Python e ver o resultado.
    :answer_b: um ambiente integrado de desenvolvimento de programas em Python.
    :answer_c: uma ferramenta que permite a execução de comandos em Python e pode ser integrada com outras ferramentas.
    :answer_d: uma casca para proteger o Python conta ataques.
    :answer_e: uma concha onde o Python fica armazenado.
    :correct: a, c
    :feedback_a: Correto
    :feedback_b: Incorreto: o Python shell não é um IDE.
    :feedback_c: Correto: como ocorre no Spyder
    :feedback_d: Incorreto: o shell se refere a uma interface por linha de comando.
    :feedback_e: Incorreto: o shell se refere a uma interface por linha de comando.

    O IPython é:



.. break

Onde chegamos e para onde vamos?
--------------------------------

Esse capítulo introduziu alguns conceitos básicos de computação sob um brevíssimo contexto histórico.
Nesse contexto, o Python é uma linguagem de programação de alto nível altamente portável (roda praticamente em qualquer computador, até mesmo em telefones celulares) e flexível (pode ser usada para diversos tipos de aplicação). O Anaconda fornece um pacote com várias ferramentas integradas, facilitando a instalação e manutenção do ambiente de desenvolvimento. 

Nos próximos capítulos vamos utilizar o IPython para ilustrar alguns comandos do Python e o IDE Spyder para escrever programas em Python. 





.. Referências

.. Para saber mais
    ---------------
    * `Como Pensar Como um Cientista da Computação - Aprendendo com Python: Versão Interativa <https://panda.ime.usp.br/pensepy/static/pensepy/index.html>`_, tradução do livro interativo produzido no projeto `Runestone Interactive <http://runestoneinteractive.org/>`_ por Brad Miller and David Ranum. 
    * [versão interativa em inglês]  `How to Think Like a Computer Scientist: Interactive Edition <https://runestone.academy/runestone/books/published/thinkcspy/index.html>`_, livro interativo produzido no projeto `Runestone Interactive <http://runestoneinteractive.org/>`_ por Brad Miller and David Ranum. 
    * [versão original em inglês] `How to Think Like a Computer Scientist: Learning with Python <https://open.umn.edu/opentextbooks/textbooks/how-to-think-like-a-computer-scientist-learning-with-python>`_, por Allen Downey, Franklin W. Olin, Jeff Elkner, and Chris Meyers.
    * Vídeos
        - `Introdução à Computação com Python <https://www.coursera.org/learn/ciencia-computacao-python-conceitos>`_ do professor Fábio Kon do Departamento de Computação do IME-USP no Coursera.        
        - `Python para Zumbis <https://www.pycursos.com/python-para-zumbis/>`_ do professor Fernando Masanori da FATEC de São José dos Campos. 
    * sobre Computação e computadores:
        - `Breve história da computação <http://www.ime.usp.br/~macmulti/historico/>`_
        - `O que é um Computador <http://pt.wikipedia.org/wiki/Computador>`_


..  Glossário
    ---------
    - Calculadora: máquina que apenas realiza cálculos, sem ou com uma capacidade limitada de programação.
    - Computador: 
    - Hardware:
    - Software:
    - Número binário:
    - bit:
    - byte:
    - ULA: unidade lógica e aritmética.
    - UCP: unidade central de processamento.
    - memória:
    - programa:
    - computação:
    - linguagem de máquina
    - linguagem de alto nível
    - linguagem de baixo nível
    - linguagem de montagem


