..  shortname:: Topico Listas
..  description:: Listas

Lista: estrutura sequencial indexada
====================================
 
.. index:: lista, tipo list, fatia de lista, concatenação, estrutura indexada,
	   índice, apelido, clone de lista, len()

Tópicos
-------

    - `Listas <https://python.ime.usp.br/pensepy/static/pensepy/09-Listas/listas.html>`__

        - `Concatenação e repetição
	  <https://python.ime.usp.br/pensepy/static/pensepy/09-Listas/listas.html#concatenacao-e-repeticao>`__;
        - `comprimento de uma lista <https://panda.ime.usp.br/pensepy/static/pensepy/09-Listas/listas.html#comprimento-de-uma-lista>`__;
        - `Fatias de listas <https://python.ime.usp.br/pensepy/static/pensepy/09-Listas/listas.html#fatias-de-listas>`__;
        - `Apelidos (aliasing) <https://python.ime.usp.br/pensepy/static/pensepy/09-Listas/listas.html#apelidos-aliasing>`__;
        - `Clones de listas <https://python.ime.usp.br/pensepy/static/pensepy/09-Listas/listas.html#clonando-listas>`__.

	..  - `Pertinência em uma lista <https://python.ime.usp.br/pensepy/static/pensepy/09-Listas/listas.html#pertinencia-em-uma-lista>`__;


    - Comando `for`

        - `O comando for
	  <https://python.ime.usp.br/pensepy/static/pensepy/03-PythonTurtle/olatartaruga.html#o-laco-for>`__
        - `Mais sobre o comando for <https://python.ime.usp.br/pensepy/static/pensepy/07-Iteracao/maisiteracao.html#mais-sobre-o-comando-for>`__;

.. break

Vídeos
------

    - `Coleções (listas, vetores)
      <https://www.youtube.com/watch?v=krVca97F13I&list=PLcoJJSvnDgcKpOi_UeneTNTIVOigRQwcn&index=27>`__;
    - `O comando for
      <https://www.youtube.com/watch?v=ctmPwp52b7w&index=28&list=PLcoJJSvnDgcKpOi_UeneTNTIVOigRQwcn>`__;
    - `Manipulação de listas <https://www.youtube.com/watch?v=LrXapVI66so&list=PLcoJJSvnDgcKpOi_UeneTNTIVOigRQwcn&index=29>`__;
    - `Objetos na memória <https://www.youtube.com/watch?v=4TN8IdsJ78k&list=PLcoJJSvnDgcKpOi_UeneTNTIVOigRQwcn&index=30>`__.
      
.. Vídeos do curso `Python para zumbis <http://pingmind.com/python-para-zumbis/>`__ de Fernando Masanori:

..    - `Listas <https://www.youtube.com/watch?v=IKbN7PnFqTQ>`__; 
..    - `Inverter uma lista <https://www.youtube.com/watch?v=_ezpAhoU5B4>`__; e 
..    - `Comando de repetição (for) <https://www.youtube.com/watch?v=SWz-TDaVOjU>`__.
	   
.. break

Introdução
----------

Até aqui, trabalhamos com variáveis simples, capazes de armazenar
apenas um tipo, como ``bool``, ``float`` e ``int``. 

Nessa aula introduziremos o conceito de *lista* (= tipo ``list``), uma estrutura
sequencial indexada muito utilizada e uma das principais estruturas
básicas do Python.

Uma lista (= ``list``) em Python é uma sequência ou coleção ordenada 
de valores de qualquer tipo ou classe tais como ``int``, ``float``, ``bool``, ``str`` 
e mesmo ``list``, entre outros. 

Diariamente utilizamos listas para organizar informação, como a lista
de coisas a fazer, lista de compras, lista de filmes em cartaz etc.

Existem várias maneiras de criarmos uma lista.
A maneira mais simples é envolver os elementos da 
lista por colchetes ( ``[`` e ``]``).
Podemos criar a lista contendo os 5 primeiros
primos da seguinte maneira:

.. sourcecode:: python
		
    >>> primos = [2, 3, 5, 7, 11]

Podemos criar uma lista de vários objetos de tipos distintos:

.. sourcecode:: python
		
    >>> uma_lista = [11, "oi", 5.4, True]
    >>> outra_lista = ["joão", "masculino", 15, 1.78, "brasileira", "solteiro"]
    >>> fernanda = ["Fernanda", "Montenegro", 1929, "Central do Brasil", 1998, "Atriz", "Rio de Janeiro, RJ"]

Observe o uso de colchetes (``[``, ``]``) para marcar o início e o final
da lista, e os elementos separados por vígula.

Uma lista pode ser criada vazia, da seguinte forma:

.. sourcecode:: python
		
    >>> lista_vazia = []

.. break

Comprimento de uma lista
------------------------

.. index:: len(), comprimento de uma lista

A função ``len()`` retorna o *comprimento* (= o número de elementos ou objetos) 
de uma lista:
 
.. Para saber o comprimento da lista (o número de elementos contidos na
.. lista), usamos a função ``len()``:

.. sourcecode:: python
		
    >>> len(primos)
    5
    >>> len(uma_lista) 
    4
    >>> len(outra_lista)
    6
    >>> len(fernanda)
    7
    >>> len(lista_vazia)
    0
    

.. break

Índices
-------

.. index:: índices, índices negativos, índices inválidos

Cada valor na lista é identificado por um índice. 

Dizemos que uma lista é uma estrutura sequencial indexada
pois os seus elementos podem ser acessados sequencialmente
utilizando índices.
O primeiro elemento da lista tem índice 0,
o segundo tem índice 1, e assim por diante. 
Observe que, por começar pelo índice zero, 
o último elemento da lista  ``primos``, o número 11,
tem índice 4, sendo que essa lista tem comprimento 5.

Para acessar um elemento de uma lista usamos 
o operador de indexação ``[]``.
A expressão dentro dos colchetes especifica o índice.  
O índice do primeiro elemento é 0.
O seguinte programa imprime os valores da lista ``primos``:

.. activecode:: exemplo_percorrer_lista_com_while
   
    primos = [2, 3, 5, 7, 11]		
    i = 0
    while i < len(primos):
        print( "elemento de indice %d = %d"%(i, primos[i]) )
        i = i + 1

Simule a execução desse programa e observe que a variável ``i`` recebe o
valor zero que corresponde ao primeiro índice e, enquanto o índice for
menor que o comprimento da lista (= ``len(primos)``), 
o elemento de índice ``i`` é impresso.

**Índices negativos** indicam elementos da direita 
para a esquerda ao invés de  da esquerda para 
a direita.

.. activecode:: indices_negativos

    numeros = [17, 123, 87, 34, 66, 8398, 44]
    print("%3d: %d" %(2,numeros[2]))
    print("%3d: %d" %(5,numeros[-5]))
    print("%3d: %d" %(9-8,numeros[9-8]))
    print("%3d: %d" %(-2,numeros[-2]))
    print("%3d: %d" %(len(numeros)-1,numeros[len(numeros)-1]))
    print("%3d: %d" %(-7,numeros[-7]))

Um erro comum em programas é a utilização de índices inválidos (= *list index out of range*):

.. activecode:: indice_invalido_0

    numeros = [17, 123, 87, 34, 66, 8398, 44]
    print("%3d: %d" %(len(numeros),numeros[len(numeros)])) # índice inválido

.. activecode:: indice_invalido_1

    numeros = [17, 123, 87, 34, 66, 8398, 44]
    print("3d: %d" %(-8,numeros[-8])) # índice inválido

.. activecode:: indice_invalido_2

    lista_vazia = []
    lista_vazia[0] = 10   # índice inválido

.. break

Concatenação e Repetição
------------------------

.. admonition:: Nota sobre eficiência computacional

    Por ser um curso introdutório, utilizaremos nesse curso
    apenas as operações de concatenação e fatiamento de listas. Nosso
    objetivo é empoderar os alunos para que
    eles possam entender, aprender e usar essas estruturas rapidamente
    em seus programas, independente da eficácia computacional dessas
    soluções, permitindo que o aluno mantenha foco no problema e não
    na linguagem.
    O custo computacional dessas soluções será abordado em
    cursos mais avançados.

Duas listas podem ser concatenadas utilizando o operador ``+``.

.. activecode:: concatenacao_1

    um = [10, 11, 12]
    dois = [20, 21]
    
    tres = um + dois
    print(tres)
    
Nesse exemplo, a concatenação da lista ``um`` com a lista ``dois``
cria uma nova lista formada por cópias dos elementos da lista ``um`` e
``dois``.

Uma lista vazia é o **elemento neutro** da concatenação de listas:

.. activecode:: concatenacao_1b

    um = [10, 11, 12]
    dois = [20, 21]
    
    tres = um + [] + dois + []
    print(tres)


O exemplo abaixo, concatena a lista ``um`` três vezes:

.. activecode:: concatenacao_2

    um = [10, 11, 12]
    
    tres = um + um + um
    print(tres)

Para concatenar uma lista repetidas vezes, podemos utilizar o operador
``*`` para ``multiplicar`` uma lista por um inteiro:

.. activecode:: repeticao_1

    um = [10, 11, 12]
    
    print("um + um + um", um + um + um)
    print("3 * um", 3 * um)
    print("um * 3", um * 3)
    print("[] * 3", [] * 3)

Observe que, assim como o ``*`` em operações aritméticas, o operador
de repetição ``*`` de listas também é comutativo, assim como a
repetição de listas vazias resulta em uma lista vazia.

O operador de repetição ``*`` é bastante útil para criar uma lista de
comprimento ``N`` com valor inicial determinado como:

.. activecode:: repeticao_2

    # cria lista de tamanho n com zeros
    n = int(input("Digite o tamanho da lista: "))
    lista = [0] * n
    print(lista)

.. break

Fatias de um lista
------------------

Muitas vezes, ao invés de considerar a lista completa, é necessário
consider apenas um pedaço contínuo da lista, que podemos definir por 2
índices que marcam o início e o fim desse pedaço, que chamamos de
fatia da lista. 

Em Python, uma fatia de uma lista é definida colando o ínicio e o fim
entre colchetes, separados por `:`, como:

.. sourcecode:: python
		
    >>> primos = [2, 3, 5, 7, 11]
    >>> primos[1:2]
    [3]
    >>> primos[2:4]
    [5, 7]
    >>> primos[:3] # observe que o início não precisa ser definido
    [2, 3, 5] 
    >>> primos[3:] # observe que o fim não precisa definido
    [7, 11]
    >>> primos[:] 
    [2, 3, 5, 7, 11]

Observe que o intervalo é sempre fechado à esquerda (inclui o
primeiro elemento na fatia) e aberto à direita (não inclui o último elemento).
A operação de fatiamento devolve uma cópia do pedaço da lista definido pelo intervalo.


.. break

Referência versus cópia
-----------------------

.. index:: referência, cópia

Quando uma lista é atribuída a uma variável, dizemos que a variável
passa a fazer uma **referência** a lista. Assim, várias variáveis com nomes
distintos podem fazer referência a **mesma** lista.

.. codelens:: clonar_uma_lista_0 

    lista_1 = ["oi", 2, 3.14, True]
    lista_2 = lista_1
    lista_2[2] = 5

    print(lista_1)  # observe que o elemento lista_1[2] TAMBÉM foi modificado
    
Nesse exemplo, para que possamos trabalhar com a ``lista_2`` sem
alterar os elementos da ``lista_1``, precisamos **copiar** a lista_1.

A forma mais comum para copiar uma lista é criando uma fatia da lista
inteira como:
 
.. sourcecode:: python 

    lista_1 = ["oi", 2, 3.14, True]
    lista_2 = lista_1[:]   # cria uma cópia da lista_1
    lista_2[2] = 5

    print(lista_1)  # observe que o elemento lista_1[2] NÃO foi modificado

.. break

Comando ``for``
---------------

.. index:: comando for

Vários problemas envolvendo listas requerem percorrer (varrer) todos
os elementos da lista, um a um, do início até o fim. Nesses casos,
podemos utilizar o comando ``for``:

.. codelens:: exemplo_percorrer_lista_com_for_elem
		
    primos = [2, 3, 5, 7, 11, 13]
    print("Varredura usando for: ") 
    for elemento in primos:
        print("Valor da variável elemento: ", elemento)
    print("Fim da varredura com for")
    
    print()
    print("Varredura usando while: ") 
    i = 0
    while i < len(primos):
	print("Valor na posição %d da lista: %d"%(i, primo[i]))
	i += 1

Para usar o comando ``for``, você precisa definir uma variável que vai
receber o valor de um elemento da lista a cada iteração.
Nesse exemplo, a variável ``elemento`` recebe o valor
de cada elemento da lista ``primos``, ou seja, primeiro o valor
2, depois 3, 5, e assim por diante, até 13. A repetição termina
quando o último elemento da lista ``primos`` for utilizado.

Observe também que a varredura da lista pode ser feita usando o
comando ``while``. A sequência de índices válidos é gerada variando
a variável i de 0 até ``len(primos)``.

Muitas vezes é mais conveniente varrer uma lista usando uma variável
que assume valores dentro do intervalo dos índices válidos. Com isso,
temos mais poder e flexibilidade no acesso aos elementos da lista,
como por exemplo, varrer a lista da direita para a esquerda, ou pegar
apenas os elementos de índice par.

Nesses casos também, quando queremos ter controle sobre os índices,
é muito comum utilizar o comando ``for`` em conjunto com a função
``range()``. Basicamente, devemos chamar a função ``range()`` para
que ela "devolva uma lista"
contendo os valores dos índices que queremos utilizar.

A função ``range()`` recebe 3 parâmetros: ``início``, ``fim`` e
``passo``, que definem a sequência de números a ser criada. Modifique
o trecho de programa abaixo para ver como a função range se comporta
em conjunto com o ``for``:

.. activecode:: funcao_range_com_for

    inicio = 10
    fim    = 20
    passo  = 3
    for i in range(inicio, fim, passo):
        print( i )

Observe que, como na definição de intervalos em fatias de lista,
o intervalo de números cobertos pela função ``range()`` é fechado
no ``início`` e aberto no ``fim``.

Quando o passo desejado é 1 (um), o valor não precisa ser
passado a função ``range()``, como abaixo:

.. activecode:: funcao_range_com_passo_1

    inicio = 5
    fim    = 11
    for i in range(inicio, fim):
        print( i )

Para facilitar mais ainda a varredura de listas, quando o início
desejado é 0 (zero) e o passo 1 (um), você pode definir apenas o
``fim``, como:

.. activecode:: exemplo_percorrer_lista_com_for_range
	
    primos = [2, 3, 5, 7, 11, 13]	
    for i in range(len(primos)):
        print( "%d: %d"%(i,primos[i]) )

Nesse exemplo, usando a função ``len()`` o final do intervalo a ser
gerado pela função ``range()``.	Para saber mais sobre essa função, consulte a
`documentação da função range() <https://docs.python.org/3/library/functions.html#func-range>`__.

Para verificar se você entendeu como funciona a função ``range()``,
modifique o ``inicio``, ``fim`` e ``passo`` no trecho de programa
abaixo para varrer a lista de forma reversa, ou seja, da direita para
a esquerda.

.. activecode:: exemplo_percorrer_lista_reversa

    primos = [2, 3, 5, 7, 11, 13]
    # modifique os valores abaixo
    inicio = 0
    fim    = len(primos)
    passo  = 1	

    for i in range(inicio, fim, passo):
        print( "%d: %d"%(i,primos[i]) )

   
.. break
      
Exercícios com listas
---------------------


Exercício 1
...........

(exercício 1 da `lista de exercícios sobre vetores <https://www.ime.usp.br/~macmulti/exercicios/vetores/index.html>`__). 

Dados n > 0 e uma sequência com n números reais, imprimí-los na ordem inversa a da leitura.

.. activecode:: aula_lista_ex1_tentativa

    def main():
        # escreva o seu programa abaixo e remova o print()
        print("Vixe! Ainda nao fiz o exercicio!")
      
    main()

Clique `aqui <exercicios/ex91.html>`__ para ver uma solução.

    

Exercício 2
...........

Dada uma sequência de ``n > 0`` números reais, imprimi-los eliminando as repetições.


.. activecode:: aula_lista_ex2_tentativa

    def main():
        # escreva o seu programa abaixo e remova o print()
        print("Vixe! Ainda nao fiz o exercicio!")
      
    main()


.. AINDA SEM SOLUÇÃO      
.. Clique `aqui <exercicios/ex92.html>`__ para ver uma solução.


   
Exercício 3
...........

Dados dois números naturais ``m`` e ``n`` e duas sequências ordenadas com ``m`` e
n números inteiros, obter uma única sequência ordenada contendo todos
os elementos das sequências originais sem repetição. 

Sugestão: Imagine uma situação real, por exemplo, dois fichários de uma biblioteca.

.. activecode:: aula_lista_ex3_tentativa

    def main():
        # escreva o seu programa abaixo e remova o print()
        print("Vixe! Ainda nao fiz o exercicio!")
      
    main()
      
.. AINDA SEM SOLUÇÃO
.. Clique `aqui <exercicios/ex93.html>`__ para ver uma solução.


Exercício 4
...........


Dados um número inteiro ``n`` e uma sequência com ``n`` números reais, 
determinar a maior soma de um segmento da sequência (com pelo menos um elemento). 
Um *segmento* é uma subsequência de números consecutivos.

Para  ``n == 12`` e a sequência

.. sourcecode:: python

    5   -2   -2   -7   3   14  10  -3   9   -6   4   1 

a soma do segmento de soma máxima é ``3+14+10-3+9 = 33``.

.. activecode:: aula_lista_ex4_tentativa

    def main():
        # escreva o seu programa abaixo e remova o print()
        print("Vixe! Ainda nao fiz o exercicio!")
      
    main()


.. AINDA SEM SOLUÇÃO
.. Clique `aqui <exercicios/ex94.html>`__ para ver uma solução.


   





