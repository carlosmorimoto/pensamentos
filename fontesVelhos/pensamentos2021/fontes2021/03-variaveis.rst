
.. Variáveis, tipos e funções de entrada e saída 

.. Pensando nos Dados: entra, salva, transforma e sai

.. Pensamento e comunicação

Introdução
----------

.. index:: valor, tipo de dados, string, str, str()

.. index:: inteiro, int, int(), real, float, float()

.. index:: input(), print()

.. raw:: html

    <div align="right">
    <p></p>
    <blockquote>
    <p>
    "
    O maior problema com a comunicação é a ilusão</br>
    de que ela foi alcançada.
    "
    </p>
    <p>William H. Whyte</p>
    </blockquote>
    <p></p>
    </div>

Programas precisam se comunicar, ou seja, trocar informações com outros programas ou com pessoas. 
Nesse capítulo vamos ver como os programas recebem e transmitem informações para pessoas.
Vamos estender o conceito de computador como calculadora que cobrimos no capítulo anterior
para computador como **processador de dados**, ou seja, que recebe dados, transforma 
os dados em outros valores e comunica o resultado de alguma maneira.

Nos exemplos interativos desse livro vamos usar as ferramentas CodeLens e ActiveCode
que permitem a troca de mensagens entre o programa e o usuário dentro do próprio navegador.

No seu computador, essa troca de mensagens ocorre em um terminal executando iPython. 
No exercício de laboratório desse capítulo 
você vai ver também como trabalhar com arquivos fonte em Python usando o Spyder.

.. break

Para onde vamos
---------------

Ao final desse capítulo você deverá ser capaz de:

    - identificar variáveis em programas em Python;
    - usar o operador de atribuição `=` para criar e manipular variáveis;
    - usar a função input();
    - usar a função print();
    - usar valores das classes int, float, str, e bool;
    - usar as função de conversão int(), float(), bool() e str();
    - simular um programa simples em Python.

.. break


Comunicação e interação
-----------------------

.. No capítulo anterior vimos como o Python calcula o valor de uma expressão. 
    Basicamente, uma expressão é reduzida um operador por vez, começando dos operadores com maior precedência até que a expressão seja totalmente reduzida a um único valor numérico ou booleano.
    Vimos também que o iPython pode ser utilizado como "calculadora" para resolver expressões. 

.. A maioria das calculadoras são máquinas dedicadas que possuem um teclado reduzido onde 
    o usuário pode entrar com os operandos e operadores, e um tela reduzida também (apenas uma linha) para exibir os valores digitados e os resultados das operações. 

.. Os computadores tipicamente possuem interfaces gráficas que recebem informações de um 
    teclado e mouse e exibem informações em um monitor. Vamos evitar a complexidade de criar interfaces gráficas e nos concentrar em problemas mais simples que podem ser resolvidos em terminais por linhas de comando, como o iPython. 

Diferente dos programas que você deve estar acostumado a usar, que possuem interfaces com elementos gráficos que você pode selecionar com o mouse, os programas que vamos desenvolver nesse curso precisam ser executados dentro de um **terminal**. 

Como vimos, um terminal permite interagir com o computador por meio de linhas de comando. Esse tipo de interface era bastante comum antes das interfaces gráficas e vai nos permitir manter o foco nos algoritmos ao invés da interface. 

Se você ainda tem dúvidas sobre o funcionamento de um terminal, imagine um terminal como um programa de troca de mensagens, como o WhatsApp, Telegram ou Signal, que você usa para "trocar mensagens" com o programa. Você deve escrever alguma mensagem na linha de comando para enviar ao programa, e o programa responde imprimindo uma mensagem no terminal. 
Assim, quando um exercício mencionar que um programa "recebe dados", estamos dizendo que o programa deve ler os dados na forma de textos que devem ser digitados pelo usuário na linha de comando do terminal. 
Depois de ler os dados, o programa deve processar os dados para calcular a resposta e 
imprimir uma mensagem avisando o usuário do resultado.
 
Esse processo é ilustrado pela figura abaixo.

.. figure:: ../Figuras/cap03/arara-shell.png
    :align: center
    :width: 800px
    :alt: Figura 3.1: Arara usando programa no Python Shell.

    Figura 3.1: O usuário usa o iPython (terminal Python ou Python shell) para interagir com o programa Python sendo executado no computador. A função ``input()`` permite que o Python receba um texto digitado pelo usuário e a função ``print()`` permite que o Python imprima textos na janela do Python Shell.

Nesse capítulo você vai aprender a usar a função ``input()`` do Python para ler um texto do teclado e imprimir um texto usando a função ``print()``. Como mostra a figura 3.1, essas duas funções fazem a ponte (entrada e saída) entre o usuário e o programa.

Os dados que entram e saem devem ficar armazenados e são acessados por meio de **variáveis**. O uso de variáveis é ainda fundamental para armazenar resultados intermediários necessários para processar os dados. Antes portanto de introduzir as funções de entrada e saída, vamos discutir alguns conceitos relacionados à criação e uso de variáveis.

.. Para escrever e executar nosso primeiro programa vamos usar o Spyder, um aplicativo chamado de "ambiente integrado de desenvolvimento" que reune várias ferramentas (de forma integrada) que facilitam o desenvolvimento de programas. 


.. break

Variáveis e o comando de atribuição
-----------------------------------

.. index:: nome de variável, comando de atribuição

Uma **variável** é simplesmente um **nome** que faz referência a um objeto na memória que corresponde, por exemplo, ao resultado de alguma expressão aritmética. Para criar uma variável, precisamos associá-la a um objeto ou valor usando o **comando de atribuição** que corresponde ao símbolo ``=``, como no trecho abaixo:

.. code-block:: Python

    variável = expressão

Para entender como  utilizar o comando de atribuição para criar e modificar o valor de variáveis, execute o trecho de código Python no CodeLens abaixo, acompanhando a lista de comentários a seguir antes de clicar em ``Next``.

* Com a seta vermelha na Linha 1: pi = 3.14

    - clique em ``Next`` e repare  que abaixo do quadro de saída aparece um outro quadro chamado ``Global frame``. Esse quadro mostra uma tabela com os nomes das variáveis criadas e seus valores associados. O comando de atribuição calcula a expressão do lado direito, nesse caso é o valor ``3.14``, e cria a variável ``pi`` que passa a fazer referência ao valor da expressão. 

* Com a seta na linha 2: print( 'pi = ', pi )

    - clique em ``Next``. A mensagem "pi = 3.14" é impressa na saída e a seta desce para a próxima linha.

* Com a seta na linha 3: novo_pi = pi

    - clique em ``Next``. O valor da expressão do lado direito é calculado. Desse lado, há apenas a variável ``pi``. Para calcular a expressão, todas as variáveis são substituídas pelos valores a que fazem referência e, portanto, o valor da expressão se torna ``3.14``. A variável com nome ``novo_pi`` é criada e faz referência ao valor ``3.14``. 

* Com a seta na linha 4: print( 'novo_pi = ', novo_pi )

    - clique em ``Next``. A mensagem "novo_pi = 3.14" é impressa na saída e a seta desce para a próxima linha.

* Com a seta na linha 5: pi = 2 * pi

    - clique em ``Next``. Veja que o valor a que ``pi`` faz referência é atualizado. Mas observe também que o valor de ``novo_pi`` não é alterado. 

* Com a seta na linha 6: print( 'pi = ', pi )

    - clique em ``Next``. A mensagem "pi = 6.28" é impressa na saída e o programa se encerra (não é mais possível clicar em ``Next``).


.. codelens:: cl03_criacao_de_uma_variavel
    :showoutput:

    pi = 3.14
    print( 'pi = ', pi )
    novo_pi = pi
    print( 'novo_pi = ', novo_pi )
    pi = 2 * pi
    print( 'pi = ', pi )


.. admonition:: **Nomes de variáveis**

   Nesse curso o nome de uma variável será sempre iniciada por uma letra
   minúscula e poderá ser seguida por outras letras minúsculas e números, como
   em: ``contador``, ``ind``, ``i``, ``j``, ``a1``, ``r2d2``, ``bb8`` etc.
   
   Escolha sempre um nome **significativo ou comum**, para facilitar o
   entendimento sobre o que variável representa ou realiza. Por exemplo, ``i``
   e ``j`` são nomes comuns para contar o número de iterações em um laço, mas
   podemos utilizar o nome ``contador`` e as vezes algumas abreviações como
   ``cont`` e ``aux``. Para melhorar a clareza de seus programas, sugerimos
   também o uso de nomes compostos separados pelo caractere `_`, como em
   ``conta_pares`` e ``conta_impares``. Evitaremos também o uso de caracteres
   acentuados para facilitar a compatibilidade com a ferramenta online
   utilizada no curso.
   
   Vamos chamar de **constantes** variáveis que **não devem** mudar de valor 
   após serem inicializadas. Para indicar uma constante utilizaremos nomes
   formados por todas as letras em maiúscula, como por exemplo
   ``MAXIMO_TAMANHO = 100``.

   **Palavras reservadas** como o nome de comandos e funções nativas do Python
    (``if``, ``while``, ``print()`` etc) também não podem ser utilizadas como nome de
    variáveis.


.. admonition:: **Não confunda atribuição com igualdade**

    Nós somos muito treinados a ler o símbolo "=" como "igual". Lembre-se que o operador relacional de igualdade é representado como "==" e a expressão "pi == 3.14" devolve ``True`` ou ``False`` mas **não** cria ou altera o valor da variável ``pi``. 
    
    Um reflexo desse "treinamento" quando começamos a programar é ignorar a ordem dos elementos do comando de atribuição (pois o símbolo é lido como "igual"). A ordem é muito importante, sendo que devemos sempre escrever o nome de uma variável do lado esquerdo e a expressão do lado direito do comando de atribuição "=".

    Finalmente quando usamos construções como 'novo_pi = pi' é comum imaginar que ambas são "a mesma" variável (outro reflexo de "igual"), ou seja, modificando-se o valor associado a uma delas, o valor da outra também é alterada. Isso não é verdade como pudemos observar no exemplo do CodeLens acima. 


.. break

Tipos de dados
--------------

.. index:: int, float, bool

Antes de pensar na solução é necessário ter um profundo entendimento do problema. 

Um passo importante para o entendimento do problema é a **especificação** dos tipos de 
dados usados na comunicação, ou seja, na entrada e na saída dos dados de seus programas.

Já conhecemos 3 tipos usados no Python para trabalhar com expressões aritméticas e lógicas:

* ``int``: para números inteiros, 
* ``float``: para números reais e 
* ``bool``: para os valores booleanos ``True`` e ``False``).

Em nosso dia-a-dia, muitas vezes, não nos preocupamos com o tipo de um resultado e costumamos informar apenas um valor. Por exemplo, considere as seguintes perguntas:

* quantos anos você tem?
* qual a sua altura?
* você já almoçou?

Imagine agora que você é o computador, que só sabe se comunicar em código binário, e recebe algo como "00010000". O que você, computador, faz com isso? 

Para saber o que fazer, precisamos saber que tipo de dado esses bits representam para conseguir decodificá-los. 
Por exemplo, esses bits podem ser usados para representar um inteiro ``16``, ou um real ``1.6`` ou ainda um booleano ``True``. 


.. admonition:: Importância da especificação

    Você pode encontrar certa dificultade para definir o tipo de algum dado, como "idade", pois algumas pessoas preferem responder 16.5 ao invés de 16 anos, por exemplo. Para simplificar nosso raciocínio e reduzir o trabalho de codificação, vamos assumir nesse curso que todos os dados de entrada e de saída possuem um tipo específico e que não deve ser alterado.

    Assim, se a especificação define que o programa deve devolver um inteiro (com o valor da idade), mas você resolver modificar o tipo para um real, outros programas que usam esse resultado podem não mais funcionar pois não vão "entender" a resposta do seu programa. 
    

.. break

Tipos e valores
---------------

Observe que qualquer valor inteiro como ``2`` também pode ser escrito como um real usando a notação ``2.0`` (ou seja, a existência do ponto decimal indica que se trata de um número real, mesmo que a parte fracionária seja zero). Assim, ao escrever seus programas, procure sempre escrever os números reais de forma explícita, incluindo o '.0' para indicar que a grandeza que deseja representar se trata de um real e não um inteiro. 

Ao calcular expressões aritméticas é comum utilizar valores de tipos diferentes. No trecho de código abaixo, a variável ``perimetro`` é calculada multiplicando o inteiro ``2`` por números reais. Execute esse trecho passo-a-passo clicando em ``Next`` para verificar o tipo do valor associado a essa variável, resultado da expressão "2 * pi * raio". 

.. codelens:: cl03_dois_pi_raio_exemplo

    pi = 3.1415   # número real
    raio = 1.0    # número real pois tem '.0'
    perimetro = 2 * pi * raio   # 2 é inteiro. Mas qual o tipo de perimetro?
    print( 'tipo: ', type(perimetro) )

Para manter consistência nos dados, é desejável gerar um resultado que tenha o mesmo tipo dos dados utilizados, como ``2 * 4`` deve resultar no valor ``8`` inteiro. 
Mas existem excessões como a divisão, onde "5 / 2" resulta no ``float`` ``2.5``. 
Quando há mistura de tipos, o Python realiza os cálculos usando o tipo mais "poderoso", ou seja, em expressões com inteiros e reais, o resultado será real (``float``).  

Como o Python considera valores, também não há problema em comparar valores de tipos diferentes. Assim, a expressão ``5 == 5.0`` resulta no valor booleano ``True`` (do tipo ``bool``). 

.. admonition:: Evite comparar igualdade entre floats

    Devido a representação finita de número reais como números em ponto flutuante, números reais em Python são, na verdade, aproximações. 
    
    Por exemplo, considere o número Pi4 que representa o valor da constante matemática Pi usando 4 casas decimais (``3.1415``)
    e o número Pi5 que representa o mesmo valor mas com 5 casas decimais (``3.14159``).
    Qual seria o valor da expressão Pi4 == Pi5?

    Outro problema de usar aproximações ao realizar cálculos é o acumulo de erros. 

    Por exemplo execute o trecho a seguir, procurando pensar no resultado antes de clicar o ``Next``.

    .. codelens:: problema_igualdade_entre_floats

        x = 0.01 + 0.01 + 0.01 + 0.01 + 0.01 + 0.01 
        print( x == 0.06 )
        print( 'x = ', x )

    Se for realmente necessário comparar floats, use um intervalo de precisão epsilon, pequeno o sufiente para a sua aplicação, como no trecho de código abaixo.

    .. codelens:: outr_problema_igualdade_entre_floats

        s = 0.06
        x = 0.01 + 0.01 + 0.01 + 0.01 + 0.01 + 0.01 
        print( x == s ) # x é igual a s

        eps = 0.0001
        print( s - eps < x < s + eps ) ## x está perto o suficiente de s?


.. exercício associe valores x tipos



.. break

Tipo string (str)
-----------------

Um outro tipo que vamos utilizar bastante é chamado de "string" e é abreviado por ``str``. Na verdade já usamos esse tipo quando introduzimos a função ``print()``, vamos agora entender melhor o seu funcionamento.

Uma string é uma sequência de 0 ou mais caracteres que representam um texto. Para indicar uma string no Python, devemos escrevê-lo entre apóstrofes (') ou aspas ("). Essa dualidade facilita a inclusão do caractere apóstrofe ou aspas dentro de uma string, como ilustra o trecho de código abaixo. Execute esse programa passo-a-passo (clicando em ``Next``) e verifique o resultado de cada comando de atribuição. 

.. codelens:: cl03_exemplo_strings

    carmem = "Carmem Miranda"
    print( carmem )
    lata = "d'água"
    print( lata )
    "d'água"
    seq = ' 1, "dois", 3'
    print( seq )
    print( type(seq) )


Concatenação de strings
.......................

É possível **concatenar** duas strings usando o operador ``+``. Por exemplo, execute passo-a-passo o trecho de programa abaixo e veja o que ocorre com as variáveis e na saída do programa. Como sempre, procure pensar no resultado antes de clicar ``Next``. 

.. codelens:: cl03_exemplo_concatenacao

    p = "Pedro"
    a = "Álvares"
    c = "Cabral"
    pa = p + a
    print( pa, p, a )
    nome = p + ' ' + a + c
    print( nome )

Observe que os caracteres em branco são parte da string.
Assim, a concatenação de ``p + a`` resulta em 'PedroÁlvares' pois 
nem ``p`` nem ``a`` foram definidas com espaço. 
Observe que na expressão (com strings!) que cria a variável ``nome`` apenas um espaço
foi definido entre os dois primeiros nomes, mas não há espaço entre os
dois últimos nomes.

.. admonition:: Concatenação ou Soma?

    O operador ``+`` é utilizado tanto para concatenar strings quando os dois operandos são do tipo ``str`` como para somar quando os dois operadores são números. No entanto, quando um operador é string e o outro for um número, o Python indica um erro como mostrado em uma sessão do iPython abaixo. 

    .. code-block:: Python

        In [4]: 'Pedro' + 3
        ---------------------------------------------------------------------------
        TypeError                                 Traceback (most recent call last)
        <ipython-input-1-cb21c6a527de> in <module>
        ----> 1 'Pedro' + 3
        
        TypeError: can only concatenate str (not "int") to str
        
        In [5]: 'Pedro ' * 3
        Out[5]: 'Pedro Pedro Pedro'

    No entanto, observe na linha de comando "In [5]" que é possível realizar múltiplas concateções usando o operador ``*``. 

    Execute o código abaixo e note que a mensagem de erro pode ser diferente do iPython. Corrija o erro substituindo o operador ``+`` pelo operador ``*`` e execute novamente para ver o resultado.

    .. activecode:: ac03_teste_concatenacao_multipla_de_strings

        soma_3 = 'Pedro' + 3
        mult_3 = 'Pedro ' * 3 


.. break

Conversão entre tipos
---------------------

Acabamos de ver que expressões aritméticas podem misturar valores inteiros com reais. 
Assim, quando um operador aritmético encontra um operando inteiro e outro real, o Python promove o inteiro para real para realizar o cálculo. Essas conversões são feitas automaticamente pelo Python, de forma **implícita**.

.. mchoice:: mc03_real_ou_inteiro
    :answer_a: 14
    :answer_b: 14.0
    :answer_c: as alternativas a e b estão corretas
    :answer_d: 20.0
    :correct: b
    :feedback_a: o resultado não pode ser um ``int``.
    :feedback_b: Correto.
    :feedback_c: o resultado não pode ser um ``int``.
    :feedback_d: a multiplicação tem precedência sobre a soma.

    Qual o valor da expressão ``2 + 3 * 4.0``


Ao processar dados é muito comum no entanto precisar converter o tipo de um dado em outro. 
A conversão entre tipos nativos do Python pode ser realizada de forma **explícita** usando as funções de mesmo nome como mostra os exemplos abaixo.

.. code-block:: Python

    >>> 4 / 2
    2.0
    >>> int(4/2)
    2
    >>> float(2 * 4)
    8.0


.. break

Funções para se comunicar com o usuário
---------------------------------------

Lembre-se que a comunicação do programa com o usuário no terminal deve ser feita usando 
funções de entrada e saída, que permitem que um programa receba uma mensagem do usuário (escrita no terminal) e imprima mensagens de texto (no terminal).
 
Nesse curso, como os programas serão executados no iPython (ou Python Shell), vamos utilizar a função ``input()`` para que o seu programa receba um texto digitado pelo usuário (no terminal) e vamos utilizar a função ``print()`` para que um programa possa imprimir um texto (no terminal). 


Função input()
..............

Para usar a função ``input()`` devemos fazer o seguinte.

.. code-block:: Python

    var = input( mensagem )

A função ``input()`` recebe uma mensagem (uma string) que é exibida no terminal. A função dessa mensagem é explicar ao usuário o que digitar. Ao terminar de digitar, o usuário deve teclar ENTER para enviar a string digitada ao programa. O programa atribui essa string à variável ``var``. 


Função print()
..............

Para usar a função ``print()`` devemos fazer o seguinte.

.. code-block:: Python

    print( lista_de_valores_separada_por_vírgulas )
    
A função ``print()`` recebe uma lista de valores separada por vírgulas e imprime cada valor (transforma o valor para uma string e imprime a string), adicionando um espaço em branco entre duas strings e pulando de linha ao final.

O trecho de código no ActiveCode abaixo mostra como essas funções podem ser usadas. Edite esse código para fazer outras perguntas e respostas. 

.. activecode:: ac03_ola_mundo

    nome = input('Qual o seu nome? ')
    print("olá", nome, "!")
    print()   # um print() sem nada apenas pula uma linha
    print('O tipo da variável nome é:', type(nome))



Observe que a função ``print()`` procura facilitar a impressão e torná-la mais legível, por exemplo, inserindo um espaço automaticamente entre dois valores da lista e pulando para a próxima linha ao final da impressão,
para que cada ``print()`` apareça em uma linha. Um ``print()`` sem nada entre parênteses apenas pula uma linha.

Esse comportamento pode ser modificado definindo alguns parâmetros extras, como ``sep`` e ``end``. 

    * O parâmetro ``sep`` pode ser usado para alterar a string inserida automaticamente entre dois valores consecutivos da lista. O valor padrão é um único espaço em brando. Exemplos:

        .. code-block:: Python

            >>> print(2, 3, 4, sep=' ')   # com um espaço é o comportamento padrão:
            2 3 4
            >>> print(2, 3, 4, sep='')    # com uma string vazia, sem espaço, resulta em:
            234
            >>> print(2, 3, 4, sep='-*-') # com a string '-*-' resulta em:
            2-*-3-*-4

    * O parâmetro ``end`` pode ser usado para alterar a string inserida automaticamente ao final da lista de valores. O valor default é o caractere especial que pula uma linha (representado por ''\n''). Observe os seguintes exemplos (clique em ``Run`` caso você estiver lendo esse texto online usando um navegador).

        .. activecode:: exemplo_print_end_default

            print('primeira', 'linha', end='\n')   # comportamento padrão
            print('segunda', 'linha', end='\n')

        A saída desse trecho de código é 

            primeira linha
            segunda linha

        .. activecode:: exemplo_print_end_pula_duas

            print('primeira', 'linha', end='\n\n')   # pula duas linhas
            print('segunda', 'linha')

        A saída desse trecho de código é 

            primeira linha

            segunda linha
        
        .. activecode:: exemplo_print_end_nao_pula

            print('primeira', 'linha', end=' <<>> ')   # string sem pula linha
            print('segunda', 'linha')

        A saída desse trecho de código é 

            primeira linha <<>> segunda linha


    * Os parâmetros ``sep`` e ``end`` podem ser combinados.

        .. activecode:: exemplo_print_end_nao_pula_com_sep

            print('primeira', 'linha', sep='**', end=' <<>> ')  
            print('segunda', 'linha', sep='%%', end='<<FIM>>')

        A saída desse trecho de código é 

            primeira**linha <<>> segunda%%linha<<FIM>>

.. break

Exercício comentado: Como somar 2 números inteiros?
---------------------------------------------------

O objetivo desse exercício é aplicar tudo o que vimos até agora e aproveitar para apresentar algumas características da linguagem Python. 

Vamos começar tentando somar dois números usando apenas variáveis. Para isso execute o script abaixo no ActiveCode: 

.. activecode:: cap3_ex1_1
    :caption: Como somar dois números?
    :nocodelens: 

    Tentativa 0: execute o programa abaixo e veja o que acontece.

    ~~~~

    a = 3
    b = 4
        soma = a + b
        print('A soma de a + b é: soma')

    ====

Clique no botão "Run" para executar esse programa.

Depure o programa
.................

Depurar (às vezes chamado de debugar, do inglês **debug**), é a atividade de  corrigir problemas do programa. 

O primeiro **bug** desse programa é sintático. Para corrigir esse problema basta
corrigir a tabulação e clique em RUN para executar o programa novamente. 

Sem o erro sintático, o Python consegue executar o programa até o final e imprimir na saída a string fornecida à função ``print()``.
Nesse caso, a ``print()`` está imprimindo a string que recebeu, 
onde o **nome** da variável ``soma`` é impresso ao invés de seu valor.

.. activecode:: cap3_ex1_2
    :caption: Como somar dois números?
    :nocodelens: 

    Tentativa 1: execute o programa abaixo e veja o que acontece.

    ~~~~

    a = 3
    b = 4
    soma = a + b
    print('A soma de a + b é:', soma)

Observe que o print recebeu dois **valores** separados por vírgula:

    - uma string (entre apóstrofes) e
    - a variável ``soma`` (ao invés da string 'soma')

A string é impressa diretamente sem apóstrofes (pois esse é o seu **valor**) e, ao invés de imprimir a string 'soma', a ``print()`` imprime o **valor** da variável.

Mas como imprimir os valores de ``a`` e ``b`` ao invés de seus nomes?

.. activecode:: cap3_ex1_3
    :caption: Como somar dois números?
    :nocodelens: 

    Tentativa 2: execute o programa abaixo e veja o que acontece.

    ~~~~

    a = 3
    b = 4
    soma = a + b
    print('A soma de', a, '+', b, 'é:', soma)

       
Podemos quebrar ainda mais a lista de valores passada ao print, com strings e variáveis, para que a mensagem fique mais clara.

Uma forma mais elegante para imprimir mensagens e substituir valores de variáveis é usando uma string formatada. 

.. activecode:: cap3_ex1_3_formatada
    :caption: Como somar dois números?
    :nocodelens: 

    Tentativa 3: execute o programa abaixo e veja o que acontece.

    ~~~~

    a = 3
    b = 4
    soma = a + b
    print( f'A soma de {a} + {b} é: {soma}' )

Uma string formatada começa com o caractere "f". Dentro da string formatada, os elementos entre chaves, como "{a}" e "{b}", indicam nomes de variáveis. Esses elementos são substituídos pelos valores das variáveis correspondentes. 


Mas como fazer com que os valores a serem somados sejam definidos por um usuário?

.. activecode:: cap3_ex1_4
    :caption: Como somar dois números?
    :nocodelens: 

    Tentativa 4: execute o programa abaixo e veja o que acontece.

    ~~~~
    # tudo que vem após o simbolo # até o final da linha é um comentário
    # comentários são mensagens aos programadores 
    # que não são executados pelo Python

    PROMPT_1 = "Digite o primeiro numero: "  # PROMPT_1 é uma constante
    PROMPT_2 = "Digite o segundo numero: "   # PROMPT_2 é outra constante
             
    a = input(PROMPT_1) # veja o texto de PROMPT_1
    b = input(PROMPT_2)
    
    soma = a + b
    print( f'A soma de {a} + {b} é: {soma}' )
                
Utilizamos a função ``input()`` para receber a resposta do usuário à mensagem que colocamos como string passado à função ``input()``. Como a resposta do usuário também é uma string (e não um **número**) a operação ``+`` é realizada com strings, ou seja, seus valores são concatenados.

Precisamos, portanto, de um maneira para converter uma string em um número, para que o Python obtenha a soma desses números.


.. activecode:: cap3_ex1_5
    :caption: Como somar dois números?
    :nocodelens: 

    Tentativa 5: execute o programa abaixo e veja o que acontece.

    ~~~~

    a_str = input("Digite o primeiro numero: ")
    b_str = input("Digite o segundo numero: ")
    a_int = int(a_str) # converte string/texto para inteiro
    b_int = int(b_str) # converte string/texto para inteiro
    soma = a_int + b_int
    print( f'A soma de {a_int} + {b_int} é: {soma}' )

                
A função ``int()`` converte uma string para um número inteiro (se possível, senão fornece uma mensagem de erro). Como não precisamos guardar as respostas na forma de texto, podemos simplificar o programa combinando as funções ``int()`` e ``input()`` da seguinte forma:
    
.. activecode:: cap3_ex1_6
    :caption: Como somar dois números?
    :nocodelens: 

    Tentativa 6: execute o programa abaixo e veja o que acontece.
    ~~~~
    a = int(input("Digite o primeiro numero: "))
    b = int(input("Digite o segundo numero: "))
    soma = a + b
    print( f'A soma de {a} + {b} é: {soma}' )


.. break

Laboratório: Spyder
--------------------

.. index:: IDE (Integrated Development Environment), Spyder

O Spyder `<https://www.spyder-ide.org/>`__ 
é um ambiente integrado de desenvolvimento de programas em Python que vem junto com o Anaconda. 
Apesar da interface do Spyder parecer um tanto complexa por reunir muitas ferramentas, não se assuste. Uma vez que você entenda o funcionamento e o propósito de cada uma, elas lhe serão muito úteis no desenvolvimento dos seus programas pois permitem que você teste cada ideia e elimine suas dúvidas rapidamente. 

Caso você não tenha ainda instalado Python em seu computador siga as instruções fornecidas no laboratório do capítulo 1. Quando terminar de instalar, inicie o Spyder. 

Para abrir o Spyder, digite "spyder" na área de busca do Windows (como você fez anteriormente para abrir o Anaconda Prompt). 
A figura 3.2 mostra a tela inicial do Spyder. 
Ela deve aparecer com essas 3 partes (que vamos chamar de janelas). 
Caso você não esteja vendo alguma dessas partes, clique na opção
"Tools" da barra de menu (topo da janela) e depois em 
"Reset Spyder to factory defaults" para que o Spyder volte a ter essa
configuração.

.. figure:: ../Figuras/spyder/spyder01.png
    :alt: Figura 3.2: Tela inicial do Spyder.
    :align: center

    Figura 3.2: Tela inicial do Spyder.

A janela na parte superior direita dessa configuração inicial tem 3 "tabs":
"Variable explorer", "File explorer" e "Help". Selecione (clique) o tab "Variable explorer" para que o Spyder exiba os valores das variáveis que você utilizar (como na figura). O tab "Help" possui informações sobre o Spyder e o tab "File explorer" ajuda você a achar arquivos na sua máquina.

A janela na parte inferior direita tem 2 tabs. Selecione o tab "IPython console". O IPython console (que vamos chamar simplesmente de IPython) é a mesma ferramenta que usamos no laboratório do capítulo anterior, mas integrada ao Spyder. 


Usando o editor do Spyder para escrever programas em Python
...........................................................

Vamos agora escrever um programa simples utilizando o Spyder. 
O programa deve ler dois inteiros ``a`` e ``b`` e imprimir sua soma.

Uma primeira tentativa pode ser a seguinte:

.. code-block:: Python

    a = input("Digite o valor de a: ")
    b = input("Digite o valor de b: ")
        soma = a + b
        print("A soma de a + b é: ", soma)

Copie esse trecho de código no editor do Spyder e **salve** o programa.

Para salvar o programa em um arquivo no computador que você estiver usando, clique na opção "File -> Save as" na barra de menu do Spyder. O nome precisa terminar com a extensão ".py" para indicar que é um arquivo em Python, como "primeiro_prog.py". 

Depois de salvo, execute o programa utilizando a opção "Run -> Run" do menu (ou use a tecla de atalho 'F5'). O Spyder oferece vários "atalhos" (combinações de teclas) que você pode aprender para agilizar o seu trabalho. Essas combinações de teclas existentes são mostradas no canto direito de cada opção do menu. 

O programa é executado na janela do IPython.
Se você digitou o programa exatamente como ilustrado, o Python deve indicar um erro de tabulação (*IndentationError*) no console ou shell. Isso porque a coluna onde cada linha inicia indica ao Python a que bloco a linha pertence. Por isso, a primeira linha de código **precisa** começar na primeira coluna do editor (ou seja, sem nenhum espaço no início da linha). 

Para corrigir esse problema, remova todos os espaços iniciais de todas as linhas como:

.. code-block:: Python

    a = input("Digite o valor de a: ")
    b = input("Digite o valor de b: ")
    soma = a + b
    print("A soma de a + b é: ", soma)

salve e execute novamente. 

O Python não deve indicar nenhum erro dessa vez. Se para ``a`` digitarmos o 3 e para ``b`` digitarmos 4, a saída do programa nesse caso é '34'. 

Isso porque "esquecemos" que a função ``input()`` devolve uma string, que precisa ser convertida para um inteiro. Isso pode ser corrigido convertendo-se cada string para inteiro da seguinte maneira:

.. code-block:: Python
    :linenos:

    a_str = input("Digite o valor de a: ")
    a_int = int(a_str)
    b = int(input("Digite o valor de b: "))
    soma = a_int + b
    print("A soma de a + b é: ", soma)

Nesse programa, ``a_str`` recebe uma string que é convertida para um valor inteiro pela função ``int()`` e esse valor inteiro é atribuído para a variável ``a_int``. 
A linha 3 mostra que essas funções podem ser compostas para produzir um código mais compacto mas ainda legível. 

Salve e execute esse programa e verifique se ele se comporta como esperado.


Usando o iPython dentro do Spyder
.................................

Observe a sequência de comandos digitados na janela do iPython na figura 3.3 e
que mostra as janelas "Variable explorer" e "IPython console" do Spyder (as duas janelas do lado direito) após a execução dos comandos. 
Procure digitar e executar a mesma sequência no iPython do Spyder em seu computador.

.. figure::  ../Figuras/spyder/console01.png
    :alt: Figura 3.3: Janelas "Variable explorer" e "IPython console" do Spyder.
    :align: center

    
Quando o Python é iniciado nenhuma variável está definida. Assim ao digitarmos a palavra (nome da variável) ``pi``, o Python devolve o erro ``NameError`` indicando que o nome ``pi`` ainda não foi definido. 

O comando de atribuição digitado na linha [2] faz com que a variável com nome ``pi`` seja criada e associada ao valor da expressão do lado direito do comando, nesse caso, ``3.14``. 
Note que na janela "Variable explorer" a variável ``pi`` é criada com esse valor. 

Observe que o comando de atribuição na linha [2] não gera uma saída, ou linha com ``Out[2]``. Lembre-se que o shell interpreta e executa comandos. No caso de uma atribuição, uma variável é criada ou atualizada (modificada). Sem o comando de atribuição ``=``, o Python tenta entender o nome como alguma variável conhecida e resolver uma expressão usando o seu valor associado. 
Observe que, ao digitarmos novamente o nome ``pi`` como na linha [3], o IPython reconhece o nome e resolve a expressão substituindo o nome pelo valor associado (armazenado na memória como mostrado na janela Variable explorer). 

A linha [4] é semelhante à linha [2] e cria uma nova variável de nome ``novo_pi``. Como a expressão do lado direito do símbolo ``=`` contém apenas a variável ``pi``, o **valor** referenciado por ``pi`` é atribuído à ``novo_pi`` (veja o que acontece na janela "Variable explorer" após a execução de cada comando de atribuição). 

A linha [5] mostra como uma variável pode ser atualizada. Primeiro a expressão à direita é calculado usando o valor atual de ``pi`` e o novo valor (6.28) é atribuído a própria variável. Observe que a mesma variável ``pi`` tem um valor associado **antes** da atribuição, quando a expressão é calculada, e um valor diferente **após** a execução do comando.

A linha [6] mostra o novo valor de ``pi`` (ou calcula o valor da expressão, que nesse caso é o valor associado a ``pi``) e a linha [7] mostra  que a variável ``novo_pi`` **não é alterada**, mesmo tendo recebido o valor associado a ``pi`` na linha [4].
Ou seja, cada variável faz referência ao último valor que lhe é atribuído e, portanto, os valores antigos são perdidos a cada nova atribuição. 
            
.. break

Teste o seu conhecimento
------------------------

.. mchoice:: questao_tabulacao
    :answer_a: todos os blocos foram digitados corretamente.
    :answer_b: apenas os blocos A e B
    :answer_c: apenas o bloco C
    :answer_d: apenas o bloco D
    :correct: c
    :feedback_a: Incorreto: lembre-se que a tabulação é importante em Python.
    :feedback_b: Incorreto: lembre-se que a tabulação é importante em Python.
    :feedback_c: Correto. As linhas devem começar na primeira coluna.
    :feedback_d: Incorreto: lembre-se que a tabulação é importante em Python.

    Cada bloco corresponde a um programa em Python com apenas 2 linhas. Qual desses programas foi digitado corretamente no editor do Spyder?

    .. code-block:: python

        # Bloco A
        nome = input('Digite seu nome: ')
            print('Olá', nome)

        # Bloco B
            nome = input('Digite seu nome: ')
        print('Olá', nome)

        # Bloco C
        nome = input('Digite seu nome: ')
        print('Olá', nome)

        # Bloco D
            nome = input('Digite seu nome: ')
            print('Olá', nome)


Esse exercício é um típico programa para processar dados.

.. activecode:: ac03_conversor_CF
    
    :caption: Conversor Celsius para Farenheit

    Escreva um programa que leia uma temperatura em 
    graus Celsius e converta para Fahrenheit onde:

    F = C * 9 / 5 + 32

    ~~~~
    # Pense: o programa deve ler um int ou float?
    # 
    # Pense: o programa deve imprimir uma mensagem?
    #
    # Veja que o enunciado não manda imprimir o resultado. 
    # Isso não quer dizer que o programa não imprime uma mensagem
    # mas sem uma definição, a mensagem tem um formato livre.


.. break

Onde chegamos e para onde vamos?
--------------------------------

Nesse capítulo vimos que é muito importante especificar os tipos de dados de entrada e saída para que os programas possam se comunicar. 

Vimos como usar variáveis para poder ter acesso a esses dados e também armazenar valores intermediários necessários para o processamento dos dados. 

Os programas que vamos desenvolver nesse curso devem ser executados em um terminal e a interação com o usuário será por meio de mensagens de texto, usando a função de entrada de texto ``input()`` e a de saída ``print()``. 

Como essas funções só trabalham com textos (strings), muitas vezes é necessário transformar as strings em outros tipos de dados mais apropriados à aplicação, usando as funções de conversão de tipos como ``int()``, ``float()`` e ``bool()``. 

O comando de atribuição, usado para criar e modificar os valores associados a variáveis, é um dos conceitos mais fundamentais desse capítulo. Pelo símbolo utilizado, é muito importante não confundir esse comando com "igualdade".

O próximo capitulo introduz outro conceito fundamental, ainda bastante ligado à lógica matemática, e que vai nos permitir tratar problemas bem mais complexos que os vistos até aqui.






