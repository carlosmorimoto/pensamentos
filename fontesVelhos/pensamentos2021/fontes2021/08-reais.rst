..  shortname:: Tópico: números reais
..  description:: Tipo float


Números reais
=============

.. index:: float, float()
.. index:: /, //


Tópicos
-------

    - Leia sobre o tipo ``float`` na seção `Variáveis de tipos de dados <https://python.ime.usp.br/pensepy/static/pensepy/02-Conceitos/conceitos.html#variaveis-e-tipos-de-dados>`__.
    - Leia sobre a função de conversão ``float()``, que converte de ``str`` para ``float``, nas seções 
        - `Funções para conversão de valores <https://python.ime.usp.br/pensepy/static/pensepy/02-Conceitos/conceitos.html#funcoes-para-conversao-de-valores>`__ e 
        - `Operadores e operandos <https://python.ime.usp.br/pensepy/static/pensepy/02-Conceitos/conceitos.html#operadores-e-operandos>`__. 
        - Veja a diferença entre ``/`` e ``//`` e como ``%`` se comporta com ``float``.

    - Exercícios com o tipo `float`.

.. break

Vídeos
------

    - `Valores e tipos em Python <https://www.youtube.com/watch?v=UZ7_oudJ150&index=6&list=PLcoJJSvnDgcKpOi_UeneTNTIVOigRQwcnVídeos------>`__;
    - `Exercício Resolvido: Bhaskara <https://www.youtube.com/watch?v=v_QoUdfahng&index=11&list=PLcoJJSvnDgcKpOi_UeneTNTIVOigRQwcn>`__.

.. break
      
Introdução
----------

Um **número real** em um computador é representado na notação de `ponto
flutuante <https://pt.wikipedia.org/wiki/Ponto_flutuante>`__ (do inglês
`floating point`). Dada a limitação de dígitos imposta por essa notação, os
resultados dos cálculos realizados pelos computadores são aproximações dos
valores reais. Por exemplo, o valor real de `Pi` tem infinitas casas decimais
mas, no computador, é necessário limitar o número de casas decimais,
aproximando o valor de `Pi`.

Para que possamos confiar nos resultados, uma *boa prática* é tentar minimizar
a propagação de erros devido ao uso dessas aproximações. Assim, em geral:

    - Operações de divisão e multiplicação são "seguras", podem ser executadas em qualquer ordem.
    - Adições e subtrações são "perigosas", pois quando números de
      magnitudes muito diferentes são envolvidos, os dígitos menos
      significativos são perdidos.
    - Essa perda de dígitos pode ser inevitável e sem consequência
      (quando os dígitos perdidos são insignificantes para o resultado
      final) ou catastróficos (quando a perda é magnificada e causa um
      grande erro no resultado).
    - Quanto mais cálculos são realizados (em particular quando os
      cálculos são feitos de forma iterativa), mais importante é
      considerar esse exercício.
    - O método de cálculo pode ser estável (tende a reduzir os erros
      de arredondamento) ou instável (tende a magnificar os
      erros). Frequentemente, há soluções estáveis e instáveis para um
      exercício.
    - Para saber mais dê uma olhada em http://floating-point-gui.de/errors/propagation.


Se você já aprendeu a utilizar funções,
você pode escrever as soluções dos exercícios a seguir usando funções.

.. break

Exercícios 
----------

Exercício 1
...........

Nota: exercício 2 da lista de exercícios sobre reais.

Dado um número inteiro :math:`n > 0`, determinar o número harmônico
:math:`H_n` dado por

.. math::
   
    H_n = 1 + \frac{1}{2} + \frac{1}{3} + \frac{1}{4} + \ldots + \frac{1}{n}

Imprima cada termo da sequência e o resultado final.
Faça 2 implementações:

    - 1) Da direita para a esquerda e
    - 2) da esquerda para direita.

Qual dessas formas é estável?

Observe que para :math:`n = 10`, os resultados são iguais, mas para
:math:`n = 100 \ldots`

.. activecode:: aula_reais_numero_harmonico_tentativa

    def main():
        # escreva o seu programa abaixo e remova o print()
        print("Vixe! Ainda nao fiz o Exercício!")
                
    #-----------------------------------------------------            
    if __name__ == '__main__': # chamada da funcao principal 
        main() 


Exercício 2
...........

Nota: exercício 6 da lista de exercícios sobre reais.

Dados :math:`x` real e :math:`n` natural, calcular uma aproximação para :math:`\cos(x)` através dos :math:`n` primeiros termos da seguinte série

.. math::
   \cos(x) = 1-\frac{x^2}{2!}+\frac{x^4}{4!}-\frac{x^6}{6!}+\ldots+(-1)^k \frac{x^{2k}}{2k!} + \ldots

Compare com os resultados de sua calculadora!

.. activecode:: aula_reais_cos

    def main():
        # escreva o seu programa abaixo e remova o print()
        print("Vixe! Ainda nao fiz o Exercício!")
                
    #-----------------------------------------------------            
    if __name__ == '__main__': # chamada da funcao principal 
        main() 

Exercício 3
...........

Nota: exercício 7 da lista de exercícios sobre reais.

Dados :math:`x` real e :math:`\epsilon` real, :math:`\epsilon > 0`, calcular uma aproximação para :math:`\sin(x)` através da seguinte série infinita:

.. math::
   \sin(x) = \frac{x}{1!}-\frac{x^3}{3!}+\frac{x^5}{5!}+\ldots+(-1)^k \frac{x^{2k+1}}{(2k+1)!} + \ldots


incluindo todos os termos até que :math:`\frac{|x^{2k+1}|}{(2k+1)!} < \epsilon`

Compare com os resultados de sua calculadora!

.. activecode:: aula_reais_sin

    def main():
        # escreva o seu programa abaixo e remova o print()
        print("Vixe! Ainda nao fiz o Exercício!")
                
    #-----------------------------------------------------            
    if __name__ == '__main__': # chamada da funcao principal 
        main() 

Exercício 4
...........

Nota: Questão 1 da `Prova 1 de 2014 <http://www.ime.usp.br/~mac2166/provas/P1-2014.html">`__.


Na figura, no plano cartesiano, a região sombreada não inclui as
linhas de bordo. Note que o eixo ``y`` cai bem no meio da figura, 
e usamos o lado do quadrado para indicar as ordenadas 
correspondentes.

Escreva na página do desenho um programa que lê as coordenadas
cartesianas ``(x, y)`` de um ponto, ambas do tipo ``float``
e imprime ``dentro`` se esse ponto está na região, e ``fora``
caso contrário.

.. image:: ./Figuras/face.png


.. activecode:: aula08_ex81_tentativa

    def main():
        # escreva o seu programa abaixo e remova o print()
        print("Vixe! Ainda nao fiz o Exercício!")
                
    #-----------------------------------------------------            
    if __name__ == '__main__': # chamada da funcao principal 
        main() 

Clique
    - `aqui <exercicios/ex81a.html>`__ para ver uma 1a solução.
    - `aqui <exercicios/ex81b.html>`__ para ver uma 2a solução.
    - `aqui <exercicios/ex81c.html>`__ para ver uma 3a solução.
    - `aqui <exercicios/ex81d.html>`__ para ver uma 4a solução.
    - `aqui <exercicios/ex81e.html>`__ para ver uma 5a solução.
    - `aqui <exercicios/ex81f.html>`__ para ver uma 6a solução.

    

Exercício 5
...........

Dado um número real :math:`x` e um número real :math:`\text{epsilon} > 0`, calcular uma
aproximação de :math:`e^x` através da seguinte série infinita:

.. math::
   
    e^x = 1 + x + \frac{x^2} {2!} + \frac{x^3}{3!} + \ldots + \frac{x^k}{k!} + \ldots

Inclua na aproximação todos os termos até o primeiro de valor absoluto
(módulo) menor do que epsilon.

.. activecode:: aula_reais_e_x

    def main():
        # escreva o seu programa abaixo e remova o print()
        print("Vixe! Ainda nao fiz o exercicio!")
                
    #-----------------------------------------------------            
    if __name__ == '__main__': # chamada da funcao principal 
        main() 


Exercício 6
...........

Dados números reais :math:`x >= 0` e :math:`\text{epsilon} > 0`, calcular uma aproximação da raiz quadrada de :math:`x` através da seguinte sequência:
    - :math:`r_0 = x` e 
    - :math:`r_{n+1} = (r_n+ \frac{x}{r_n}) / 2`.

Exemplos:
    - Para :math:`x = 3, r_0 = 3, r_1 = 2, r_2 = 1.75, r_3 = 1.732143, r_4 = 1.732051`
    - Para :math:`x = 4, r_0 = 4, r_1 = 2.5, r_2 = 2.05, r_3 = 2.000610, r_4 = 2.000000`
    - Para :math:`x = 5, r_0 = 5, r_1 = 3, r_2 = 2.33, r_3 = 2.238095, r_4 = 2.236068`
    - Para :math:`x = 0.81, r_0=0.81, r_1=0.905, r_2=0.9000138122, r_3=0.9000000001`
    
A aproximação será o primeiro valor :math:`r_{n+1}` tal que :math:`|r_{n+1}-r_n| < \text{epsilon}`.


.. activecode:: aula_reais_raiz_quadrada

    def main():
        # escreva o seu programa abaixo e remova o print()
        print("Vixe! Ainda nao fiz o exercicio!")
                
    #-----------------------------------------------------            
    if __name__ == '__main__': # chamada da funcao principal 
        main() 

      

Exercício 7
...........
      

Nota: `Exercício 4 da lista de exercícios com funções - parte I <http://www.ime.usp.br/~macmulti/exercicios/funcoes1/index.html>`__

**Parte A**

Faça uma função `arctan` que recebe o número real :math:`x \in [0,1]` e devolve uma aproximação do arco tangente de :math:`x` (em radianos) através da série incluindo todos os termos da série

.. math::

    \text{arctan}(x) = x - \frac{x^3}{3} + \frac{x^5}{5} - \frac{x^7}{7} + \ldots 

incluindo todos os termos da série até

.. math::

    \big\| \frac{x^k}{k} < 0.0001 \big\| 

    
.. activecode:: aula15_ex01_a_tentativa

    # escreva o seu programa abaixo e remova o print()
    print("Vixe! Ainda nao fiz esse exercicio!")

   

**Parte B**

Faça uma função `angulo` que recebe um ponto de coordenadas
cartesianas reais :math:`(x,y)`, com :math:`x > 0` e :math:`y > 0`
e devolve o ângulo formado pelo vetor :math:`(x,y)` e o eixo horizontal.

A tabela abaixo mostra exemplos com ângulos correspondentes a algumas
coordenadas:

.. table:: Exemplos

    ======  ==============
    (x, y)  ângulo	   
    ======  ==============
    (0, 1)  90 graus
    (2, 2)  45 graus
    (1, 4)  75 graus
    (5, 1)  11 graus
    ======  ==============

Use a função do item anterior mesmo que você não a tenha feito. Note
que a função só calcula o arco tangente de números entre 0 e 1, e o
valor devolvido é o ângulo em radianos
(use o valor  = 3.14 radianos = 180 graus). 
Para calcular o valor do ângulo pedido, use a seguinte fórmula:

.. math::

    \begin{array}{lll}
    \alpha = & \text{arctan}( y/x ) & \text{caso} y < x ; \text{ou}\\    
    \alpha = & \pi/2 - \text{arctan}( x/y ) & \text{caso contrário}\\
    \end{array}

.. activecode::  aula15_ex01_b_tentativa

    # escreva o seu programa abaixo e remova o print()
    print("Vixe! Ainda nao fiz esse exercicio!")


**Parte C**

Faça um programa que, dados n pontos do primeiro quadrante
(:math:`x > 0 \text{e} y > 0`)
através de suas coordenadas cartesianas, determina o ponto que
forma o menor ângulo com o eixo horizontal. Use a função do item
anterior, mesmo que você não a tenha feito . 


.. activecode:: aula15_ex01_c_tentativa

    # escreva o seu programa abaixo e remova o print()
    print("Vixe! Ainda nao fiz esse exercicio!")


Exercício 8
...........

**Parte A**

Escreva uma função com protótipo

.. sourcecode:: python

    def divide (d, m, n):
        ''' (int, int, int) -> bool, int, int
	    '''
	    
que recebe três inteiros positivos `m, n, d`, e retorna
`False, m, n` caso `d` não for divisor de `m` ou `n`.
Caso contrário, 
função retorna
`True`, `m'` e `n'`, onde
`m'=m/d` caso `m` for múltiplo de `d` e `m' = m` caso contrário, e 
`n'=n/d` caso `n` for múltiplo de `d` e `n' = n` caso contrário.

Exemplos:

.. table:: divide(d, m, n)

    ==========    =================
     d,  m,  n    resultado
    ==========    =================
    2,  5,  15    False, 5, 15
    3,  7,   9    True, 7, 3
    3,  9,  21    True, 3, 7
    ==========    =================
    

Escreva a sua solução abaixo:

.. activecode:: aula15_ex02_a_tentativa

    # escreva o seu programa abaixo e remova o print()
    print("Vixe! Ainda nao fiz esse exercicio!")


     

**Parte B** 

Escreva um programa que lê dois inteiros positivos `m` e `n` e
calcula, usando a função acima, o mínimo múltiplo comum entre `m` e `n`.



.. activecode:: aula15_ex02_b_tentativa

    # escreva o seu programa abaixo e remova o print()
    print("Vixe! Ainda nao fiz esse exercicio!")


      
Exercício 9
...........

**Parte A**

Faça uma função com protótipo

.. sourcecode:: python

    def somabit (b1, b2, vemum):
        ''' (int, int, int) -> int, int
        ''' 
      
que recebe três bits (inteiros 0 ou 1) `b1`, `b2` e `vemum` e retorna
dois bits, um bit `soma` representando a soma de
`b1 + b2 + vemum` e o bit `vaium` para o próximo par de bits.
      

.. activecode:: aula15_ex03_a_tentativa

    # escreva o seu programa abaixo e remova o print()
    print("Vixe! Ainda nao fiz esse exercicio!")


**Parte B**

Escreva um programa que lê dois números em binário e calcula um
número em binário que é a soma dos dois números dados. Utilize a
função da parte A.
      

.. activecode:: aula15_ex03_b_tentativa

    # escreva o seu programa abaixo e remova o print()
    print("Vixe! Ainda nao fiz esse exercicio!")


