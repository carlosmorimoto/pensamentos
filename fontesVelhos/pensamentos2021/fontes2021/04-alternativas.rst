Introdução
----------

.. index:: if, if-else, if-elif-else

.. index:: % 
  
.. raw:: html

    <div align="right">
    <p></p>
    <blockquote>
    <p>
    "
    Se correr o bicho pega.</br>
    Se ficar o bicho come. 
    "
    </p>
    <p>Ditado popular</p>
    </blockquote>
    <p></p>
    </div>

O foco do capítulo anterior foi na entrada e na saída dos dados, que permitem que programas
se comuniquem em particular com as pessoas. 
Realçamos a importância da escolha dos tipos para representar cada dado e
aprendemos a salvar e manipular dados por meio de variáveis. 

O processamento dos dados que realizamos até aqui ficou limitado ao cálculo de expressões, 
feito de forma **linear** ou **sequencial** como no caso da conversão de uma temperatura em 
Celsius para Fahrenheit.
Nesse processamento há uma sequência de instruções em que cada uma é executada, uma após a outra.

Soluções de problemas mais complexos requererem o tratamento de casos
alternativos, criando bifurcações no fluxo de execução fazendo que nem
toda instrução seja executada.
Em forma não linear alguns trechos de código são pulados ou tratados de forma alternativa.
Por exemplo, um caminho da bifurcação pode cuidar da conversão de graus Celsius para Fahrenheit,
enquanto que um outro caminho alternativo pode fazer a conversão de Fahrenheit para Celsius.
Para isso, é necessário instruir o computador para executar certos trechos do programa apenas
quando alguma condição é satisfeita: o valor recebido está em graus Celsius e deve ser convertido para Fahrenheit ou
está em Fahrenheit e deve ser convertido para Celsius.

A identificação e decomposição de um problema em alternativas,  
definição das condições e da lógica para aplicação dessas condições 
é o fundamento do pensamento computacional que vamos treinar nesse capítulo.

.. break

Para onde vamos
-------------------

Depois do treinamento neste capítulo você terá aplicado e identificado situações para o emprego:

    - de execução *condicional* com ``if``;
    - de execução *alternativa* com  ``if-else``;
    - de execução *em cadeia* com  ``if-elif-else``;
    - de condições na forma de expressões relacionais e lógicas; e
    - do operador de resto de divisão ``%``.

..     - Usar o operador de divisão inteira ``//``; e
  
.. break

Alternativa simples
-------------------

Antes de pensar na solução é necessário ter um profundo entendimento do problema. 

Ao pensar no problema muitas alternativas tendem a surgir naturalmente.
Problemas muito complexos apresentam um grande número de casos que precisamos considerar.
Para tratar problemas complexos assim, precisamos quebrar o problema em subproblemas mais simples, pedaços ou
"classes" de problemas e depois quebrar as classes em subclasses, e
assim por diante, até que consigamos resolver o subproblema sem
precisar decompo-lo.

Para ajudar você a pensar no problema, um primeiro passo portanto é
identificar as alternativas ou classes como, por exemplo, escolher se
devemos ficar ou correr.  A seguir podemos tentar identificar
propriedades dos dados que nos permita tomar decisões como, se o bicho
for gordo e lento a gente corre, e  o bicho for manso a gente
fica, e o bicho for forte e rápido...
A identificação dessas classes e propriedades fazem parte do modelamento do problema.

Problemas simples tem poucas classes e propriedades. Por exemplo, para
saber se uma pessoa tem idade para tirar a carteira de motorista,
podemos testar a propriedade "idade" e caso a idade for maior ou igual
a 18 anos, então a pessoa pode tirar a carteira.
Caso contrário, a pessoa não pode.
Essa situação tem portanto apenas 2 classes, que
são as das pessoas com 18 ou mais anos e as com menos de 18 anos de idade.

Assim, um problema mais simples de classificação surge quando podemos
dividir o problema original em apenas duas classes como, por exemplo, maior ou
menor, com ou sem fome, dentro ou fora, alto ou baixo, certo ou
errado, verdadeiro ou falso etc.


.. break

Estratégia para resolver um problema de classificação
.....................................................

Uma estratégia para resolver um problema binário de classificação, ou
seja, um problema que tem apenas duas classes, é criar uma **variável de estado** ou **indicadora**.
Essa variável indicará o subproblema que deverá ser tratado.
Inicialmente, a variável é deixada arbitrariamente em um dos possíveis estados.
Em seguida um teste é realizado para garantir que a variável está no estado correto ou
que o seu estado deverá ser alterado. 

Considerando o problema anterior de decidir se uma pessoa tem idade para
ter uma carteira de motorista, que é de 18 anos, podemos pensar da seguinte forma

#. Suponha que a pessoa tem menos de 18 anos 
#. Se a idade da pessoa for maior ou igual a 18:
    #. então agora sabemos que a pessoa tem menos de 18 

O `Se a idade...` é o ponto que, digamos, verificamos a carteira de identidade da pessoa.
Após seguir os passos acima saberemos se a pessoa em questão pode ou não tirar sua carta de motorista.

Vamos considerar um outro exemplo.
Suponha que uma aluno está aprovado em uma disciplina se sua nota é maior ou igual a 5.
Para decidirmos se o aluno está aprovado podemos seguir passos idênticos aos que fizemos acima. 
Consideraremos duas classes: aprovado e reprovado.
Construirmos uma solução supondo que o aluno está em uma classe, digamos na classe dos aprovados. 
Podemos indicar isso usando uma variável de estado ``aprovado`` e atribuir o valor booleano ``True``
como ``aprovado = True``. É evidente que essa nossa suposição inicial pode estar incorreta e,
para corrigir isso, podemos olhar o histórico escolar e testar
"se a nota é menor que 5" e, se for o caso, alteramos a condição do aluno para aprovado fazendo ``aprovado = False``.

Uma outra solução possível seria inicialmente supor que o aluno está reprovado, indicando
isso na nossa variável de estado fazendo ``aprovado = False``.
Essas duas alternativas de solução estão
ilustrados na figura 4.1, na forma de *fluxogramas* e também de *pseudocódigos*.
Apesar dessas duas  estratégias, políticas ou algoritmos serem idênticos, há situações em que a seleção arbitrária do estado inicial pode tornar a solução mais simples ou mais rebuscadas.

..  O fonte da figura está em:
    https://app.diagrams.net/#G1EDPqosSsuMViX4KcJVdpB-7F2ax-Sb__


.. figure:: ../Figuras/cap04/alternativa_simples.png
    :align: center
    :alt: Figura 4.1: alternativas para classificar um aluno

    Figura 4.1: Fluxogramas e pseudocódigo facilitam o pensamento.
    
Um **fluxograma** é uma forma gráfica para visualizarmos fluxo de execução em que os dados são processados.

Passemos a examinar mais atentamente fluxograma da esquerda.
O  "Início" indica o primeiro bloco de instruções a serem executados.
Nesse bloco a nota é lida e a variável de estado ``situação`` recebe um valor é inicial supondo
que o aluno está "aprovado". 

Depois deste bloco o processamento segue para o losango que contém
teste que será feito.  O resultado deste teste ou condição pode ser
verdadeiro (``True``) ou falso (``False``).  A partir do losango, o
fluxo de execução seguirá o caminho indicado pela seta com rótulo
correspondente ao resultado da condição.  Observe que há um *desvio*
do fluxo quando a condição ``nota < 5.0`` no losango é satisfeita e o
seu resultado é ``True``.  Nesse caso a classe do aluno indicada por
``situação`` será modificada de "aprovado" para "reprovado".  Após a
execução condicional desse bloco o processamento volta ao seu curso
normal, imprimindo o valor "aprovado" ou "reprovado" de ``situação``.
No caso da condição da condição ``nota < 5.0`` ser ``False`` o
processamento segue direto para o bloco que imprime o valor de
``situação``.

No fluxograma da direita mostra que alternativamente podemos
inicializar ``situação`` no estado "reprovado".  Nesse casso é
necessário também modificar a condição no losango para ``nota >= 5.0``
e alterar o estado de ``situação`` de "reprovado" para "aprovado" se
essa nova condição for ``True``.

Uma outra forma de pensar é usar **pseudocódigo**, como ilustrado na
parte inferior da figura 4.1.  Note a  tabulação da quarta linha no
pseudocódigo. Essa tabulação indica que a instrução da linha está subordinada ao
resultado da condição; ela só será executada se a condição for ``True``.

A diferença entre os dois pseudocódigos apresentados está em uma
*decisão de projeto*. No pseudocódigo da esquerda optamos por escrever
``situação = "aprovado"`` declarando assim que o estado inicial, ou
*default*, do aluno é "aprovado". No restante do código procuramos
certificar essa decisão inicial. Após verificar a nota, se necessário,
modificando o estado fazendo ``situação = "reprovado"``.
O pseudocódigo da direita corresponde ao estado default de "reprovado".

Fluxogramas e pseudocódigos são usados para escrevermos soluções ou
esboços de solução de uma forma mais solta sem nos preocuparmos
prematuramente com detalhes da linguagem de programação.
Isso permite que tenhamos mais liberdade para concentrar o nosso
pensamento no projeto da solução.  

A seguir veremos como transformar soluções na forma de fluxograma ou pseudocódigo para um
script na linguagem Python.

.. vale a pena sempre ficar trocando "classe" e "estado"?

.. break

Comando ``if``        
..............

O pseudocódigo ilustrado na figura 4.1 está em português, mas adota uma estrutura muito semelhante com Python.
Se trocarmos "leia a nota" pela função ``input()``, e traduzirmos a "se" para ``if`` estaremos muito próximos de um
trecho de código válido em Python, como o ``CodeLens`` abaixo pode atestar.
Execute esse programa passo-a-passo com valores diferentes de nota, como ``4.5``, ``5.0`` e ``6.2``,
para monitorar quando a variável ``situacao`` muda de valor.
Como nomes de variáveis nos códigos em Python não usaremos acentuação como em ``éãüàê`` ou símbolos gráfico como ``ç``.

.. activecode:: cl04_pseudo_se_para_if_python
                                            
    nota = float(input('Digite a nota: ')) # linha 1
    situacao = 'aprovado'                  # linha 2 
    if nota < 5.0:                         # linha 3  
        situacao = 'reprovado'             # linha 4
    print( situacao )                      # linha 5

.. admonition:: **Como funciona esse trecho?**

    Inicialmente, na ``linha 1``, o programa lê um valor e associa a variável ``nota`` a este valor.
    Assim, ``nota`` passa a ser uma referência ou apelido para o valor lido.
    Em seguida, na ``linha 2`` é criada a variável de estado ``situacao`` associada a um valor arbitrário com um valor apropriado, o programa testa se a nota é
    menor que 5.0 na ``linha 3``. Se o resultado da condição for
    ``True``, o programa altera ``situacao`` de ``'aprovado'`` para
    ``'reprovado'``.  Se o resultado da condição for ``False``, a variável
    ``situacao`` fica inalterada pois nossa hipotése inicial se confirmou.
   
    Veja que nesse exemplo a situação do aluno é definida por uma string ``'aprovado'`` ou ``'reprovado'``.
    Isso permite que o resultado seja impresso diretamente na ``linha 5``.
    Entretanto, a variável de estado poderia armazenar qualquer coisa para indicar a situação, como um valor booleano ``True`` ou ``False``. 

    Em português ``if` significa *se*. 
    O comando ``if`` permite que uma parte do programa seja executada apenas quando uma certa condição é ``True``.
    A sintaxe do comando ``ìf`` é a seguinte::

..   Sintaxe do comando if

.. code-block:: Python

    if condição_do_if:
        # bloco executado se a condição for verdadeira
        comando_1
        comando_2
        ...
        comando_m       # último comando do bloco
        
    comando_após_if

Apenas se ``condição_do_if`` for ``True``, o bloco contendo os comandos ``comando_1``  a ``comando_m`` é executado.
Em caso contrário, se ``condição_do_if`` for ``False``, esse bloco não é executado, ele é "pulado",  e a execução do programa continua com o
comando ``comando_após_if``. Observe com **muita atenção** os dois pontos ``:`` e a tabulação das linhas seguintes.
Essa tabulação é absolutamente fundamental para indicar indica o bloco ou lista de comandos que serão executados se ``condição_do_if`` é ``True``.
Veremos isto várias outras vezes, mas ajuda a nos habituarmos e notarmos que em  Python os dois pontos ``:``
indicam que estamos prestes a definir uma lista de comando que serão executados como uma unidade, um bloco.

 
.. admonition:: **Como testar o seu programa**

    Quanto mais complexo um algoritmo, mais fácil é esquecer     de tratar alguma classe.
    Uma forma de melhorar a qualidade e correção dos seus programas é **testá-los**.
    Testes não asseguram que um programa está correto, mas são úteis para encontrarmos erro.

    Para testar o seu programa, para cada categoria escolha valores representativos. 
    No caso do exemplo do exercício para verificar se um aluno está aprovado ou reprovado, não deixe de testar o programa com valores
    limites de cada categoria tais como 0.0, 4.9, 5.0, e 10.0. 

    Testar se a entrada de um programa é válida não é uma tarefa fácil.
    Consistência de dados é um exercício a parte.
    Para nos concentrarmos em um exercício por vez, **supermos que a entrada é sempre válida** a não ser que seja explicitamente pedido para que se teste consitência.
    Desta forma, no exercício das notas, você pode supor que os valores fornecidos ao programa são valores ``float`` entre ``0`` e ``10``.
    Portanto, não se preocupe se o valor fornecido ao programa é um ``float`` entre ``0`` e ``10`` ou se é uma string como `'a'` ou se é qualquer outra valor.
    

.. break 

Alternativas mutuamente exclusivas
----------------------------------

Em muitas ocasiões temos que decidir entre duas ou mais possibilidades de ação.
Ir ao cinema ou estudar computação?
Dificilmente faríamos as duas coisas simultaneamente: ir ao cinema estudar computação.
Frequentemente há mais do que duas possibilidades, entretanto no momento nos concentraremos em apenas duas possibilidades.

Do ponto de vista de programação, em várias ocasiões é necessário executar blocos de forma mutuamente exclusiva.
A execução de um exclui a execução da outra e vice-versa.
Essa situação  corresponde a uma bifurcação no caminho do fluxo de execução do programa.
Exatamente igual a uma bifurcação em uma estrada.
Para aqueles e aquelas familiares com o mapa do estado de São Paulo esse bifurcação é semelhante a ir de São Paulo a Campinas pela Rodovia Anhanguera ou pela Rodovia Bandeirantes.
A origem e o destino são fixos, entretendo o caminho que nos leva de um ponto a outro é diferente.

O problema de classificação binária pode também ser entendido como um problema de execução "mutuamente exclusiva",
como ilustra o fluxograma e o código fonte na figura 4.2.

..  O fonte da figura está em:
    https://app.diagrams.net/#G1lKtCwQuhcSWHzf0vq__NtrbnakUUMLde


.. figure:: ../Figuras/cap04/alternativa_if_else.png
    :align: center
    :alt: Figura 4.2: Exemplo de alternativa com execução mutuamente exclusiva para classificar um aluno

    Figura 4.2: Fluxogramas e pseudocódigo do exemplo anterior mas com execução mutuamente exclusiva.

Na figura, o losango com a condição ``nota >= 5`` é um  ponto de bifurcação.
Se ``nota >= 5.0`` é ``False`` o fluxo segue pelo caminho da esquerda e ``situação`` passa a ser ``reprovada``, em caso contrário o fluxo segue o caminho da direita
é ``situação`` será ``aprovada``.

.. break

Comando ``if-else``
...................

Para escrever a execução mutuamente exclusiva em Python podemos utilizar o comando ``if-else`` (que significa *se-senão* em português), cuja sintaxe é a seguinte::

.. Sintaxe do comando if-else

.. code-block:: Python

    if condição:
        # bloco contendo comandos a serem executados 
        dentro_do_if_1
        dentro_do_if_2
        ...
        dentro_do_if_m
    else:
        dentro_do_else_1
        dentro_do_else_2
        ...
        dentro_do_else_n
        
    comando_após_if


Apenas caso a ``condição`` for verdadeira, o bloco contendo os comandos ``dentro_do_if_1``  a ``dentro_do_if_m`` é executado. Caso contrário, esse bloco ``dentro_do_if`` é pulado e o programa executa os comandos ``dentro_do_else_1`` a ``dentro_do_else_n``. Ao final do bloco, a execução do programa continua com o comando ``comando_após_if``.

O fluxograma da figura 4.2 ilustra que, 
ao invés de iniciar uma variável de estado com a aluna em situação de ``aprovada`` como mostrado na figura 4.1, podemos usar essa construção com execução mutuamente exclusiva, como ilustrado pelo seguinte trecho de código em Python (novamente, muito parecido com o pseudocódigo, mas em inglês):


.. activecode:: cl04_if_aprovado_else_reprovado

    nota = float(input('Digite a nota: '))
    if nota >= 5.0:
        situacao = 'aprovada'
    else:
        situacao = 'reprovada'
    print( situacao )


.. admonition:: **Como funciona esse trecho?**

    Logo após ler a nota na linha *1*), a nota é testada na linha *2* e, caso seja maior ou igual 5.0, a situação da aluna é marcada como `aprovada`. Caso contrário, o programa executa o bloco da cláusula ``else`` na linha *5*, marcando a situação como `reprovada`. A última linha imprime a situação da aluna.

    Embora essa solução seja bastante clara e elegante para aplicar o ``if-else``, 
    para situações que dependam de uma **variável de estado**, recomendamos o uso da versão **sem** o ``else``. Além de ser mais curto escrever apenas o ``if``, esse tipo de solução força o pensamento a começar em algum estado (inicialização da variável de estado) e isso pode ajudar a estruturar melhor a solução.

    **Mas então, quando devemos usar o ``else``?** 
    
    A resposta (ao menos por enquanto) é usar o ``if-else`` para evitar a execução de computações desnecessárias e caras. Por exemplo, imagine que o Cebolinha dependa da resposta da Mônica para pintar a casa de vermelho ou de verde. O Cebolinha pode arriscar e primeiro pintar a casa de verde e, dependendo da resposta da Mônica, ter de repintar a casa depois de vermelho. Em pseudo código, isso poderia ser escrito como:

    * pinte a casa de verde
    * leia a cor preferida da Mônica
    * se a cor preferida for vermelho
        * pinte a casa de vermelho

    Veja que há um risco grande do Cebolinha ter de gastar muita tinta seguindo esse algoritmo. Nesse caso, seria mais ecológico seguir os seguintes passos:

    * leia a cor preferida da Mônica
    * se a cor preferida for vermelho
        * pinte a casa de vermelho
    * senão:
        * pinte a casa de verde

    Veja que desse jeito a gente consegue ajudar o Cebolinha a economizar muita tinta e tempo!

    Assim como é bom para o Cebolinha economizar tinta nesse exemplo, ao escrever programas, **procure evitar computações desnecessárias**. O ``else`` pode ser muito útil para isso. No caso da variável de estado, o custo computacional é irrelevante e podemos ganhar na qualidade da solução com uma estratégia mais clara de começar em um estado e ir decompondo o problema em subalternativas.

.. break

Prefira ``if-else`` a ``if-if``
.................................

Muitos programadores novatos tendem a limitar o pensamento a casos específicos, ou seja, tentam tratar um caso de cada vez por meio de vários ``if``s. Esse padrão ``if-if`` é desaconselhável pois torna o programa mais difícil de ler, o que também dificulta corrigir e fazer a manutenção do código. Como exemplo, considere o seguinte trecho de código para determinar se um número é par ou ímpar:

.. activecode:: ac04_nao_use_if_if_se_puder_evitar
    :nocanvas:

    n = int(input("Digite um numero: ")

    if n % 2 == 0:  # se n é múltiplo de 2
        estado = "par"
    if n % 2 != 0:  # se n não é múltiplo de 2
        estado = "ímpar"

    print(f"o número {n} é {estado}")	

.. admonition:: **Como funciona esse trecho?**

    Após ``n`` receber um número, o programa testa, na linha *3*, se ``n`` é múltiplo de 2
    usando o operador aritmético ``%``. Esse operador determina o resto da divisão
    de ``n`` por ``2``. Caso a condição seja verdadeira (= ``True``),
    o programa armazena na variável ``estado`` que o número é par.

    Na linha *5* o programa recalcula o resto da divisão de ``n`` por
    2 e verifica se o resto é diferente de zero, ou seja, se o número não é
    múltiplo de 2 e, nesse caso, armazena na variável ``estado`` que o número é ímpar.

    Observe que a condição dos ``ifs`` nas linhas *3* e *5* foram executadas,
    mesmo quando na linha *3* o número já foi identificado como par, o teste da
    linha *5* é realizado.

.. break

Pare e pense um instante
........................

Procure pensar em como evitar o ``if-if`` nesse caso, modificando o código acima para (edite no próprio ActiveCode) para:

* **Solução alternativa 1**: usar ``if-else`` ao invés de ``if-if``: altere o ``if`` na linha *5* para usar um ``else``. Observe que essa solução é mais eficiente pois evita a computação de "n não é múltiplo de 2", e também é mais elegante e clara pois o uso do ``else`` indica que o processamento é feito de forma mutualmente exclusiva; 

.. activecode:: ac04_troque_if_if_por_if_else
    :nocanvas:

    n = int(input("Digite um numero: ")

    if n % 2 == 0:  # se n é múltiplo de 2
        estado = "par"
    if n % 2 != 0:  # se n não é múltiplo de 2
        estado = "ímpar"

    print(f"o número {n} é {estado}")	

* **Solução alternativa 2**: usar um só ``if`` ao invés de ``if-if``: simplesmente assumindo que o valor inicial de ``estado = "ímpar"`` antes do primeiro ``if``; e

.. activecode:: ac04_troque_if_if_por_um_if_apenas
    :nocanvas:

    n = int(input("Digite um numero: ")

    if n % 2 == 0:  # se n é múltiplo de 2
        estado = "par"
    if n % 2 != 0:  # se n não é múltiplo de 2
        estado = "ímpar"

    print(f"o número {n} é {estado}")	

* **Outros múltiplos**: modifique a condição do ``if`` para verificar se um número é múltiplo de 3 ou não. Experimente outros números também para entender o operador ``%``.

Embora a solução ``if-if`` produza resultados corretos, o uso do comando ``if-else`` ou apenas o ``if`` torna o programa bem mais elegante e seguro.


.. admonition:: **Variável de estado**

    Pense agora na variável ``estado``. Você já deve ter pensando que essa variável é  inútil e que podemos eliminá-la e colocar ``prints`` nas linhas *4* e *6*, com as mensagens adequadas. 

    Se você pensou assim, com relação ao funcionamento do programa, você tem razão. 

    Por outro lado, gostaríamos que você procure usar esse padrão de pensamento:
    
    #. carregar os dados e definir os estados iniciais;
    #. processar os dados e ir evoluindo os estados até que, só ao final, 
    #. terminar o programa exibindo o resultado. 
   
    Seguiremos esse padrão ao longo desse livro pois facilita a leitura, o entendimento e a manutenção do código. 
    
.. break 


Alternativas múltiplas
----------------------

Tipicamente, quando um programa aumenta de complexidade, o número de alternativas cresce.
Por exemplo, além de aprovado ou reprovado, considere que um aluno pode também ficar de recuperação se sua nota estiver entre 3.0 e 5.0. Há várias formas de resolver esse problema. Vamos discutir por hora apenas a forma usando ``if-else`` aninhados para ilustrar outro recurso do Python para tornar a notação desses casos mais compacta e, portanto, mais simples de ler e manter.

..  O fonte da figura está em:  
    https://app.diagrams.net/#G17T3Xf_-qnTIfeZYPSzWp2HqxVtTE5YQR

.. figure:: ../Figuras/cap04/if-else-if-else.png
    :align: center
    :alt: Figura 4.3: Exemplo de uso de if-else aninhados para classificar um aluno

    Figura 4.3: Fluxogramas e pseudocódigo para classificar a situação de um aluno entre ``aprovado``, ``reprovado`` e de ``recuperação``.  


A figura 4.3 exibe um fluxograma e um pseudocódigo equivalente para classificar a situação de uma aluna como aprovada, reprovada ou de recuperação.
Uma solução **aninhando** comandos ``if-else`` seria:


.. activecode:: acl04_Exemplo_de_uso_do_if-else_aninhados_00

    nota = int(input("Digite uma nota: "))

    if nota >= 5.0:
        situação = "aprovado"
    else:
        if nota >= 3.0:
            situação = "recuperação"
        else:
            situação = "reprovado"
    print( situação )	

       
.. admonition:: **Como funciona esse trecho?**

   Observe com atenção a **tabulação** desse programa, que mostra um ``if-else`` na mesma coluna da linha *1* e, dentro do primeiro ``else``, vemos um outro comando ``if-else``. Por esse deslocamento sabemos que o segundo ``if`` só será testado caso a condição do primeiro ``if`` (da linha *3*) seja falsa.

   Vamos simular esse trecho usando 3 testes: nota 2 (que deve resultar em reprovado), nota 4 (que deve resultar em recuperação) e nota 7 que deve resultar em aprovado. Lembre-se de cobrir todas as alternativas ao definir seus testes.

   Quando a nota é 7, a condição do primeiro ``if`` é verdadeira e assim o programa guarda que a situação do aluno é "aprovado". 
   Quando a condição do ``if`` é verdadeira o ``else`` correspondente não é executado, e o programa termina imprimindo "aprovado", que é o valor associado à ``situação``. 

   Quando a nota é 4, a condição do primeiro  ``if`` é falsa e o bloco dentro do ``else`` correspondente (que está na mesma coluna desse ``if``) é executado, ou seja, o segundo ``if-else`` é executado. Para isso a condição do ``if`` na linha *6* é testada, resultando em verdadeiro. O programa guarda que a ``situação`` do aluno é então de "recuperação", o ``else`` correspondente não é executado e o programa termina imprimindo "recuperação", que é o valor associado à ``situação``. 

   Finalmente quando a nota é 2, a condição dos dois ``ifs`` é falsa e, nesse caso, o bloco dentro do segundo ``else`` (linha *8*) é executado, associando o valor "reprovado" à variável ``situação``. O programa passa para o final, imprimindo o resultado em ``situação``. 



.. break

Comando ``if-elif-else``
........................

Para simplificar ainda mais o código de programas com ``if-else`` aninhados, o Python oferece o comando ``if-elif-else``, cuja sintaxe é a seguinte.


.. Sintaxe do comando if-elif-else

.. code-block:: Python

    if condição:
        # bloco contendo comandos a serem executados 
        dentro_do_if_1
        dentro_do_if_2
        ...
        dentro_do_if_p

    elif condição elif1:
        dentro_do_elif1_1
        dentro_do_elif1_2
        ...
        dentro_do_elif1_m1

    elif condição elif2:
        dentro_do_elif2_1
        dentro_do_elif2_2
        ...
        dentro_do_elif2_m2

    # outros possíveis blocos de elif, cada um com a sua condição 

    elif condição elifk:
        dentro_do_elifk_1
        dentro_do_elifk_2
        ...
        dentro_do_elifk_mk
    
    else:                   # o else é opcional
        dentro_do_else_1
        dentro_do_else_2
        ...
        dentro_do_else_q
    
    comando_apos_if


Ou seja, o ``if-elif-else`` pode ter um ou mais blocos com ``elif``, cada um
com a sua condição específica. Cada bloco (condição) é testado um de cada vez,
até que uma condição seja satisfeita e apenas os comandos dentro desse bloco
são executados. Quando nenhuma condição é satisfeita, os comandos dentro do
``else`` são executados. Assim,  **não** é necessário que haja um ``else``
no final.


    
A figura 4.4 ilustra mostra como a contração de ``else-if`` como ``efif`` (em pseudocódigo estamos usando "senão-se") pode tornar o código mais legível e portanto mais fácil de ler e entender. Como o Python desloca cada bloco de um tab, após vários tabs pode se tornar difícil ler o código que fica espremido no canto direito da página. O pseudocódigo também ilustra esse benefício de formatação e simplificação da leitura do código ao comparar o uso de ``else`` (senão) seguido de ``if`` (se) e de ``elif`` (senão-se junto). 

..  O fonte da figura está em:  
    https://app.diagrams.net/#G1Y2cw7OCcMmKkng7AbzPbM1NKWEjS_Z2l

.. figure:: ../Figuras/cap04/if-elif-else.png
    :align: center
    :alt: Figura 4.4: Exemplo de uso de if-elif-else para classificar um aluno

    Figura 4.4: Fluxogramas e pseudocódigo para classificar a situação de um aluno entre ``aprovado``, ``reprovado`` e de ``recuperação`` usando a contração "senão-se" (``elif``).


Usando ``elif`` poderíamos escrever o algoritmo da figura 4.4 da seguinte forma:
    
.. activecode:: cl04_Exemplo_de_uso_do_if-elif-else

    nota = float(input("Digite uma nota: "))

    if nota >= 5.0:
        situação = "aprovado"
    elif nota >= 3.0:
        situação = "recuperação"
    else:
        situação = "reprovado"
    print( situação )	


O ``elif`` portanto é apenas uma contração do ``else if`` que torna mais claro
o tratamento das várias alternativas, encadeando as condições. Blocos de
``elif`` podem ser repetidos várias vezes. Suponha por exemplo que gostaríamos
de conhecer os alunos aprovados ``com louvor``, ou seja, com nota superior
a 9.0. Nesse caso, uma outra solução seria:

    
.. activecode:: cl04_Outro_exemplo_de_uso_do_if-elif-else
    
        nota = float(input("Digite uma nota: "))
        if nota >= 9.0: 
            situação = "com louvor!"
        elif nota >= 5.0:
            situação = "aprovado"
        elif nota >= 3.0:
            situação = "recuperação"
        else:
            situação = "reprovado"
        print( situação )	


Faça os seus próprios experimentos com o trecho de código a seguir.

.. activecode:: condicao_em_cadeia

    n = 25   # altere esse valor e simule esse trecho novamente
    if n < 0:
        print(n, "< 0")
    elif n == 0:
        print(n, "== 0")
    elif n < 10:
        print("0 < ", n,"< 10")
    elif n < 20:
        print("10 <=", n, "< 20")
    elif n < 30:
        print("20 <=", n, "< 30")
    elif n < 50:
        print("30 <=", n, "< 50")
    else:
        print("50 <=", n)

    print("Termino do teste.")
  
.. break

Exercícios
----------

Nessa seção de exercícios, procure **desenhar** sua solução usando fluxogramas antes de escrever o código em Python. Desenhar a solução e conseguir ver um mapa do fluxo da informação é uma ferramenta poderosa tanto para aprendizes, para desenvolver a habilidade de identificar e organizar essas estruturas, quanto para programadores avançados trabalhando em problemas complexos.


Exercício 1
...........

Escreva um programa que leia dois números inteiros e determina a soma dos pares.
Por exemplo, para os números 6 e 7, 
como apenas o 6 é par, a soma deve ser 6. 
Para os número 1 e 3, o resultado deve ser zero e para 4 e 12, o resultado deve ser 16.

Dica: para resolver esse exercício utilize o operador "%", que retorna o resto da divisão. Assim::

.. Exemplos de operações com %

:: 

    1 % 3 é 1
    2 % 3 é 2 
    3 % 3 é 0
    4 % 3 é 1

.. activecode:: cap04_ex1_soma_pares_tentativa

    # Escreva o seu programa aqui


Exercício 2
...........

Escreva um programa que leia 3 números inteiros e determinar quantos números da sequência são pares e
quantos são ímpares.  Por exemplo, para os números
1, 2 e 3, o resultado deve ser 1 par e 2 ímpares.

.. activecode:: cap04__ex2_conta_pares_e_impares

    # Escreva o seu programa aqui



Exercício 3
...........

Escreva um programa que leia três números naturais e verifica se eles formam os lados de um triângulo retângulo. **DICA** Os números não precisam estar ordenados. Por exemplo, para os número 3, 5 e 4, a resposta deve ser sim, e para 8, 1, 6 a resposta deve ser não. 

.. activecode:: cap04_ex3_lados_triangulo_retangulo

    # Escreva o seu programa aqui


Exercício 4
...........

Escreva um programa que leia três números inteiros
e os imprima em ordem crescente.
Por exemplo, para os número 13, 7 e 4, a resposta deve ser 4, 7 e 13 e para 8, 9, 10 a resposta deve ser 8, 9 e 10.

.. activecode:: cap04_ex4_ordena3

    # Escreva o seu programa aqui

.. break

Laboratório: perdendo o medo do Spyder
--------------------------------------

O último laboratório introduziu o Spyder, o ambiente de desenvolvimento integrado (IDE) que adotamos nesse curso por ser aberto (Open Source), grátis e fácil de instalar. Ele na verdade vem junto com o Anaconda, um pacote que reúne, além do Sypder, vários módulos que facilitam o desenvolvimento de programas científicos em Python. Nesse laboratório vamos reforçar o uso do Spyder e aproveitar para ganhar ilustrar como linhas distintas de pensamento podem ser exploradas para criar várias soluções para um mesmo problema.

Se pudermos comparar "aprender a resolver problemas" com "aprender a andar de bicicleta", muitas vezes o foco é na bicleta que vamos usar. Nessa metáfora, vamos considerar o ambiente de programação como a roupa que a gente precisa "vestir" para poder andar melhor de bicicleta, que ajudam nos ajudam a nos sentir confortáveis e seguros.

Assim como um roupa nova pode parecer justa ou apertada, como pode ser difícil de ajustar o cinto, ou como o capacete pode parecer um pouco pesado e desconfortável, toda ferramenta nova também cria um certo desconforto mas, depois que a gente consegue se acostumar e a roupa se ajusta na gente, o desconforto some, a confiança aumenta e permite que a gente se esqueça do traje e consiga focar em pedalar e, quem sabe, pensar e apreciar a paisagem.

Por isso, sugerimos que você use o Spyder para criar, digitar e executar vários programas nesse laboratório.
Todos esses programas usam algoritmos distintos mas que produzem o mesmo resultado. Gostaríamos que vocês consigam ficar confortáveis o suficiente com o uso do Spyder para conseguirem pensar e apreciar todos os cheiros, cores e texturas de cada alternativa.

.. Mas sabemos que, no início, pode ser díficil mudar de marcha, fazer uma curva, ou desviar de um buraco. Alguns  novatos podem também começar a sentir desconforto e dor em músculos que foram pouco exercitados antes, alguns caem ou se sentem frustrados por não ter a força para pedalar mais rápido. 

Mas para atingir esse estágio a gente vai precisar pedalar bastante. Nesse laboratório, procure digitar caractere a caractere os programas abaixo, ou seja, não use CTRL-C para copiar e CTRL-V para colar o programa no editor. 

O Spyder é uma ferramenta bastante sofisticada que pode ajudar você de várias maneiras. 
Mesmo que você saiba digitar bem rápido, ao digitar várias programas você vai começar a entender e descobrir os recursos que o editor do Spyder lhe oferece. Aprenda a usar as teclas de aceleração como `F5` para executar e CTRL-S para salvar o arquivo (embora algumas combinações dependam do seu sistema operacional), observe que o Python muda a cor de algumas palavras, procure entender o que cada cor significa, procure entender o que o Spyder tenta comunicar sobre o seu programa enquanto você está digitando. Aprenda a configurar o Spyder, modificando a cor de fundo, ou o tamanho da fonte, ou usando um tema diferente para todo o ambiente.

É natural ter um pouco de medo de usar algo complexo e sofisticado, mas quanto mais você usar, mais você vai se sentir confortável e leve usando o Spyder e, quem sabe, até se divertir um pouco!). 

Exercício: classifique um ponto como dentro ou fora
...................................................

Esse exercício foi baseado na questão 1 da `Prova 1 de 2014 <http://www.ime.usp.br/~mac2166/provas/P1-2014.html">`__ da disciplina MAC2166, criada pelo [Prof. Arnaldo Mandel](https://www.ime.usp.br/~am).

Use o Spyder para escrever um programa que lê as coordenadas cartesianas ``(x, y)`` de um ponto, ambas do tipo ``float`` e imprime ``dentro`` se esse ponto está na região, e ``fora`` caso contrário.
A região sombreada não inclui as bordas, ou seja, pontos nas bordas de uma região estão ``fora``.

Note que o eixo ``y`` cai bem no meio da figura, e usamos o lado do quadrado para indicar as ordenadas correspondentes.

.. figure:: ../Figuras/cap04/face.png
    :align: center
    :alt: Figura 4.5: exercício pra classificar os pontos da região sombreada.

    Figura 4.5: exercício pra classificar os pontos da região sombreada. Pontos sobre as bordas deem ser considerados ``fora``.


Para a sua conveniência, você pode executar cada solução abaixo usando o CodeLens. Mesmo assim, digite cada solução no seu computador para "perder o medo" do Spyder.

Solução 0 
.........

Essa solução começa supondo que está ``dentro`` e realiza testes para verificar se o ponto está ``fora``. 
A variável  ``dentro`` é uma variável de estado também chamada de **indicador de passagem** do tipo ``bool``.

.. activecode:: cl04_solucao0_p1_2014

    x = float(input("Digite x: "))
    y = float(input("Digite y: "))

    # suponha que (x,y) esta dentro
    dentro = True

    if x <= -5 or 5 <= x or y <= 0 or 8 <= y:
        # aqui sabemos que (x,y) esta fora da face
        dentro = False
    elif -3 <= x <=  3 and 1 <= y <= 2:
        # aqui sabemos que (x,y) esta na boca
        dentro = False
    elif -4 <= x <= -1 and 4 <= y <= 7:
        # aqui sabemos que (x,y) esta no olho esquerdo
        dentro = False
        if -3 < x < -2 and 5 < y < 6:
            # aqui sabemos que na verdade (x,y) esta na iris esquerda
            dentro = True
    elif  1 <= x <=  4 and 4 <= y <= 7:
        # aqui sabemos que (x,y) esta no olho direito
        dentro = False
        if  2 < x <  3 and 5 < y < 6:
            # aqui sabemos que na verdade (x,y) esta na iris direita
            dentro = True

    if dentro:
        print("dentro")
    else:
        print("fora")
            

Solução 1
.........

Essa solução começa supondo que está fora e depois...
A variável  ``dentro`` é uma variável de estado também chamada de **indicador de passagem** do tipo ``bool``.

.. activecode:: cl04_solucao1_p1_2014

    x = float(input("Digite x: "))
    y = float(input("Digite y: "))

    # suponha que (x,y) esta fora
    dentro = False

    if -5 < x < 5 and 0 < y < 8:
        # aqui sabemos que (x,y) esta na face
        dentro = True
        if -3 <= x <= 3 and 1 <= y <= 2:
            # aqui sabemos que (x,y) esta na face, mas esta na boca
            dentro = False
        elif -4 <= x <= -1 and 4 <= y <= 7:
            # aqui sabemos que (x,y) esta no olho esquerdo
            dentro = False
            if -3 < x < -2 and 5 < y < 6:
                # aqui sabemos que na verdade (x,y) esta na iris esquerda
                dentro = True
        elif  1 <= x <=  4 and 4 <= y <= 7:
            # aqui sabemos que (x,y) esta no olho direito
            dentro = False
            if  2 < x < 3 and 5 < y < 6:
                # aqui sabemos que na verdade (x,y) esta na iris direita
                dentro = True

    if dentro:
        print("dentro")
    else:
        print("fora")
        

Solução 2
.........

explora a simetria da figura em relação ao eixo y.
A variável ``x_pos`` pode ser trocada por x.      
A variável  ``dentro`` é uma variável de estado também chamada de **indicador de passagem** do tipo ``bool``.

.. activecode:: cl04_solucao2_p1_2014

    x_pos = x = float(input("Digite x: "))
    y = float(input("Digite y: "))

    if x < 0: # simetria ;-)
        x_pos = -x

    # suponha que (x,y) que esta dentro
    dentro = True

    if x_pos >= 5 or y >= 8 or y <= 0:
        # aqui sabemos que (x,y) esta fora da face
        dentro = False
    elif 0 <= x_pos <= 3 and 1 <= y <= 2:
        # aqui sabemos que (x,y) esta na boca
        dentro = False
    elif 1 <= x_pos <= 4 and 4 <= y <= 7:
        # aqui sabemos que (x,y) esta em um olho
        if not (2 < x < 3 or 5 < y < 6):
            # aqui sabemos que (x,y) esta fora da iris
            dentro = False

    if dentro:
        print("dentro")
    else:
        print("fora")
        

Solução 3
.........

Essa solução é idêntica a anterior, explora a simetria da figura em relação ao eixo ``y`` e utiliza um string para a armazenar resposta. 
A variável ``x_pos`` pode ser trocada por ``x``.      
A variável ``dentro`` é um indicador de passagem do tipo  ``str`` .

.. activecode:: cl04_solucao3_p1_2014

    x_pos = x = float(input("Digite x: "))
    y = float(input("Digite y: "))

    if x < 0: # simetria ;-)
        x_pos = -x

    # suponha que (x,y) que esta dentro
    resposta = "dentro"

    if x_pos >= 5 or y >= 8 or y <= 0:
        # aqui sabemos que (x,y) esta na face
        resposta = "fora"
    elif 0 <= x_pos <= 3 and 1 <= y <= 2:
        # aqui sabemos que (x,y) esta na boca
        resposta = "fora"
    elif 1 <= x_pos <= 4 and 4 <= y <= 7:
        # aqui sabemos que (x,y) esta em um olho
        if not (2 < x < 3 or 5 < y < 6):
            # aqui sabemos que (x,y) esta fora de uma iris
            resposta = "fora"

    print(resposta)
        

Solução 4
.........

Essa solução utiliza uma variável ``bool`` para indicar em qual parte da face está o ponto: face, boca
olho direito, olho esquerdo, íris do olho direito, íris do olho esquerdo. A condição para decidir se o ponto está ou não na região sombreada pode parecer complicada, mas é bem elegante e é um excelente exercício para treinar expressões lógicas.

.. activecode:: cl04_solucao4_p1_2014

    x = float(input("Digite x: "))
    y = float(input("Digite y: "))

    # face == True se (x,y) esta na face, False em caso contrário
    face = -5 < x < 5 and 0 < y < 8 

    # boca == True se (x,y) esta na boca, False em caso contrário
    boca = -3 <= x <= 3 and 1 <= y <= 2

    # olho_esquerdo == True se (x,y) esta no olho esquerdo
    olho_esquerdo = -4 <= x <= -1 and 4 <= y <= 7

    # iris_esquerda == True se (x,y) esta na iris esquerda
    iris_esquerda = -3 <= x <= -2 and 5 <= y <= 6

    # olho_direito  == True se (x,y) esta no olho direito
    olho_direito = 1 <= x <= 4 and 4 <= y <= 7

    # iris_direita  == True se (x,y) esta na iris direita
    iris_direita = 2 <= x <= 3 and 5 <= y <= 6

    # vixe! :-D complicado? certamente elegante...
    if iris_esquerda or iris_direita or face and not (boca or olho_esquerdo or olho_direito):
        print("dentro")
    else:
        print("fora")
        

Solução 5
.........

Essa solução é idêntica a anterior, mas explora a simetria como na solução 3.

.. activecode:: cl04_solucao5_p1_2014

    x_pos = x = float(input("Digite x: "))
    y = float(input("Digite y: "))

    if x < 0:
        x_pos = -x 

    # face == True se (x,y) esta na face
    face = x_pos <  5 and 0 < y < 8 

    # boca == True se (x,y) esta na boca
    boca = x_pos <= 3 and 1 <= y <= 2

    # olho == True se (x,y) esta em um dos olhos
    olho = 1 <= x_pos <= 4 and 4 <= y <= 7

    # iris == True se (x,y) esta em uma das iris
    iris = 2 <= x_pos <= 3 and 5 <= y <= 6

    # vixe! :-D complicado?! certamente muito elegante
    if iris or face and not (boca or olho):
        print("dentro")
    else:
        print("fora")

.. break


Onde chegamos e para onde vamos?
--------------------------------

Nesse capítulo introduzimos os comandos ``if`` ``if-else`` que são usados para controlar o fluxo da computação, permitindo que certos trechos de código sejam executados condicionalmente. As condições devem ser o resultado de uma expressão lógica ou relacional.

O entendimento de problemas tipicamente exige um modelamento que organiza os dados em classes, como dentro ou fora, alto ou baixo, aprovado, reprovado ou de recuperação, etc. Ao longo do curso vamos resolver mais exercícios para desenvolver essa habilidade de **reconhecer padrões** que facilitam o desenolvimento de soluções. 

Como primeira estratégia para começar a pensar nos algoritmos, vimos que começar assumindo um estado inicial traz benefícios e facilita nosso pensando a focar nas condições que devem ocorrer para que o programa calcule o estado correto. No laboratório, vimos uma solução (algoritmo) pode ser mais ou menos complexa dependendo desse estado inicial. Portanto, antes de você se decidir por escrever um programa que implementa algum algoritmo, é sempre bom considerar outros algoritmos. Uma forma simples é trocar a condição inicial (como assumir que o ponto está dentro ou fora) e explorando as implicações dessas condições e alternativas.

O grande poder da computação, no entanto, vem de sua capacidade de executar as mesmas instruções milhares e milhares de vezes, rapidamente, repetidamente, sem se cansar e sempre com a mesma qualidade. Esse poder é expresso pelos comandos de repetição que permitem executar um bloco de comandos de novo, e de novo, e de novo... 








O próximo capítulo introduz outro fundamento que revela 
