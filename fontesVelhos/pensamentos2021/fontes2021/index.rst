.. -- coding: utf-8 --

    Departamento de Ciência da Computação
    Instituto de Matemática e Estatística

    Março de 2021:
    Versão 21.03 - Pensamento Computacional - uma introdução com Python.

.. meta::
   :description: Versão interativa das aulas de Introdução à Computação com Python.
   :keywords: python, ciência da computação, programação


.. toc_version: 2

.. _t_o_c:

.. raw:: html

   <div style="text-align:center" class="center-block">
   <h1>Pensamento Computacional: uma introdução com Python</h1>
   <h3>-- versão interativa --</h1>

   <p>por  
   <a href="https://www.ime.usp.br/~hitoshi">Carlos H. Morimoto</a> e <a href="https://www.ime.usp.br/~coelho">José Coelho de Pina Jr.</a>
   </p>
   <style>
   button.reveal_button {
       margin-left: auto;
       margin-right: auto;
   }
   </style>
   </div>

Prefácio
::::::::

.. toctree::
    :maxdepth: 1

    ./00-prefacio.rst

Índice
::::::

.. toctree::
   :numbered:
   :maxdepth: 2

   ./01-introducao.rst
   ./02-expressoes.rst
   ./03-variaveis.rst
   ./04-alternativas.rst
   ./05-denovo.rst
   ./06-indicador.rst
   ./07-repeticoes.rst
   ./08-reais.rst
   ./09-funcoes.rst 
   ./10-listas.rst 
   ./11-funcoes-listas.rst
   ./12-strings.rst
   ./13-matrizes.rst
   ./14-busca.rst
   ./15-ordenacao.rst


Índices e tabelas
:::::::::::::::::

* :ref:`genindex`
* :ref:`search`

