'''
Descrição:
    Arquivo de configuração para tornar o livro compatível com runestone 5.x

    Quebra a arquivo rst em subseções. 
    Casa subseção recebe  um copyright.

Fontes:
    fontes: de 2017
    Fontes2021: última atualização
'''

# incluir prefácio quando pronto 
INCLUIR_PREFACIO = False
ULTIMO_CAPITULO_INCLUIDO = 5

## header, footer, prefacio e indice são strings usadas para gerar o 
## novo index.rst depois de quebrado em sessões (+ interativo?) 

HEADER = """
.. Pensamentos com Python
    Você pode editar esse arquivo, mas ele deve conter ao menos a `toctree`
    para a raiz. 

.. meta:: 
    :description: Uma versão interativa do livro "Pensamentos com Python"
    :keywords: pensamento computacional, ciência da computação, python

.. toc_version: 2

.. _t_o_c:

.. raw:: html

   <div style="text-align:center" class="center-block">
   <h1>Pensamentos com Python</h1>
   <h3>-- um curso interativo de introdução à computação --</h3>

   <p>por <a href="https://www.ime.usp.br/~hitoshi">Carlos Hitoshi Morimoto</a>  e <a href="https://www.ime.usp.br/~coelho">José Coelho de Pina Jr.</a>
   </p>
   <style>
   button.reveal_button {
       margin-left: auto;
       margin-right: auto;
   }
   </style>
   </div>

"""

PREFACIO = """
Prefácio
::::::::

.. toctree::
    :maxdepth: 1

    00-Prefacio/toctree.rst

"""

INDICE = """
Índice
::::::

.. toctree::
    :numbered:
    :maxdepth: 2

"""

FOOTER = """
Índice remissivo
::::::::::::::::

* :ref:`genindex`
* :ref:`search`


.. raw:: html

   <div style="width: 500px; margin-left: auto; margin-right: auto;">
   <a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/">
   <img alt="Creative Commons License" style="border-width:0; display:block; margin-left: auto; margin-right:auto;" src="https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png" />
   </a><br />
   <span xmlns:dct="http://purl.org/dc/terms/" href="http://purl.org/dc/dcmitype/InteractiveResource" property="dct:title" rel="dct:type">Pensamentos com Python</span><br /> 
   por <span xmlns:cc="http://creativecommons.org/ns#" property="cc:attributionName">Carlos Hitoshi Morimoto e José Coelho de Pina Jr.</span> <br />
   está sob a licença <a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/">Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License</a>.</div>

"""


copyright = """
..  Copyright (C) Carlos Hitoshi Morimoto e José Coelho de Pina Jr.
    É permitida a cópia, distribuição e/ou modificação desde documento
    sob os termos da Licença de Documentação Livre GNU (GNU Free 
    Documentation License) versão 1.3 ou versão subsequente
    publicada pela Fundação de Software Livre (Free Software 
    Foundation); com as seções invariantes sendo Prefácio e 
    Lista de Contribuidores, sem texto de capa ou contra-capa.  
    Uma cópia da licença está disponível em:
    https://www.gnu.org/licenses/licenses.pt-br.html#FDL

"""

INPATH = '../pensamentos/fontes2021/'

# INFILES: dicionarios com os arquivos fonte em INPATH
INFILES = {
    0:"00-prefacio.rst",
    1:"01-introducao.rst",
    2:"02-expressoes.rst",
    3:"03-variaveis.rst",
    4:"04-alternativas.rst",
    5:"05-denovo.rst",
    6:"06-indicador.rst",
    7:"07-repeticoes.rst",
    8:"08-reais.rst",
    9:"09-funcoes.rst",
    10:"10-listas.rst",
    11:"11-funcoes-listas.rst",
    12:"12-strings.rst",
    13:"13-matrizes.rst",
    14:"14-busca.rst",
    15:"15-ordenacao.rst"
}

file_name = "index_fontes2021.txt"

destino = './_sources/'
