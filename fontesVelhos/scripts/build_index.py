"""
Rascunho para testar como gerar arquivos toctree.rst
a partir do indice. 

"""

import os

copyright = """
..  Copyright (C) Carlos Hitoshi Morimoto e José Coelho de Pina Jr.
    É permitida a cópia, distribuição e/ou modificação desde documento
    sob os termos da Licença de Documentação Livre GNU (GNU Free 
    Documentation License) versão 1.3 ou versão subsequente
    publicada pela Fundação de Software Livre (Free Software 
    Foundation); com as seções invariantes sendo Prefácio e 
    Lista de Contribuidores, sem texto de capa ou contra-capa.  
    Uma cópia da licença está disponível em:
    https://www.gnu.org/licenses/licenses.pt-br.html#FDL

"""

#file_name = "index.txt"
file_name = "index_fontes2021.txt"

toccab = '\n::::\n\n.. toctree::\n\t:caption:\n\tmaxdepth: 1\n\n'

# destino = './dirs/'
destino = './_sources/'

def main():
    
    dirs = {}

    with open(file_name, 'r') as f:
        index = f.read().strip().split('\n')

    for linha in index:
        lin = linha.strip().split('.')
        if len(lin) == 2:
            cap = lin[1].strip().split(':')
            icap = int(lin[0])
            nome = filtre( cap[0] )

            print(lin)
            print( icap, f'{icap:02}-{nome}' )
            dirs[ icap ] = {}
            dirs[ icap ]['nome'] = f'{icap:02}-{nome}'

        elif len(lin) == 3:
            sec = lin[2].strip().split(':')
            isec = int(lin[1])
            nome = filtre( sec[0] )
            dirs[icap][isec] = f'{isec:02}-{nome}'

            print(lin[0], lin[1], nome)

    gere_diretorios( dirs, destino )

def gere_diretorios( dirs, path ):
    # diretorios
    allfiles = ''
    for cap in dirs:

        d = path + dirs[cap]['nome']
        print("criando diretorio: ", d)
        try:
            os.mkdir( d )
        except:
            print("diretório já existe! Vou limpar os arquivos..")

        dname   = dirs[cap]['nome']
        toc = toccab

        for sec in dirs[cap]:
            if sec != 'nome':
                filecab = f'{copyright}\n.. qnum::\n\t:prefix: cap{cap:02}-{sec:02}\n\t:start: 1\n\n'

                narq = dirs[cap][sec]+'.rst'
                fname = d+f'/{narq}'

                print("criando arquivo: ", narq)
                with open(fname, 'w') as arq:
                    arq.write( f"{filecab}\n" )

                toc += f"\t{narq}\n"
                allfiles += f'{dname}/{narq}\n'

        narq = 'toctree.rst'
        fname = d+f'/{narq}'
        print("criando arquivo: ", narq)
        with open(fname, 'w') as arq:
            arq.write( f"{toc}\n" )
        
        allfiles += '\n\n\n'

    with open('allChapterFiles.txt', 'w') as arq:
        arq.write( f'{allfiles}')


def filtre(s, n=4):
    s = s.replace('à', 'a')
    s = s.replace('á', 'a')
    s = s.replace('é', 'e')
    s = s.replace('í', 'i')
    s = s.replace('ó', 'o')
    s = s.replace('ú', 'u')
    s = s.replace('ã', 'a')
    s = s.replace('õ', 'o')
    s = s.replace('ç', 'c')

    s = s.replace(' um ', ' ')
    s = s.replace(' em ', ' ')
    s = s.replace(' de ', ' ')
    s = s.replace(' do ', ' ')
    s = s.replace(' da ', ' ')
    s = s.replace(' a ', ' ')
    s = s.replace(' e ', ' ')
    s = s.replace(' o ', ' ')
    s = s.replace(', ', ' ')
    s = s.split()[:n]
    return '-'.join(s)

main()
