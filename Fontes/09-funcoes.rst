..  shortname:: Tópico: funções
..  description:: Como escrever funções em Python

Introdução
----------

.. index:: funções, parâmetros, variáveis locais, def, docstring, return


.. raw:: html


    <div align="right">
    <p></p>
    <blockquote>
    <p>
    "
    Nosso objetivo final é a programação extensível. Com isso, queremos dizer a construção de módulos hierárquicos, cada módulo adicionando novas funcionalidades ao sistema.
    "
    </p>
    <p>Niklaus Wirth</p>
    </blockquote>
    <p></p>
    </div>


Já mencionamos algumas vezes que, dentre muitas habilidades, desejamos desenvolver a de decompor problemas complexos em problemas menores. Parte dessa habilidade está ligada a capacidade de identificar padrões que se repetem, que podem aparecer no mesmo problema ou em problemas similares. Uma vez identificados, podemos substituir trechos de código que se repetem (os padrões) por um nome que, em geral, corresponde a funcionalidade do trecho. 

Essa capacidade de abstrair trechos de código na forma de funções é o tipo de pensamento que vamos treinar nesse capítulo.

.. break

Para onde vamos
---------------

Ao final desse capítulo você deverá ser capaz de:

    - identificar trechos de código que se repetem;
    - criar funções usando o comando `def` para substituir esses trechos de código;
    - estruturar programas em Python na forma de várias funções;
    - criar `módulos` (ou bibliotecas de funções em Python) para reusar as funções previamente escritas em seus futuros programas.

.. break


Exercício de motivação
----------------------

Para motivar o uso de funções vamos resolver o problema de calcular o número de combinações possíveis de ``m`` elementos em grupos de ``n`` elementos, com ``n`` <= ``m`` (costuma-se dizer combinação de ``m``, ``n`` a ``n``).

Por exemplo, para combinar ``m``=3 sabores (chocolate, morango e creme) em pares (grupos de ``n``=2 sabores) há 3 combinações possíveis: chocolate-morango, chocolate-creme, e morango-creme.

O número de combinações $Comb(m,n)$ pode ser calculado pela fórmula $Comb(m,n) = m!/((m-n)!n!)$, onde a notação $m!$ indica o fatorial de ``m``.  

Nesse exercício, vamos escrever um programa que lê dois inteiros ``m`` e ``n`` e calcula a ``Comb(m,n)``.


Uma primeira solução: com "linguiça"
....................................

Uma primeira ideia é calcular  ``m!``, e depois ``n!`` e finalmente ``(m-n)!`` para calcular o resultado.


.. activecode:: ac01_programa_sem_funcao_com_linguica_v01

    m = int(input("Digite m: "))
    n = int(input("Digite n: "))

    # calcule m!
    m_fat = 1
    i = 2
    while i <= m:
        m_fat *= i
        i += 1

    # calcule n!
    n_fat = 1
    i = 2
    while i <= n:
        n_fat *= i
        i += 1

    # calcule (m-n)!
    mn_fat = 1
    i = 2
    while i <= m-n:
        mn_fat *= i
        i += 1

    resultado = m_fat//(n_fat * mn_fat)

    print(f"Comb({m},{n}) = {resultado}")

Observe que o trecho de código de calcula o fatorial de ``m_fat``, ``n_fat`` e ``mn_fat`` são muito similares, como vários gomos de linguiça (3 gomos, um para cada cálculo de fatorial). 
Vamos maquiar um pouco esses trechos para realçar que cada gomo pode ficar bem uniforme, como a seguir:

.. activecode:: ac02_programa_sem_funcao_com_linguica_v02

    m = int(input("Digite m: "))
    n = int(input("Digite n: "))

    x = m           # inicializa x com m
    # calcule x!    ==================================
    res = 1         # resultado do fatorial
    i = 2
    while i <= x:
        res *= i
        i += 1

    m_fat = res     # salva o resultado em m_fat

    x = n           # inicializa x com n
    # calcule x!    ==================================
    res = 1         # resultado do fatorial
    i = 2
    while i <= x:
        res *= i
        i += 1

    n_fat = res     # salva o resultado em n_fat

    x = m - n       # inicializa x com (m-n) 
    # calcule x!    ==================================
    res = 1         # resultado do fatorial
    i = 2
    while i <= x:
        res *= i
        i += 1

    mn_fat = res    # salva o resultado em mn_fat

    # calcule o resultado  ==========================        
    resultado = m_fat//(n_fat * mn_fat)

    print(f"Comb({m},{n}) = {resultado}")


Observe agora que o trecho que calcula ``x!`` é exatamente o mesmo e é portanto repetido 3 vezes. 
O valor de x é inicializado antes do trecho e o resultado deve ser salvo antes de "reusar" 
o trecho, já que estamos reciclando as variáveis desse trecho.
 
Esses programas que contém gomos (como linguiça) é algo que desejamos evitar. Para fazer isso, 
vamos imaginar (abstrair) que pudéssemos usar uma função ``fat(x)``, que recebe um inteiro não negativo ``x`` e devolve o valor de ``x!``. Nesse caso o programa poderia ser escrito da seguinte maneira:

.. sourcecode:: python

        m = int(input("Digite m: "))
    n = int(input("Digite n: "))

    # calcule m!        ==================================
    m_fat = fat(m)      # salva o resultado em m_fat

    # calcule n!        ==================================
    n_fat = fat(n)      # salva o resultado em n_fa

    # calcule (m-n)!    ==================================
    mn_fat = fat(m - n) # salva o resultado em mn_fat

    resultado = m_fat//(n_fat * mn_fat)

    print(f"Comb({m},{n}) = {resultado}")


O programa (sem gomos) ficaria bem mais simples de escrever e, se necessário, de modificar. A seguir, veremos como você pode criar e utilizar suas próprias funções e assim remover as linguiças do seu código.

.. break

Como criar um função
--------------------

A primeira coisa que você deve fazer para criar uma função é definir o seu **comportamento**, ou seja, o que a função deve receber e o que ela deve devolver quando for usada. Você pode pensar na função com uma "caixa preta" cujo conteúdo desconhecemos, mas que quando recebe valores válidos de entrada retorna o resultado esperado. 

.. figure:: ../Figuras/cap08/funcao.png
    :align: center
    :alt: Figura 8.1: Uma função é definida pelo seu nome, pelos parâmetros de entrada e pelos valores que retorna. 

    Figura 8.1: Uma função é definida pelo seu nome, pelos parâmetros de entrada e pelos valores que retorna. 


A sintaxe para declarar uma função em Python é a seguinte:

.. sourcecode:: python

    def nome_da_função ( parâmetros ):
        '''
        docstring contendo comentários sobre a função.
        Embora opcionais são fortemente recomendados. Os comentários
        devem descrever o papel dos parâmetros e o que a função faz.
        '''
        # corpo da função
        |
        | bloco de comandos
        |

Vamos aplicar essa sintaxe para criar a função ``fat(x)``. O comportamento de ``fat(x)`` é o seguinte:

    - Recebe um inteiro não negativo x;
    - Retorna o valor de ``x!``.

Vimos no programa da seção anterior o trecho de código que calcula o resultado. A definição da função pode usar o mesmo trecho no lugar do corpo da função: 

.. sourcecode:: python

    def fat ( x ):
        '''
        ( int ) -> int
        Recebe um inteiro x não negativo e retorna
        o valor do fatorial de x.
        '''
        # corpo da função
        res = 1         # resultado do fatorial
        i = 2
        while i <= x:
            res *= i
            i += 1

        return res

Observe que esse código só possui a **definição** da função contida no comando ``def``. 
Essa definição apenas ensina ao Python o que fazer quando a função ``fat()`` é chamada.
A função precisa ser  **chamada** (passando uma entrada) para que o Python execute a função e retorne um valor.

Assim como as variáveis usadas em uma expressão precisam ser inicializadas **antes** da expressão ser calculada, as funções também precisam ser definidas antes de serem chamadas. Caso contrário, o Python não saberia o que fazer ao encontrar a chamada, gerando um erro de "nome não definido".

O programa no CodeLens a seguir inclui, **após** a definição de ``fat()``, várias chamadas da função.
Execute o CodeLens abaixo para ver passo a passo essas várias execuções. Em particular, observe com atenção o que acontece nas chamadas e no comando ``return``. 

.. codelen:: chamada_da_funcao_fat_x

    def fat ( x ):
        '''
        ( int ) -> int
        Recebe um inteiro x não negativo e retorna
        o valor do fatorial de x.
        '''
        # corpo da função
        res = 1         # resultado do fatorial
        i = 2
        while i <= x:
            res *= i
            i += 1

        return res

    res1 = fat( 3 )   # chamada de fat com x = 3
    print( res1 )
    res2 = fat( 4 - 2 )   # chamada de fat com x = 4 - 2
    print( res2 )
    res3 = fat( 2 ) - fat( res1 - res2 )  # duas chamadas de fat
    print( res3 )


.. admonition:: **Como funciona esse trecho?**

    Observe que, toda vez que uma função ``fat()`` é chamada, o programa "interrompe" a sua execução e passa a executar a ``fat()``, a partir do início da função. No instante da chamada, o parâmetro ``x`` recebe o **valor da expressão** indicada na chamada, como por exemplo ``4-2`` ou ``res1 - res2``. 
    
    O Python continua a execução dentro da função ``fat()``, um comando de cada vez, passo-a-passo, até chegar na linha com o comando ``return``. Esse comando faz com que a função ``fat()`` termine e o **valor** em ``res`` é retornado ao lugar onde a função foi chamada (de onde o programa havia sido interrompido). O programa continua a execução a partir desse lugar, mas agora sabendo o resultado do fatorial. Observe que cada chamada é feita de um lugar diferente, usando expressões diferentes.

.. break
    
    
Esqueleto de um programa em Python
----------------------------------

Observe que, no exercício anterior, as chamadas da função ``fat()`` são feitas após a definição da função e escritas na primeira coluna, ou seja, fora do comando ``def`` (que define a função). O Python permite que as linhas de código sejam colocadas em qualquer ordem, desde que, para cada linha a ser executada, todos os **nomes** (de variáveis, funções etc) encontradas na linha tenham sido definidos antes.

Isso  permite criar códigos bem confusos pois partes do código ficam espalhadas, dificultando sua leitura, entendimento e depuração. Para estimular uma melhor organização do código e facilitar sua manutenção, sugerimos usar o seguinte "esqueleto de programa", que adotaremos no restante desse livro.


.. sourcecode:: python

    # função  principal  
    def main():
        ''' 
        Função principal, será a primeira a ser executado e
        será a responsável pela chamada de outras funções que 
        por sua vez podem ou não chamar outras funções que 
        por sua vez ...
        '''
        # corpo da função main
        |
        | bloco de comandos     
        | 

    # Declaração das funções 
    def f( parâmetros_de_f ):
        '''
        docstring da função f
        '''
        # corpo da função f
        |
        | bloco de comandos     
        | 

    def g( parâmetros_de_g ):
        '''
        docstring da função g
        '''
        # corpo da função g
        |
        | bloco de comandos     
        | 

    [...]

    # início da execução do programa
    #-----------------------------------------------------            
    if __name__ == '__main__': # chamada da funcao principal 
        main() # chamada da função main

    
.. Para nos prevenir de alguns problemas (que serão explicados mais tarde), vamos adotar nesse curso o seguinte esqueleto para escrever programas em Python com funções. 
    
Nesse esqueleto, a primeira função sempre é chamada de ``main()`` pois corresponde a função principal do programa, ou seja, aquela que resolve o problema. As demais funções auxiliares devem ser definidas, em qualquer ordem, após a definição da  ``main()`` e, por fim, para executar o programa, a função ``main()`` precisa ser chamada. 
Observe que, apesar de estar no final do arquivo, a primeira função chamada é a ``main()``, que chama as demais funções. Por isso não é mais necessário se preocupar com a ordem da definição das funções no arquivo.
            
O uso do ``if __name__ == '__main__'`` para a chamada da função ``main()`` permite que esse arquivo contendo uma ou várias funções seja incluído em outros programas (usando "import" e suas variações) sem a necessidade de reescrever ou copiar o código. Vamos esclarecer a necessidade desse ``if`` no final desse capítulo. Antes, vamos fazer um exemplo para que você possa experimentar esse esqueleto.

.. admonition:: Boa prática de programação: mantenha seu código organizado e teste cada função.

    Colocar a função ``main()`` sempre no início do arquivo fácilita o seu descobrimento, ou seja, ao editar, fica fácil voltar ao programa principal e, para quem não conhece o programa, fica fácil encontrá-lo e entender o programa faz. 
    
    Ao desenvolver soluções de problemas complexos, a decomposição do problema em funções facilita o desenvolvimento da solução pois permite que cada função seja testada **separadamente**. 
    É necessário testar cada função para termos confiança que cada bloco está executando sua parte corretamente. 


.. break


Exercício: uso do esqueleto com múltiplas funções
-------------------------------------------------

Nesse exercício, reescreva o programa que lê dois inteiros ``m`` e ``n`` e calcula a combinação ``Comb(m,n)``, mas dessa vez usando funções. Para isso, complete o ActiveCode a seguir.


.. activecode:: ac03_programa_com_funcao_sem_linguica
    '''
        Programa que lê dois inteiro m e n não negativos, 
        e calcula a combinação de m, n a n.
    '''
    def main():
        m = int(input("Digite m: ))
        n = int(input("Digite n: ))

        resultado = comb(m, n)
        print(f"Comb({m},{n}) = {resultado}")

    def comb(m, n):
        ''' (int, int) -> int
        Recebe dois inteiros m e n e retorna
        a combinação de m, n a n.
        '''
        # escreva o corpo da função

    def fatorial( n ):    ## vamos usar n ao invés de x, n ou x, é só um nome!
        ''' (int) -> int
        Recebe um inteiro não negativo n,
        e retorna n!.
        '''
        # escreva o corpo da função

    # início da execução do programa
    #-----------------------------------------------------            
    if __name__ == '__main__': # chamada da funcao principal 
        main() # chamada da função main


.. admonition:: Boa prática de programação: documente as suas funções

    Nesse livro, vamos adotar a seguinte notação para documentar as funções auxiliares.

    .. sourcecode:: python

        def comb(m, n):
            ''' (int, int) -> int
            descrição da função
            '''

    Logo após o protótipo da função (a primeira linha com o comando ``def``), segue um ``docstring``, uma string usada para documentar a função. Uma docstring é delimitada por três apóstrofes ou três aspas, e pode conter múltiplas linhas de texto.  
    
    Documentar suas funções é importante para explicar a outros programadores o que a função faz. Na primeira linha colocamos uma lista com os tipos dos parâmetros de entrada entre parênteses (nesse caso são dois inteiros), separada por ``->``, seguida pela lista de tipos dos valores retornados, nesse caso, apenas um inteiro. As demais linhas devem descrever o significado dos parâmetros e o que a função faz ao ser executada, para que outros programadores possam usar a função em outros programas.

.. break

Nome e escopo das variáveis
---------------------------

Uma solução para o exercício anterior é ilustrada no Codelens a seguir.

.. codelens:: cl02_funcao_main_combinacao_fatorial

    '''
        Programa que lê dois inteiro m e n não negativos, 
        e calcula a combinação de m, n a n.
    '''
    def main():
        m = int(input("Digite m: ))
        n = int(input("Digite n: ))

        resultado = comb(m, n)
        print(f"Comb({m},{n}) = {resultado}")

    def comb(m, n):
        ''' (int, int) -> int
        Recebe dois inteiros m e n e retorna
        a combinação de m, n a n.
        '''
        # escreva o corpo da função
        resultado = fat(m) / ( fat(n) * fat(m-n) )
        return resultado

    def fatorial( n ):
        ''' (int) -> int
        Recebe um inteiro não negativo n,
        e retorna n!.
        '''
        # escreva o corpo da função
        res = 1         # resultado do fatorial
        i = 2
        while i <= n:
            res *= i
            i += 1

        return res

    # início da execução do programa
    #-----------------------------------------------------            
    if __name__ == '__main__': # chamada da funcao principal 
        main() # chamada da função main


.. break

Módulos
-------


.. break

Exercícios
----------

Exercício 2
...........

Complete a função ``fatorial`` abaixo,
que recebe como parâmetro um número 
inteiro ``k``, ``k >= 0``, e retorna k!.

Escreva apenas o corpo da função.
Observe que o código já inclui
chamadas para a função ``fatorial``, para que
você possa testar a função.


.. activecode:: aula_funcao_ex02_tentativa

    def main():
        ''' testes da função fatorial '''
        print("0! =", fatorial(0))
        print("1! =", fatorial(1))
        print("5! =", fatorial(5))   
        print("17! =", fatorial(17))   
		
    #-----------------------------------------------------
    
    def fatorial(k):
        '''(int) -> int

        Recebe um inteiro k e retorna o valor de k!

        Pre-condição: supõe que k é um número inteiro não negativo. 
        '''

        k_fat = 1

	# COMPLETE ESSA FUNÇÃO
	
        return k_fat 

    #-----------------------------------------------------
    
    if __name__ == '__main__': # chamada da funcao principal 
        main() # chamada da função main

Clique `aqui <exercicios/funcao02.html>`__ para ver uma solução.

Exercício 3
...........

Usando a função do exercício 6.2, escreva uma função que recebe
dois inteiros, ``m`` e ``n``, como parâmetros e retorna a combinação
``m!/((m-n)!n!)``. 

.. activecode:: aula_funcao_ex03_tentativa

    def main():
        ''' Testes da função combincao '''
	print("Combinacao(4,2) =", combinacao(4,2))
	print("Combinacao(5,2) =", combinacao(5,2))
	print("Combinacao(10,4) =", combinacao(10,4))
		
    #-----------------------------------------------------
    
    def combinacao(m, n):
        '''(int, int) -> int
        Recebe dois inteiros m e n, e retorna o valor de m!/((m-n)! n!)
        '''

	# COMPLETE ESSA FUNÇÃO E MUDE O RETURN ABAIXO

        return "o resultado"

    #-----------------------------------------------------
    
    if __name__ == '__main__': # chamada da funcao principal 
        main() 


Clique `aqui <exercicios/funcao03.html>`__ para ver uma solução.


Exercício 4
...........

Usando as funções ``fatorial`` e ``combinacao`` dos exercícios anteriores,
escreva um programa que lê um inteiro ``n``, ``n >= 0`` e imprime 
os coeficientes da expansão de ``(x+y)`` elevado a ``n``.

Lembre-se de utilizar o esqueleto de programa com funções em Python.

.. activecode:: aula_funcao_ex04_tentativa

    # Escreva o seu programa usando o esqueleto sugerido
    
    def main():
        ''' Escreva aqui alguns testes '''

	print("Vixe! Ainda não fiz esse exercício.")

    #-----------------------------------------------------            
    if __name__ == '__main__': # chamada da funcao principal 
        main() 


Clique `aqui <exercicios/funcao04.html>`__ para ver uma solução.


.. break

Laboratório: crie um módulo com suas funções
--------------------------------------------


.. break

Onde chegamos e para onde vamos?
--------------------------------
