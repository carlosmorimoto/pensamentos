Estrutura do projeto
--------------------

* Dropbox/mac0110-2021/book: diretório do projeto

* Dropbox/mac0110-2021/book/pensamentos/fontes2021: fontes do livro

* Dropbox/mac0110-2021/book/pensamentos/Figuras: aqui vão as imagens?
    No fonte devemos escrever
    .. figure:: ../Figuras/ 
  ?

* Dropbox/mac0110-2021/book/scripts/:
  - config2021.py: arquivo com configurações
```python    
    INPATH = '../pensamentos/fontes2021/' # fontes
    destino = './_sources/'               # destino do fontes
    file_name = "index_fontes2021.txt"
```
  - `index_fontes2021.txt`: contém hardcoded(?) a estrutura do livro
  
  - migra2021.py: 
      RECEBE: `file_name`, `destino`, `INPATH`
      ESCREVE em `destino` os fontes em `INPATH` preparados para o
      runestone interactive versao 5+


Processo de edição
------------------

Apenas para editar e visualizar localmente o texto.

1 Escrever em Dropbox/mac0110-2021/book/pensamentos/fontes2021
2 Colocar imagens em  ?
    Dropbox/mac0110-2021/book/pensamentos/Figures
3 Executar `python migra2021.py`:
    cria Dropbox/mac0110-2021/book/scripts/_sources
4 Copiar Dropbox/mac0110-2021/book/scripts/_sources
    para Dropbox/mac0110-2021/book/pensamentos/_sources
5 executar `runestone build`
6 olhar o resultado em pensamentos/build

Pulando o runestone deploy


-------------------------------------
O fonte do texto está em .../pensamentos/fontes2021

Coelho tem usado .../pensamentos/build_pensamentos.sh para construir html

Imagens/Figuras estão sendo colocadas no diretório.

Escrever livro ou texto?

Colocar imagens externas, como fazer a referência?



