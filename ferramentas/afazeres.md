To do
-----

* panda: migração na Páscoa

* Falar da estrutura do livro interativo usando runestone...  Como "ler" aproveitar desfrutar esse livro.
* ao final de todo capítulo tem laboratório(s), pra vc fazer usando uma instalação local do python no seu computador, usando o Spyder ou um python shell, fora do navegador...
* o navegador é limitado, a gente tem de ganhar asas 
* lab 1 - instalacao do python
* lab 2 - calculador no python shell (iPython?)
* lab 3 - spyder com entrada e saida e variaveis
* lab 4 -  ep com if
* usar alguma representacao grafica como fluxograma para ilustrar primeiros programas. sugestao?

* licença: CC BY-NC-SA

MAPA:

Titulo: Pensamentos com Python

* 1 - pensamento computacional

    - Lab: objetivo: instalação anaconda

* 2 - pensamento matemático
    - pensamento lógico e aritmético

    - Lab: obj usar iPython 

metáfora: computador como calcular

* 3 - Pensamento e comunicação 

    - e interação

    - Lab: obj: introduzir Spyder
    - script: hello world

    - mensagens fazem parte do problema não da solução
    - solução começa a partir da mensagem de entrada ("dados")
    - 

metáfora para definir um contexto

* caixa preta com entrada e saída
* alguma maquina com ciclo de vida de dados

    - entra, calcula e sai (variáveis para ... )

    - conversor de temperatura: le C e sai F

entra, 
entrada saída e variáveis

* 4 - Pensando em alternativas
    * considerando alternativas
    * Pensando em possibilidades 
    * [em]/nas/com alternativas  (parece + determinístico que possibilidade)
    * em casos 
    * pensando no que fazer ... 
    * nos caminhos

if else elif

* 5 - Pensando de novo e de novo e ...

    - while ..

    - comando para materializar o pensamento

* 6 - Lembrando as passagens


* 7 - De novo de novo
    - de novo novamente
    - novamente de novo
    - repetindo de novo 



