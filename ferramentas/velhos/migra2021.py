"""
    Script para migração dos arquivos fonte do livro Pensamentos com Python
    para runestone interactive versao 5+.

    O config20xx define os parâmetros de entrada
    - INPATH
    - INFILES
    - file_name = ESTRUTURA do livro (esqueleto com capitulos e seções)
    - destino = diretorio destino
"""

import os

from config2021 import *

def main():    
    dirs = {}

    with open(file_name, 'r') as f:
        index = f.read().strip().split('\n')

    for linha in index:
        lin = linha.strip().split('.')
        if len(lin) == 2:
            cap = lin[1].strip().split(':')
            icap = int(lin[0])
            nome = filtre( cap[0] )

            print(lin)
            print( icap, f'{icap:02}-{nome}' )
            dirs[ icap ] = {}
            dirs[ icap ]['nome'] = [f'{icap:02}-{nome}', lin[1].strip()]

        elif len(lin) == 3:
            sec = lin[2].strip().split(':')
            isec = int(lin[1])
            nome = filtre( sec[0] )
            dirs[icap][isec] = f'{isec:02}-{nome}'

            print(lin[0], lin[1], nome)

    gere_diretorios( dirs, destino )


def gere_diretorios( dirs, path ):
    ''' (dict, str) -> None
        cria diretórios na pasta indicada em path. 
        cria dentro de cada um um toctree.rst 
        cria um novo index.rst 
    '''
    # para index.rst 
    indice = ''
    # diretorios
    allfiles = ''
    for cap in dirs:

        dname, fname  = dirs[cap]['nome']
        d = path + dname
        print("criando diretorio: ", d)
        if dname[:3] != '00-':  # remove o prefacio do indice
            if int(dname[:2]) <= ULTIMO_CAPITULO_INCLUIDO:
                indice += f'    ./{dname}/toctree.rst\n'
        try:
            os.mkdir( d )
        except:
            print("diretório já existe! Vou limpar os arquivos..")

        toc = f'{fname}\n'
        dots = ''.join( [':']*len(fname) )
        toc += f'{dots}\n\n'
        #toc += f'.. toctree::\n\t:caption: {dname}\n\t:maxdepth: 1\n\n'
        toc += f'.. toctree::\n\t:maxdepth: 1\n\n'

        blocos = get_blocos(cap)
        for sec in dirs[cap]:
            if sec != 'nome':
                filecab  = f'{copyright}\n'
                filecab += f'.. qnum::\n'
                filecab += f'\t:prefix: cap{cap:02}-{sec:02}\n'
                filecab += f'\t:start: 1\n\n'

                narq = dirs[cap][sec]+'.rst'
                fname = d+f'/{narq}'

                print("criando arquivo: ", narq)
                with open(fname, 'w') as arq:
                    arq.write( f"{filecab}" )
                    if sec <= len(blocos):
                        arq.write( blocos[sec-1] ) 

                toc += f"\t{narq}\n"
                allfiles += f'{dname}/{narq}\n'

        narq = 'toctree.rst'
        fname = d+f'/{narq}'
        print("criando arquivo: ", narq)
        with open(fname, 'w') as arq:
            arq.write( f"{toc}\n" )
        
        allfiles += '\n\n\n'

    with open('allChapterFiles.txt', 'w') as arq:
        arq.write( f'{allfiles}')

    if INCLUIR_PREFACIO: 
        index_rst = HEADER + PREFACIO + INDICE
    else:
        index_rst =  HEADER + INDICE

    index_rst += indice + FOOTER
    with open(path+'index.rst', 'w') as arq:
        arq.write( index_rst )

def get_blocos(cap):

    if cap not in INFILES:
        return []

    fname = INPATH + INFILES[cap]
    with open(fname, 'r') as arq:
        blocos = arq.read().split('.. break')
        
    return blocos

    


def filtre(s, n=4):
    s = s.replace('à', 'a') # déjà vu
    s = s.replace('á', 'a')
    s = s.replace('é', 'e')
    s = s.replace('í', 'i')
    s = s.replace('ó', 'o')
    s = s.replace('ú', 'u')
    s = s.replace('ã', 'a')
    s = s.replace('õ', 'o')
    s = s.replace('ç', 'c')

    s = s.replace(' um ', ' ')
    s = s.replace(' em ', ' ')
    s = s.replace(' de ', ' ')
    s = s.replace(' do ', ' ')
    s = s.replace(' da ', ' ')
    s = s.replace(' a ', ' ')
    s = s.replace(' e ', ' ')
    s = s.replace(' o ', ' ')
    s = s.replace(', ', ' ')
    s = s.split()[:n]
    return '-'.join(s)

main()
