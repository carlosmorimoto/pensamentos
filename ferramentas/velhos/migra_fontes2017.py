import os

copyright = """
..  Copyright (C) José Coelho de Pina Jr e Carlos Hitoshi Morimoto.
    É permitida a cópia, distribuição e/ou modificação desde documento
    sob os termos da Licença de Documentação Livre GNU (GNU Free 
    Documentation License) versão 1.3 ou versão subsequente
    publicada pela Fundação de Software Livre (Free Software 
    Foundation); com as seções invariantes sendo Prefácio e 
    Lista de Contribuidores, sem texto de capa ou contra-capa.  
    Uma cópia da licença está disponível em:
    https://www.gnu.org/licenses/licenses.pt-br.html#FDL

"""

INPATH = '../fontesVelhos/pensamentos2017/fontes2017/'
INFILES = {
    0:"00-prefacio.rst",
    1:"01-intro.rst",
    2:"02-expressoes.rst",
    3:"03-variaveis.rst",
    4:"04-while.rst",
    5:"05-if.rst",
    6:"06-indicador.rst",
    7:"07-repeticoes.rst",
    8:"08-reais.rst",
    9:"09-funcoes.rst",
    10:"10-listas.rst",
    11:"11-funcoes-listas.rst",
    12:"12-strings.rst",
    13:"13-matrizes.rst",
    14:"14-busca.rst",
    15:"15-ordenacao.rst"
}

file_name = "index_fontes2017.txt"

destino = '../pensamentos2022/_sources/'

def main():
    
    dirs = {}

    with open(file_name, 'r') as f:
        index = f.read().strip().split('\n')

    for linha in index:
        lin = linha.strip().split('.')
        if len(lin) == 2:
            cap = lin[1].strip().split(':')
            icap = int(lin[0])
            nome = filtre( cap[0] )

            print(lin)
            print( icap, f'{icap:02}-{nome}' )
            dirs[ icap ] = {}
            dirs[ icap ]['nome'] = [f'{icap:02}-{nome}', lin[1].strip()]

        elif len(lin) == 3:
            sec = lin[2].strip().split(':')
            isec = int(lin[1])
            nome = filtre( sec[0] )
            dirs[icap][isec] = f'{isec:02}-{nome}'

            print(lin[0], lin[1], nome)

    gere_diretorios( dirs, destino )

def gere_diretorios( dirs, path ):
    # diretorios
    allfiles = ''
    for cap in dirs:

        dname, fname  = dirs[cap]['nome']
        d = path + dname
        print("criando diretorio: ", d)
        try:
            os.mkdir( d )
        except:
            print("diretório já existe! Vou limpar os arquivos..")

        toc = f'{fname}\n'
        dots = ''.join( [':']*len(fname) )
        toc += f'{dots}\n\n'
        #toc += f'.. toctree::\n\t:caption: {dname}\n\t:maxdepth: 1\n\n'
        toc += f'.. toctree::\n\t:maxdepth: 1\n\n'

        blocos = get_blocos(cap)

        for sec in dirs[cap]:
            if sec != 'nome':
                filecab  = f'{copyright}\n'
                filecab += f'.. qnum::\n'
                filecab += f'\t:prefix: cap{cap:02}-{sec:02}\n'
                filecab += f'\t:start: 1\n\n'

                narq = dirs[cap][sec]+'.rst'
                fname = d+f'/{narq}'

                print("criando arquivo: ", narq)
                with open(fname, 'w') as arq:
                    arq.write( f"{filecab}" )
                    if sec <= len(blocos):
                        arq.write( blocos[sec-1] ) 

                toc += f"\t{narq}\n"
                allfiles += f'{dname}/{narq}\n'

        narq = 'toctree.rst'
        fname = d+f'/{narq}'
        print("criando arquivo: ", narq)
        with open(fname, 'w') as arq:
            arq.write( f"{toc}\n" )
        
        allfiles += '\n\n\n'

    with open('allChapterFiles.txt', 'w') as arq:
        arq.write( f'{allfiles}')


def get_blocos(cap):

    if cap not in INFILES:
        return []

    fname = INPATH + INFILES[cap]
    with open(fname, 'r') as arq:
        blocos = arq.read().split('.. break')
        
    return blocos

    


def filtre(s, n=4):
    s = s.replace('á', 'a')
    s = s.replace('é', 'e')
    s = s.replace('í', 'i')
    s = s.replace('ó', 'o')
    s = s.replace('ú', 'u')
    s = s.replace('ã', 'a')
    s = s.replace('õ', 'o')
    s = s.replace('ç', 'c')

    s = s.replace(' um ', ' ')
    s = s.replace(' em ', ' ')
    s = s.replace(' de ', ' ')
    s = s.replace(' do ', ' ')
    s = s.replace(' da ', ' ')
    s = s.replace(' a ', ' ')
    s = s.replace(' e ', ' ')
    s = s.replace(' o ', ' ')
    s = s.replace(', ', ' ')
    s = s.split()[:n]
    return '-'.join(s)

main()
