
# Como gerar o livro "pensamentos"?

* Exec

## Logs do projeto "Livro de introdução a computação em Python"

* 2022-02-21
    * repositório atualizado

* 2022-02-15
    * criei o repositório "pensamentos" no bitbucket -- versão 2022
    * o diretório "velho" contém pensamentos2017 e pensamentos2021
    * runestone 6.0: o runestone 6.0 agora tem um book server. Precisamos estudar. 
    * criei uma planilha para gerenciar o esqueleto e fomentar ideias:
        * [Organização do volume 1](https://docs.google.com/spreadsheets/d/1dyUoGUZ22j2XwOZANjVs_khXCfLwOQweQhA_j7nOGLg/edit#gid=0)

* 2021-04-12

    - criei "pensamentos" para livro versão 2021
    - mv old stuff para pensamentos2017
    - criei scripts migra2021.py
    - configurei conf.py para novo título: Pensamentos com Python - um curso interativo de introdução à computação

* opção
    * manter a parte interativa, mesmo sem saber se o upload incremental dos capítulos vai causar problemas com as contas.

* tema usado para o livro online

    - https://pypi.org/project/sphinx-bootstrap-theme/

## arquivo .py

### build_index.py

Esse é um programa teste. O script que gera os arquivos é o migra.

Entrada

* file_name:  estrutura do livro.
    * #. nome do capitulo
    * #.#. seção

Saida

* cria diretorios, arquivos e toctree.rst


